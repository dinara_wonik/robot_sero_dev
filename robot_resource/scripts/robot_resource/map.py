#!/usr/bin/python
# -*- coding: utf8 -*-#
#
# Copyright 2017 Wonik Robotics. All rights reserved.
# 
# This file is a proprietary of Wonik Robotics
# and cannot be redistributed, and/or modified
# WITHOUT ANY ALLOWANCE OR PERMISSION OF Wonik Robotics.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

'''
2017.10.29 ('c')void 
'''

import os, sys, thread, time, traceback
import rospy
import rosparam
import roslib.packages

from tf.transformations import quaternion_from_euler 

import geometry_msgs.msg as geometry_msgs
import service_common_msgs.msg as service_common_msgs

PKG_NAME  = 'robot_resource'
PARM_NAME = '/robot_resource/maps'
       

def read_map_list(maps_param_name=PARM_NAME):
    try: 
        maps = rospy.get_param(maps_param_name)
        return maps.keys()
    except:
        err = 'parameter error'
        rospy.logerror(err)
        return []
        

def read_map(map_name, maps_param_name=PARM_NAME):
    try: 
        maps = rospy.get_param(maps_param_name)
    except:
        err = 'parameter error'
        rospy.logerror(err)
        raise ValueError(err) 
    
    map_name = map_name.strip()
    
    try:
        map_info = maps[map_name]    
    except KeyError:
        err = "'%s' is not exist. available maps : %s"%(map_name, ', '.join([k for k in maps]))
        rospy.logerr(err)
        raise ValueError(err)  
    
    def check_map_info(map_info): 
        for k in ('floor_plan',):
            try:
                map_file_name = map_info[k]
            except KeyError:
                err = "'%s' is not exist"%(k)
                rospy.logerr(err)
                raise ValueError(err)    
            
            resource = roslib.packages.find_resource(PKG_NAME, map_file_name)

            if(len(resource) == 0):
                err = "'%s' is not exist"%(map_file_name)
                rospy.logerr(err)
                raise ValueError(err)    
            
            if(len(resource) > 1):
                err = "'%s' is duplicated"%(map_file_name)
                rospy.logerr(err)
                raise ValueError(err)                 
            
            # path 
            map_info[k] = resource[0]
        
        try:
            init_pose = map_info['init_pose']
        except :
            err = "'init_pose' is not exist"
            rospy.logerr(err)
            raise ValueError(err)          
        
        if(len(init_pose) != 6):
            # x, y, theta, cov_x, cov_y, cov_theta
            err = "init_pose size error"
            rospy.logerr(err)
            raise ValueError(err)   
        
        try:
            init_pose = [float(v) for v in init_pose]
        except: 
            err = "init_pose type error. the type of value is not floating point"
            rospy.logerr(err)
            raise TypeError(err)   
        
        return map_info, init_pose                       
    
    map_info, init_pose = check_map_info(map_info)

    map_info_msg = service_common_msgs.MapInfo()
    
    map_info_msg.name = map_name
    
    map_info_msg.floor_plan = map_info['floor_plan']
    # map_info_msg.prohibited_area = map_info['prohibited_area']
    # map_info_msg.driving_area = map_info['driving_area']
    
    x, y, th = init_pose[:3]
    cov_x, cov_y, cov_th = init_pose[3:]    
    covariance = (
        cov_x, 0, 0,  0, 0,      0,
        0, cov_x, 0,  0, 0,      0,
        0,     0, 0,  0, 0,      0,
        0,     0, 0,  0, 0,      0,
        0,     0, 0,  0, 0,      0,
        0,     0, 0,  0, 0, cov_th,
                  )
                  
    map_info_msg.initial_pose.header.frame_id = 'map' 
    map_info_msg.initial_pose.pose.pose.position = geometry_msgs.Point(x, y, 0)
    map_info_msg.initial_pose.pose.pose.orientation = geometry_msgs.Quaternion(*quaternion_from_euler(0, 0, th))
    map_info_msg.initial_pose.pose.covariance = covariance 
    
    return map_info_msg

def read_unit_test(map_name, maps_param_name=PARM_NAME):
    try: 
        maps = rospy.get_param(maps_param_name)
    except:
        err = 'parameter error'
        rospy.logerror(err)
        raise ValueError(err) 
    
    map_name = map_name.strip()
    
    try:
        map_info = maps[map_name]    
    except KeyError:
        err = "'%s' is not exist. available maps : %s"%(map_name, ', '.join([k for k in maps]))
        rospy.logerr(err)
        raise ValueError(err)  
    
    try:
        resource = roslib.packages.find_resource(PKG_NAME, map_info['unit_test'])
        unit_test_yaml = resource[0]
        contents = rosparam.load_file(unit_test_yaml)
        
        unit_test_dict = contents[0][0]
        return unit_test_dict 
        
    except :
        err = "'unit_test' is not exist"
        rospy.logerr(err)
        raise ValueError(err)         

def get_map_name_from_floor_plan(floor_plan_abs_path, maps_param_name=PARM_NAME):
    # .../addy_common_pkgs/robot_resource/maps/wonik_1st_floor/test_f_area.yaml
    map_name = os.path.basename(os.path.dirname(floor_plan_abs_path)) 
    return map_name



