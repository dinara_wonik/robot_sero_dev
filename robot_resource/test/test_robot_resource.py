#!/usr/bin/python
# -*- coding: utf8 -*-#
#
# Copyright 2017 Wonik Robotics. All rights reserved.
# 
# This file is a proprietary of Wonik Robotics
# and cannot be redistributed, and/or modified
# WITHOUT ANY ALLOWANCE OR PERMISSION OF Wonik Robotics.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

'''
2017.10.29 ('c')void 
'''

import os, sys, thread, time, traceback
import rospy
import robot_resource.map

def test():
    
    print "test"
    try:
        m = robot_resource.map.read_map('foo')
    except ValueError as e:
        print e 
        
    msg = robot_resource.map.read_map('wonik_1st_floor')
    print msg
        
    msg = robot_resource.map.read_map('kumho_asiana')
    print msg    
    
    robot_resource.map.read_unit_test('wonik_1st_floor')
    
    

if __name__ == '__main__':
    rospy.init_node('robot_resource_test_node')
    test()
    rospy.spin()