#!/usr/bin/python
# -*- coding: utf8 -*-#

"""
2021.03.12 jglee
"""

import numpy as np
import rospy
import tf.transformations as trans
import workerbee_actionlib
from geometry_msgs.msg import PoseStamped, Point, Quaternion
from sero_actions_dev.msg import (
	SetObjectFinderAction, 
    SetObjectFinderGoal,
	SetObjectFinderFeedback, 
	SetObjectFinderResult,
)


def object_finder_client():
    client = workerbee_actionlib.SimpleActionClient('/sero_action_node/set_object_finder', SetObjectFinderAction) 
    client.wait_for_server()

    def on_feedback(fb):
        rospy.loginfo_throttle(1.0, "Rcv feedback\n%s" % (fb))

    def on_result(goal_status, result):
        rospy.loginfo("Rcv result. goal_status=%s, result=\n%s" % (goal_status, result))

    goal = SetObjectFinderGoal()
    goal.target.u = 640
    goal.target.v = 360
    print 'target : ',goal
    client.send_goal(goal, feedback_cb=on_feedback, done_cb=on_result)
    rospy.loginfo("send_goal()")
    try:
        rospy.loginfo("wait_for_result()")
        client.wait_for_result()
    except KeyboardInterrupt:
        rospy.loginfo("cancel_goal()")
        client.cancel_goal()
    return client.get_result()


if __name__ == "__main__":
    rospy.init_node("object_finder_client", disable_signals=True)
    rospy.loginfo("Call set object finder client")
    try:        
        result = object_finder_client()
    except rospy.ROSInterruptException:
        print "program interrupted before completion"
