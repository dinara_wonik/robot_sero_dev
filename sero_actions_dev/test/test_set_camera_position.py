#!/usr/bin/python
# -*- coding: utf8 -*-#

'''
2019.11.29 ('c')void 
'''

import time 
import numpy as np 
import rospy
import tf
from tf.transformations import quaternion_from_euler 
import workerbee_actionlib
from geometry_msgs.msg import PoseStamped, Point, Quaternion 
from workerbee_utils.utils import transform_pose, lookup_pose


from sero_mobile_msgs.msg import LptDriverState, LptSetPositionState
from workerbee_msgs.msg import ActionState
from sero_actions_dev.msg import SetCameraPoseAction, SetCameraPoseGoal, SetCameraPoseFeedback, SetCameraPoseResult



REPEAT = 100


def set_camera_pose_client(): 
    
    #tfl = tf.TransformListener()
    #time.sleep(2)
    
    client = workerbee_actionlib.SimpleActionClient('/sero_action_node/set_camera_pose', SetCameraPoseAction) 
    client.wait_for_server()

    def on_feedback(fb):
        rospy.loginfo_throttle(1.0, 'Rcv feedback\n%s'%(fb))
        
    def on_result(goal_status, result):
        rospy.loginfo('Rcv result. goal_status=%s, result=\n%s'%(goal_status, result)) 
            
    
    for i in range(REPEAT):
        
#         robot_pose = lookup_pose(tfl, target_frame='map', source_frame='base_link')
#     
#         head_pose = PoseStamped()
#         head_pose.header.frame_id = 'map'
#         head_pose.pose.position = Point(robot_pose.pose.position.x, robot_pose.pose.position.y, 1.1) 
#         head_pose.pose.orientation = Quaternion(*quaternion_from_euler(0, -40*np.pi/180, 45*np.pi/180))

        head_pose = PoseStamped()
        head_pose.header.frame_id = 'lpt_base'
        head_pose.pose.position = Point(0, 0, 0.2) 
        head_pose.pose.orientation = Quaternion(*quaternion_from_euler(0, -40*np.pi/180, 45*np.pi/180))
        
        goal = SetCameraPoseGoal(head_pose=head_pose)
        client.send_goal(goal, feedback_cb=on_feedback, done_cb=on_result)
        client.wait_for_result()
        
        head_pose = PoseStamped()
        head_pose.header.frame_id = 'lpt_base'
        head_pose.pose.position = Point(0, 0, 0.001) 
        head_pose.pose.orientation = Quaternion(*quaternion_from_euler(0, 0, 0))
    
        goal = SetCameraPoseGoal(head_pose=head_pose)
        client.send_goal(goal, feedback_cb=on_feedback, done_cb=on_result)
        client.wait_for_result()   
        
        if(client.get_result().state.action_state.code == ActionState.ERROR_FAULT):
            break
    
    return client.get_result()  


if __name__ == '__main__':
    try: 
        rospy.init_node('set_camera_pose_client') 
        result = set_camera_pose_client()
    except rospy.ROSInterruptException:
        print 'program interrupted before completion'




