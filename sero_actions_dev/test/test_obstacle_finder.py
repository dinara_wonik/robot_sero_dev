#!/usr/bin/python
# -*- coding: utf8 -*-#

"""
2021.07.20 dinara
"""

import numpy as np
import rospy
import tf.transformations as trans
import workerbee_actionlib
from geometry_msgs.msg import PoseStamped, PointStamped, Point, Quaternion
from sero_actions.msg import (
	SetObstacleFinderAction, 
    SetObstacleFinderGoal,
	SetObstacleFinderFeedback, 
	SetObstacleFinderResult,
)
import tf2_ros
import tf2_geometry_msgs



#1st floor corner

#   seq: 0
#   stamp: 
#     secs: 1627018173
#     nsecs: 654428655
#   frame_id: "map"
# point: 
#   x: 11.0904779434
#   y: 0.211468219757
#   z: 0.000954627990723

# floor robot_pose
#   seq: 162276
#   stamp: 
#     secs: 1627023692
#     nsecs: 471409798
#   frame_id: "map"
# pose: 
#   position: 
#     x: 9.67523853799
#     y: -1.34816546761
#     z: 0.0
#   orientation: 
#     x: 0.0
#     y: 0.0
#     z: -0.259448910709
#     w: -0.965756834163
# ---


#1st floor wall
#   seq: 0
#   stamp: 
#     secs: 1627024969
#     nsecs: 483785150
#   frame_id: "map"
# point: 
#   x: 6.8150601387
#   y: 0.197131097317
#   z: -0.00534057617188
# ---
#wall robot_pose
#   seq: 185844
#   stamp: 
#     secs: 1627026049
#     nsecs: 265047550
#   frame_id: "map"
# pose: 
#   position: 
#     x: 7.37918880965
#     y: -2.14196778981
#     z: 0.0
#   orientation: 
#     x: 0.0
#     y: 0.0
#     z: 0.812768031449
#     w: 0.582587441552

# GOAL_POINT = ('base_link', 1.5, 0.3, 0.0) 
# GOAL_POINT = ('map', 11.0904779434, 0.211468219757, 0.000954627990723)  # floor1 corner
# GOAL_POINT = ('map', 6.8150601387, 0.197131097317, -0.00534057617188)  # floor1 wall


#4th floor corner (next to plant)
# header: 
#   seq: 0
#   stamp: 
#     secs: 1627041788
#     nsecs: 496285721
#   frame_id: "map"
# point: 
#   x: 17.7457027435
#   y: 0.0691857486963
#   z: 0.00247192382812

# GOAL_POINT = ('map', 17.7457027435, 0.0691857486963, 0.00247192382812) # floor4 corner (for 3 poi types)
GOAL_POINT = ('map', 5.00206899643, 0.677162110806, -0.00534057617188)  # floor4 wall
# GOAL_POINT = ('map', 17.8027267456, 1.10867631435, -0.00143432617188) # floor4 corner (type1 only)


# action_poi_test.txt 

# # 1 corner 
# header: 
#   seq: 0
#   stamp: 
#     secs: 1627280840
#     nsecs: 412622873
#   frame_id: "map"
# point: 
#   x: 9.89175987244
#   y: -12.3313579559
#   z: 0.00247192382812

# GOAL_POINT = ('map', 9.89175987244, -12.3313579559, 0.00247192382812) 

# wall1
# header: 
#   seq: 0
#   stamp: 
#     secs: 1627285928
#     nsecs: 750570412
#   frame_id: "map"
# point: 
#   x: 22.8338508606
#   y: 1.00542068481
#   z: 0.00684475898743
# ---

# GOAL_POINT = ('map', 22.8338508606, 1.00542068481, 0.00684475898743) 

#wall2
#   seq: 2
#   stamp: 
#     secs: 1627286856
#     nsecs: 740107274
#   frame_id: "map"
# point: 
#   x: 22.6575984955
#   y: 13.8998994827
#   z: -0.00534057617188

# GOAL_POINT = ('map', 22.6575984955, 13.8998994827, -0.00534057617188) 

#corner2
# header: 
#   seq: 3
#   stamp: 
#     secs: 1627287454
#     nsecs: 456867792
#   frame_id: "map"
# point: 
#   x: 10.5588970184
#   y: 14.5319471359
#   z: 0.00419878959656

# GOAL_POINT = ('map', 10.5588970184, 14.5319471359, 0.00419878959656) 

#wall3
#   seq: 4
#   stamp: 
#     secs: 1627288293
#     nsecs: 302642174
#   frame_id: "map"
# point: 
#   x: 9.70013141632
#   y: 0.656915724277
#   z: 0.00247192382812

# GOAL_POINT = ('map', 9.70013141632, 0.656915724277, 0.00247192382812) 


def obstacle_finder_client():
    client = workerbee_actionlib.SimpleActionClient('/sero_action_node/set_obstacle_finder', SetObstacleFinderAction) #ros action lib
    client.wait_for_server()

    def on_feedback(fb):
        rospy.loginfo_throttle(1.0, "Rcv feedback\n%s" % (fb))

    def on_result(goal_status, result):
        rospy.loginfo("Rcv result. goal_status=%s, result=\n%s" % (goal_status, result))

    

    def make_point(simple_point):
        frame_id, sx, sy, sz = simple_point
        point = PointStamped()
        point.header.frame_id = frame_id
        point.point = Point(sx, sy, sz) 
        return point


    # detect obstacle
    goal = SetObstacleFinderGoal()
    point_to_transform = make_point(GOAL_POINT)
    goal.target.poi.header.frame_id = point_to_transform.header.frame_id
    goal.target.poi.header.stamp = rospy.Time.now()
    goal.target.poi.point.x = point_to_transform.point.x
    goal.target.poi.point.y = point_to_transform.point.y
    goal.target.poi.point.z = point_to_transform.point.z
    goal.target.poi_type = 4


    print 'target : ',goal
    client.send_goal(goal, feedback_cb=on_feedback, done_cb=on_result)
    rospy.loginfo("send_goal()")
    try:
        rospy.loginfo("wait_for_result()")
        client.wait_for_result()
    except KeyboardInterrupt:
        rospy.loginfo("cancel_goal()")
        client.cancel_goal()
    return client.get_result()


if __name__ == "__main__":

    rospy.init_node("obstacle_finder_client", disable_signals=True)
    rospy.loginfo("Call set obstacle finder client")

    try:        
        result = obstacle_finder_client()
    except rospy.ROSInterruptException:
        print "program interrupted before completion"
