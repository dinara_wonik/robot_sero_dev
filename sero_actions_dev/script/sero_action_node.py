#!/usr/bin/python
# -*- coding: utf8 -*-#

'''
2019.11.29 ('c')void  
'''

import thread, threading 
import traceback

import rospy 
import tf 
import tf2_ros
import tf2_geometry_msgs

import act_set_object_finder
import act_set_obstacle_finder

from workerbee_utils.utils import handler_guard
from workerbee_platform_config import WorkerbeeNavigationConfig
from workerbee_actionlib import RosLaunch, MutuallyExclusiveActionServerGroup, SimpleActionClient


class SeRoActionGroup(MutuallyExclusiveActionServerGroup):
    def __init__(self):
        super(SeRoActionGroup, self).__init__()
        self._lpt_base_frame = rospy.get_param('~lpt_base_frame', 'lpt_base')

        self._image_view = rospy.get_param('~image_view', False)
        self._rgb_camera_info_topic = rospy.get_param('~rgb_camera_info_topic', '/rgbd/rgb/camera_info')
        self._rgbd_pointcloud_topic = rospy.get_param('~rgbd_pointcloud_topic', '/rgbd/depth/points_of')

        self._marker_image_topic    = rospy.get_param('~marker_image_topic', '/marker_detect/fiducial_image')
        self._marker_vertices_topic = rospy.get_param('~marker_vertices_topic', '/marker_detect/fiducial_vertices')
        self._marker_transforms_topic = rospy.get_param('~marker_transforms_topic', '/marker_detect/fiducial_transforms')

        self._corner_image_topic    = rospy.get_param('~corner_image_topic', '/corner_detect/fiducial_image')
        self._corner_vertices_topic = rospy.get_param('~corner_vertices_topic', '/corner_detect/fiducial_vertices')
        self._corner_transforms_topic = rospy.get_param('~corner_transforms_topic', '/corner_detect/fiducial_transforms')

        self._p_step_array = rospy.get_param('~p_step_array', '[-45.0, 0, 45.0]')
        self._t_step_array = rospy.get_param('~t_step_array', '[-30.0, 0, 15.0]')

        self._r_pan = rospy.get_param('~r_pan', 0.2)
        self._r_tilt = rospy.get_param('~r_tilt', 1.0)
        self._number_of_pan_angle_sections = rospy.get_param('~number_of_pan_angle_sections', 3)

        self._action_servers = []
        
        self._tf_buffer = tf2_ros.Buffer() #tf buffer length
        self._tf_listener = tf2_ros.TransformListener(self._tf_buffer)

        # set_object_finder
        act_set_object_finder.Params.action_name = rospy.get_name() + '/set_object_finder'
        act_set_object_finder.Params.image_view = self._image_view
        act_set_object_finder.Params.rgb_camera_info_topic = self._rgb_camera_info_topic
        act_set_object_finder.Params.marker_image_topic    = self._marker_image_topic
        act_set_object_finder.Params.marker_vertices_topic = self._marker_vertices_topic

        act_set_object_finder.Params.p_step_array = self._p_step_array
        act_set_object_finder.Params.t_step_array = self._t_step_array

        act_set_object_finder.Params.roslaunches = [RosLaunch(pkg='sero_actions_dev', file='marker.launch'),]
        self._action_servers.append(act_set_object_finder.ActionServer(self))

        # set_obstacle_finder
        act_set_obstacle_finder.Params.action_name = rospy.get_name() + '/set_obstacle_finder'
        act_set_obstacle_finder.Params.image_view = self._image_view
        act_set_obstacle_finder.Params.rgb_camera_info_topic = self._rgb_camera_info_topic
        act_set_obstacle_finder.Params.rgbd_pointcloud_topic = self._rgbd_pointcloud_topic
        act_set_obstacle_finder.Params.marker_image_topic    = self._corner_image_topic
        # act_set_obstacle_finder.Params.marker_vertices_topic = self._corner_vertices_topic
        act_set_obstacle_finder.Params.marker_transforms_topic = self._corner_transforms_topic


        act_set_obstacle_finder.Params.r_pan = self._r_pan
        act_set_obstacle_finder.Params.r_tilt = self._r_tilt
        act_set_obstacle_finder.Params.number_of_pan_angle_sections = self._number_of_pan_angle_sections

        act_set_obstacle_finder.Params.roslaunches = [RosLaunch(pkg='sero_actions_dev', file='corner.launch'),]
        self._action_servers.append(act_set_obstacle_finder.ActionServer(self))

    @property
    def transform_listener(self):
        return self._tf_buffer

if __name__ == '__main__':
    rospy.init_node('sero_action_node')
    wn = SeRoActionGroup()
    rospy.spin()


