#!/usr/bin/python
# -*- coding: utf8 -*-#

'''
2021.05.14 ('d')dinara 

set_obstacle_finder action  
'''

import rospy
import numpy as np 
import json 
import time
import traceback
import threading
import tf
import tf.transformations as trans 
import cv2
from cv_bridge import CvBridge, CvBridgeError
import sensor_msgs.msg
import sensor_msgs.point_cloud2 as pc2
from geometry_msgs.msg import Point, Pose, Quaternion, PoseStamped, PointStamped, PoseArray, Point32, TransformStamped
from message_filters import ApproximateTimeSynchronizer, Subscriber
from sero_mobile.srv import LptSetPosition, LptSetVelocity
from sero_mobile_msgs.msg import LptDriverState, LptSetPositionState, LptPosVelStatus
import fiducial_msgs.msg 
from collections import namedtuple
import workerbee_actionlib
import workerbee_status
from workerbee_msgs.msg import ActionState
from workerbee_utils.utils import handler_guard, lookup_pose, transform_pose, orientation2array

from dynamic_reconfigure.server import Server as DynRecfgServer 
from dynamic_reconfigure.client import Client as DynRecfgClient
from sero_actions_dev.cfg import ObstacleFinderConfig

from sero_actions_dev.msg import (
    SetObstacleFinderAction, 
    SetObstacleFinderGoal,
    SetObstacleFinderFeedback, 
    SetObstacleFinderResult,
    ObjectFinderState,
)
import tf2_ros
import tf2_geometry_msgs
from rospy_tutorials.srv import *
from pc_detection.srv import *
from pc_detection.msg import *
from tf2_sensor_msgs.tf2_sensor_msgs import do_transform_cloud
import math
import time
from std_msgs.msg import Empty

# ProcInput = namedtuple("ProcInput", "rgb_camera_info,marker_image_msg, marker_vertices_msg")
ProcInput = namedtuple("ProcInput", "rgb_camera_info, marker_image_msg, marker_transforms_msg, pointclouds_msg")

red_color = (0, 0, 255)
blue_color = (255, 0, 0)

class Params(object):
    action_name=None 
    image_view=None
    rgb_camera_info_topic = None
    marker_image_topic = None
    # marker_vertices_topic = None
    marker_transforms_topic = None
    rgbd_pointcloud_topic = None

    p_step_array = None
    t_step_array = None
    point1 = None
    point2 = None
    point3 = None
    point4 = None
    L = None

def msg2dict(ros_msg):
    return message_converter.convert_ros_message_to_dictionary(ros_msg) 

def check_error_conditions():
    '''
    해당하는 조건의 InfoItem을 리턴한다.         
    workerbee_status.get_info('A\\B\\C')
    return {'Some condition': 'Some error'}
    ''' 
    return [] 

class LptPosVelReceiver(object):
    def __init__(self, topic):
        self._pos_vel = LptPosVelStatus() 
        self._sub = rospy.Subscriber(topic, LptPosVelStatus, self._callback)
        self._status_error = False
    
    @property
    def pos_vel(self):
        return self._pos_vel
    
    @property
    def lpt_status(self):
        return self._status_error
    
    def _callback(self, pos_vel):
        self._pos_vel = pos_vel
        if((pos_vel.lift_status == 1 or pos_vel.lift_status == 65) and pos_vel.pan_status == 1 and pos_vel.tilt_status == 1):
            self._status_error = False
        else : 
            self._status_error = True

class MarkerDetectionProxy(object): 
    def __init__(self, tfl, tf_buffer, rgb_camera_info_topic, marker_image_topic, marker_transforms_topic, rgbd_pointcloud_topic):        
        self._tfl = tfl
        self._tf_buffer = tf_buffer

        # radian 
        self._angle_tolerance = 2.0 * np.pi/180 
        
        # mm  
        self._position_tolerance = 3.0 

        srv_lpt_set_position = '/sero_mobile/lpt_set_position' 
        srv_lpt_set_velocity = '/sero_mobile/lpt_set_velocity'
                        
        # # global planner 
        rospy.wait_for_service(srv_lpt_set_position)  
        rospy.wait_for_service(srv_lpt_set_velocity)
        
        # # proxy 
        self._lpt_set_position = rospy.ServiceProxy(srv_lpt_set_position, LptSetPosition) 
        self._lpt_set_velocity = rospy.ServiceProxy(srv_lpt_set_velocity, LptSetVelocity) 
        
        self._pos = LptPosVelReceiver(topic='/sero_mobile/lpt') 

        self._rgb_camera_info_topic  = rgb_camera_info_topic
        self._marker_image_topic     = marker_image_topic
        # self._marker_vertices_topic = marker_vertices_topic
        self._marker_transforms_topic = marker_transforms_topic
        self._rgbd_pointcloud_topic = rgbd_pointcloud_topic
        self.ready_flag = False
        self.marker_detection = False
        self.mission_fail = False
        self.no_acting = True
        self.set_target_flag = False
        self.is_done = False
        self.image_tolerance = 20
        self.image_view = False
        self.is_done_count = 0 
        self.targeting_on_view = False
        self.lpt_status_error = False

        self.bridge = CvBridge()
        self.get_marker_image_msg = sensor_msgs.msg.Image()
        self.marker_image_update = False

        self.marker_pose = [0, 0]
        self.marker_prediction_pose = [0, 0]

        self.lpt_set_velocity()
        self.dynamic_srv = DynRecfgServer(ObstacleFinderConfig, self.reconfigure_callback)

        self.lift = 0
        self.p_step_array = [-45., 0, 45.]
        self.t_step_array = [-30., 0, 15.]

        self.point1 = [0.4, -0.5]
        self.point2 = [-0.6, -0.5]
        self.point3 = [0.6, 0.5]
        self.point4 = [0.4, 0.5]

        # self.L = 1.7
        self.ats_subs = []

        self.one_marker_detected = False

        self.obstacle_srv_response = False #assumes False init. 
        self.transformed_pc_list = []
        self.transformed_marker_list = []
        self.transformdata_list = []

        self.start_search_intersection_point_and_obstacle = False
        self.done_search_intersection_point_and_obstacle = False

        self.all_cloud_points = np.array([[0,0,0]])
        self.all_cloud_points = self.all_cloud_points[1:]
        self.start_pc_service = False
        
        self.pointcloud_counter = 0

        self.action_data_pub = rospy.Publisher('action_data_pub', ActionData, queue_size=10)

    def __del__(self):   
        if self.image_view :
            cv2.destroyAllWindows()
            
    def marker_sub(self):        
        self.ats_subs.append(Subscriber(self._rgb_camera_info_topic, sensor_msgs.msg.CameraInfo))    
        self.ats_subs.append(Subscriber(self._marker_image_topic, sensor_msgs.msg.Image)) 
        # self.ats_subs.append(Subscriber(self._marker_vertices_topic, fiducial_msgs.msg.FiducialArray)) 
        self.ats_subs.append(Subscriber(self._marker_transforms_topic, fiducial_msgs.msg.FiducialTransformArray)) 
        self.ats_subs.append(Subscriber(self._rgbd_pointcloud_topic, sensor_msgs.msg.PointCloud2)) 

        self._ats = ApproximateTimeSynchronizer(self.ats_subs, queue_size=5, slop=0.1)
        self._ats.registerCallback(self.callback_ats)
      
    def marker_unsub(self):
        [sub.unregister() for sub in self.ats_subs]
        # self._ats.unregister()
    
    def marker_regist(self):
        [sub.register() for sub in self.ats_subs]

    def reconfigure_callback(self, config, level):
        print "======== call dynamic_reconfigure_callback ========"
        self.image_tolerance = config.image_target_tolerance
        self._position_tolerance = config.lpt_position_tolerance
        self._angle_tolerance = config.lpt_angle_tolerance / 180.0 * np.pi 
        print "image_tolerance    : ", config.image_target_tolerance
        print "position_tolerance : ", config.lpt_position_tolerance
        print "angle_tolerance    : ", config.lpt_angle_tolerance
        return config 

    def set_image_view(self, flag):
        self.image_view = flag
        if self.image_view : 
            self.v = threading.Thread(target=self.image_view_run)
            self.v.start()
        return True

    def image_view_run(self):
        while not rospy.is_shutdown():
            if self.marker_image_update is False : 
                rospy.sleep(3)
            else : 
                try:
                    frame = self.bridge.imgmsg_to_cv2(self.get_marker_image_msg, "bgr8")
                    frame = np.array(frame, dtype=np.uint8)
                    cv2.imshow("Image window", frame)
                    cv2.waitKey(1)
                except CvBridgeError, e:
                    print e
        # end while loop
        cv2.destroyAllWindows()




    def set_r(self, r_pan, r_tilt):
        self.r_pan = r_pan
        self.r_tilt = r_tilt

        print "=== set_r ==="
        print "r_pan    : ", self.r_pan
        print "r_tilt    : ", self.r_tilt



    def set_fail(self):
        self.mission_fail = True
 


    def poi_searching_move(self):


        if self.marker_detection:

            print("Start visiting points")
            idx = 0

            for pan in self.p_step_array:
                for tilt in self.t_step_array:
                    idx+=1
                    print(pan, " - ", tilt)
                    
                    if tilt > 1.0:  # check with lpt_tilt_max from SeroMobile.cfg, load as rosparam
                       tilt = 1.0 

                    self.lpt_set_position(0.0, pan, tilt)
                    print("Visit point ", idx)
                    rospy.sleep(5.0)

            if (self.one_marker_detected is False):
                rospy.loginfo('%s: Fault: Marker not found' % Params.action_name)
                self.set_fail()
                self.lpt_set_position(0.0, 0.0, 0.0)
            else:
                self.start_pc_service = True
            return



    def get_intersection_point_and_obstacle_client(self):
        rospy.wait_for_service('get_intersection_point_and_obstacle')
        try:
            get_intersection_point_and_obstacle = rospy.ServiceProxy('get_intersection_point_and_obstacle', GetObstacle)
            # set midpoint of transformed_marker_list
            self.marker_midpoint = self.calc_marker_midpoint()

            print("-------------------------------- marker_midpoint: ", self.marker_midpoint)
            resp1 = get_intersection_point_and_obstacle(self.transformed_pc_list, self.target_poi, self.marker_midpoint)
            return resp1.found_obstacle
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)


    def calc_marker_midpoint(self):
        marker_midpoint = PointStamped()

        for i, marker_point in enumerate(self.transformed_marker_list):
            if marker_point is not None:
                marker_midpoint.header.frame_id = marker_point.header.frame_id
                marker_midpoint.header.stamp = marker_point.header.stamp
                
                marker_midpoint.point.x += marker_point.point.x
                marker_midpoint.point.y += marker_point.point.y
                marker_midpoint.point.z += marker_point.point.z

        print (i)
        print(marker_midpoint.header.frame_id)
        print(marker_midpoint.header.stamp)
        marker_midpoint.point.x = marker_midpoint.point.x/(i+1)
        marker_midpoint.point.y = marker_midpoint.point.y/(i+1)
        marker_midpoint.point.z = marker_midpoint.point.z/(i+1)
        return marker_midpoint

    #### RM
    # def debug_marker_midpoint(self):
    #     marker_midpoint = PointStamped()

    #     marker_midpoint.point.x = 0.0
    #     marker_midpoint.point.y = 0.0
    #     marker_midpoint.point.z = 0.0
    #     return marker_midpoint



    def transform_data(self):

        transform_time = time.time()
        for pc, tf, marker_list in self.transformdata_list:
            pointcloud_in_baselink = do_transform_cloud(pc, tf) 
            self.transformed_pc_list.append(pointcloud_in_baselink)

            for markerpose in marker_list:
                if markerpose is not None:
                    marker_in_baselink = tf2_geometry_msgs.do_transform_point(markerpose, tf)
                    self.transformed_marker_list.append(marker_in_baselink)

        print('    self.transformed_pc_list size', len(self.transformed_pc_list))
        print('self.transformed_marker_list size', len(self.transformed_marker_list))
        print(self.transformed_marker_list)
        print("\n\n")
        t_elapsed = time.time() - transform_time
        print("\n")
        print('transform_data(pc, tf, marker_list) processing time: ', int(t_elapsed*1000))
        print("\n")
        return




    def callback_ats(self, rgb_camera_info, marker_image_msg, marker_transforms_msg, pointclouds_msg):
       

        print 'ats...'
        ats_start = time.time()

        if self.done_search_intersection_point_and_obstacle:
            self.lpt_set_position(0.0, 0.0, 0.0)
            print("Return to origin")
            self.is_done = True
            return


        self.pointcloud_counter +=1
        input = ProcInput(rgb_camera_info=rgb_camera_info, marker_image_msg=marker_image_msg, 
                            marker_transforms_msg=marker_transforms_msg, pointclouds_msg=pointclouds_msg) 

        self.ready_flag = True
        if self.lpt_status :
            self.lpt_status_error = True


        self.get_marker_image_msg = input.marker_image_msg
        self.marker_image_update = True

        if self.no_acting :
            return 

        self.marker_detection = True
        target_frame = "base_link"

        pointcloud_data = input.pointclouds_msg
        marker_data = input.marker_transforms_msg

        print("len marker_data ", len(marker_data.transforms))
        print("pointcloud_data frame_id: ", pointcloud_data.header.frame_id)
        print("marker_data frame_id: ",     marker_data.header.frame_id)
        print("pointcloud_data stamp: ", pointcloud_data.header.stamp)
        print("marker_data stamp: ",     marker_data.header.stamp)

        if not self.start_search_intersection_point_and_obstacle: #and self.pointcloud_counter%2 == 0:
            # Transform
            try:
                lookuptime = time.time()
                transform = self._tf_buffer.lookup_transform(target_frame,
                                                   pointcloud_data.header.frame_id,
                                                   pointcloud_data.header.stamp,
                                                   rospy.Duration(0.2))               
                
                elapsed_lookup = time.time() - lookuptime

                print('------------------------------ lookup_transform elapsed time: ', int(elapsed_lookup*1000), ' ms. ')
                print("pointcloud transform to base_link success")



                markerpose_list = []
                if(len(marker_data.transforms) == 0):
                    markerpose_list.append(None)
                else:
                    for fid_transform in marker_data.transforms: 
                        marker_point = PointStamped()
                        marker_point.header.frame_id = pointcloud_data.header.frame_id
                        marker_point.header.stamp = pointcloud_data.header.stamp
                        marker_point.point.x = fid_transform.transform.translation.x
                        marker_point.point.y = fid_transform.transform.translation.y
                        marker_point.point.z = fid_transform.transform.translation.z
                        markerpose_list.append(marker_point)


                # print( "------------------------- markerpose_list: ------------------ ", markerpose_list)
                transformdata_tuple = (pointcloud_data, transform, markerpose_list)
                self.transformdata_list.append(transformdata_tuple)

            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                traceback.print_exc()
                return 






        print("--- one_marker_detected flag: ",self.one_marker_detected)
        print("--- start_pc_service flag: ", self.start_pc_service)
        print("--- start_search_intersection_point_and_obstacle flag: ", self.start_search_intersection_point_and_obstacle)

        if(len(marker_data.transforms)!=0): 
            self.one_marker_detected = True
        


        if self.one_marker_detected is True:
            #### check if enough pointclouds gathered to start looking for intersection point
            if self.start_pc_service and not self.start_search_intersection_point_and_obstacle:
                #### transform gathered data at once 
                self.transform_data()
                #### start service
                self.set_search_intersection_point_and_obstacle()
                
                #### publish actiondata
                # self.pub_action_data()
                self.start_search_intersection_point_and_obstacle = True

        ats_elapsed = time.time() - ats_start
        print('callback_ats elapsed time: ', int(ats_elapsed*1000))
        print("\n")


            
    @property
    def lpt_status(self):
        return self._pos.lpt_status

    @property
    def pos_vel(self):
        return self._pos.pos_vel

    def run(self):
        rospy.loginfo('run()')
        if(self.ready_flag is True):
            rospy.sleep(1)
            self.searching_move()

    def reset(self):
        self.marker_detection = False
        self.mission_fail = False
        self.no_acting = False
        self.is_done = False
        self.set_target_flag = False
        self.image_view = False
        self.is_done_count = 0 
        self.targeting_on_view = False
        self.lpt_status_error = False

    def set_target_view(self,target_u,target_v):
        self.target_u = target_u
        self.target_v = target_v
        if(self.target_u == 0 and self.target_v == 0):
            self.set_target_flag = False
        else:
            self.set_target_flag = True


    def set_target_goal(self, transformed_poi):
        self.target_poi = transformed_poi


    def set_L(self, L):
        self.L = L


    def set_poi_angles(self):
        self.alpha_pan_angle = np.arctan2(self.target_poi.point.y, self.target_poi.point.x)

        dist_to_poi_l = math.sqrt(self.target_poi.point.x*self.target_poi.point.x + self.target_poi.point.y*self.target_poi.point.y)
        self.beta_tilt_angle = np.arctan2(self.L, dist_to_poi_l)

        print "=== set_poi_angles ==="
        print "alpha_pan_angle    : ", self.alpha_pan_angle
        print "beta_tilt_angle    : ", self.beta_tilt_angle


    def set_range_angles(self):
        dist_to_poi_l = math.sqrt(self.target_poi.point.x*self.target_poi.point.x + self.target_poi.point.y*self.target_poi.point.y
                                  + self.target_poi.point.z*self.target_poi.point.z)
        self.theta_pan_angle = np.arctan2(self.r_pan, dist_to_poi_l)

        l_prime =math.sqrt(self.L*self.L + dist_to_poi_l*dist_to_poi_l)
        self.theta_tilt_angle = np.arctan2(self.r_tilt, l_prime)

        print "=== set_range_angles ==="
        print "theta_pan_angle    : ", self.theta_pan_angle
        print "theta_tilt_angle    : ", self.theta_tilt_angle


    def set_movement_step(self, number_of_pan_angle_sections):


        self.p_step_array = []

       
        for i in range(number_of_pan_angle_sections, 0, -1):
            self.p_step_array.append(self.alpha_pan_angle - i*self.theta_pan_angle/number_of_pan_angle_sections)

        for i in range(1, number_of_pan_angle_sections+1):
            self.p_step_array.append(self.alpha_pan_angle + i*self.theta_pan_angle/number_of_pan_angle_sections)




        self.t_step_array = [self.beta_tilt_angle - self.theta_tilt_angle, self.beta_tilt_angle + self.theta_tilt_angle]


        print "=== set_movement_step ==="
        print "p_step_array    : ", self.p_step_array
        print "t_step_array    : ", self.t_step_array





    def set_marker_detection(self):
        self.t = threading.Thread(target=self.run)
        self.t.start()
        return True
 


    def run_poi_area_search(self):
        rospy.loginfo('run_poi_area_search()')
        if(self.ready_flag is True):
            rospy.sleep(1)
            self.poi_searching_move()


    def set_obstacle_detection(self):
        self.t = threading.Thread(target=self.run_poi_area_search)
        self.t.start()
        return True



    def run_obstacle_srv(self):
        rospy.loginfo('run_obstacle_srv()')
        print("Requesting GetObstacle service")

        self.obstacle_srv_response = self.get_intersection_point_and_obstacle_client()
        print("get_intersection_point_and_obstacle_client response: ", self.obstacle_srv_response)
        self.done_search_intersection_point_and_obstacle = True

        if self.done_search_intersection_point_and_obstacle:
            self.lpt_set_position(0.0, 0.0, 0.0)
            print("Return to origin")
            self.is_done = True
            return



    def set_search_intersection_point_and_obstacle(self):
        self.start_search_intersection_point_and_obstacle = True
        print("start_search_intersection_point_and_obstacle flag: ", self.start_search_intersection_point_and_obstacle)
        self.t = threading.Thread(target=self.run_obstacle_srv)
        self.t.start()
        return True
 

    def pub_action_data(self):
        print("######### Publish ActionData")
        self.marker_midpoint = self.calc_marker_midpoint()
        actiondata_msg = ActionData()
        actiondata_msg.list_of_pointclouds_baselink = self.transformed_pc_list
        actiondata_msg.poi_baselink = self.target_poi
        actiondata_msg.marker_midpoint = self.marker_midpoint

        self.action_data_pub.publish(actiondata_msg)
        return True

    def transform_poi_to_baselink(self, poi):
        target_frame = "base_link"

        try:
            transform = self._tf_buffer.lookup_transform(target_frame,
                                               poi.header.frame_id, #source frame
                                               rospy.Time.now(), #extrapolation?
                                               rospy.Duration(1.0))
            
            point_transformed = tf2_geometry_msgs.do_transform_point(poi, transform)

            print("transform poi to base_link success")
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            rospy.sleep(0.2)

        return point_transformed 






    def transform_L_to_baselink(self):
        target_frame = "base_link"
        L = 0.0
        try:
            transform = self._tf_buffer.lookup_transform(target_frame,
                                               "rgbd_link", #source frame
                                               rospy.Time.now(), #rospy.Time(0),
                                               rospy.Duration(1.0))                 #1.0
            
            L = transform.transform.translation.z
            print("transform rgbd_link to base_link success")
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            rospy.sleep(0.2)


        return L


    def lpt_set_position(self, lift, pan, tilt): 
        self._lift = lift
        self._pan = pan
        self._tilt = tilt
               
        result = self._lpt_set_position(lift, pan, tilt)
        
        return result

    def lpt_set_velocity(self):
        lift_vel = 75/4.
        # pan_vel = 1.0471975511965976/4.
        # tilt_vel = 0.5235987755982988/4.
        pan_vel = 1.0471975511965976/8.
        tilt_vel = 0.5235987755982988/8.
        self._lpt_set_velocity(lift_vel, pan_vel, tilt_vel)  
    
    def is_done_check(self):        
        if(self.is_done):
            self.marker_detection = False
            return True 
        else:
            return False 

class ActionServer(workerbee_actionlib.MutuallyExclusiveActionServer):
    def __init__(self, action_server_group):
        super(ActionServer, self).__init__(
            action_server_group, 
            Params.action_name, 
            SetObstacleFinderAction
        )        
        self._action_server_group = action_server_group
        self._last_execution_time = None 
        print 'Action Name : ',Params.action_name
        print 'image_view : ',Params.image_view
        print 'rgb_camera_info_topic : ',Params.rgb_camera_info_topic
        print 'marker_image_topic    : ',Params.marker_image_topic
        # print 'marker_vertices_topic : ',Params.marker_vertices_topic
        print 'marker_transforms_topic : ',Params.marker_transforms_topic
        print 'rgbd_pointcloud_topic : ',Params.rgbd_pointcloud_topic



        print 'r_pan          : ',Params.r_pan
        print 'r_tilt          : ',Params.r_tilt
        print 'number_of_pan_angle_sections          : ',Params.number_of_pan_angle_sections

        self.controller = MarkerDetectionProxy(self.ag.transform_listener, self.ag.transform_listener2, Params.rgb_camera_info_topic, Params.marker_image_topic,
                                                 Params.marker_transforms_topic, Params.rgbd_pointcloud_topic)
        self.controller.set_r(Params.r_pan, Params.r_tilt)
        self.controller.set_image_view(Params.image_view)
        self.controller.marker_sub()
        ##### self.controller.marker_unsub()
        
           
    def _result_for_preemption(self, new_action_name):
        result = SetObstacleFinderResult()
        result.state.code =ObjectFinderState.ERROR_PREEMPTED
        result.state.details= ('PREEMPTED BY NEW ACTION(%s)'%(new_action_name))
        return result 
    
    
    def _excute_callback(self, goal):      
        rospy.loginfo('start SetObstacleFinderAction. goal=\n%s'%(str(goal)))
        ##### self.controller.marker_sub()
        rospy.sleep(3)
        
        self.controller.reset()
        self.controller.lpt_set_velocity()
        self.controller.no_acting = False

        result = SetObstacleFinderResult()

        transformed_L = self.controller.transform_L_to_baselink()
        transformed_poi = self.controller.transform_poi_to_baselink(goal.target.poi)

        print("--------------------------------------")
        print("transformed_poi from goal.target.poi: ", transformed_poi.point.x, transformed_poi.point.y, transformed_poi.point.z)

        print("transformed_L: ", transformed_L)
        print("--------------------------------------")

        self.controller.set_target_goal(transformed_poi)
        self.controller.set_L(transformed_L)
        self.controller.set_poi_angles()
        self.controller.set_range_angles()
        self.controller.set_movement_step(Params.number_of_pan_angle_sections)


        res = self.controller.set_obstacle_detection()
        print("res: ", res)
        # self.controller.set_target_view(goal.target.u, goal.target.v)
        # res = self.controller.set_marker_detection()

        # if(res != True): 
        #     result.state.code = ObjectFinderState.ERROR_ETC
        #     result.state.details = 'ERROR_ETC'
        #     self._as.set_aborted(result)    
        #     return
                        
        # start executing the action
        rate = rospy.Rate(10)
        self._last_execution_time = rospy.Time.now()
        while(True): 
            rate.sleep()

            result = SetObstacleFinderResult()
            feedback = SetObstacleFinderFeedback() 

            # check faults 
            # faults = workerbee_status.get_fault()
            # if(len(faults) > 0):
            #     rospy.loginfo('%s: Fault' % Params.action_name)
            #     self.controller.set_fail()
            #     result.state.code = ObjectFinderState.ERROR_ETC
            #     result.state.details = 'ERROR_ETC : workerbee_status faults'
            #     self._as.set_aborted(result)
            #     self.controller.marker_unsub()
            #     break

            # check complete condition 

            if(self.controller.is_done_check() is True):
                rospy.loginfo('%s: Succeeded' % Params.action_name) 
                result.state.code = ObjectFinderState.NO_ERROR
                result.state.details = 'NO_ERROR : Action Succeeded'
                result.obstacle_detected = self.obstacle_srv_response
                self._as.set_succeeded(result)
                self.controller.marker_unsub() 
                break

            if(self.controller.lpt_status_error is True):
                rospy.loginfo('%s: Fault' % Params.action_name)
                result.state.code = ObjectFinderState.ERROR_CONTROLLER
                result.state.details = 'ERROR_CONTROLLER : LPT Status Error'
                self._as.set_aborted(result)
                self.controller.marker_unsub()
                break

            if(self.controller.mission_fail is True):
                rospy.loginfo('%s: Fault' % Params.action_name)
                result.state.code = ObjectFinderState.ERROR_TIMEOUT
                result.state.details = 'ERROR_TIMEOUT : NO MARKER'
                self._as.set_aborted(result)
                self.controller.marker_unsub()
                break

            if(self.controller.ready_flag is False):
                rospy.loginfo('%s: Fault' % Params.action_name)
                result.state.code = ObjectFinderState.ERROR_ETC
                result.state.details = 'ERROR_ETC : No RGBD Image Topic'
                rospy.logerr('ERROR_ETC : No RGBD Image Topic')
                self._as.set_aborted(result)
                self.controller.marker_unsub()
                break

            if(self._as.is_active() is False):
                self.controller.marker_unsub()
                # aborted
                break   

            # check preemption
            if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % Params.action_name)
                result.state.code = ObjectFinderState.ERROR_ETC
                result.state.details = 'ERROR_ETC : Preempted'
                self._as.set_preempted(result)
                self.controller.marker_unsub()
                break
            feedback.state.code = 0
            feedback.state.details = 'NO_ERROR'

            self._as.publish_feedback(feedback) 


