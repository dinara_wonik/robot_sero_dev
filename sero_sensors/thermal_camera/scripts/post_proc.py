#!/usr/bin/python
# -*- coding: utf8 -*-#

"""
2020.07.08 ('c')void 
"""

import os
import itertools
import traceback
import time
import datetime
import random
import colorsys
import Queue
import cv2
import collections
import thread, threading
import logging
import logging.handlers
import numpy as np
import rospy
import tf
import tf.transformations as trans
import ros_numpy
import dynamic_reconfigure.server
from message_filters import ApproximateTimeSynchronizer, Subscriber
from dynamic_reconfigure.server import Server
from cv_bridge import CvBridge, CvBridgeError
import sensor_msgs.msg
import std_msgs.msg 
from thermal_camera.cfg import ThermalCamConfig

import matplotlib.pyplot as plt


class PostProcess(object):
    def __init__(self):
        self._thermal_image_topic = rospy.get_param("~thermal_image_topic", "thermal/mono16/image_raw")
        self._kelvin_topic = rospy.get_param("~kelvin_topic", "thermal/kelvin")
        self._rgb_topic = rospy.get_param("~rgb_topic", "thermal/rgb/image_raw")

        self._bridge = CvBridge()
        
        self._srv = dynamic_reconfigure.server.Server(ThermalCamConfig, self._reconfig_callback)
        
        self._pub_kelvin = rospy.Publisher(self._kelvin_topic, sensor_msgs.msg.Image, queue_size=10)
        self._pub_rgb = rospy.Publisher(self._rgb_topic, sensor_msgs.msg.Image, queue_size=10)
        
        self._subs = []
        s = rospy.Subscriber(self._thermal_image_topic, sensor_msgs.msg.Image, self._thermal_callback, queue_size=10)
        self._subs.append(s)

    def _reconfig_callback(self, config, level):
        return config
                        
    def _thermal_callback(self, msg):        
        cv_image = self._bridge.imgmsg_to_cv2(msg, "passthrough")  
        temp_image = cv_image.astype(np.float32)/100 # kelvin 
        msg_kelvin = self._bridge.cv2_to_imgmsg(temp_image, "passthrough")
        
        TEMP_MIN = 20.
        TEMP_MAX = 40.      
        
        temp_image -= 273.15        
        temp_image[np.where(temp_image<TEMP_MIN)] = TEMP_MIN
        temp_image[np.where(temp_image>TEMP_MAX)] = TEMP_MAX
        temp_image = 255./(TEMP_MAX-TEMP_MIN) * temp_image
        temp_image = temp_image.astype(np.uint8)
        rgb_image = cv2.applyColorMap(temp_image, cv2.COLORMAP_JET)
        msg_rgb = self._bridge.cv2_to_imgmsg(rgb_image, "bgr8")
        
        self._pub_kelvin.publish(msg_kelvin)
        self._pub_rgb.publish(msg_rgb)
        

if __name__ == "__main__": 
    
    rospy.init_node("post_proc")
    node = PostProcess()
    rospy.spin()
    node.release()