#!/usr/bin/python
# -*- coding: utf8 -*-#

"""
2020.07.08 ('c')void 
"""

import os
import itertools
import traceback
import time
import datetime
import random
import colorsys
import Queue
import cv2
import collections
import thread, threading
import logging
import logging.handlers
import numpy as np
import rospy
import tf
import tf.transformations as trans
import ros_numpy
import dynamic_reconfigure.server
from message_filters import ApproximateTimeSynchronizer, Subscriber
from dynamic_reconfigure.server import Server
from cv_bridge import CvBridge, CvBridgeError
import sensor_msgs.msg
import std_msgs.msg 
from thermal_camera.cfg import ThermalCamConfig

import matplotlib.pyplot as plt



def rawtokelvin(val):
    return val / 100.0

def ktoc(val):
    return val - 273.15

def ktof(val):
    return 1.8 * ktoc(val) + 32.0


def _test1(val, _scale):
    return (val - 273.15)*_scale
    

def conv(cv_thermal_image):
    MIN = 293.15 #20c
    MAX = 313.15 #40c
    temp_image = rawtokelvin(cv_thermal_image).astype(np.float32)
    
    # COLORMAP_HOT
    temp_image[temp_image<MIN] = MIN
    temp_image[temp_image>MAX] = MAX

    temp_image1 = _test1(temp_image, 2.55*(100/(MAX-MIN))).astype(np.uint8)
    cv_image_color = cv2.applyColorMap(temp_image1, cv2.COLORMAP_JET)
    
    # TEST 
    cv_image_color[np.where(temp_image <= MIN)] = 0

    return cv_image_color


from scipy.interpolate import griddata

def conv1(cv_thermal_image):
    MIN = 293.15 #20c
    MAX = 313.15 #40c
    temp_image = rawtokelvin(cv_thermal_image).astype(np.float32)


    idx = np.where(temp_image > MIN)
    points = np.array(zip(*idx))
    values = temp_image[idx]

    h, w = cv_thermal_image.shape[:2]
    grid_x, grid_y = np.mgrid[0:h, 0:w]

    temp_image = griddata(points, values, (grid_x, grid_y), method='cubic')
    
    # COLORMAP_HOT
    temp_image[np.where(temp_image<MIN)] = MIN

    temp_image1 = _test1(temp_image, 2.55*(100/(MAX-MIN))).astype(np.uint8)
    cv_image_color = cv2.applyColorMap(temp_image1, cv2.COLORMAP_JET)
    
    return cv_image_color



class PostProcess(object):
    def __init__(self):
        self._rgb_image_topic = rospy.get_param("~rgb_image_topic", "/rgbd/rgb/image_rect_color")
        self._thermal_image_topic = rospy.get_param("~thermal_image_topic", "thermal/mono16/sw_registered/image_rect_raw")
        self._robot_frame_id = rospy.get_param("~robot_frame", "base_link")

        self._bridge = CvBridge()
        
        self._rgb_queue = []
        self._thermal_queue = Queue.Queue()
        self._queue_max = 60
        
        self._sync = 0
        self._show_image = False
        
        self._srv = dynamic_reconfigure.server.Server(ThermalCamConfig, self._reconfig_callback)
        
        subs = []
        s = rospy.Subscriber(self._rgb_image_topic, sensor_msgs.msg.Image, self._rgb_callback, queue_size=60)
        subs.append(s)

        s = rospy.Subscriber(self._thermal_image_topic, sensor_msgs.msg.Image, self._thermal_callback, queue_size=10)
        subs.append(s)
                
        thread.start_new_thread(self._thread_proc, ())
        
    def _reconfig_callback(self, config, level):
        self._sync = config.sync
        self._show_image = config.show_image
        return config
    
    def _rgb_callback(self, msg):
        self._rgb_queue.insert(0, msg) 
        self._rgb_queue = self._rgb_queue[:self._queue_max]
        
    def _thermal_callback(self, msg):
        self._thermal_queue.put(msg)
    
    def _thread_proc(self):
        while(True): 
            thermal_image_msg = self._thermal_queue.get()

            if(len(self._rgb_queue) == 0):
                continue 

            # sync 
            sync_stamp = thermal_image_msg.header.stamp - rospy.Duration.from_sec(self._sync)
            error = [(msg.header.stamp - sync_stamp) for msg in self._rgb_queue]
            error = np.abs(error)
            idx = np.where(error == np.min(error))[0][0]
            rgb_image_msg = self._rgb_queue[idx]
    
            if(self._show_image):
                cv_rgb_image = self._bridge.imgmsg_to_cv2(rgb_image_msg, "bgr8")      
                cv_thermal_image = self._bridge.imgmsg_to_cv2(thermal_image_msg, "passthrough")  
                                
                alpha = 0.5
                output = cv2.addWeighted(conv1(cv_thermal_image), alpha, cv_rgb_image, 1 - alpha, 0)
                cv2.imshow('sync & matching', output)
                cv2.waitKey(1)      
            
            # empty 
            while(self._thermal_queue.qsize() > 0):
                self._thermal_queue.get()
 


if __name__ == "__main__": 
    
    rospy.init_node("post_proc")
    node = PostProcess()
    rospy.spin()
    node.release()