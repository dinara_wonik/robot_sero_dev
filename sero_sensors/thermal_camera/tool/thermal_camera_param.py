#!/usr/bin/python3
# -*- coding: utf8 -*-#
#

import numpy as np 


# lepton-3-3.5-datasheet.pdf
FOV_H  =  57. * np.pi/180 
FOV_D  =  71. * np.pi/180
WIDTH  = 160.
HEIGHT = 120. 


def camera_model():
    # https://stackoverflow.com/questions/39992968/how-to-calculate-field-of-view-of-the-camera-from-camera-intrinsic-matrix
    def get_focal_length():
        f = 0.5 * WIDTH/np.tan(FOV_H/2)
        
        if(False):
            DIAG = np.linalg.norm(np.array((WIDTH, HEIGHT)))
            f = DIAG/np.tan(FOV_D/2)
            
        return f 
    
    def arr2str(arr):
        return "[%s]"%(', '.join(map(lambda x: "%.5f"%x, arr))) 
    
    f = get_focal_length()
    fx = f 
    fy = f 
    cx = WIDTH/2
    cy = HEIGHT/2 
    
    # distortion parameters
    distortion_model = 'plumb_bob'
    D = np.zeros(5)
    
    K = [[fx,  0, cx],
         [ 0, fy, cy],
         [ 0,  0,  1],]
    K = np.array(K) 
    
    R = np.array(np.eye(3)) 
    
    # P = K(R|t)
    Rt = np.zeros((3,4))
    Rt[:3,:3] = R 
    P = np.dot(K, Rt) 
    
    format = \
"""
image_width: %d
image_height: %d
camera_name: lepton
camera_matrix:
  rows: 3
  cols: 3
  data: %s
distortion_model: plumb_bob
distortion_coefficients:
  rows: 1
  cols: 5
  data: %s
rectification_matrix:
  rows: 3
  cols: 3
  data: %s
projection_matrix:
  rows: 3
  cols: 4
  data: %s
"""    
    print(format%(WIDTH, HEIGHT, arr2str(K.flatten()), arr2str(D.flatten()), arr2str(R.flatten()), arr2str(P.flatten())))


if __name__ == "__main__":
    camera_model()

