#!/usr/bin/python
# -*- coding: utf8 -*-#

# 2021.02.08 jglee


import rospy
import roslaunch
import rosparam
import rospkg
import numpy as np
import os
import netifaces
import yaml
import traceback

import ctypes
from openni import openni2 # pip install openni
from openni import _openni2 as c_api


def get_astra_serial_numbers():
    serial_numbers = []
    
    try:
        dll_path = os.path.join(rospkg.RosPack().get_path('astra_camera'), "include/openni2_redist/x64")
        openni2.initialize(dll_path)
        
        for dev in openni2.Device.open_all() :
            info = dev.get_device_info()
            
            if(info.vendor == "Orbbec" and info.name == "Astra"):
                serial_number = str(dev.get_property(c_api.ONI_DEVICE_PROPERTY_SERIAL_NUMBER, (ctypes.c_char * 100)).value) 
                serial_numbers.append(serial_number)
                    
        openni2.unload() 
    except:
        rospy.logfatal("can't initialize openni(dll_path=%s)"%(dll_path))
    
    return serial_numbers


class RgbdLaunch(object) :
    '''
    Astra pro 의 SN에 맞는 calibration file 경로를 얻고 launch 시킴 
    '''
    def __init__(self):  
        serial_numbers = get_astra_serial_numbers() 

        if(len(serial_numbers) == 0):
            rospy.logfatal("can not find Astra Camera")
            return 
                
        serial_number = serial_numbers[0] 
        rospy.loginfo("find Astra(sn=%s)"%(serial_number))
        
        calib = self.search_calibration_file(serial_number)
        if(calib is None):
            rospy.logfatal("can not find camera calibration files for SN=%s"%(serial_number))
            return 
        
        rgb_path, ir_path = calib
        
        # launch 
        uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
        cli_args = ['sero_sensors', 'astrapro_sero.launch', 'depth_camera_info_url:=file://%s'%ir_path, 'rgb_camera_info_url:=file://%s'%rgb_path]
        roslaunch_file = roslaunch.rlutil.resolve_launch_arguments(cli_args)
        roslaunch_args = cli_args[2:]
        roslaunch_command = [(roslaunch_file[0], roslaunch_args)]
        self.parent = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_command, verbose=True, is_rostest=True)
        self.parent.start()
        
    def search_calibration_file(self, serial_number):
        cal_dir = os.path.join(rospkg.RosPack().get_path('workerbee_platform_config'), "configs/rgbd_calibration/%s"%(serial_number))
        if(os.path.exists(cal_dir)):
            rgb_path = os.path.join(cal_dir, "calibration_rgb.yaml")
            ir_path = os.path.join(cal_dir, "calibration_ir.yaml")
            return (rgb_path, ir_path)
        else:
            return None 
        
if __name__ == '__main__':
    rospy.init_node('astra_bringup_node')
    rospy.loginfo("start astra_bringup_node!!!")
    r = RgbdLaunch()
    rospy.spin()