#ifndef PC_DETECTOR_NODE_H_
#define PC_DETECTOR_NODE_H_

#include <ros/ros.h>
#include <ros/console.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <image_geometry/pinhole_camera_model.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/crop_box.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include <pc_detection/GetObstacle.h>
#include <pc_detection/ActionData.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/radius_outlier_removal.h>

#include <pcl/common/intersections.h>
#include <visualization_msgs/Marker.h>
#include <rviz/helpers/color.h>

#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

//for pointToPlaneDistance
#include <pcl/common/distances.h>
#include <pcl/sample_consensus/sac_model_plane.h>

//icp
#include <pcl/registration/icp.h>
#include <pcl/registration/ia_ransac.h>
#include <chrono>


//cov.matrix
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/common/common.h>
#include <pcl/common/impl/centroid.hpp>

#include <dynamic_reconfigure/server.h>
#include <pc_detection/PCDetectionConfig.h>

#include <angles/angles.h>

namespace pc_detection {


// Singleton class example: https://www.tutorialspoint.com/Explain-Cplusplus-Singleton-design-pattern
//	class Singleton {
//	   static Singleton *instance;
//	   int data;
//
//	   // Private constructor so that no objects can be created.
//	   Singleton() {
//	      data = 0;
//	   }
//
//	   public:
//	   static Singleton *getInstance() {
//	      if (!instance)
//	      instance = new Singleton;
//	      return instance;
//	   }
//
//	   int getData() {
//	      return this -> data;
//	   }
//
//	   void setData(int data) {
//	      this -> data = data;
//	   }
//	};


class Publisher {
public:
    Publisher(const ros::NodeHandle &nh);
    ros::NodeHandle nh_;
    static Publisher* instance;
    ros::Publisher pub_pc_wall1_;
    ros::Publisher pub_pc_wall2_;
    ros::Publisher pub_pc_floor_;
    ros::Publisher pub_pc_obstacle_;
    ros::Publisher pub_pc_merged_;
    ros::Publisher pub_intersection_point_marker_;
    ros::Publisher pub_poi_;
    ros::Publisher pub_marker_poi_;
    ros::Publisher pub_inliers_;
    ros::Publisher pub_pc_cornerframe_;

    static Publisher *initialize(const ros::NodeHandle& nh)
    {
        if (!instance)
            instance = new Publisher(nh);
        return instance;
    }
    static Publisher *getInstance()
    {
        return instance;
    }

};

class DynamicRcfg {
public:
    DynamicRcfg();
    static DynamicRcfg* instance;
    dynamic_reconfigure::Server<pc_detection::PCDetectionConfig> dyn_rec_server_;
    void start_dynamic_reconfigure();

    //DynamicReconfigure parameters

    double input_x_min;
    double input_x_max;
    double input_y_min;
    double input_y_max;
    double input_z_min;
    double input_z_max;

    double output_x_min;
    double output_x_max;
    double output_y_min;
    double output_y_max;
    double output_z_min;
    double output_z_max;

    double output_x_min_poitype2;
    double output_x_max_poitype2;
    double output_y_min_poitype2;
    double output_y_max_poitype2;
    double output_z_min_poitype2;
    double output_z_max_poitype2;

    double output_x_min_poitype3;
    double output_x_max_poitype3;
    double output_y_min_poitype3;
    double output_y_max_poitype3;
    double output_z_min_poitype3;
    double output_z_max_poitype3;

    double output_x_min_wall;
    double output_x_max_wall;
    double output_y_min_wall;
    double output_y_max_wall;
    double output_z_min_wall;
    double output_z_max_wall;


    int obstacle_size;
    double cloud_width_length_ratio;
    double voxel_size;
    double wall_voxel_size;
    int min_cluster_size;
    double floor_angle;
    double floor_thickness;
    double wall_angle;
    double wall_thickness;
    double wall_percentage;
    int pc_sampling;

    void dynamic_reconf_callback(pc_detection::PCDetectionConfig& config, int32_t level) {

        std::cout << "-------------- dynamic_reconf_callback -----------------" <<std::endl;

        DynamicRcfg::getInstance()->input_x_min = config.input_x_min;
        DynamicRcfg::getInstance()->input_x_max = config.input_x_max;
        DynamicRcfg::getInstance()->input_y_min = config.input_y_min;
        DynamicRcfg::getInstance()->input_y_max = config.input_y_max;
        DynamicRcfg::getInstance()->input_z_min = config.input_z_min;
        DynamicRcfg::getInstance()->input_z_max = config.input_z_max;

        DynamicRcfg::getInstance()->output_x_min = config.output_x_min;
        DynamicRcfg::getInstance()->output_x_max = config.output_x_max;
        DynamicRcfg::getInstance()->output_y_min = config.output_y_min;
        DynamicRcfg::getInstance()->output_y_max = config.output_y_max;
        DynamicRcfg::getInstance()->output_z_min = config.output_z_min;
        DynamicRcfg::getInstance()->output_z_max = config.output_z_max;

        DynamicRcfg::getInstance()->output_x_min_poitype2 = config.output_x_min_poitype2;
        DynamicRcfg::getInstance()->output_x_max_poitype2 = config.output_x_max_poitype2;
        DynamicRcfg::getInstance()->output_y_min_poitype2 = config.output_y_min_poitype2;
        DynamicRcfg::getInstance()->output_y_max_poitype2 = config.output_y_max_poitype2;
        DynamicRcfg::getInstance()->output_z_min_poitype2 = config.output_z_min_poitype2;
        DynamicRcfg::getInstance()->output_z_max_poitype2 = config.output_z_max_poitype2;

        DynamicRcfg::getInstance()->output_x_min_poitype3 = config.output_x_min_poitype3;
        DynamicRcfg::getInstance()->output_x_max_poitype3 = config.output_x_max_poitype3;
        DynamicRcfg::getInstance()->output_y_min_poitype3 = config.output_y_min_poitype3;
        DynamicRcfg::getInstance()->output_y_max_poitype3 = config.output_y_max_poitype3;
        DynamicRcfg::getInstance()->output_z_min_poitype3 = config.output_z_min_poitype3;
        DynamicRcfg::getInstance()->output_z_max_poitype3 = config.output_z_max_poitype3;

        DynamicRcfg::getInstance()->output_x_min_wall = config.output_x_min_wall;
        DynamicRcfg::getInstance()->output_x_max_wall = config.output_x_max_wall;
        DynamicRcfg::getInstance()->output_y_min_wall = config.output_y_min_wall;
        DynamicRcfg::getInstance()->output_y_max_wall = config.output_y_max_wall;
        DynamicRcfg::getInstance()->output_z_min_wall = config.output_z_min_wall;
        DynamicRcfg::getInstance()->output_z_max_wall = config.output_z_max_wall;


        DynamicRcfg::getInstance()->obstacle_size = config.obstacle_size;
        DynamicRcfg::getInstance()->cloud_width_length_ratio = config.cloud_width_length_ratio;
        DynamicRcfg::getInstance()->voxel_size = config.voxel_size;
        DynamicRcfg::getInstance()->wall_voxel_size = config.wall_voxel_size;
        DynamicRcfg::getInstance()->min_cluster_size = config.min_cluster_size;
        DynamicRcfg::getInstance()->floor_angle = config.floor_angle;
        DynamicRcfg::getInstance()->floor_thickness = config.floor_thickness;
        DynamicRcfg::getInstance()->wall_angle = config.wall_angle;
        DynamicRcfg::getInstance()->wall_thickness = config.wall_thickness;
        DynamicRcfg::getInstance()->wall_percentage = config.wall_percentage;
        DynamicRcfg::getInstance()->pc_sampling = config.pc_sampling;

    }

    static DynamicRcfg *initialize()
    {
        if (!instance)
            instance = new DynamicRcfg();
        return instance;
    }
    static DynamicRcfg *getInstance()
    {
        return instance;
    }

};

class ServiceRcv {
public:
    ServiceRcv(const ros::NodeHandle &nh);
    ros::NodeHandle nh_;

    ros::ServiceServer service_;
    bool get_intersection_point_and_obstacle(pc_detection::GetObstacle::Request& req, pc_detection::GetObstacle::Response &res);
};

class Detector {
public:
    Detector(const ros::NodeHandle &nh, int poi_type);


    //	private:
    std::string _name = ros::this_node::getName();
    void PointCloudCb(const pc_detection::ActionData &actiondata_msg);


    ros::NodeHandle nh_;
    ros::Subscriber sub_actiondata_;

    typedef pcl::PointCloud<pcl::PointXYZ> PointCloudType;
    typedef pcl::PCLPointCloud2            PointCloud2Type;
    sensor_msgs::PointCloud2 pc2_input__;

    const float RANGE_MAX = 3.0;

    tf2_ros::StaticTransformBroadcaster static_tf_broadcaster_;
    tf2_ros::Buffer tf_;
    tf2_ros::TransformListener tfListener;
    pcl::PointCloud<pcl::PointXYZ> cloud_segmented_wall1;
    pcl::PointCloud<pcl::PointXYZ> cloud_segmented_wall2;
    pcl::PointCloud<pcl::PointXYZ> cloud_segmented_floor;
    pcl::PointCloud<pcl::PointXYZ> cloud_segmented_obstacle;



    pcl::ModelCoefficients::Ptr coefficients_floor = pcl::ModelCoefficients::Ptr(new pcl::ModelCoefficients);
    pcl::ModelCoefficients::Ptr coefficients_wall1 = pcl::ModelCoefficients::Ptr(new pcl::ModelCoefficients);
    pcl::ModelCoefficients::Ptr coefficients_wall2 = pcl::ModelCoefficients::Ptr(new pcl::ModelCoefficients);



    PointCloudType::Ptr no_planars_cloud = PointCloudType::Ptr(new PointCloudType());

    Eigen::Vector4d plane_wall1, plane_wall2, plane_floor;
    Eigen::Vector3d intersection_point;

    bool floor_plane_detected = false;
    bool wall_planes_detected = false;
    bool poi_type_correct = true;


    pcl::PointIndices::Ptr inliers_wall1 = pcl::PointIndices::Ptr(new pcl::PointIndices);
    pcl::PointIndices::Ptr inliers_wall2 = pcl::PointIndices::Ptr(new pcl::PointIndices);


    void publishPOIs(const pcl::PointXYZ& user_poi_point, const pcl::PointXYZ& poi_point);

    PointCloudType::Ptr getInputCropbox(const PointCloudType::ConstPtr& pc_in_baselink, const geometry_msgs::PointStamped& marker_midpoint);
    PointCloudType::Ptr getICP(const std::vector<sensor_msgs::PointCloud2>& list_of_pointclouds_baselink, const geometry_msgs::PointStamped& marker_midpoint, double voxel_size);

    PointCloudType::Ptr getFloorPlane(const PointCloudType::Ptr& pc_in_baselink);
    void getWalls(const PointCloudType::Ptr& pc_in_baselink, const pcl::PointXYZ& poi_baselink);
    std::vector<pcl::PointCloud<pcl::PointXYZ>> clusterExtraction(const std::vector<pcl::PointCloud<pcl::PointXYZ>>& planes);
    std::vector<pcl::ModelCoefficients> clusterProcessing(const std::vector<pcl::PointCloud<pcl::PointXYZ>>& max_cluster_planes);
    std::vector<double> computeEigenValues(const pcl::PointCloud<pcl::PointXYZ>::Ptr& input_cloud);

    PointCloudType::Ptr removePlanars(const PointCloudType::Ptr& pc_in_baselink, int poi_type);
    bool getIntersectionPoint(const PointCloudType::Ptr& pc_in_baselink, const pcl::PointXYZ& poi_baselink);

    bool getObstacleCropbox(const PointCloudType::Ptr& pc_in_baselink);
    PointCloudType::Ptr getTransformedObstacleCloud (	const Eigen::Vector3d& intersection_point,
            const PointCloudType::Ptr& obstacle_in_baselink);
    PointCloudType::Ptr getSingleWallTransformedObstacleCloud (	const Eigen::Vector3d& intersection_point,
            const PointCloudType::Ptr& obstacle_in_baselink);

    pcl::PCDWriter writer;
    // PointCloudType::Ptr merged_pointcloud = PointCloudType::Ptr(new PointCloudType());
    std::string merged_pointcloud_frameid;
    int poi_type;
;

};

}
#endif
