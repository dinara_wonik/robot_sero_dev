// 2021.07.27 ('d')dinara

#include "pc_detection/detector_node.h"

int main(int argc, char **argv) {
    ros::init(argc, argv, "pc_detector");
    ros::NodeHandle nh;


    //    try {
    //    	pc_detection::ServiceRcv service_rcv(nh);
    //		pc_detection::Publisher::initialize(nh);
    //		pc_detection::DynamicRcfg::initialize();
    //
    //		printf("------------in detector_main.cpp------------\n");
    //		ros::spin();
    //    }
    //    catch (const std::exception &e) {
    //    	ROS_ERROR("%s: %s", nh.getNamespace().c_str(), e.what());
    //    }


    try {
        pc_detection::Detector detector_node(nh,0);
        pc_detection::Publisher::initialize(nh);
        pc_detection::DynamicRcfg::initialize();
        pc_detection::DynamicRcfg::getInstance()->start_dynamic_reconfigure();

        printf("------------in detector_main.cpp------------\n");
        ros::spin();
    }
    catch (const std::exception &e) {
        ROS_ERROR("%s: %s", nh.getNamespace().c_str(), e.what());
    }
}
