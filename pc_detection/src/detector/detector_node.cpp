// 2021.07.30 ('d')dinara
//test-branch
//test squash
//test
#include "pc_detection/detector_node.h"

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>



namespace pc_detection {

Publisher* Publisher::instance = nullptr;
DynamicRcfg* DynamicRcfg::instance = nullptr;
//// ref.: https://stackoverflow.com/questions/2145331/c-undefined-reference-to-instance-in-singleton-class

Publisher::Publisher(const ros::NodeHandle &nh)
    : nh_(nh),
    pub_pc_floor_(nh_.advertise<sensor_msgs::PointCloud2>("floor", 1, true)),
    pub_pc_wall1_(nh_.advertise<sensor_msgs::PointCloud2>("wall_1", 1, true)),
    pub_pc_wall2_(nh_.advertise<sensor_msgs::PointCloud2>("wall_2", 1, true)),
    pub_pc_obstacle_(nh_.advertise<sensor_msgs::PointCloud2>("obstacle", 1, true)),
    pub_intersection_point_marker_(nh_.advertise<visualization_msgs::Marker>("intersection_point", 1, true)),
    pub_poi_(nh_.advertise<visualization_msgs::Marker>("poi", 1, true)),
    pub_marker_poi_(nh_.advertise<visualization_msgs::Marker>("marker_poi", 1, true)),

    pub_pc_merged_(nh_.advertise<sensor_msgs::PointCloud2>("icp_merged_pointcloud", 1, true)),
    pub_inliers_(nh_.advertise<sensor_msgs::PointCloud2>("other_except_floor", 1, true)),
    pub_pc_cornerframe_(nh_.advertise<sensor_msgs::PointCloud2>("pc_corner_frame", 1, true))
{}

DynamicRcfg::DynamicRcfg()
{}

ServiceRcv::ServiceRcv(const ros::NodeHandle &nh)
    : nh_(nh),
    service_(nh_.advertiseService("get_intersection_point_and_obstacle", &ServiceRcv::get_intersection_point_and_obstacle, this))
{}

Detector::Detector(const ros::NodeHandle &nh, int srv_poi_type)
    : nh_(nh),
    sub_actiondata_(nh_.subscribe("/action_data_pub", 1, &Detector::PointCloudCb, this)),
    tfListener(tf_),
    poi_type(srv_poi_type)
{}


//separate function due to singleton nature of DynamicRcfg class
void DynamicRcfg::start_dynamic_reconfigure(){
    dyn_rec_server_.setCallback(boost::bind(&DynamicRcfg::dynamic_reconf_callback, this, _1, _2));
}

Detector::PointCloudType::Ptr Detector::getInputCropbox(const PointCloudType::ConstPtr& pc_in_baselink, const geometry_msgs::PointStamped& marker_midpoint) {

    //	    std::cout << "Original input size: " << pc_in_baselink->points.size() << std::endl;
    PointCloudType::Ptr cropped_cloud = PointCloudType::Ptr(new PointCloudType());


    pcl::CropBox<pcl::PointXYZ> crop_box;
    crop_box.setMin(Eigen::Vector4f(marker_midpoint.point.x - DynamicRcfg::getInstance()->input_x_min, marker_midpoint.point.y - DynamicRcfg::getInstance()->input_y_min, -3.0, 1));
    crop_box.setMax(Eigen::Vector4f(marker_midpoint.point.x + DynamicRcfg::getInstance()->input_x_max, marker_midpoint.point.y + DynamicRcfg::getInstance()->input_y_max,  RANGE_MAX, 1));
    crop_box.setInputCloud(pc_in_baselink);
    crop_box.filter(*cropped_cloud);


    //	    std::cout << "Cropped input size: " << cropped_cloud->points.size() << std::endl;
    return cropped_cloud;
}


Detector::PointCloudType::Ptr Detector::getICP(const std::vector<sensor_msgs::PointCloud2>& list_of_pointclouds_baselink, const geometry_msgs::PointStamped& marker_midpoint, double voxel_size){


    PointCloudType::Ptr target = PointCloudType::Ptr(new PointCloudType());
    int idx = 0;
    int pc_counter = 0;

    int sampling_size = DynamicRcfg::getInstance()->pc_sampling;
    std::cout << "sampling size : " << sampling_size << std::endl << std::endl;

    for (const sensor_msgs::PointCloud2& pointcloud_rosmsg : list_of_pointclouds_baselink) {

        PointCloudType::Ptr source = PointCloudType::Ptr(new PointCloudType());


        //			std::cout<< "pointcloud_rosmsg.header.frame_id : "<< pointcloud_rosmsg.header.frame_id << std::endl;
        //			std::cout<< "pointcloud_rosmsg.header.stamp : "<< pointcloud_rosmsg.header.stamp << std::endl;
        // std::cout<< "idx: " << idx << std::endl;

        merged_pointcloud_frameid = pointcloud_rosmsg.header.frame_id;
        fromROSMsg(pointcloud_rosmsg, *source);


        source = getInputCropbox(source, marker_midpoint);
        //std::cout << "source size:"<< source->points.size() <<std::endl;


        if (idx==0) {
            //set first icp target
            //std::cout<< "idx: " << idx << std::endl;

            target = source;
        }


        if (idx%sampling_size==0) {
            pc_counter+=1;
            std::cout<< "idx: " << idx << std::endl;

            pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;

            // int iterations = 10;
            // icp.setMaximumIterations (iterations);

            icp.setMaxCorrespondenceDistance (voxel_size);
            //std::cout << "check.\n";

            auto atime = std::chrono::system_clock::now();;
            icp.setInputSource (source);
            //std::cout << "check.\n";

            icp.setInputTarget (target);
            //std::cout << "check.\n";

            icp.align (*source); //
            //std::cout << "check.\n";

            auto atime_end = std::chrono::system_clock::now();
            std::cout << "icp align time " << std::chrono::duration_cast<std::chrono::milliseconds>(atime_end - atime).count() << "ms.\n";

            *target += *source;

            PointCloudType::Ptr target_voxel = PointCloudType::Ptr(new PointCloudType());

            auto start_time = std::chrono::steady_clock::now();
            // Create the filtering object
            pcl::VoxelGrid<pcl::PointXYZ> sor;
            sor.setLeafSize (voxel_size, voxel_size, voxel_size);
            sor.setInputCloud (target);
            sor.filter (*target_voxel);
            auto end_time = std::chrono::steady_clock::now();
            std::cout << "icp filter time " << std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count() << "ms.\n";

            target = target_voxel;
        }

        idx+=1;
    }

    std::cout << std::endl;
    std::cout << "pc_counter: " << pc_counter << std::endl;
    return target;
}


void Detector::PointCloudCb(const pc_detection::ActionData& actiondata_msg) {


    ROS_INFO("in c++ Detector::PointCloudCb ");
    auto pc_start_time = std::chrono::steady_clock::now();

    std::cout << actiondata_msg.list_of_pointclouds_baselink.size() << std::endl;

    // checking no icp merged (original data before voxelization)
//    PointCloudType::Ptr no_icp_merged = PointCloudType::Ptr(new PointCloudType());
//    for (const sensor_msgs::PointCloud2& pointcloud_rosmsg : actiondata_msg.list_of_pointclouds_baselink) {
//        PointCloudType::Ptr no_icp_p = PointCloudType::Ptr(new PointCloudType());
//        fromROSMsg(pointcloud_rosmsg, *no_icp_p);
//        *no_icp_merged += *no_icp_p;
//        merged_pointcloud_frameid = pointcloud_rosmsg.header.frame_id;
//
//    }

    poi_type = actiondata_msg.poi_type;
    //      poi_type = 4; //inner corner x-side

    double voxel_size;
    if (poi_type == pc_detection::GetObstacle::Request::POI_WALL) {
        voxel_size = DynamicRcfg::getInstance()->wall_voxel_size;
    }
    else {
        voxel_size = DynamicRcfg::getInstance()->voxel_size;
    }

    PointCloudType::Ptr target = PointCloudType::Ptr(new PointCloudType());
    auto start_time = std::chrono::steady_clock::now();

    target = getICP(actiondata_msg.list_of_pointclouds_baselink, actiondata_msg.marker_midpoint, voxel_size);


    auto end_time = std::chrono::steady_clock::now();
    std::cout << "getICP time " << std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count() << "ms." << std::endl;

    PointCloudType::Ptr voxel_merged_pointcloud = PointCloudType::Ptr(new PointCloudType());
    // Create the filtering object
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    sor.setInputCloud (target);
    sor.setLeafSize (DynamicRcfg::getInstance()->voxel_size, DynamicRcfg::getInstance()->voxel_size, DynamicRcfg::getInstance()->voxel_size);
    sor.filter (*voxel_merged_pointcloud);

    PointCloudType::Ptr merged_pointcloud = PointCloudType::Ptr(new PointCloudType());
    merged_pointcloud = voxel_merged_pointcloud;
    // writer.write<pcl::PointXYZ> ("/home/d/Documents/mergedcorner.pcd", *merged_pointcloud, false);

    sensor_msgs::PointCloud2 merged_pointcloud_rosmsg;
    pcl::toROSMsg(*merged_pointcloud, merged_pointcloud_rosmsg);
    merged_pointcloud_rosmsg.header.frame_id = merged_pointcloud_frameid;
    merged_pointcloud_rosmsg.header.stamp = ros::Time::now();
    ROS_DEBUG_STREAM("merged_pointcloud_rosmsg.header: \n" << merged_pointcloud_rosmsg.header);
    ROS_DEBUG("DEBUG TEST ");

    //		pub_pc_merged_.publish(merged_pointcloud_rosmsg);
    Publisher::getInstance()->pub_pc_merged_.publish(merged_pointcloud_rosmsg);



    //poi_point handling
    pcl::PointXYZ user_poi_point(actiondata_msg.poi_baselink.point.x, actiondata_msg.poi_baselink.point.y, actiondata_msg.poi_baselink.point.z);
    pcl::PointXYZ poi_point(actiondata_msg.marker_midpoint.point.x, actiondata_msg.marker_midpoint.point.y, 0.0); //xy plane - topview



    std::cout<<"------------------ marker poi point : " << poi_point << std::endl;
    publishPOIs(user_poi_point, poi_point);



    bool int_point = getIntersectionPoint(merged_pointcloud, poi_point);
    std::cout<< "int_point : "<< int_point << std::endl;

    if (int_point == true){
        bool found_obstacle = getObstacleCropbox(no_planars_cloud); //segm
        if (found_obstacle == true)
            std::cout << "found_obstacle : " << std::boolalpha << true << std::endl;
        else
            std::cout << "found_obstacle : " << std::boolalpha << false << std::endl;
    }
    else {
        bool found_obstacle = false;
        std::cout << "found_obstacle : " << std::boolalpha << false << std::endl;
    }


    if (floor_plane_detected == true){
        ROS_INFO("floor_plane_detected : %s", true ? "true" : "false" );
    }
    else{
        ROS_INFO("floor_plane_detected : %s", false ? "true" : "false" );

    }

    if (wall_planes_detected == true){
        ROS_INFO("wall_planes_detected : %s", true ? "true" : "false" );
    }
    else{
        ROS_INFO("wall_planes_detected : %s", false ? "true" : "false" );
    }

    if (poi_type_correct == true){
        ROS_INFO("poi_type_correct : %s", true ? "true" : "false" );
    }
    else{
        ROS_INFO("poi_type_correct : %s", false ? "true" : "false" );
    }


    auto pc_end_time = std::chrono::steady_clock::now();
    std::cout << "pc processing time " << std::chrono::duration_cast<std::chrono::milliseconds>(pc_end_time - pc_start_time).count() << "ms.\n";

    ROS_INFO("finish Detector::PointCloudCb");
}

bool ServiceRcv::get_intersection_point_and_obstacle(pc_detection::GetObstacle::Request& req,
                                                    pc_detection::GetObstacle::Response& res)
{
    pc_detection::Detector detector_node(nh_, req.poi_type);

    ROS_INFO("in c++ Detector::get_intersection_point_and_obstacle");
    auto pc_start_time = std::chrono::steady_clock::now();

    std::cout << req.list_of_pointclouds_baselink.size() << std::endl;
    Detector::PointCloudType::Ptr target = Detector::PointCloudType::Ptr(new Detector::PointCloudType());

    auto start_time = std::chrono::steady_clock::now();
    target = detector_node.getICP(req.list_of_pointclouds_baselink, req.marker_midpoint, 0.0);

    auto end_time = std::chrono::steady_clock::now();
    std::cout << "getICP time " << std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count() << "ms." << std::endl;

    Detector::PointCloudType::Ptr voxel_merged_pointcloud = Detector::PointCloudType::Ptr(new Detector::PointCloudType());
    // Create the filtering object
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    sor.setInputCloud (target);
    sor.setLeafSize (DynamicRcfg::getInstance()->voxel_size, DynamicRcfg::getInstance()->voxel_size, DynamicRcfg::getInstance()->voxel_size);
    sor.filter (*voxel_merged_pointcloud);

    Detector::PointCloudType::Ptr merged_pointcloud = Detector::PointCloudType::Ptr(new Detector::PointCloudType());
    merged_pointcloud = voxel_merged_pointcloud;

    sensor_msgs::PointCloud2 merged_pointcloud_rosmsg;
    pcl::toROSMsg(*merged_pointcloud, merged_pointcloud_rosmsg);
    merged_pointcloud_rosmsg.header.frame_id = detector_node.merged_pointcloud_frameid;
    merged_pointcloud_rosmsg.header.stamp = ros::Time::now();
    std::cout<< "merged_pointcloud_rosmsg.header.frame_id : "<< merged_pointcloud_rosmsg.header.frame_id << std::endl;
    std::cout<< "merged_pointcloud_rosmsg.header.stamp : "<< merged_pointcloud_rosmsg.header.stamp << std::endl;

    //			detector_node.pub_pc_merged_.publish(merged_pointcloud_rosmsg);
    Publisher::getInstance()->pub_pc_merged_.publish(merged_pointcloud_rosmsg);



    //poi_point handling
    pcl::PointXYZ user_poi_point(req.poi_baselink.point.x, req.poi_baselink.point.y, req.poi_baselink.point.z);
    pcl::PointXYZ poi_point(req.marker_midpoint.point.x, req.marker_midpoint.point.y, 0.0); //xy plane - topview

    std::cout<<"------------------ marker poi point : " << poi_point << std::endl;
    detector_node.publishPOIs(user_poi_point, poi_point);



    bool int_point = detector_node.getIntersectionPoint(merged_pointcloud, poi_point);
    std::cout<< "int_point : "<< int_point << std::endl;

    if (int_point == true){
        res.found_obstacle = detector_node.getObstacleCropbox(detector_node.no_planars_cloud); //segm
        res.intersectionp_detected = true;
        std::cout << "intersectionp_detected : " << std::boolalpha << true << std::endl;

        if (res.found_obstacle == true)
            std::cout << "res.found_obstacle : " << std::boolalpha << true << std::endl;
        else
            std::cout << "res.found_obstacle : " << std::boolalpha << false << std::endl;
    }
    else {
        res.found_obstacle = false;
        std::cout << "res.found_obstacle : " << std::boolalpha << false << std::endl;
        res.intersectionp_detected = false;
        std::cout << "intersectionp_detected : " << std::boolalpha << false << std::endl;
    }


    if (detector_node.floor_plane_detected == true){
        res.floor_plane_detected = true;
        std::cout << "floor_plane_detected : " << std::boolalpha << true << std::endl;
    }
    else{
        res.floor_plane_detected = false;
        std::cout << "floor_plane_detected : " << std::boolalpha << false << std::endl;
    }

    if (detector_node.wall_planes_detected == true){
        res.wall_planes_detected = true;
        std::cout << "wall_planes_detected : " << std::boolalpha << true << std::endl;
    }
    else{
        res.wall_planes_detected = false;
        std::cout << "wall_planes_detected : " << std::boolalpha << false << std::endl;
    }

    if (detector_node.poi_type_correct == true){
        res.poi_type_correct = true;
        std::cout << "poi_type_correct : " << std::boolalpha << true << std::endl;
    }
    else{
        res.poi_type_correct = false;
        std::cout << "poi_type_correct : " << std::boolalpha << false << std::endl;
    }

    auto pc_end_time = std::chrono::steady_clock::now();
    std::cout << "pc processing time " << std::chrono::duration_cast<std::chrono::milliseconds>(pc_end_time - pc_start_time).count() << "ms.\n";




    ROS_INFO("finish Detector::get_intersection_point_and_obstacle");

    return true;
}




void Detector::publishPOIs(const pcl::PointXYZ& user_poi_point, const pcl::PointXYZ& poi_point) {
    visualization_msgs::Marker user_poi;
    user_poi.header.frame_id = "base_link";
    user_poi.header.stamp = ros::Time();
    user_poi.type = visualization_msgs::Marker::POINTS;
    user_poi.action = visualization_msgs::Marker::ADD;
    user_poi.scale.x = 0.2;
    user_poi.scale.y = 0.2;
    user_poi.scale.z = 0.2;
    user_poi.color.a = 1.0;
    user_poi.color.r = 1.0;
    user_poi.color.g = 0.0;
    user_poi.color.b = 0.0;

    geometry_msgs::Point p_poi;
    p_poi.x = user_poi_point.x;
    p_poi.y = user_poi_point.y;
    p_poi.z = user_poi_point.z;

    std_msgs::ColorRGBA c_poi;
    c_poi.r = 1.0;
    c_poi.g = 0.0;
    c_poi.b = 0.0;
    c_poi.a = 1.0;

    user_poi.points.push_back(p_poi);
    user_poi.colors.push_back(c_poi);
    //		pub_poi_.publish(user_poi);
    Publisher::getInstance()->pub_poi_.publish(user_poi);

    visualization_msgs::Marker marker_poi;
    marker_poi.header.frame_id = "base_link";
    marker_poi.header.stamp = ros::Time();
    marker_poi.type = visualization_msgs::Marker::POINTS;
    marker_poi.action = visualization_msgs::Marker::ADD;
    marker_poi.scale.x = 0.2;
    marker_poi.scale.y = 0.2;
    marker_poi.scale.z = 0.2;
    marker_poi.color.a = 1.0;
    marker_poi.color.r = 0.0;
    marker_poi.color.g = 1.0;
    marker_poi.color.b = 0.0;

    geometry_msgs::Point p_mpoi;
    p_mpoi.x = poi_point.x;
    p_mpoi.y = poi_point.y;
    p_mpoi.z = poi_point.z;

    std_msgs::ColorRGBA c_mpoi;
    c_mpoi.r = 0.0;
    c_mpoi.g = 1.0;
    c_mpoi.b = 0.0;
    c_mpoi.a = 1.0;

    marker_poi.points.push_back(p_mpoi);
    marker_poi.colors.push_back(c_mpoi);
    //		 pub_marker_poi_.publish(marker_poi);
    Publisher::getInstance()->pub_marker_poi_.publish(marker_poi);

}


bool Detector::getIntersectionPoint(const PointCloudType::Ptr& pc_in_baselink, const pcl::PointXYZ& poi_point) {

    // Planar segmentation for each plane wall1, wall2, floor
    std::cout << "PointCloud representing the pc_in_baselink component: " << pc_in_baselink->points.size () << " data points." << std::endl;
    bool intersection_result = false;
    bool ans = false;


    //Floor
    PointCloudType::Ptr segm = PointCloudType::Ptr(new PointCloudType());
    segm = getFloorPlane(pc_in_baselink);
    PointCloudType::Ptr floor_removed_cloud = PointCloudType::Ptr(new PointCloudType());
    *floor_removed_cloud = *segm;
//    floor_removed_cloud = segm; // error - [pcl::%s::applyFilter] The indices size exceeds the size of the input. in removePlanars - extractIndices

    if (floor_plane_detected == false) {
        intersection_result = false;
        ROS_ERROR("Floor plane not found.");
        return intersection_result;
    }

    // 2 walls search
    getWalls(segm, poi_point);


    switch(poi_type) {
    case pc_detection::GetObstacle::Request::POI_CORNER : {

        std::cout << "CASE POI_TYPE_1" << std::endl << std::endl;
        //no planes was found
        if (inliers_wall1->indices.size() == 0 || inliers_wall2->indices.size() == 0) {
            intersection_result = false;
            wall_planes_detected = false;
            ROS_ERROR("Wall planes not found.");
            return intersection_result;
        }
        else {
            wall_planes_detected = true;
        }

        no_planars_cloud = removePlanars(floor_removed_cloud, poi_type);

        ans = pcl::threePlanesIntersection (plane_wall1, plane_wall2, plane_floor, intersection_point, 1e-3);
        std::cout << "intersecting --> : " << std::noboolalpha << ans << std::endl;
        // intersection_point is translation part of transform matrix (corner coord. to baselink)
        std::cout << "intersection_point --> : " << intersection_point << std::endl;
        break;
    }

    case pc_detection::GetObstacle::Request::POI_CORNER_X_SIDE : {
        std::cout << "CASE POI_TYPE_2" << std::endl << std::endl;
        //no planes was found
        if (inliers_wall1->indices.size() == 0 || inliers_wall2->indices.size() == 0) {
            intersection_result = false;
            wall_planes_detected = false;
            ROS_ERROR("Wall planes not found.");
            return intersection_result;
        }
        else {
            wall_planes_detected = true;
        }

        no_planars_cloud = removePlanars(floor_removed_cloud, poi_type);

        ans = pcl::threePlanesIntersection (plane_wall1, plane_wall2, plane_floor, intersection_point, 1e-3);
        std::cout << "intersecting --> : " << std::noboolalpha << ans << std::endl;
        // intersection_point is translation part of transform matrix (corner coord. to baselink)
        std::cout << "intersection_point --> : " << intersection_point << std::endl;
        break;
    }

    case pc_detection::GetObstacle::Request::POI_CORNER_Y_SIDE : {
        std::cout << "CASE POI_TYPE_3" << std::endl << std::endl;
        //no planes was found
        if (inliers_wall1->indices.size() == 0 || inliers_wall2->indices.size() == 0) {
            intersection_result = false;
            wall_planes_detected = false;
            ROS_ERROR("Wall planes not found.");
            return intersection_result;
        }
        else {
            wall_planes_detected = true;
        }

        no_planars_cloud = removePlanars(floor_removed_cloud, poi_type);

        ans = pcl::threePlanesIntersection (plane_wall1, plane_wall2, plane_floor, intersection_point, 1e-3);
        std::cout << "intersecting --> : " << std::noboolalpha << ans << std::endl;
        // intersection_point is translation part of transform matrix (corner coord. to baselink)
        std::cout << "intersection_point --> : " << intersection_point << std::endl;
        break;
    }

    case pc_detection::GetObstacle::Request::POI_WALL : {

        //check floor unit vector and set the correct one
        Eigen::Vector3d floor_coeffs(coefficients_floor->values[0], coefficients_floor->values[1], coefficients_floor->values[2]);
        double floor_coeff_3;
        if (floor_coeffs[2] < 0) {
            floor_coeffs = Eigen::Vector3d(-floor_coeffs[0], -floor_coeffs[1], -floor_coeffs[2]);
            floor_coeff_3 = -coefficients_floor->values[3];
        }
        else {
            floor_coeffs = Eigen::Vector3d(floor_coeffs[0], floor_coeffs[1], floor_coeffs[2]);
            floor_coeff_3 =  coefficients_floor->values[3];
        }

        Eigen::Vector3d wall_coeffs(coefficients_wall1->values[0], coefficients_wall1->values[1], coefficients_wall1->values[2]);
        //Here, wall unit vector direction doesn't matter
        //even if resulting dir_vector's dir. is opposite(- all vals), final result is ok
        Eigen::Vector3d dir_vector = floor_coeffs.cross(wall_coeffs);

        std::cout << "floor_coeffs --> : " << floor_coeffs << std::endl;
        std::cout << "wall_coeffs --> : " << wall_coeffs << std::endl;

        std::cout << "dir_vector --> : " << dir_vector << std::endl;

        double a1, b1, c1, d1, a2,b2, c2, d2;
        double tempy, tempz;
        Eigen::Vector3d point_a, temp_sum, marker_poi_point;
        std::cout << "CASE POI_TYPE_4: Single wall case" << std::endl << std::endl;
        ROS_DEBUG_STREAM("intersection_point --> : \n" << intersection_point);

        if (inliers_wall1->indices.size() == 0) {
            intersection_result = false;
            wall_planes_detected = false;
            ROS_ERROR("POI_TYPE_4: No wall not found.");
            return intersection_result;
        }
        else {
            wall_planes_detected = true;
        }

        no_planars_cloud = removePlanars(floor_removed_cloud, poi_type);


        // Find intersection point between intersection_line and perpendicular marker_poi_point
        // https://www.programmersought.com/article/17085081834/

        a1= floor_coeffs[0];
        b1= floor_coeffs[1];
        c1= floor_coeffs[2];
        d1= floor_coeff_3;
        a2= coefficients_wall1->values[0];
        b2= coefficients_wall1->values[1];
        c2= coefficients_wall1->values[2];
        d2= coefficients_wall1->values[3];


//        a1= coefficients_floor->values[0];
//        b1= coefficients_floor->values[1];
//        c1= coefficients_floor->values[2];
//        d1= coefficients_floor->values[3];
//        a2= coefficients_wall1->values[0];
//        b2= coefficients_wall1->values[1];
//        c2= coefficients_wall1->values[2];
//        d2= coefficients_wall1->values[3];

        if (b1 == 0 || b2 == 0) {
            intersection_result = false;
            return intersection_result;
        }

        tempz= -(d1 / b1 - d2 / b2) / (c1 / b1 - c2 / b2);
        tempy= (-c1 / b1)*tempz - d1 / b1;
        // https://stackoverflow.com/questions/5227373/minimal-perpendicular-vector-between-a-point-and-a-line
        // X == A + ((P-A).D)D
        // P = marker_poi_point
        // A = x, tempy, tempz
        // D = dir_vector

        marker_poi_point = Eigen::Vector3d(poi_point.x, poi_point.y, poi_point.z);
        point_a = Eigen::Vector3d(0.0, tempy, tempz);
        std::cout << "point_a --> : " << point_a << std::endl;

        temp_sum = ((marker_poi_point - point_a).dot(dir_vector))*dir_vector;
        intersection_point = point_a + temp_sum;
        ans = 1;
        std::cout << "intersecting --> : " << std::noboolalpha << ans << std::endl;

        // intersection_point is translation part of transform matrix (wall coord. to baselink)
        std::cout << "intersection_point --> : " << intersection_point << std::endl;
        std::cout << "marker_midpoint --> : " << poi_point << std::endl;
        break;
    }

    default : {
        ROS_ERROR("POI_TYPE not correctly given");
        intersection_result = false;
        poi_type_correct = false;
        return intersection_result;
    }
    }


    //
    if (ans == 1)
    {
        intersection_result = true;
    }
    else intersection_result = false;
    std::cout << "intersection_point --> : " << intersection_point << std::endl;

    visualization_msgs::Marker marker;
    marker.header.frame_id = "base_link";
    marker.header.stamp = ros::Time();
    marker.type = visualization_msgs::Marker::POINTS;
    marker.action = visualization_msgs::Marker::ADD;
    marker.scale.x = 0.1;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color.a = 1.0;
    marker.color.r = 0.0;
    marker.color.g = 1.0;
    marker.color.b = 0.0;

    geometry_msgs::Point p;

    p.x = intersection_point[0];
    p.y = intersection_point[1];
    p.z = intersection_point[2];

    std_msgs::ColorRGBA c;
    c.r = 0.0;
    c.g = 0.0;
    c.b = 1.0;
    c.a = 1.0;

    marker.points.push_back(p);
    marker.colors.push_back(c);


    //		pub_intersection_point_marker_.publish(marker);
    Publisher::getInstance()->pub_intersection_point_marker_.publish(marker);

    //Publish the new cloud
    sensor_msgs::PointCloud2 segmented_output_wall1;
    pcl::toROSMsg(cloud_segmented_wall1, segmented_output_wall1);
    segmented_output_wall1.header.frame_id = merged_pointcloud_frameid;
    segmented_output_wall1.header.stamp = ros::Time::now();

    //		std::cout<< "segmented_output_wall1.header.frame_id : "<< segmented_output_wall1.header.frame_id << std::endl;
    //		std::cout<< "segmented_output_wall1.header.stamp : "<< segmented_output_wall1.header.stamp << std::endl;
    //        pub_pc_wall1_.publish(segmented_output_wall1);
    Publisher::getInstance()->pub_pc_wall1_.publish(segmented_output_wall1);




    sensor_msgs::PointCloud2 segmented_output_wall2;
    pcl::toROSMsg(cloud_segmented_wall2, segmented_output_wall2);
    segmented_output_wall2.header.frame_id = merged_pointcloud_frameid;
    segmented_output_wall2.header.stamp = ros::Time::now();

    //		std::cout<< "segmented_output_wall2.header.frame_id : "<< segmented_output_wall2.header.frame_id << std::endl;
    //		std::cout<< "segmented_output_wall2.header.stamp : "<< segmented_output_wall2.header.stamp << std::endl;
    //        pub_pc_wall2_.publish(segmented_output_wall2);
    Publisher::getInstance()->pub_pc_wall2_.publish(segmented_output_wall2);




    sensor_msgs::PointCloud2 segmented_output_floor;
    pcl::toROSMsg(cloud_segmented_floor, segmented_output_floor);
    segmented_output_floor.header.frame_id = merged_pointcloud_frameid;
    segmented_output_floor.header.stamp = ros::Time::now();

    //		std::cout<< "segmented_output_floor.header.frame_id : "<< segmented_output_floor.header.frame_id << std::endl;
    //		std::cout<< "segmented_output_floor.header.stamp : "<< segmented_output_floor.header.stamp << std::endl;
    //		pub_pc_floor_.publish(segmented_output_floor);
    Publisher::getInstance()->pub_pc_floor_.publish(segmented_output_floor);

    return intersection_result;
}





Detector::PointCloudType::Ptr Detector::getTransformedObstacleCloud (const Eigen::Vector3d& intersection_point,
        const PointCloudType::Ptr& obstacle_in_baselink) {



    Eigen::Vector3d floor(coefficients_floor->values[0], coefficients_floor->values[1], coefficients_floor->values[2]);
    Eigen::Vector3d wall1(coefficients_wall1->values[0], coefficients_wall1->values[1], coefficients_wall1->values[2]);
    Eigen::Vector3d wall2(coefficients_wall2->values[0], coefficients_wall2->values[1], coefficients_wall2->values[2]);
    Eigen::Vector3d n_xy;

    if (floor[2] < 0) { //make sure floor signs are always pos., i.e. upper facing vector
        n_xy = Eigen::Vector3d(-floor[0], -floor[1], -floor[2]);
    }
    else {
        n_xy = Eigen::Vector3d(floor[0], floor[1], floor[2]);
    }

    //Determine corner frame's coordinate system
    //4 unit vectors centered at intersection point
    Eigen::Vector3d n_yz(wall1[0], wall1[1], wall1[2]);
    Eigen::Vector3d neg_n_yz(-wall1[0], -wall1[1], -wall1[2]);
    Eigen::Vector3d n_xz(wall2[0], wall2[1], wall2[2]);
    Eigen::Vector3d neg_n_xz(-wall2[0], -wall2[1], -wall2[2]);


    std::vector<Eigen::Vector3d> vectors;
    vectors.push_back(n_yz);
    vectors.push_back(neg_n_yz);
    vectors.push_back(n_xz);
    vectors.push_back(neg_n_xz);

    std::cout << "n_yz :  " << n_yz << std::endl;
    std::cout << "neg_n_yz :  " << neg_n_yz << std::endl;
    std::cout << "n_xz :  " << n_yz << std::endl;
    std::cout << "neg_n_xz :  " << neg_n_xz << std::endl;


    Eigen::Vector3d baselink_dist(intersection_point[0], intersection_point[1], intersection_point[2]); //points inward corner neg.sign
    //This angle measurement will make sure angles are between 0 and pi (in rad)
    //https://liuzhiguang.wordpress.com/2017/06/12/find-the-angle-between-two-vectors/
    //So that, when you select max 2 angles, will be correct
    double angle1 =  std::atan2(n_yz.cross(baselink_dist).norm(), n_yz.dot(baselink_dist));
    double angle2 =  std::atan2(neg_n_yz.cross(baselink_dist).norm(), neg_n_yz.dot(baselink_dist));
    double angle3 =  std::atan2(n_xz.cross(baselink_dist).norm(), n_xz.dot(baselink_dist));
    double angle4 =  std::atan2(neg_n_xz.cross(baselink_dist).norm(), neg_n_xz.dot(baselink_dist));


    std::vector<double> angles;
    angles.push_back(angle1);
    angles.push_back(angle2);
    angles.push_back(angle3);
    angles.push_back(angle4);

    std::cout << "in radians "<<std::endl;
    std::cout << "angle1 :  " << angles[0] << std::endl;
    std::cout << "angle2 :  " << angles[1] << std::endl;
    std::cout << "angle3 :  " << angles[2] << std::endl;
    std::cout << "angle4 :  " << angles[3] << std::endl;

    std::cout << "in degrees "<<std::endl;
    std::cout << "angle1 :  " << angles[0]* 180/M_PI << std::endl;
    std::cout << "angle2 :  " << angles[1]* 180/M_PI << std::endl;
    std::cout << "angle3 :  " << angles[2]* 180/M_PI << std::endl;
    std::cout << "angle4 :  " << angles[3]* 180/M_PI << std::endl;

    std::vector<size_t> v_idxs(angles.size());
    std::iota(v_idxs.begin(), v_idxs.end(), 0);
    std::stable_sort(v_idxs.begin(), v_idxs.end(),
            [&angles](size_t i1, size_t i2) {return angles[i1] > angles[i2];});

    //2 main unit vectors x, y
    std::cout << "maxElement1:" << vectors[v_idxs[0]] << std::endl;
    std::cout << "angle_1:" << angles[v_idxs[0]] << std::endl;
    double angle_1 = angles[v_idxs[0]];

    std::cout << "maxElement2:" << vectors[v_idxs[1]] << std::endl;
    std::cout << "angle_2:" << angles[v_idxs[1]] << std::endl;
    double angle_2 = angles[v_idxs[1]];

    Eigen::Vector3d max1 = vectors[v_idxs[0]];
    Eigen::Vector3d max2 = vectors[v_idxs[1]];


    //no need this, ignore
    //https://www.mathworks.com/matlabcentral/answers/180131-how-can-i-find-the-angle-between-two-vectors-including-directional-information
    //counterclockwise, from base vector
    //a = atan2d(x1*y2-y1*x2,x1*x2+y1*y2);
    //    double a = std::atan2(baselink_dist[0]*max1[1]-baselink_dist[1]*max1[0], baselink_dist[0]*baselink_dist[1]+max1[0]*max1[1]);
    //    double b = std::atan2(baselink_dist[0]*max2[1]-baselink_dist[1]*max2[0], baselink_dist[0]*baselink_dist[1]+max2[0]*max2[1]);
    //    std::cout << "from base vector to a:" << a << std::endl;
    //    std::cout << "from base vector to b:" << b << std::endl;


    Eigen::Vector3d candidate_x;
    Eigen::Vector3d candidate_y;

    //Determine which is x, which is y
    //compare y-coord according to baselink
    std::cout << "max1[1] or n_yz[1]:" << max1[1] << std::endl;
//    std::cout << "n_xz[1]:" << n_xz[1] << std::endl;
    std::cout << "max2[1] or n_xz[1]:" << max2[1] << std::endl;

//Old way - checking y component of vector
//    if (max1[1] > 0){
//        candidate_x = max1;
//        candidate_y = max2;
//    }
//    else{
//        candidate_x = max2;
//        candidate_y = max1;
//    }

    //New method
    //First, find floor vector as a cross product of 2 selected max vectors (from 2 max angles above)
    Eigen::Vector3d candidate_f1;
    Eigen::Vector3d candidate_f2;
    candidate_f1 = max1.cross(max2);
    candidate_f2 = max2.cross(max1);

    std::cout << "candidate_f1 z: " << candidate_f1 << std::endl;
    std::cout << "candidate_f2 z: " << candidate_f2 << std::endl;

    if (candidate_f1[2]>0){ //if cross pr. of max1 and max2 facing upward (floor is always upward, as checked above)
        candidate_x = max1;
    }
    else {
        candidate_x = max2;
    }
    Eigen::Vector3d u_x;
    Eigen::Vector3d u_y;

    std::cout << "candidate_x: " << candidate_x << std::endl;
//    std::cout << "candidate_y: " << candidate_y << std::endl;

    //Then, find u_y from floor x candidate_x
    u_y = n_xy.cross(candidate_x);
    //Then, reverse to find u_x as floor x u_y and then negate (or, can do: u_x = u_y x floor, instead)
    u_x = n_xy.cross(u_y);
    u_x = Eigen::Vector3d(-u_x[0], -u_x[1], -u_x[2]);

    std::cout << "u_x: " << u_x << std::endl;
    std::cout << "u_y: " << u_y << std::endl;

    //when first u_y
//    u_y = n_xy.cross(candidate_x);
//    u_x = n_xy.cross(u_y);
//    u_x = Eigen::Vector3d(-u_x[0], -u_x[1], -u_x[2]);

//    candidate_x:  0.0129817
//      0.999621
//    -0.0242733
//    candidate_y:  -0.999752
//    -0.0164267
//    -0.0150446
//    u_x: 0.0131104
//     0.999682
//    -0.021322
//    u_y:  -0.99896
//    0.0140241
//    0.0432825

    //when first u_x
//    u_x = n_xy.cross(candidate_y);
//    u_x = Eigen::Vector3d(-u_x[0], -u_x[1], -u_x[2]);
//    u_y = n_xy.cross(u_x);

//    candidate_x:  0.0129817
//      0.999621
//    -0.0242733
//    candidate_y:  -0.999752
//    -0.0164267
//    -0.0150446
//    u_x: -0.0160956
//      0.997932
//    -0.0200118
//    u_y: -0.997185
//    -0.015205
//    0.0438118


    //Coordinate frame set
    //Calculate rotation part of transform matrix
    Eigen::Matrix3d rotation_matrix;
    rotation_matrix << 	u_x[0], u_y[0], n_xy[0],
                        u_x[1], u_y[1], n_xy[1],
                        u_x[2], u_y[2], n_xy[2];

//    std::cout << rotation_matrix <<std::endl;

    ROS_DEBUG_STREAM("rotation_matrix: \n" << rotation_matrix);

    Eigen::Quaterniond rot_q(rotation_matrix);
    std::cout << "rot_q.x():\n" << rot_q.x() << std::endl;
    std::cout << "rot_q.y():\n" << rot_q.y() << std::endl;
    std::cout << "rot_q.z():\n" << rot_q.z() << std::endl;
    std::cout << "rot_q.w():\n" << rot_q.w() << std::endl;

    Eigen::Translation3d trans(intersection_point[0], intersection_point[1], intersection_point[2]);
    Eigen::Isometry3d corner_to_baselink_tf  = Eigen::Isometry3d(trans * rot_q);



    std::cout << "corner_to_baselink_tf translation:\n" << corner_to_baselink_tf.translation() << std::endl;
    std::cout << "corner_to_baselink_tf rotation:\n" << corner_to_baselink_tf.rotation() << std::endl;


    Eigen::Isometry3d corner_to_baselink_tf_ = corner_to_baselink_tf.inverse();
    std::cout << "inverse translation:\n" << corner_to_baselink_tf_.translation() << std::endl;
    std::cout << "inverse rotation:\n" << corner_to_baselink_tf_.rotation() << std::endl;


    Eigen::Quaterniond q(corner_to_baselink_tf.rotation());
    std::cout << "q.x():\n" << q.x() << std::endl;
    std::cout << "q.y():\n" << q.y() << std::endl;
    std::cout << "q.z():\n" << q.z() << std::endl;
    std::cout << "q.w():\n" << q.w() << std::endl;



    // Transform publish
    geometry_msgs::TransformStamped transformStamped;
    transformStamped.header.stamp = ros::Time::now();
    transformStamped.header.frame_id = "base_link"; 	// corner_frame baselink to corner frame corner_to_baselink_tf_ = corner_to_baselink_tf.inverse()
    transformStamped.child_frame_id = "corner_frame";		// base_link (inverse of (corner to baselink = corner_to_baselink_tf))

    transformStamped.transform.translation.x = corner_to_baselink_tf.translation().x();
    transformStamped.transform.translation.y = corner_to_baselink_tf.translation().y();
    transformStamped.transform.translation.z = corner_to_baselink_tf.translation().z();

    transformStamped.transform.rotation.x = q.x();
    transformStamped.transform.rotation.y = q.y();
    transformStamped.transform.rotation.z = q.z();
    transformStamped.transform.rotation.w = q.w();


    std::cout << "transformStamped.transform.translation.x:\n" << transformStamped.transform.translation.x << std::endl;
    std::cout << "transformStamped.transform.translation.y:\n" << transformStamped.transform.translation.y << std::endl;
    std::cout << "transformStamped.transform.translation.z:\n" << transformStamped.transform.translation.z << std::endl;

    std::cout << "transformStamped.transform.rotation.x:\n" << transformStamped.transform.rotation.x << std::endl;
    std::cout << "transformStamped.transform.rotation.y:\n" << transformStamped.transform.rotation.y << std::endl;
    std::cout << "transformStamped.transform.rotation.z:\n" << transformStamped.transform.rotation.z << std::endl;
    std::cout << "transformStamped.transform.rotation.w:\n" << transformStamped.transform.rotation.w << std::endl;

    static_tf_broadcaster_.sendTransform(transformStamped);
    //



    PointCloudType::Ptr obstacle_in_corner_frame = PointCloudType::Ptr(new PointCloudType());
    for (int idx = 0; idx < obstacle_in_baselink->points.size(); idx++) {

        Eigen::Vector3d p_in_b(obstacle_in_baselink->points[idx].x, obstacle_in_baselink->points[idx].y, obstacle_in_baselink->points[idx].z);
        Eigen::Vector3d p_in_c = Eigen::Vector3d(corner_to_baselink_tf_ * p_in_b);



        pcl::PointXYZ p_in_c_cloud_point(p_in_c[0], p_in_c[1], p_in_c[2]);
        obstacle_in_corner_frame->push_back(p_in_c_cloud_point);
    }


    std::cout << "obstacle_in_baselink size: " << obstacle_in_baselink->points.size() << std::endl;
    std::cout << "obstacle_in_corner_frame size: " << obstacle_in_corner_frame->points.size() << std::endl;


    return obstacle_in_corner_frame;
}


Detector::PointCloudType::Ptr Detector::getSingleWallTransformedObstacleCloud (const Eigen::Vector3d& intersection_point,
        const PointCloudType::Ptr& obstacle_in_baselink) {


    Eigen::Vector3d floor(coefficients_floor->values[0], coefficients_floor->values[1], coefficients_floor->values[2]);
    Eigen::Vector3d wall1(coefficients_wall1->values[0], coefficients_wall1->values[1], coefficients_wall1->values[2]);
    Eigen::Vector3d n_xy;

    if (floor[2] < 0) {
        n_xy = Eigen::Vector3d(-floor[0], -floor[1], -floor[2]);
    }
    else {
        n_xy = Eigen::Vector3d(floor[0], floor[1], floor[2]);
    }

    //Determine wall frame's coordinate system
    //2 unit vectors centered at marker_poi_point point
    Eigen::Vector3d n_yz(wall1[0], wall1[1], wall1[2]);
    Eigen::Vector3d neg_n_yz(-wall1[0], -wall1[1], -wall1[2]);


    std::cout << n_yz <<std::endl;
    std::cout << neg_n_yz <<std::endl;


    Eigen::Vector3d baselink_dist(intersection_point[0], intersection_point[1], intersection_point[2]);
    std::cout << "baselink_dist: " << baselink_dist <<std::endl<<std::endl;

    //https://liuzhiguang.wordpress.com/2017/06/12/find-the-angle-between-two-vectors/
    double theta_yz =  std::atan2(n_yz.cross(baselink_dist).norm(), n_yz.dot(baselink_dist));
    double _theta_yz =  std::atan2(baselink_dist.cross(n_yz).norm(), baselink_dist.dot(n_yz));


    std::cout << "theta_yz: "<< theta_yz <<std::endl;
    std::cout << "_theta_yz: "<< _theta_yz <<std::endl;

    //ignore, just for check
    double theta_neg_yz =  std::atan2(neg_n_yz.cross(baselink_dist).norm(), neg_n_yz.dot(baselink_dist));
    std::cout << "theta_neg_yz: "<< theta_neg_yz <<std::endl;


    Eigen::Vector3d u_x;
    Eigen::Vector3d u_y;

////    old method
//    if (n_yz[0] < 0){ //x is opposite to baselink x
//        u_x = n_yz;
//    }
//    else{
//        u_x = neg_n_yz;
//    }
//    u_y = n_xy.cross(u_x);

    Eigen::Vector3d candidate_x;
    Eigen::Vector3d candidate_y;


    //the larger angle will be for correct wall unit vector ( facing inward to robot)
    if (theta_yz > theta_neg_yz){
        candidate_x = n_yz;
    }
    else{
        candidate_x = neg_n_yz;
    }
    //Then, find u_y from floor x candidate_x
    u_y = n_xy.cross(candidate_x);
    //Then, reverse to find u_x as floor x u_y and then negate (or, can do: u_x = u_y x floor, instead)
    u_x = n_xy.cross(u_y);
    u_x = Eigen::Vector3d(-u_x[0], -u_x[1], -u_x[2]);


    //Calculate rotation part of transform matrix
    Eigen::Matrix3d rotation_matrix;
    rotation_matrix << 	u_x[0], u_y[0], n_xy[0],
                        u_x[1], u_y[1], n_xy[1],
                        u_x[2], u_y[2], n_xy[2];

    std::cout << rotation_matrix <<std::endl;
    ROS_DEBUG_STREAM("rotation_matrix: \n" << rotation_matrix);

    Eigen::Quaterniond rot_q(rotation_matrix);
    std::cout << "rot_q.x():\n" << rot_q.x() << std::endl;
    std::cout << "rot_q.y():\n" << rot_q.y() << std::endl;
    std::cout << "rot_q.z():\n" << rot_q.z() << std::endl;
    std::cout << "rot_q.w():\n" << rot_q.w() << std::endl;


    Eigen::Translation3d trans(intersection_point[0], intersection_point[1], intersection_point[2]);
    Eigen::Isometry3d corner_to_baselink_tf  = Eigen::Isometry3d(trans * rot_q);



    std::cout << "corner_to_baselink_tf translation:\n" << corner_to_baselink_tf.translation() << std::endl;
    std::cout << "corner_to_baselink_tf rotation:\n" << corner_to_baselink_tf.rotation() << std::endl;


    Eigen::Isometry3d corner_to_baselink_tf_ = corner_to_baselink_tf.inverse();
    std::cout << "inverse translation:\n" << corner_to_baselink_tf_.translation() << std::endl;
    std::cout << "inverse rotation:\n" << corner_to_baselink_tf_.rotation() << std::endl;


    Eigen::Quaterniond q(corner_to_baselink_tf.rotation());
    std::cout << "q.x():\n" << q.x() << std::endl;
    std::cout << "q.y():\n" << q.y() << std::endl;
    std::cout << "q.z():\n" << q.z() << std::endl;
    std::cout << "q.w():\n" << q.w() << std::endl;



    // Transform publish
    geometry_msgs::TransformStamped transformStamped;
    transformStamped.header.stamp = ros::Time::now();
    transformStamped.header.frame_id = "base_link"; 	// corner_frame baselink to corner frame corner_to_baselink_tf_ = corner_to_baselink_tf.inverse()
    transformStamped.child_frame_id = "wall_frame";		// base_link (inverse of (corner to baselink = corner_to_baselink_tf))

    transformStamped.transform.translation.x = corner_to_baselink_tf.translation().x();
    transformStamped.transform.translation.y = corner_to_baselink_tf.translation().y();
    transformStamped.transform.translation.z = corner_to_baselink_tf.translation().z();

    transformStamped.transform.rotation.x = q.x();
    transformStamped.transform.rotation.y = q.y();
    transformStamped.transform.rotation.z = q.z();
    transformStamped.transform.rotation.w = q.w();


    std::cout << "transformStamped.transform.translation.x:\n" << transformStamped.transform.translation.x << std::endl;
    std::cout << "transformStamped.transform.translation.y:\n" << transformStamped.transform.translation.y << std::endl;
    std::cout << "transformStamped.transform.translation.z:\n" << transformStamped.transform.translation.z << std::endl;

    std::cout << "transformStamped.transform.rotation.x:\n" << transformStamped.transform.rotation.x << std::endl;
    std::cout << "transformStamped.transform.rotation.y:\n" << transformStamped.transform.rotation.y << std::endl;
    std::cout << "transformStamped.transform.rotation.z:\n" << transformStamped.transform.rotation.z << std::endl;
    std::cout << "transformStamped.transform.rotation.w:\n" << transformStamped.transform.rotation.w << std::endl;

    static_tf_broadcaster_.sendTransform(transformStamped);
    //



    PointCloudType::Ptr obstacle_in_corner_frame = PointCloudType::Ptr(new PointCloudType());
    for (int idx = 0; idx < obstacle_in_baselink->points.size(); idx++) {

        Eigen::Vector3d p_in_b(obstacle_in_baselink->points[idx].x, obstacle_in_baselink->points[idx].y, obstacle_in_baselink->points[idx].z);
        Eigen::Vector3d p_in_c = Eigen::Vector3d(corner_to_baselink_tf_ * p_in_b);



        pcl::PointXYZ p_in_c_cloud_point(p_in_c[0], p_in_c[1], p_in_c[2]);
        obstacle_in_corner_frame->push_back(p_in_c_cloud_point);
    }


    std::cout << "obstacle_in_baselink size: " << obstacle_in_baselink->points.size() << std::endl;
    std::cout << "obstacle_in_corner_frame size: " << obstacle_in_corner_frame->points.size() << std::endl;


    return obstacle_in_corner_frame;
}


bool Detector::getObstacleCropbox(const PointCloudType::Ptr& pc_in_baselink) {


    bool obstacle_result = false;
    std::cout << "segm size: " << pc_in_baselink->points.size() << std::endl;

    PointCloudType::Ptr pc_tc = PointCloudType::Ptr(new PointCloudType());
    PointCloudType::Ptr pc_in_corner_frame = PointCloudType::Ptr(new PointCloudType());

    if (poi_type == 4) //wall
    {
        pc_in_corner_frame = getSingleWallTransformedObstacleCloud(intersection_point, pc_in_baselink);
        sensor_msgs::PointCloud2 pc_in_wall_frame_cloud;
        pcl::toROSMsg(*pc_in_corner_frame, pc_in_wall_frame_cloud);
        pc_in_wall_frame_cloud.header.frame_id = "wall_frame";
        pc_in_wall_frame_cloud.header.stamp = ros::Time::now();
        //			pub_pc_cornerframe_.publish(pc_in_wall_frame_cloud);
        Publisher::getInstance()->pub_pc_cornerframe_.publish(pc_in_wall_frame_cloud);

    }
    else
    { // all corner cases
        pc_in_corner_frame = getTransformedObstacleCloud(intersection_point, pc_in_baselink);
        sensor_msgs::PointCloud2 pc_in_corner_frame_cloud;
        pcl::toROSMsg(*pc_in_corner_frame, pc_in_corner_frame_cloud);
        pc_in_corner_frame_cloud.header.frame_id = "corner_frame";
        pc_in_corner_frame_cloud.header.stamp = ros::Time::now();
        //			pub_pc_cornerframe_.publish(pc_in_corner_frame_cloud);
        Publisher::getInstance()->pub_pc_cornerframe_.publish(pc_in_corner_frame_cloud);

    }


    pcl::CropBox<pcl::PointXYZ> crop_box;

    switch(poi_type) {
    case pc_detection::GetObstacle::Request::POI_CORNER: {
        std::cout << "Cropping obstacle for POI_TYPE_1: Inner corner" << std::endl << std::endl;
        crop_box.setMin(Eigen::Vector4f(DynamicRcfg::getInstance()->output_x_min, DynamicRcfg::getInstance()->output_y_min, DynamicRcfg::getInstance()->output_z_min, 1));
        crop_box.setMax(Eigen::Vector4f(DynamicRcfg::getInstance()->output_x_max, DynamicRcfg::getInstance()->output_y_max, DynamicRcfg::getInstance()->output_z_max, 1));
        break;
    }
    case pc_detection::GetObstacle::Request::POI_CORNER_X_SIDE: {
        std::cout << "Cropping obstacle for POI_TYPE_2: Inner corner - x side" << std::endl << std::endl;
        crop_box.setMin(Eigen::Vector4f(DynamicRcfg::getInstance()->output_x_min_poitype2, DynamicRcfg::getInstance()->output_y_min_poitype2, DynamicRcfg::getInstance()->output_z_min_poitype2, 1));
        crop_box.setMax(Eigen::Vector4f(DynamicRcfg::getInstance()->output_x_max_poitype2, DynamicRcfg::getInstance()->output_y_max_poitype2, DynamicRcfg::getInstance()->output_z_max_poitype2, 1));
        break;
    }
    case pc_detection::GetObstacle::Request::POI_CORNER_Y_SIDE: {
        std::cout << "Cropping obstacle for POI_TYPE_3: Inner corner - y side" << std::endl << std::endl;
        crop_box.setMin(Eigen::Vector4f(DynamicRcfg::getInstance()->output_x_min_poitype3, DynamicRcfg::getInstance()->output_y_min_poitype3, DynamicRcfg::getInstance()->output_z_min_poitype3, 1));
        crop_box.setMax(Eigen::Vector4f(DynamicRcfg::getInstance()->output_x_max_poitype3, DynamicRcfg::getInstance()->output_y_max_poitype3, DynamicRcfg::getInstance()->output_z_max_poitype3, 1));
        break;
    }
    case pc_detection::GetObstacle::Request::POI_WALL: {
        std::cout << "Cropping obstacle for POI_TYPE_4: Single wall" << std::endl << std::endl;
        crop_box.setMin(Eigen::Vector4f(DynamicRcfg::getInstance()->output_x_min_wall, DynamicRcfg::getInstance()->output_y_min_wall, DynamicRcfg::getInstance()->output_z_min_wall, 1));
        crop_box.setMax(Eigen::Vector4f(DynamicRcfg::getInstance()->output_x_max_wall, DynamicRcfg::getInstance()->output_y_max_wall, DynamicRcfg::getInstance()->output_z_max_wall, 1));
        break;
    }
    default : {
        ROS_ERROR("POI_TYPE not correctly given");
        obstacle_result = false;
        poi_type_correct = false;
        return obstacle_result;
    }
    }
    crop_box.setInputCloud(pc_in_corner_frame);
    crop_box.filter(*pc_tc);



    std::cout<< "pc_tc->size() : "<< pc_tc->size() << std::endl;
    std::cout << "PointCloud representing the crop_box obstacle component: " << pc_tc->points.size () << " data points." << std::endl;

    if(pc_tc->size() > DynamicRcfg::getInstance()->obstacle_size){
        obstacle_result = true;
        sensor_msgs::PointCloud2 left_cloud;
        pcl::toROSMsg(*pc_tc, left_cloud);
        if (poi_type == 4)
            left_cloud.header.frame_id = "wall_frame";
        else
            left_cloud.header.frame_id = "corner_frame";
        left_cloud.header.stamp = ros::Time::now();

        std::cout<< "left_cloud.header.frame_id : "<< left_cloud.header.frame_id << std::endl;
        std::cout<< "left_cloud.header.stamp : "<< left_cloud.header.stamp << std::endl;
        //			pub_pc_obstacle_.publish(left_cloud);
        Publisher::getInstance()->pub_pc_obstacle_.publish(left_cloud);

    }
    else {
        std::cout<< "obstacle_result false" << std::endl;
        obstacle_result = false;
    }


    return obstacle_result;
}




Detector::PointCloudType::Ptr Detector::getFloorPlane(const PointCloudType::Ptr& pc_in_baselink){

    std::cout << "PointCloud representing the pc_in_baselink component: " << pc_in_baselink->points.size () << " data points." << std::endl;
    PointCloudType::Ptr segm = PointCloudType::Ptr(new PointCloudType());

    // Floor
    // do passthrough once before looking for perpendicular plane


    pcl::PointIndices::Ptr inliers_floor (new pcl::PointIndices);
    pcl::PointIndices::Ptr inliers_all_except_floor (new pcl::PointIndices);

    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg_floor;
    // Optional
    seg_floor.setOptimizeCoefficients(true);
    Eigen::Vector3f axis = Eigen::Vector3f(0.0,0.0,1.0);
    seg_floor.setAxis(axis);
    seg_floor.setEpsAngle(DynamicRcfg::getInstance()->floor_angle * (M_PI/180.0f));
    seg_floor.setMaxIterations(500);

    // Mandatory
    seg_floor.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
    seg_floor.setMethodType(pcl::SAC_RANSAC);
    seg_floor.setDistanceThreshold (DynamicRcfg::getInstance()->floor_thickness);

    seg_floor.setInputCloud(pc_in_baselink);
    seg_floor.segment(*inliers_floor, *coefficients_floor);

    if (inliers_floor->indices.size() == 0)
    {
        PCL_ERROR("Could not estimate a planar model for the floor plane.\n");
        floor_plane_detected = false;
    }
    else{
        floor_plane_detected = true;
    }

    plane_floor << coefficients_floor->values[0], coefficients_floor->values[1], coefficients_floor->values[2], coefficients_floor->values[3];

    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract_floor;
    extract_floor.setInputCloud(pc_in_baselink);
    extract_floor.setIndices(inliers_floor);
    extract_floor.setNegative(false);
    extract_floor.filter(cloud_segmented_floor);

    std::cout << "PointCloud representing the cloud_segmented_floor component: " << cloud_segmented_floor.points.size () << " data points." << std::endl;

    // Remove the planar inliers, extract the rest
    extract_floor.setNegative (true);
    extract_floor.filter (*segm);



    std::cout << "PointCloud representing the all_except_floor (segm) component: " << segm->points.size () << " data points." << std::endl;
    sensor_msgs::PointCloud2 segm_no_floor;
    pcl::toROSMsg(*segm, segm_no_floor);
    //		pub_inliers_.publish(segm_no_floor);
    Publisher::getInstance()->pub_inliers_.publish(segm_no_floor);

    std::cout << "PointCloud representing the pc_in_baselink component: " << pc_in_baselink->points.size () << " data points." << std::endl;

    ROS_INFO("%s: fitted plane floor: %fx%s%fy%s%fz%s%f=0 (inliers: %zu/%i)",
            _name.c_str(),
            coefficients_floor->values[0],(coefficients_floor->values[1]>=0?"+":""),
            coefficients_floor->values[1],(coefficients_floor->values[2]>=0?"+":""),
            coefficients_floor->values[2],(coefficients_floor->values[3]>=0?"+":""),
            coefficients_floor->values[3],
            inliers_floor->indices.size(), pc_in_baselink->height*pc_in_baselink->width);

    //		writer.write<pcl::PointXYZ> ("/home/d/Desktop/global_cloud_2_nonfloor.pcd", *segm, false);

    return segm;
}


void Detector::getWalls(const PointCloudType::Ptr& segment_except_floor, const pcl::PointXYZ& poi_baselink){


    // 2 walls search

    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients(true);
    Eigen::Vector3f axis_z = Eigen::Vector3f(0.0,0.0,1.0);
    seg.setAxis(axis_z);
    seg.setEpsAngle(DynamicRcfg::getInstance()->wall_angle * (M_PI/180.0f)); // first is in degree, ie 2,10 deg. less than 5.0 cannot find inliers exception
    seg.setMaxIterations(500);

    // Mandatory
    seg.setModelType(pcl::SACMODEL_PARALLEL_PLANE  );
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setDistanceThreshold (DynamicRcfg::getInstance()->wall_thickness); //0.02

    int _min_percentage = DynamicRcfg::getInstance()->wall_percentage; //10 for clustering debugging only
    int original_size(segment_except_floor->height*segment_except_floor->width);
    int n_planes(0);

    std::vector<pcl::ModelCoefficients> plane_coeff_list_except_floorplane;
    std::vector<pcl::PointCloud<pcl::PointXYZ>> planes_list_except_floorplane;
    std::vector<pcl::PointIndices> plane_indices;

    pcl::PointIndices::Ptr inliers = pcl::PointIndices::Ptr(new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr coefficients = pcl::ModelCoefficients::Ptr(new pcl::ModelCoefficients);

    while (segment_except_floor->height*segment_except_floor->width>original_size*_min_percentage/100)
    {

        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (segment_except_floor);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0)
        {
            std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }

        PointCloudType::Ptr cloud_f = PointCloudType::Ptr(new PointCloudType());
        // Extract the planar inliers from the input cloud
        pcl::ExtractIndices<pcl::PointXYZ> extract;
        extract.setInputCloud (segment_except_floor);
        extract.setIndices (inliers);
        extract.setNegative (false);
        ////plane_coeff_list_except_floorplane.push_back(*coefficients);
        plane_indices.push_back(*inliers);


        // Get the points associated with the planar surface
        pcl::PointCloud<pcl::PointXYZ> temp_cloud_segmented;
        extract.filter (temp_cloud_segmented);
        std::cout << "PointCloud representing the temp_cloud_segmented component: " << temp_cloud_segmented.points.size () << " data points." << std::endl;
        planes_list_except_floorplane.push_back(temp_cloud_segmented);

        // Remove the planar inliers, extract the rest
        extract.setNegative (true);
        extract.filter (*cloud_f);
        segment_except_floor->swap(*cloud_f);


        // Display infor
        ROS_INFO("%s: fitted plane %i: %fx%s%fy%s%fz%s%f=0 (inliers: %zu/%i)",
                _name.c_str(),n_planes,
                coefficients->values[0],(coefficients->values[1]>=0?"+":""),
                coefficients->values[1],(coefficients->values[2]>=0?"+":""),
                coefficients->values[2],(coefficients->values[3]>=0?"+":""),
                coefficients->values[3],
                inliers->indices.size(),original_size);
        ROS_INFO("%s: points left in cloud %i",_name.c_str(),segment_except_floor->width*segment_except_floor->height);

        // Nest iteration
        n_planes++;
        std::cout << "n_planes: --> : " << n_planes << std::endl;

    }




    //cluster extraction for each plane
    std::vector<pcl::PointCloud<pcl::PointXYZ>> max_cluster_planes;
    max_cluster_planes = clusterExtraction(planes_list_except_floorplane);
    std::vector<pcl::ModelCoefficients> max_cluster_coeffs;

    // cluster coeffs processing
    max_cluster_coeffs = clusterProcessing(max_cluster_planes);

    std::cout << "max_cluster_coeffs.size(): " << max_cluster_coeffs.size() << std::endl;
    if (max_cluster_coeffs.size() == 0) {
        return;
    }

    planes_list_except_floorplane = max_cluster_planes;
    plane_coeff_list_except_floorplane = max_cluster_coeffs;
    //

    std::cout << "plane_coeff_list_except_floorplane.size(): " << plane_coeff_list_except_floorplane.size() << std::endl;
    if (plane_coeff_list_except_floorplane.size() == 0) {
        return;
    }


    // distance check
    std::vector<double> dists;

    for (int idx = 0; idx < plane_coeff_list_except_floorplane.size(); idx++) {
        std::cout << idx << std::endl;
        pcl::ModelCoefficients plane_coefficients = plane_coeff_list_except_floorplane[idx];
        std::cout << plane_coefficients.values[0] << " , " << plane_coefficients.values[1] << " , " << plane_coefficients.values[2] << " , " << plane_coefficients.values[3] << std::endl;

        Eigen::Vector4f ground_plane_params = Eigen::Vector4f(	plane_coefficients.values[0],
                plane_coefficients.values[1],
                plane_coefficients.values[2],
                plane_coefficients.values[3]);

        double dist_wall = pcl::pointToPlaneDistance(poi_baselink, ground_plane_params);
        std::cout << "dist_wall: " << dist_wall << std::endl;
        dists.push_back(dist_wall);
    }


    //https://stackoverflow.com/questions/10580982/c-sort-keeping-track-of-indices
    //codegrepper.com/code-examples/cpp/c%2B%2B+sorting+and+keeping+track+of+indexes
    std::vector<size_t> indices(dists.size());
    std::iota(indices.begin(), indices.end(), 0);
    std::stable_sort(indices.begin(), indices.end(),
            [&dists](size_t i1, size_t i2) {return dists[i1] < dists[i2];});

    // sort dists
    std::sort(dists.begin(), dists.end());


    std::cout << "Sorted \n";
    for (auto& x : dists)
        std::cout << x << " ";
    std::cout << std::endl;
    std::cout << "minElement1:" << dists[0] << std::endl;
    std::cout << "minElement2:" << dists[1] << std::endl;

    for (auto& x : indices)
        std::cout << x << " ";
    std::cout << std::endl;

    //if (indices.size() >=0)
    *coefficients_wall1 = plane_coeff_list_except_floorplane[indices[0]];
    *coefficients_wall2 = plane_coeff_list_except_floorplane[indices[1]];

    plane_wall1 << coefficients_wall1->values[0], coefficients_wall1->values[1], coefficients_wall1->values[2], coefficients_wall1->values[3];
    plane_wall2 << coefficients_wall2->values[0], coefficients_wall2->values[1], coefficients_wall2->values[2], coefficients_wall2->values[3];

    cloud_segmented_wall1 = planes_list_except_floorplane[indices[0]];
    cloud_segmented_wall2 = planes_list_except_floorplane[indices[1]];

    *inliers_wall1 = plane_indices[indices[0]];
    *inliers_wall2 = plane_indices[indices[1]];
    return ;
}


std::vector<pcl::PointCloud<pcl::PointXYZ>> Detector::clusterExtraction(const std::vector<pcl::PointCloud<pcl::PointXYZ>>& planes){

    double voxel_size;
    if (poi_type == pc_detection::GetObstacle::Request::POI_WALL) {
            voxel_size = DynamicRcfg::getInstance()->wall_voxel_size;
        }
        else {
            voxel_size = DynamicRcfg::getInstance()->voxel_size;
        }

    std::vector<pcl::PointCloud<pcl::PointXYZ>> max_clusters;

    //each plane
    for (const pcl::PointCloud<pcl::PointXYZ> &plane : planes) {
        const PointCloudType::Ptr cloud = PointCloudType::Ptr(new PointCloudType());
        *cloud = plane;
        // Creating the KdTree object for the search method of the extraction
        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
        tree->setInputCloud (cloud);

        std::vector<pcl::PointIndices> cluster_indices;
        pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
        ec.setClusterTolerance (2* voxel_size); 	// 2*voxel size
        ec.setMinClusterSize (DynamicRcfg::getInstance()->min_cluster_size);		//maxcluster filtering based on cluster size; if 10 ->  more clusters for debugging

        ec.setMaxClusterSize (cloud->size() + 1);
        ec.setSearchMethod (tree);
        ec.setInputCloud (cloud);
        ec.extract (cluster_indices);


        std::cout << "Size of cluster_indices: " << cluster_indices.size() << std::endl;
        int idx = 0;

        //each cluster
        for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
        {
            if (idx==0) {
                pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
                for (const auto& cluster_id : it->indices)
                    cloud_cluster->push_back ((*cloud)[cluster_id]);

                cloud_cluster->width = cloud_cluster->size ();
                cloud_cluster->height = 1;
                cloud_cluster->is_dense = true;
                max_clusters.push_back(*cloud_cluster);
                std::cout << "PointCloud representing the Cluster: " << cloud_cluster->size () << " data points." << std::endl;

            }
            idx++;
        }

    }

    return max_clusters;
}


std::vector<pcl::ModelCoefficients> Detector::clusterProcessing(const std::vector<pcl::PointCloud<pcl::PointXYZ>>& max_cluster_planes){


    std::vector<pcl::ModelCoefficients> max_cluster_coeffs;

    // cluster processing
    for (const pcl::PointCloud<pcl::PointXYZ> &cl : max_cluster_planes) {
        const PointCloudType::Ptr cloud = PointCloudType::Ptr(new PointCloudType());
        *cloud = cl;
        std::cout << "----- PointCloud representing the max cluster: " << cloud->size () << " data points." << std::endl;

        std::vector<double> eigenvals;
        eigenvals = computeEigenValues(cloud);
        std::cout << "----- Cluster eigenvals: " << eigenvals[0] << " , " << eigenvals[1] << " , " << eigenvals[2] << std::endl;


        double eigen_param;
        // if not zero
        if (eigenvals[1] != 0)
            eigen_param = std::sqrt(eigenvals[0] / eigenvals[1]);
        else {
            std::cout << "----- Could not estimate eigen_param: " << eigen_param << std::endl;
            break;
        }
        std::cout << "----- eigen_param: " << eigen_param << std::endl;


        //maxcluster filtering based on eigen_param
        if (eigen_param < DynamicRcfg::getInstance()->cloud_width_length_ratio) { //10
            pcl::SACSegmentation<pcl::PointXYZ> seg_cluster;
            seg_cluster.setOptimizeCoefficients(true);
            seg_cluster.setModelType(pcl::SACMODEL_PLANE);
            seg_cluster.setMethodType(pcl::SAC_RANSAC);
            seg_cluster.setDistanceThreshold (0.02);

            pcl::PointIndices::Ptr cl_inliers = pcl::PointIndices::Ptr(new pcl::PointIndices);
            pcl::ModelCoefficients::Ptr cl_coeffs = pcl::ModelCoefficients::Ptr(new pcl::ModelCoefficients);
            seg_cluster.setInputCloud (cloud);
            seg_cluster.segment (*cl_inliers, *cl_coeffs);
            max_cluster_coeffs.push_back(*cl_coeffs);
        }
    }


    return max_cluster_coeffs;
}


/* ref: https://www.programmersought.com/article/35344268112/ */
std::vector<double> Detector::computeEigenValues(const pcl::PointCloud<pcl::PointXYZ>::Ptr& input_cloud){


    Eigen::Matrix<double, 4, 1> centroid;
    pcl::compute3DCentroid(*input_cloud, centroid);  // calculate centroid
    Eigen::Matrix<double, 3, 3> convariance_matrix;  // Covariance matrix
    pcl::computeCovarianceMatrix(*input_cloud, centroid, convariance_matrix);
    Eigen::Matrix3d eigenVectors;
    Eigen::Vector3d eigenValues;
    pcl::eigen33(convariance_matrix, eigenVectors, eigenValues);

    // The built-in eigenvalue accuracy is not high
    //Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double, 3, 3>> eigen_solver(convariance_matrix.transpose()*convariance_matrix);
    //eigenValues = eigen_solver.eigenvalues();

    // Find the first two larger eigenvalues
    Eigen::Vector3d::Index maxRow, maxCol, minRow, minCol;
    eigenValues.maxCoeff(&maxRow, &maxCol);
    eigenValues.minCoeff(&minRow, &minCol);
    // l1 > l2 > l3
    const double& l1 = eigenValues[maxRow];
    const double& l2 = eigenValues[3 - maxRow - minRow]; // This is a clever calculation, based on 0 + 1 + 2 = 3
    const double& l3 = eigenValues[minRow];

    std::vector<double> eigenvals;
    eigenvals.push_back(l1);
    eigenvals.push_back(l2);
    eigenvals.push_back(l3);
    return eigenvals;

}


Detector::PointCloudType::Ptr  Detector::removePlanars(const PointCloudType::Ptr& cloud, int poi_type) {

    PointCloudType::Ptr non_planar_cloud = PointCloudType::Ptr(new PointCloudType());
    PointCloudType::Ptr non_planar_cloud2 = PointCloudType::Ptr(new PointCloudType());

//    cloud->points.size(): 941
//    inliers_wall1.size(): 505

//    cloud->points.size(): 3584
//    inliers_wall1.size(): 505

    pcl::ExtractIndices<pcl::PointXYZ> extract;
    extract.setInputCloud(cloud);
    std::cout << "cloud->points.size(): " << cloud->points.size() <<std::endl;
    extract.setIndices(inliers_wall1);
    std::cout << "inliers_wall1.size(): " << inliers_wall1->indices.size() <<std::endl;

    extract.setNegative(true);
    extract.filter(*non_planar_cloud);

    if (poi_type == 4) { //single wall case
        return non_planar_cloud;
    }

    extract.setInputCloud(non_planar_cloud);
    extract.setIndices(inliers_wall2);

    extract.setNegative(true);
    extract.filter(*non_planar_cloud2);

    return non_planar_cloud2;

}

}
