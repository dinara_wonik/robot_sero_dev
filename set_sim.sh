alias cw='cd ~/ros_ws/wrobo_ws'
alias cs='cd ~/ros_ws/wrobo_ws/src'
alias cm='cd ~/ros_ws/wrobo_ws && catkin_make -DCMAKE_BUILD_TYPE=Release'
source ~/ros_ws/wrobo_ws/devel/setup.bash
source ~/ros_ws/wrobo_ws/install/setup.bash
export ROS_MASTER_URI=http://localhost:11311
export ROS_HOSTNAME=localhost
export DISPLAY=:0
export OPENBLAS_NUM_THREADS=1
export LC_ALL=en_US.UTF-8



