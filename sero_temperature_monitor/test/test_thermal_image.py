#!/usr/bin/python
# -*- coding: utf8 -*-#

"""
2021.03.12 jglee
"""
import numpy as np
import rospy
import tf.transformations as trans
import actionlib
from sensor_msgs.msg import Image, CompressedImage
from sero_temperature_monitor.msg import (
    SetThermalImageAction, 
    SetThermalImageGoal,
    SetThermalImageFeedback, 
    SetThermalImageResult,
)
import cv2
from cv_bridge import CvBridge, CvBridgeError

def fire_monitor_client():
    client = actionlib.SimpleActionClient('/sero_temperature_monitor/set_thermal_image', SetThermalImageAction) 
    client.wait_for_server()
    bridge = CvBridge()

    def on_feedback(fb):
        print '-------------------'
        rospy.loginfo_throttle(1.0, "Rcv feedback" )
        print 'max_temperature : ', fb.max_temperature
        print 'min_temperature : ', fb.min_temperature

        frame_rgb     = bridge.compressed_imgmsg_to_cv2(fb.rgb, "bgr8")
        frame_thermal = bridge.compressed_imgmsg_to_cv2(fb.thermal, "bgr8")

        cv2.imshow("Rgb window", frame_rgb)
        cv2.imshow("Thermal window", frame_thermal)
        cv2.waitKey(50)

    def on_result(goal_status, result):
        rospy.loginfo("Rcv result. goal_status=%s, result=\n%s" % (goal_status, result))

    goal = SetThermalImageGoal()
    goal.time_out = 8
    print 'time out sec : ', goal.time_out
    client.send_goal(goal, feedback_cb=on_feedback, done_cb=on_result)
    rospy.loginfo("send_goal()")
    try:
        rospy.loginfo("wait_for_result()")
        client.wait_for_result()
    except KeyboardInterrupt:
        rospy.loginfo("cancel_goal()")
        client.cancel_goal()
    return client.get_result()


if __name__ == "__main__":
    rospy.init_node("thermal_image_client")
    rospy.loginfo("Call set thermal image client")
    try:        
        result = fire_monitor_client()
    except rospy.ROSInterruptException:
        print "program interrupted before completion"
