#!/usr/bin/python
# -*- coding: utf8 -*-#
'''
2021.03.24 jglee
'''
import rospy
import actionlib
import copy
from sensor_msgs.msg import Image, CompressedImage
from std_msgs.msg import String

from sero_temperature_monitor.msg import (
    SetThermalImageAction,
    SetThermalImageGoal,
    SetThermalImageFeedback,
    SetThermalImageResult,
)
from sero_temperature_monitor.msg import (
    SetFireDetectionAction,
    SetFireDetectionGoal,
    SetFireDetectionFeedback,
    SetFireDetectionResult,
)
import json

WAIT_TIME_SEC = 5

class HikDataReceiver(object):
    def __init__(self, topics):
        self._hik_data = json.loads('{"max":-1.0, "min":-1.0, "on_fire":0}')
        self._rgb_image = CompressedImage()
        self._thr_image = CompressedImage()

        self._sub_hik_data = rospy.Subscriber(topics['hik_data'], String, self._callback_hik_data)
        self._sub_rgb_image = rospy.Subscriber(topics['rgb_image'], CompressedImage, self._callback_rgb_image)
        self._sub_thr_image = rospy.Subscriber(topics['thr_image'], CompressedImage, self._callback_thr_image)

    @property
    def hik_data(self):
        return self._hik_data

    @property
    def rgb_image(self):
        return self._rgb_image

    @property
    def thr_image(self):
        return self._thr_image

    def _callback_hik_data(self, data):
        self._hik_data = json.loads(data.data)

    def _callback_rgb_image(self, data):
        self._rgb_image = data

    def _callback_thr_image(self, data):
        self._thr_image = data


class FireDetection(object):
    def __init__(self, name, sub_data):

        self._wait_time = WAIT_TIME_SEC
        self._wait_thrImgArray = []
        self._msgArray = []

        self._action_name = name + '/set_fire_detection'
        self._as = actionlib.SimpleActionServer(self._action_name, SetFireDetectionAction, execute_cb=self.execute_cb,
                                                auto_start=False)
        self._sub_data = sub_data
        self._as.start()

    def execute_cb(self, goal):
        rospy.loginfo('%s: Executing, creating time out %.2f' % (self._action_name, goal.time_out))
        r = rospy.Rate(1)

        time_start = rospy.Time.now()
        time_out = goal.time_out  # t.secs=10
        print 'Goal : time_out :', time_out

        result = SetFireDetectionResult()
        feedback = SetFireDetectionFeedback()

        feedback.stamp = rospy.Time.now()
        feedback.rgb = CompressedImage()
        feedback.thermal = CompressedImage()
        feedback.is_fire = False

        self._wait_thrImgArray = []
        self._msgArray = []

        while True:
            time_now = rospy.Time.now()
            during_time = int((time_now - time_start).to_sec())

            print 'feedback ... during_time : %d' % during_time

            if self._wait_time < during_time:
                feedback.stamp = rospy.Time.now()
                feedback.rgb   = self._sub_data.rgb_image
                #feedback.thermal = self._sub_data.thr_image
                #feedback.is_fire = self._sub_data.hik_data['on_fire']

                self._wait_thrImgArray.append(self._sub_data.thr_image)
                self._msgArray.append(self._sub_data.hik_data['on_fire'])

                feedback.thermal = self._wait_thrImgArray[during_time - self._wait_time]
                feedback.is_fire = self._msgArray[during_time - self._wait_time]

                #print "========================="
                #print self._sub_data.rgb_image.header
                #print "-------------------------"
                #print self._sub_data.thr_image.header
                #print "-------------------------"
                #print self._sub_data.hik_data

                r.sleep()

                self._as.publish_feedback(feedback)

                if (during_time > time_out + self._wait_time):
                    rospy.loginfo('%s: Succeeded' % self._action_name)
                    result.is_success = True
                    self._as.set_aborted(result)
                    break
            else:
                self._wait_thrImgArray.append(self._sub_data.thr_image)
                print "_wait_thrImgArray len=", len(self._wait_thrImgArray)
                self._msgArray.append(self._sub_data.hik_data['on_fire'])
                r.sleep()


class ThermalImage(object):
    def __init__(self, name, sub_data):
        self._wait_time = WAIT_TIME_SEC
        self._wait_thrImgArray = []
        self._msgMaxArray = []
        self._msgMinArray = []

        self._action_name = name + '/set_thermal_image'
        self._as = actionlib.SimpleActionServer(self._action_name, SetThermalImageAction, execute_cb=self.execute_cb,
                                                auto_start=False)
        self._sub_data = sub_data
        self._as.start()

    def execute_cb(self, goal):
        rospy.loginfo('%s: Executing, creating time out %.2f' % (self._action_name, goal.time_out))
        r = rospy.Rate(1)

        time_start = rospy.Time.now()
        time_out = goal.time_out  # t.secs=10
        print 'Goal : time_out :', time_out

        result = SetThermalImageResult()
        feedback = SetThermalImageFeedback()

        feedback.stamp = rospy.Time.now()
        feedback.rgb = CompressedImage()
        feedback.thermal = CompressedImage()
        feedback.max_temperature = -1
        feedback.min_temperature = -1

        self._wait_thrImgArray = []
        self._msgMaxArray = []
        self._msgMinArray = []

        while True:
            time_now = rospy.Time.now()
            during_time = int((time_now - time_start).to_sec())

            print 'feedback ... during_time : %d' % during_time

            if self._wait_time < during_time:
                feedback.stamp = rospy.Time.now()
                feedback.rgb   = self._sub_data.rgb_image
                # feedback.thermal = self._sub_data.thr_image

                #feedback.max_temperature = self._sub_data.hik_data['max']
                #feedback.min_temperature = self._sub_data.hik_data['min']

                self._wait_thrImgArray.append(self._sub_data.thr_image)
                self._msgMaxArray.append(self._sub_data.hik_data['max'])
                self._msgMinArray.append(self._sub_data.hik_data['min'])

                feedback.thermal = self._wait_thrImgArray[during_time - self._wait_time]
                feedback.max_temperature = self._msgMaxArray[during_time - self._wait_time]
                feedback.min_temperature = self._msgMinArray[during_time - self._wait_time]


                #print "========================="
                #print  self._sub_data.rgb_image.header
                #print  "-------------------------"
                #print  self._sub_data.thr_image.header
                #print  "-------------------------"
                #print  self._sub_data.hik_data

                self._as.publish_feedback(feedback)
                r.sleep()

                if (during_time > time_out + self._wait_time):
                    rospy.loginfo('%s: Succeeded' % self._action_name)
                    result.is_success = True
                    self._as.set_aborted(result)
                    break
            else:
                self._wait_thrImgArray.append(self._sub_data.thr_image)
                self._msgMaxArray.append(self._sub_data.hik_data['max'])
                self._msgMinArray.append(self._sub_data.hik_data['min'])
                r.sleep()


if __name__ == '__main__':
    rospy.init_node('sero_temperature_monitor')
    topics = {'hik_data': '/hikvision_thermal_camera_node/fire_temperature',
              'rgb_image': '/rtsp01/image_raw/compressed', 'thr_image': '/rtsp02/image_raw/compressed'}
    sub_data = HikDataReceiver(topics)
   
    f = FireDetection(rospy.get_name(), sub_data)
    t = ThermalImage(rospy.get_name(), sub_data)
    rospy.spin()
