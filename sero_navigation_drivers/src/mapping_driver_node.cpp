/*
 * mapping_driver_node.cpp
 *
 *  Created on: 2019.12.12
 *      Author: ('c')void
 */

#include "common.h"
#include <workerbee_platform_msgs/MappingDriverState.h>
#include <workerbee_platform/MappingComputeVelocity.h>


class DriverNode
{
    ros::NodeHandle private_nh_;
    ManualDriver manual_driver_;
    std::vector<ros::ServiceServer> servers_;

    std::string code2str(int code) {
        switch(code) {
        case workerbee_platform_msgs::MappingDriverState::NO_ERROR:
            return "NO_ERROR";
        case workerbee_platform_msgs::MappingDriverState::ERROR_CONTROLLER:
            return "ERROR_CONTROLLER";
        case workerbee_platform_msgs::MappingDriverState::ERROR_TIMEOUT:
            return "ERROR_TIMEOUT";
        case workerbee_platform_msgs::MappingDriverState::ERROR_ETC:
            return "ERROR_ETC";
        default:
            return workerbee_utils::string_format("UNKNOWN_CODE(%d)", code);
        }
    }

    bool srv_compute_velocity(workerbee_platform::MappingComputeVelocity::Request& req, workerbee_platform::MappingComputeVelocity::Response& rsp) {
        if(manual_driver_.compute_velocity(req.collision_detection, rsp.cmd_vel) == false) {
            std::string details;
            CommonDriverStatePublisher::State state = manual_driver_.get_state(details);

            switch(state) {
                case CommonDriverStatePublisher::ERROR_COSTMAP:
                case CommonDriverStatePublisher::ERROR_COLLISION:
                case CommonDriverStatePublisher::ERROR_OBSTACLE:
                    rsp.state.code = workerbee_platform_msgs::MappingDriverState::ERROR_CONTROLLER;
                    break;
                case CommonDriverStatePublisher::ERROR_TIMEOUT:
                    rsp.state.code = workerbee_platform_msgs::MappingDriverState::ERROR_TIMEOUT;
                    break;
                default:
                    rsp.state.code = workerbee_platform_msgs::MappingDriverState::ERROR_ETC;
                    break;
            }

            rsp.state.details = workerbee_utils::string_format("%s. %s", code2str(rsp.state.code).c_str(), details.c_str());
        }
        else {
            rsp.state.code = workerbee_platform_msgs::MappingDriverState::NO_ERROR;
        }

        manual_driver_.clear();

        return true;
    }

public:
    DriverNode() : private_nh_("~"), manual_driver_(private_nh_){
        servers_.push_back(private_nh_.advertiseService("compute_velocity", &DriverNode::srv_compute_velocity, this));
    }
    ~DriverNode() {}
};


int main(int argc, char** argv) {
    ros::init(argc, argv, "manual_driver_node");
    DriverNode node;
    ros::spin();
    return 0;
}



