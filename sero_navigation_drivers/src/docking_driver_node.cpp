/*
 * docking_driver_node.cpp
 *
 *  Created on: 2019.07.20
 *      Author: ('c')void
 */


#include <string>
#include <vector>

#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <pluginlib/class_loader.hpp>
#include <nav_core/base_local_planner.h>

#include <costmap_2d/costmap_2d_ros.h>
#include <costmap_2d/costmap_2d.h>
#include <geometry_msgs/Twist.h>

#include <std_msgs/ColorRGBA.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <workerbee_platform/DockingComputeVelocity.h>
#include <workerbee_platform/IsGoalReached.h>
#include <workerbee_platform/StartDocking.h>
#include <workerbee_status/workerbee_status.h>
#include <workerbee_utils/accessories.h>

#include <sero_docking_driver/docking_driver.h>

#include "accel_limit.h"


std::string code2str(int code)
{
    switch(code) {
    case workerbee_platform_msgs::DockingDriverState::NO_ERROR:
        return "NO_ERROR";
    case workerbee_platform_msgs::DockingDriverState::GOAL_REACHED:
        return "GOAL_REACHED";
    case workerbee_platform_msgs::DockingDriverState::ERROR_COSTMAP:
        return "ERROR_COSTMAP";
    case workerbee_platform_msgs::DockingDriverState::ERROR_COLLISION:
        return "ERROR_COLLISION";
    case workerbee_platform_msgs::DockingDriverState::ERROR_OBSTACLE:
        return "ERROR_OBSTACLE";
    case workerbee_platform_msgs::DockingDriverState::ERROR_MISALIGNMENT:
        return "ERROR_MISALIGNMENT";
    case workerbee_platform_msgs::DockingDriverState::ERROR_CONNECTION_TIMEOUT:
        return "ERROR_CONNECTION_TIMEOUT";
    case workerbee_platform_msgs::DockingDriverState::ERROR_ETC:
        return "ERROR_ETC";
    default:
        return workerbee_utils::string_format("UNKNOWN_CODE(%d)", code);
    }
}

int str2code(std::string str_code) {
    if(str_code == std::string("NO_ERROR")){
        return workerbee_platform_msgs::DockingDriverState::NO_ERROR;
    }
    else if(str_code == std::string("GOAL_REACHED")){
        return workerbee_platform_msgs::DockingDriverState::GOAL_REACHED;
    }
    else if(str_code == std::string("ERROR_COSTMAP")){
        return workerbee_platform_msgs::DockingDriverState::ERROR_COSTMAP;
    }
    else if(str_code == std::string("ERROR_COLLISION")){
        return workerbee_platform_msgs::DockingDriverState::ERROR_COLLISION;
    }
    else if(str_code == std::string("ERROR_OBSTACLE")){
        return workerbee_platform_msgs::DockingDriverState::ERROR_OBSTACLE;
    }
    else if(str_code == std::string("ERROR_MISALIGNMENT")){
        return workerbee_platform_msgs::DockingDriverState::ERROR_MISALIGNMENT;
    }
    else if(str_code == std::string("ERROR_CONNECTION_TIMEOUT")){
        return workerbee_platform_msgs::DockingDriverState::ERROR_CONNECTION_TIMEOUT;
    }
    else {
        return -1;
    }
}


class DockingDriverNode
{
    tf2_ros::Buffer tf_;
    tf2_ros::TransformListener tfl_;
    std::string station_frame_id_;
    costmap_2d::Costmap2DROS* costmap_ros_;
    std::vector<ros::ServiceServer> servers_;

    AccelerationLimitFilter accel_filter_;

    boost::shared_ptr<sero_docking_driver::DockingDriver> docking_driver_;

    bool srv_compute_velocity(workerbee_platform::DockingComputeVelocity::Request& req, workerbee_platform::DockingComputeVelocity::Response& rsp) {
        costmap_2d::Costmap2D* cm = costmap_ros_->getCostmap();
        boost::unique_lock<costmap_2d::Costmap2D::mutex_t> costmap_lock(*cm->getMutex());

        geometry_msgs::Twist cmd_vel;
        bool success = docking_driver_->computeVelocityCommands(cmd_vel);
        if(!success) {
            std::string str_state, message;

            workerbee_status::get_info(std::string("DockingDriverState/state"), str_state);
            workerbee_status::get_info(std::string("DockingDriverState/message"), message);

            rsp.state.code = str2code(str_state);
            rsp.state.details = workerbee_utils::string_format("%s. %s", str_state.c_str(), message.c_str());
        }

        rsp.cmd_vel = accel_filter_.filtering(cmd_vel);

        return true;
    }

    bool srv_is_goal_reached(workerbee_platform::IsGoalReached::Request& req, workerbee_platform::IsGoalReached::Response& rsp) {
        rsp.state = docking_driver_->isGoalReached();
        return true;
    }

    bool srv_start(workerbee_platform::StartDocking::Request& req, workerbee_platform::StartDocking::Response& rsp) {

        accel_filter_.reset();

        bool success = docking_driver_->start(req.mode, req.retry);
        if(!success) {
            std::string str_state, message;

            workerbee_status::get_info(std::string("DockingDriverState/state"), str_state);
            workerbee_status::get_info(std::string("DockingDriverState/message"), message);

            rsp.state.code = str2code(str_state);
            rsp.state.details = workerbee_utils::string_format("%s. %s", str_state.c_str(), message.c_str());
        }

        return true;
    }

public:
    DockingDriverNode()
    : tf_(ros::Duration(10))
    , tfl_(tf_)
    {
        ros::NodeHandle private_nh("~");
        private_nh.param("station_frame_id", station_frame_id_, std::string("station"));

        double max_translational_acceleration;
        double max_rotational_acceleration;
        double control_freqeuncy;

        private_nh.param("max_translational_acceleration", max_translational_acceleration, 1.0);
        private_nh.param("max_rotational_acceleration",    max_rotational_acceleration,    3.0);
        private_nh.param("control_freqeuncy",              control_freqeuncy, 10.0);

        accel_filter_.set_accelerations(max_translational_acceleration, max_rotational_acceleration);
        accel_filter_.set_freq(control_freqeuncy);
        accel_filter_.enable(true);

        costmap_ros_ = new costmap_2d::Costmap2DROS("local_costmap", tf_);
        costmap_ros_->pause();

        docking_driver_ = boost::shared_ptr<sero_docking_driver::DockingDriver>(new sero_docking_driver::DockingDriver);
        docking_driver_->initialize("DockingDriver", &tf_, costmap_ros_);

        ROS_INFO("DockingDriverNode. DockingDriver has been initialized");

        costmap_ros_->start();

        servers_.push_back(private_nh.advertiseService("compute_velocity", &DockingDriverNode::srv_compute_velocity, this));
        servers_.push_back(private_nh.advertiseService("is_goal_reached", &DockingDriverNode::srv_is_goal_reached, this));
        servers_.push_back(private_nh.advertiseService("start", &DockingDriverNode::srv_start, this));

        ROS_INFO("DockingDriverNode. All service has been initialized");
    }

    virtual ~DockingDriverNode() {
        if(costmap_ros_) {
            delete costmap_ros_;
        }
    }
};


int main(int argc, char** argv) {
    ros::init(argc, argv, "sero_docking_driver");
    DockingDriverNode node;
    ros::spin();
    return 0;
}





