/*
 * accel_limit.h
 *
 *  Created on: May 11, 2020
 *      Author: ('c')void
 */

#ifndef __ACCEL_LIMIT_H__
#define __ACCEL_LIMIT_H__

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

class AccelerationLimitFilter
{
    bool init_;
    bool enable_;
    geometry_msgs::Twist twist_prv_;
    double control_freq_;

    double translation_acceleration_limit_;  // m/sec^2
    double angular_acceleration_limit_;      // rad/sec^2

    double delta_ta_max_, delta_aa_max_;

    double sign(double a) {
        if(a >= 0) {
            return 1;
        }
        else {
            return -1;
        }
    }

public:
    AccelerationLimitFilter(double control_freq=10)
    : init_(false), enable_(false), control_freq_(control_freq)
    {
        set_accelerations(0.5, 2.0);
    }

    virtual ~AccelerationLimitFilter() {}

    void set_accelerations(double max_translation_accel, double max_angular_accel) {
        translation_acceleration_limit_ = max_translation_accel;
        angular_acceleration_limit_ = max_angular_accel;

        delta_ta_max_ = translation_acceleration_limit_/control_freq_;
        delta_aa_max_ = angular_acceleration_limit_/control_freq_;

        ROS_INFO("AccelerationLimit was updated. max_translation_accel=%.2f, max_angular_accel=%.2f", max_translation_accel, max_angular_accel);
    }

    void reset() {
        twist_prv_ = geometry_msgs::Twist();
    }

    geometry_msgs::Twist filtering(const geometry_msgs::Twist::ConstPtr& twist_in) {
        geometry_msgs::Twist t = *twist_in;
        return filtering(t);
    }

    geometry_msgs::Twist filtering(geometry_msgs::Twist& twist_in) {

        if(!init_) {
            init_ = true;
            twist_prv_ = twist_in;
            return twist_in;
        }

        if(!enable_) {
            return twist_in;
        }

        geometry_msgs::Twist twist_out;


        double delta_ta_x = twist_in.linear.x  - twist_prv_.linear.x;
        double sign_ta_x = sign(delta_ta_x);

        double delta_ta_y = twist_in.linear.y  - twist_prv_.linear.y;
        double sign_ta_y = sign(delta_ta_y);

        double delta_aa = twist_in.angular.z - twist_prv_.angular.z;
        double sign_aa = sign(delta_aa);

        if(fabs(delta_ta_x) > delta_ta_max_) {
            delta_ta_x = sign_ta_x*delta_ta_max_;
        }

        if(fabs(delta_ta_y) > delta_ta_max_) {
            delta_ta_y = sign_ta_y*delta_ta_max_;
        }

        if(fabs(delta_aa) > delta_aa_max_) {
            delta_aa = sign_aa*delta_aa_max_;
        }

        twist_out = twist_in;
        twist_out.linear.x  = twist_prv_.linear.x + delta_ta_x;
        twist_out.linear.y  = twist_prv_.linear.y + delta_ta_y;
        twist_out.angular.z = twist_prv_.angular.z + delta_aa;

        twist_prv_ = twist_out;

        return twist_out;
    }

    void set_freq(float freq) {
        control_freq_ = freq;
    }

    void enable(bool e) {
        enable_ = e;
    }
};


#endif /* __ACCEL_LIMIT_H__ */
