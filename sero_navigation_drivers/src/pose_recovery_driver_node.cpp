/*
 * pose_recovery_driver_node.cpp
 *
 *  Created on: 2020.09.15
 *      Author: ('c')void
 *
 */

#include "common.h"

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iostream>
#include <iomanip>
#include <ctime>

#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>

#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <workerbee_platform_msgs/PoseRecoveryDriverState.h>
#include <workerbee_platform/PoseRecoveryComputeVelocity.h>
#include <workerbee_platform/PoseRecoveryReset.h>
#include <workerbee_platform/PoseRecoveryIsComplete.h>
#include <workerbee_utils/geometry_utils.h>

#include <wr_map_server/MapMatchingAction.h>


typedef actionlib::SimpleActionClient<wr_map_server::MapMatchingAction> MapMatchingClient;


std::string code2str(int code)
{
    switch(code) {
    case workerbee_platform_msgs::PoseRecoveryDriverState::NO_ERROR:
        return "NO_ERROR";
    case workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_CONTROLLER:
        return "ERROR_CONTROLLER";
    case workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_TIMEOUT:
        return "ERROR_TIMEOUT";
    case workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_ETC:
        return "ERROR_ETC";
    default:
        return workerbee_utils::string_format("UNKNOWN_CODE(%d)", code);
    }
}


std::string get_date_str(const char* format="%Y%m%d_%H%M%S")
{
    std::ostringstream oss;
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    oss << std::put_time(&tm, format);
    return oss.str();
}


class PositionRecovery
{
public:
    PositionRecovery() {}
    virtual ~PositionRecovery() {}
};


class PositionRecoveryAmcl : public PositionRecovery
{
    ros::NodeHandle& private_nh_;
    PoseRecoveryDriver driver_;
    std::string global_frame_id_;
    std::string robot_frame_id_;
    double pos_std_;
    double yaw_std_;
    double initial_delay_;
    double timeout_;

    ros::Time start_;

    std::vector<ros::ServiceServer> servers_;
    ros::Publisher pub_initpose_;


    bool srv_recovery(workerbee_platform::PoseRecoveryReset::Request& req, workerbee_platform::PoseRecoveryReset::Response& rsp) {
        /*
         *  다른 측위 수단이 없으므로 로봇의 현재위치를 사용함.
         */
        geometry_msgs::PoseStamped s, t;
        s.header.frame_id = robot_frame_id_;
        s.header.stamp = ros::Time::now();
        s.pose.orientation.w = 1;

        workerbee_utils::Geometry::transform_pose(*driver_.get_tf(), global_frame_id_, s, t);

        geometry_msgs::PoseWithCovarianceStamped p;
        p.header = t.header;
        p.pose.pose = t.pose;

        double var_pos = pos_std_*pos_std_;
        double var_yaw = yaw_std_*yaw_std_;

        boost::array<double, 36> c = { var_pos, 0, 0, 0, 0,       0,
                                       0, var_pos, 0, 0, 0,       0,
                                       0,       0, 0, 0, 0,       0,
                                       0,       0, 0, 0, 0,       0,
                                       0,       0, 0, 0, 0,       0,
                                       0,       0, 0, 0, 0, var_yaw};
        p.pose.covariance = c;

        pub_initpose_.publish(p);

        rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::NO_ERROR;

        start_ = ros::Time::now();

        return true;
    }

    bool srv_is_complete(workerbee_platform::PoseRecoveryIsComplete::Request& req, workerbee_platform::PoseRecoveryIsComplete::Response& rsp) {
        if((ros::Time::now() - start_).toSec() >= timeout_) {
            rsp.complete = true;
        }
        else {
            rsp.complete = false;
        }

        rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::NO_ERROR;
        rsp.state.details = workerbee_utils::string_format("%s", code2str(rsp.state.code).c_str());

        return true;
    }

    bool srv_compute_velocity(workerbee_platform::PoseRecoveryComputeVelocity::Request& req, workerbee_platform::PoseRecoveryComputeVelocity::Response& rsp) {
        if((ros::Time::now() - start_).toSec() < initial_delay_) {
            rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::NO_ERROR;
            rsp.state.details = workerbee_utils::string_format("%s. initial delay", code2str(rsp.state.code).c_str());
            driver_.clear();
            return true;
        }

        if(driver_.compute_velocity(req.collision_detection, rsp.cmd_vel) == false) {
            std::string details;
            CommonDriverStatePublisher::State state = driver_.get_state(details);

            switch(state) {
                case CommonDriverStatePublisher::ERROR_COSTMAP:
                case CommonDriverStatePublisher::ERROR_COLLISION:
                case CommonDriverStatePublisher::ERROR_OBSTACLE:
                    rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_CONTROLLER;
                    break;
                case CommonDriverStatePublisher::ERROR_TIMEOUT:
                    rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_TIMEOUT;
                    break;
                default:
                    rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_ETC;
                    break;
            }

            rsp.state.details = workerbee_utils::string_format("%s. %s", code2str(rsp.state.code).c_str(), details.c_str());
        }
        else {
            rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::NO_ERROR;
            rsp.state.details = workerbee_utils::string_format("%s", code2str(rsp.state.code).c_str());
        }

        driver_.clear();

        return true;
    }

public:
    PositionRecoveryAmcl(ros::NodeHandle& private_nh) : private_nh_(private_nh), driver_(private_nh_){
        private_nh_.param("global_frame_id", global_frame_id_, std::string("map"));
        private_nh_.param("robot_frame_id", robot_frame_id_, std::string("base_link"));
        private_nh_.param("pos_std", pos_std_, 1.0);
        private_nh_.param("yaw_std", yaw_std_, 3.14);
        private_nh_.param("initial_delay", initial_delay_, 3.0);
        private_nh_.param("timeout", timeout_, 10.0);

        timeout_ += initial_delay_;

        pub_initpose_ = private_nh_.advertise<geometry_msgs::PoseWithCovarianceStamped>("/initialpose", 10);
        servers_.push_back(private_nh_.advertiseService("compute_velocity", &PositionRecoveryAmcl::srv_compute_velocity, this));
        servers_.push_back(private_nh_.advertiseService("recovery", &PositionRecoveryAmcl::srv_recovery, this));
        servers_.push_back(private_nh_.advertiseService("is_complete", &PositionRecoveryAmcl::srv_is_complete, this));

        ROS_INFO("Start PositionRecoveryAmcl");
    }

    virtual ~PositionRecoveryAmcl() {}
};


class PositionRecoveryMapMatching : public PositionRecovery
{
    enum State {
        INITIALIZE,
        MAPPING,
        MAP_MATCHING,
        COMPLETE,
    };

    ros::NodeHandle& private_nh_;
    PoseRecoveryDriver driver_;
    std::string global_frame_id_;
    std::string robot_frame_id_;
    double initial_delay_;
    double matching_timeout_;
    double mapping_time_;

    ros::Time start_;
    State state_;

    workerbee_platform_msgs::PoseRecoveryDriverState final_driver_state_;

    std::string local_map_path_;
    std::string local_map_topic_;

    boost::shared_ptr<MapMatchingClient> ac_;

    std::vector<ros::ServiceServer> servers_;
    ros::Publisher pub_initpose_;
    ros::Publisher pub_reset_gmapping_;


    const char* state_str(State s) {
        switch(s) {
        case INITIALIZE: return "INITIALIZE";
        case MAPPING: return "MAPPING";
        case MAP_MATCHING: return "MATCHING";
        case COMPLETE: return "COMPLETE";
        default:
            return "NONE";
        }
    }

    bool get_robot_pose(std::string frame_id, geometry_msgs::PoseStamped& robot_pose) {
        robot_pose = geometry_msgs::PoseStamped();

        geometry_msgs::PoseStamped s;
        s.header.frame_id = robot_frame_id_;
        s.header.stamp = ros::Time::now();
        s.pose.orientation.w = 1;

        return workerbee_utils::Geometry::transform_pose(*driver_.get_tf(), frame_id, s, robot_pose);
    }

    void callback_matching_result(actionlib::SimpleClientGoalState state, wr_map_server::MapMatchingResultConstPtr result) {
        do {
            if(!result->state) {
                ROS_ERROR("callback_matching_result. error %s", result->error.c_str());

                final_driver_state_.code = workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_ETC;
                final_driver_state_.details = workerbee_utils::string_format("%s. %s",
                        code2str(final_driver_state_.code).c_str(),
                        result->error.c_str());
                break;
            }

            geometry_msgs::PoseStamped pose_local_map;
            if(get_robot_pose("local_map", pose_local_map) == false) {
                ROS_ERROR("Could not get robot position on the local map");
                final_driver_state_.code = workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_ETC;
                final_driver_state_.details = workerbee_utils::string_format("%s. Could not get robot position on the local map",
                        code2str(final_driver_state_.code).c_str() );
                break;
            }

            // use only orientation
            pose_local_map.pose.position.x = 0;
            pose_local_map.pose.position.y = 0;
            pose_local_map.pose.position.z = 0;

            geometry_msgs::PoseStamped pose_global_map;
            tf2::doTransform(pose_local_map, pose_global_map, result->T_l2g);

            ROS_INFO("callback_matching_result");
            ROS_INFO_STREAM("pose_local_map.\n" << pose_local_map);
            ROS_INFO_STREAM("pose_global_map.\n" << pose_global_map);

            // init pose
            geometry_msgs::PoseWithCovarianceStamped p;
            p.header = pose_global_map.header;
            p.pose.pose = pose_global_map.pose;

            double pos_std = 0.5;  // FIXME
            double yaw_std = 10 * M_PI/180;

            double var_pos = pos_std*pos_std;
            double var_yaw = yaw_std*yaw_std;

            boost::array<double, 36> c = { var_pos, 0, 0, 0, 0,       0,
                    0, var_pos, 0, 0, 0,       0,
                    0,       0, 0, 0, 0,       0,
                    0,       0, 0, 0, 0,       0,
                    0,       0, 0, 0, 0,       0,
                    0,       0, 0, 0, 0, var_yaw};
            p.pose.covariance = c;

            pub_initpose_.publish(p);

            ros::Duration(1.0).sleep();
        }while(0);

        state_ = COMPLETE;
    }

    void callback_matching_active() {
        ROS_INFO("Start Map Matching Action");
    }

    void callback_matching_feedback(wr_map_server::MapMatchingFeedbackConstPtr feedback) {
        ROS_INFO("Map matching feedback. progress=%s", feedback->progress.c_str());
    }

    bool srv_recovery(workerbee_platform::PoseRecoveryReset::Request& req, workerbee_platform::PoseRecoveryReset::Response& rsp) {
        /*
         *  start mapping
         */
        rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::NO_ERROR;
        ac_->cancelAllGoals();
        start_ = ros::Time::now();
        state_ = INITIALIZE;
        final_driver_state_.code = workerbee_platform_msgs::PoseRecoveryDriverState::NO_ERROR;
        final_driver_state_.details = "";

        // restart gmapping
        pub_reset_gmapping_.publish(std_msgs::Empty());

        return true;
    }

    bool srv_is_complete(workerbee_platform::PoseRecoveryIsComplete::Request& req, workerbee_platform::PoseRecoveryIsComplete::Response& rsp) {
        rsp.complete = (state_ == COMPLETE);
        rsp.state = final_driver_state_;
        return true;
    }

    bool srv_compute_velocity(workerbee_platform::PoseRecoveryComputeVelocity::Request& req, workerbee_platform::PoseRecoveryComputeVelocity::Response& rsp) {
        float run_time = (ros::Time::now() - start_).toSec();
        rsp.cmd_vel = geometry_msgs::Twist();

        ROS_INFO_THROTTLE(1, "PoseRecovery(%s) %.1f sec", state_str(state_), run_time);

        if(state_ == INITIALIZE) {
            if(run_time < initial_delay_) {
                rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::NO_ERROR;
                rsp.state.details = workerbee_utils::string_format("%s. initial delay", code2str(rsp.state.code).c_str());
            }
            else {
                start_ = ros::Time::now();
                state_ = MAPPING;
            }
        }
        else if(state_ == MAPPING) {
            if(run_time > mapping_time_) {
                std::string local_map_name;
                if(save_local_map(local_map_name)) {
                    wr_map_server::MapMatchingGoal goal;
                    goal.query_map = local_map_name + ".yaml";

                    geometry_msgs::PoseStamped pose_local_map;
                    get_robot_pose("local_map", pose_local_map);
                    goal.robot_pose_on_local_map.header = pose_local_map.header;
                    goal.robot_pose_on_local_map.point = pose_local_map.pose.position;

                    if(ac_->waitForServer(ros::Duration(0.2))) {
                        start_ = ros::Time::now();
                        ac_->sendGoal(goal,
                                boost::bind(&PositionRecoveryMapMatching::callback_matching_result, this, _1, _2),
                                boost::bind(&PositionRecoveryMapMatching::callback_matching_active, this),
                                boost::bind(&PositionRecoveryMapMatching::callback_matching_feedback, this, _1));

                        start_ = ros::Time::now();
                        state_ = MAP_MATCHING;
                    }
                    else {
                        rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_ETC;
                        rsp.state.details = workerbee_utils::string_format("%s. can not connect to map matching action server",
                                code2str(rsp.state.code).c_str());
                        state_ = COMPLETE;
                    }
                }
                else {
                    rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_ETC;
                    rsp.state.details = workerbee_utils::string_format("%s. local mapping error", code2str(rsp.state.code).c_str());
                    state_ = COMPLETE;
                }
            }
            else {
                /*
                 *  rotation
                 */
                if(driver_.compute_velocity(req.collision_detection, rsp.cmd_vel)) {
                    rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::NO_ERROR;
                    rsp.state.details = workerbee_utils::string_format("%s", code2str(rsp.state.code).c_str());
                }
                else {
                    std::string details;
                    CommonDriverStatePublisher::State state = driver_.get_state(details);

                    switch(state) {
                    case CommonDriverStatePublisher::ERROR_COSTMAP:
                    case CommonDriverStatePublisher::ERROR_COLLISION:
                    case CommonDriverStatePublisher::ERROR_OBSTACLE:
                        rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_CONTROLLER;
                        break;
                    case CommonDriverStatePublisher::ERROR_TIMEOUT:
                        rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_TIMEOUT;
                        break;
                    default:
                        rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::ERROR_ETC;
                        break;
                    }

                    rsp.state.details = workerbee_utils::string_format("%s. %s", code2str(rsp.state.code).c_str(), details.c_str());
                }
            }
        }
        else if(state_ == MAP_MATCHING){
            // FIXME. TIMEOUT

            rsp.state.code = workerbee_platform_msgs::PoseRecoveryDriverState::NO_ERROR;
            rsp.state.details = workerbee_utils::string_format("%s", code2str(rsp.state.code).c_str());
        }
        else if(state_ == COMPLETE) {
            rsp.state = final_driver_state_;
        }

        driver_.clear();
        return true;
    }

    bool save_local_map(std::string& local_map_name) {
        // save local map
        local_map_name = local_map_path_ + get_date_str("_%Y%m%d_%H%M%S");
        std::string cmd_save = workerbee_utils::string_format("rosrun map_server map_saver map:=%s -f %s > /tmp/local_mapping.txt",
                                                               local_map_topic_.c_str(), local_map_name.c_str());
        ROS_INFO("START MAP SAVING. %s", cmd_save.c_str());
        std::system(cmd_save.c_str());
        ROS_INFO("END MAP SAVING");

        return true;
    }

public:
    PositionRecoveryMapMatching(ros::NodeHandle& private_nh) : private_nh_(private_nh), driver_(private_nh_){
        private_nh_.param("global_frame_id", global_frame_id_, std::string("map"));
        private_nh_.param("robot_frame_id", robot_frame_id_, std::string("base_link"));
        private_nh_.param("initial_delay", initial_delay_, 3.0);
        private_nh_.param("matching_timeout", matching_timeout_, 10.0);
        private_nh_.param("mapping_time", mapping_time_, 10.0);

        private_nh_.param("local_map_path", local_map_path_, std::string("/tmp/local_map"));
        private_nh_.param("local_map_topic", local_map_topic_, std::string("/local_map"));

        ac_ = boost::shared_ptr<MapMatchingClient>(new MapMatchingClient("/map_matching", true));

        state_ = INITIALIZE;

        pub_initpose_ = private_nh_.advertise<geometry_msgs::PoseWithCovarianceStamped>("/initialpose", 10);
        pub_reset_gmapping_ = private_nh_.advertise<std_msgs::Empty>("/slam_gmapping/reset", 10);

        servers_.push_back(private_nh_.advertiseService("compute_velocity", &PositionRecoveryMapMatching::srv_compute_velocity, this));
        servers_.push_back(private_nh_.advertiseService("recovery", &PositionRecoveryMapMatching::srv_recovery, this));
        servers_.push_back(private_nh_.advertiseService("is_complete", &PositionRecoveryMapMatching::srv_is_complete, this));

        ROS_INFO("Start PositionRecoveryMapMatching");
    }

    virtual ~PositionRecoveryMapMatching() {}
};


int main(int argc, char** argv) {
    ros::init(argc, argv, "pose_recovery_driver_node");

    ros::NodeHandle private_nh("~");
    PositionRecovery* node = 0;

    std::string recovery_method;
    private_nh.param("recovery_method", recovery_method, std::string("amcl"));

    if(recovery_method == "amcl") {
        node = new PositionRecoveryAmcl(private_nh);
    }
    else if(recovery_method == "map_matching") {
        node = new PositionRecoveryMapMatching(private_nh);
    }

    if(!node) {
        ROS_ERROR("'%s' is not supported", recovery_method.c_str());
    }

    ros::spin();

    if(node) {
        delete(node);
    }

    return 0;
}



