/*
 * common.h
 *
 *  Created on: Dec 15, 2019
 *      Author: ('c')void
 */

#ifndef __DRIVER_COMMON_H__
#define __DRIVER_COMMON_H__


#include <string>
#include <vector>

#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <pluginlib/class_loader.hpp>
#include <nav_core/base_local_planner.h>

#include <costmap_2d/costmap_2d_ros.h>
#include <costmap_2d/costmap_2d.h>
#include <costmap_2d/footprint.h>
#include <geometry_msgs/Twist.h>

#include <workerbee_status/workerbee_status.h>
#include <workerbee_utils/accessories.h>
#include <workerbee_utils/geometry_utils.h>
#include <workerbee_utils/moving.h>


class CommonDriverStatePublisher
{
public:
    enum State  {
        NO_ERROR=0,
        ERROR_COSTMAP,
        ERROR_COLLISION,
        ERROR_OBSTACLE,
        ERROR_TIMEOUT,
        ERROR_ETC,
    };

private:
    State state_;
    std::string message_;

    const char* state_code_str(State code) {
        switch(code) {
        case NO_ERROR:
            return "NO_ERROR";
        case ERROR_COSTMAP:
            return "ERROR_COSTMAP";
        case ERROR_COLLISION:
            return "ERROR_COLLISION";
        case ERROR_OBSTACLE:
            return "ERROR_OBSTACLE";
        case ERROR_TIMEOUT:
            return "ERROR_TIMEOUT";
        case ERROR_ETC:
            return "ERROR_ETC";
        default:
            return "NONE";
        }
    }

public:
    CommonDriverStatePublisher()
    : state_(NO_ERROR)
    {}
    virtual ~CommonDriverStatePublisher() {}

    void set_state(State code, std::string message) {
        state_ = code;
        message_ = message;

        if(!(code == NO_ERROR)) {
            //ROS_ERROR("%s", message.c_str());
        }
    }

    State get_state() {
        return state_;
    }

    std::string get_detailed_message() {
        return workerbee_utils::string_format("state=%s, message=%s", state_code_str(state_), message_.c_str());
    }

    void clear() {
        state_ = NO_ERROR;
        message_ = "";
    }
};


class CommonDriver
{
protected:
    boost::mutex mutex_;

    tf2_ros::Buffer tf_;
    tf2_ros::TransformListener tfl_;
    std::string cfg_footprint_;

    double cfg_obstacle_detection_range_from_surface_;

    std::vector<geometry_msgs::Point> footprint_;

    costmap_2d::Costmap2DROS* costmap_ros_;
    workerbee_utils::CostmapUtil costmap_util_;

    CommonDriverStatePublisher driver_state_pub_;

    bool collision_detection(const std::vector<geometry_msgs::Point>& footprint) {
        tf::Stamped<tf::Pose> robot_pose_g;
        if (!costmap_util_.getRobotPose(robot_pose_g)) {
            std::string e = "could not get robot pose";
            driver_state_pub_.set_state(CommonDriverStatePublisher::ERROR_COSTMAP, "could not get robot pose in the global frame of costmap");
            return false;
        }

        std::vector<tf::Pose> lethal_obstacle_list;
        if(costmap_util_.get_obstacle_list(lethal_obstacle_list, robot_pose_g) == false) {
            driver_state_pub_.set_state(CommonDriverStatePublisher::ERROR_COSTMAP, "could not get obstacle list");
            return false;
        }

        if(footprint.size() == 0) {
            driver_state_pub_.set_state(CommonDriverStatePublisher::ERROR_ETC, "invalid footprint. footprint.size() == 0");
            return false;
        }

        double footprint_x_min =  std::numeric_limits<double>::infinity();
        double footprint_x_max = -std::numeric_limits<double>::infinity();
        double footprint_y_min =  std::numeric_limits<double>::infinity();
        double footprint_y_max = -std::numeric_limits<double>::infinity();

        for(size_t i=0; i<footprint.size(); i++) {
            if(footprint[i].x < footprint_x_min) {
                footprint_x_min = footprint[i].x;
            }

            if(footprint[i].x > footprint_x_max) {
                footprint_x_max = footprint[i].x;
            }

            if(footprint[i].y < footprint_y_min) {
                footprint_y_min = footprint[i].y;
            }

            if(footprint[i].y > footprint_y_max) {
                footprint_y_max = footprint[i].y;
            }
        }

        double footprint_x_min_p = footprint_x_min - cfg_obstacle_detection_range_from_surface_;
        double footprint_y_min_p = footprint_y_min - cfg_obstacle_detection_range_from_surface_;
        double footprint_x_max_p = footprint_x_max + cfg_obstacle_detection_range_from_surface_;
        double footprint_y_max_p = footprint_y_max + cfg_obstacle_detection_range_from_surface_;

        std::vector<tf::Pose> obstacle_list;

        if(true) { // direction_ == FORWARD
            // check front side
            for(std::vector<tf::Pose>::iterator p = lethal_obstacle_list.begin(); p != lethal_obstacle_list.end(); p++) {
                if(footprint_y_min_p <= p->getOrigin().y() && p->getOrigin().y() <= footprint_y_max_p &&
                   footprint_x_min   <= p->getOrigin().x() && p->getOrigin().x() <= footprint_x_max_p) {
                    obstacle_list.push_back(*p);
                }
            }
        }
        else {
            // check rear side
            for(std::vector<tf::Pose>::iterator p = lethal_obstacle_list.begin(); p != lethal_obstacle_list.end(); p++) {
                if(footprint_y_min_p <= p->getOrigin().y() && p->getOrigin().y() <= footprint_y_max_p &&
                   footprint_x_min_p <= p->getOrigin().x() && p->getOrigin().x() <= footprint_x_max) {
                    obstacle_list.push_back(*p);
                }
            }
        }

        if(obstacle_list.size() > 0) {
            std::string obstacle_message = workerbee_utils::string_format("OBSTACLE_NUM=%lu, OBSTACLES(x,y)=", obstacle_list.size());
            for(size_t i=0; i<obstacle_list.size(); i++) {
                obstacle_message += workerbee_utils::string_format(" (%.3f,%.3f)", obstacle_list[i].getOrigin().x(), obstacle_list[i].getOrigin().y());
            }

            driver_state_pub_.set_state(CommonDriverStatePublisher::ERROR_OBSTACLE, obstacle_message);
            return false;
        }
        else {
            return true;
        }
    }

public:
    CommonDriver(ros::NodeHandle& nh)
    : tf_(ros::Duration(10))
    , tfl_(tf_)
    {
        nh.param("footprint", cfg_footprint_, std::string("[]"));
        nh.param("obstacle_ragne", cfg_obstacle_detection_range_from_surface_, 0.2);

        costmap_2d::makeFootprintFromString(cfg_footprint_, footprint_);

        costmap_ros_ = new costmap_2d::Costmap2DROS("local_costmap", tf_);
        costmap_ros_->pause();

        costmap_util_.initialize(costmap_ros_);

        costmap_ros_->start();
    }

    virtual ~CommonDriver() {
        if(costmap_ros_) {
            costmap_ros_->stop();
            delete costmap_ros_;
        }
    }

    tf2_ros::Buffer* get_tf() {
        return &tf_;
    }

    void clear() {
        driver_state_pub_.clear();
    }

    CommonDriverStatePublisher::State get_state(std::string& details) {
        details = driver_state_pub_.get_detailed_message();
        return driver_state_pub_.get_state();
    }

    virtual bool compute_velocity(bool enable_collision_detection, geometry_msgs::Twist& cmd_vel) = 0;
};


class ManualDriver : public CommonDriver
{
    std::string cfg_cmd_vel_topic_;
    ros::Subscriber sub_cmd_vel_;
    ros::Time time_input_;
    double cfg_timeout_;
    geometry_msgs::Twist cmd_vel_;

    void cb_cmd_vel(const geometry_msgs::Twist::ConstPtr& msg) {
        boost::mutex::scoped_lock l(mutex_);
        cmd_vel_ = *msg;
        time_input_ = ros::Time::now();
    }

public:
    ManualDriver(ros::NodeHandle& nh)
    : CommonDriver(nh)
    {
        nh.param("cmd_vel_topic", cfg_cmd_vel_topic_, std::string("/manual_control/cmd_vel"));
        nh.param("timeout", cfg_timeout_, 0.3);

        time_input_ = ros::Time::now();
        sub_cmd_vel_ = nh.subscribe(cfg_cmd_vel_topic_, 10, &ManualDriver::cb_cmd_vel, this);
    }

    bool compute_velocity(bool enable_collision_detection, geometry_msgs::Twist& cmd_vel) {
        costmap_2d::Costmap2D* cm = costmap_ros_->getCostmap();
        boost::unique_lock<costmap_2d::Costmap2D::mutex_t> costmap_lock(*cm->getMutex());

        cmd_vel = geometry_msgs::Twist();

        geometry_msgs::PoseStamped robot_pose_msg;
        if (!costmap_ros_->getRobotPose(robot_pose_msg)) {
            std::string e = "could not get robot pose";
            driver_state_pub_.set_state(CommonDriverStatePublisher::ERROR_COSTMAP, "could not get robot pose in the global frame of costmap");
            return false;
        }

        tf::Stamped<tf::Pose> robot_pose_g;
        tf::poseStampedMsgToTF(robot_pose_msg, robot_pose_g);

        if(enable_collision_detection) {
            int footprint_cost = costmap_util_.get_footprint_cost(footprint_, robot_pose_g);
            if(footprint_cost < 0) {
                driver_state_pub_.set_state(CommonDriverStatePublisher::ERROR_COLLISION, "collision detected");
                return false;
            }

            if(collision_detection(footprint_) == false) {
                return false;
            }
        }

        {
            boost::mutex::scoped_lock l(mutex_);
            double dt = (ros::Time::now() - time_input_).toSec();
            if(dt < cfg_timeout_) {
                cmd_vel = cmd_vel_;
                return true;
            }
            else {
                std::string message = workerbee_utils::string_format("%.2f sec", dt);
                driver_state_pub_.set_state(CommonDriverStatePublisher::ERROR_TIMEOUT, message);
                return false;
            }
        }
    }
};


class PoseRecoveryDriver : public CommonDriver
{
    double rotation_speed_;

public:
    PoseRecoveryDriver(ros::NodeHandle& nh)
    : CommonDriver(nh)
    {
        nh.param("rotation_speed", rotation_speed_, 60 * M_PI/180);
    }

    bool compute_velocity(bool enable_collision_detection, geometry_msgs::Twist& cmd_vel) {
        costmap_2d::Costmap2D* cm = costmap_ros_->getCostmap();
        boost::unique_lock<costmap_2d::Costmap2D::mutex_t> costmap_lock(*cm->getMutex());

        cmd_vel = geometry_msgs::Twist();

        geometry_msgs::PoseStamped robot_pose_msg;
        if (!costmap_ros_->getRobotPose(robot_pose_msg)) {
            std::string e = "could not get robot pose";
            driver_state_pub_.set_state(CommonDriverStatePublisher::ERROR_COSTMAP, "could not get robot pose in the global frame of costmap");
            return false;
        }

        tf::Stamped<tf::Pose> robot_pose_g;
        tf::poseStampedMsgToTF(robot_pose_msg, robot_pose_g);

        if(enable_collision_detection) {
            // collision detection
            int footprint_cost = costmap_util_.get_footprint_cost(footprint_, robot_pose_g);
            if(footprint_cost < 0) {
                driver_state_pub_.set_state(CommonDriverStatePublisher::ERROR_COLLISION, "collision detected");
                return false;
            }

            if(collision_detection(footprint_) == false) {
                return false;
            }
        }

        cmd_vel.angular.z = rotation_speed_;

        return true;
    }
};



#endif /* __DRIVER_COMMON_H__ */
