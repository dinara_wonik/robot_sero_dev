/*
 * moveto_driver_node.cpp
 *
 *  Created on: Jul 2, 2019
 *      Author: ('c')void
 */


#define DIRECT_INTERFACE 0


#include <string>
#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <stdexcept>
#include <iostream>
#include <list>
#include <vector>

#include <ros/ros.h>
#include <pluginlib/class_loader.hpp>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/static_transform_broadcaster.h>

#if DIRECT_INTERFACE
#include <water_strider/local_planner_ros.h>
#else
#include <nav_core/base_local_planner.h>
#endif
#include <nav_core/base_global_planner.h>

#include <costmap_2d/costmap_2d_ros.h>
#include <costmap_2d/costmap_2d.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/GetPlan.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Vector3.h>
#include <std_srvs/Trigger.h>

#include <workerbee_utils/geometry_utils.h>
#include <workerbee_utils/interpolation.h>

#include <workerbee_platform/MoveToComputeVelocity.h>
#include <workerbee_platform/IsGoalReached.h>
#include <workerbee_platform/MoveToSetGoal.h>
#include <workerbee_status/workerbee_status.h>
#include <workerbee_utils/accessories.h>

#include <workerbee_platform_msgs/MoveToDriverState.h>

#include "accel_limit.h"


template <typename T>
class ThreadSafeQueue
{
    // https://gist.github.com/holtgrewe/8728757
public:
    // Returns whether could push/pop or queue was closed.  Currently, we only implemented the blocking
    // operations.
    enum QueueResult
    {
        OK,
        CLOSED
    };

    // Initialize the queue with a maximal size.
    explicit ThreadSafeQueue(size_t maxSize = 0) : state(State::OPEN), currentSize(0), maxSize(maxSize)
    {}

    // Push v to queue.  Blocks if queue is full.
    void push(T const & v)
    {
        // Create temporary queue.
        decltype(list) tmpList;
        tmpList.push_back(v);

        // Pushing with lock, only using list<>::splice().
        {
            std::unique_lock<std::mutex> lock(mutex);

            // Wait until there is space in the queue.
            while (currentSize == maxSize)
                cvPush.wait(lock);

            // Check that the queue is not closed and thus pushing is allowed.
            if (state == State::CLOSED)
                throw std::runtime_error("Trying to push to a closed queue.");

            // Push to queue.
            currentSize += 1;
            list.splice(list.end(), tmpList, tmpList.begin());

            // Wake up one popping thread.
            if (currentSize == 1u)
                cvPop.notify_one();
        }
    }

    // Push to queue with rvalue reference.
    void push(T && v)
    {
        // Create temporary queue.
        decltype(list) tmpList;
        tmpList.push_back(v);

        // Pushing with lock, only using list<>::splice().
        {
            std::unique_lock<std::mutex> lock(mutex);

            // Wait until there is space in the queue.
            while (currentSize == maxSize)
                cvPush.wait(lock);

            // Check that the queue is not closed and thus pushing is allowed.
            if (state == State::CLOSED)
                throw std::runtime_error("Trying to push to a closed queue.");

            // Push to queue.
            currentSize += 1;
            list.splice(list.end(), tmpList, tmpList.begin());

            // Wake up one popping thread.
            cvPop.notify_one();
        }
    }

    // Pop value from queue and write to v.
    //
    // If this succeeds, OK is returned.  CLOSED is returned if the queue is empty and was closed.
    QueueResult pop(T & v)
    {
        decltype(list) tmpList;

        // Pop into tmpList which is finally written out.
        {
            std::unique_lock<std::mutex> lock(mutex);

            // If there is no item then we wait until there is one.
            while (list.empty() && state != State::CLOSED)
                cvPop.wait(lock);

            // If the queue was closed and the list is empty then we cannot return anything.
            if (list.empty() && state == State::CLOSED)
                return CLOSED;

            // If we reach here then there is an item, get it.
            currentSize -= 1;
            tmpList.splice(tmpList.begin(), list, list.begin());
            // Wake up one pushing thread.
            cvPush.notify_one();
        }

        // Write out value.
        v = tmpList.front();

        return OK;
    }

    // Pushing data is not allowed any more, when the queue is
    void close() noexcept
    {
        std::unique_lock<std::mutex> lock(mutex);
        state = State::CLOSED;

        // Notify all consumers.
        cvPop.notify_all();
    }

    size_t size() {
        std::unique_lock<std::mutex> lock(mutex);
        return currentSize;
    }

    size_t max_size() {
        return maxSize;
    }

    bool is_full() {
        std::unique_lock<std::mutex> lock(mutex);
        return (currentSize >= maxSize);
    }

private:

    // Whether the queue is running or closed.
    enum class State
    {
        OPEN,
        CLOSED
    };

    // The current state.
    State state;
    // The current size.
    size_t currentSize;
    // The maximal size.
    size_t maxSize;
    // The condition variables to use for pushing/popping.
    std::condition_variable cvPush, cvPop;
    // The mutex for locking the queue.
    std::mutex mutex;
    // The list that the queue is implemented with.
    std::list<T> list;
};




/*
 *  NOTE.
 *   - local planner의 State 를 받으려면 plugin을 쓰지 않는게 좋다.
 */

std::string code2str(int code)
{
    switch(code) {
    case workerbee_platform_msgs::MoveToDriverState::NO_ERROR:
        return "NO_ERROR";
    case workerbee_platform_msgs::MoveToDriverState::GOAL_REACHED:
        return "GOAL_REACHED";
    case workerbee_platform_msgs::MoveToDriverState::ERROR_PLANNER:
        return "ERROR_PLANNER";
    case workerbee_platform_msgs::MoveToDriverState::ERROR_CONTROLLER:
        return "ERROR_CONTROLLER";
    case workerbee_platform_msgs::MoveToDriverState::ERROR_CONDITION:
        return "ERROR_CONDITION";
    case workerbee_platform_msgs::MoveToDriverState::ERROR_ETC:
        return "ERROR_ETC";
    default:
        return workerbee_utils::string_format("UNKNOWN_CODE(%d)", code);
    }
}


class MoveToDriverNode
{
    tf2_ros::Buffer tf_;
    tf2_ros::TransformListener tfl_;

    std::string local_planner_;
    std::string global_planner_;
    std::string robot_frame_id_;
    std::string robot_heading_frame_id_;

    tf2_ros::StaticTransformBroadcaster broadcaster_;

#if DIRECT_INTERFACE
    boost::shared_ptr<water_strider::LocalPlannerROS> controller_;
#else
    pluginlib::ClassLoader<nav_core::BaseLocalPlanner> blp_loader_;
    boost::shared_ptr<nav_core::BaseLocalPlanner> controller_;
#endif

    std::mutex controller_mutex_;
    costmap_2d::Costmap2DROS* controller_costmap_ros_;
    costmap_2d::Costmap2DROS* planner_costmap_ros_;

    pluginlib::ClassLoader<nav_core::BaseGlobalPlanner> bgp_loader_;
    boost::shared_ptr<nav_core::BaseGlobalPlanner> planner_;

    ThreadSafeQueue<workerbee_platform::MoveToSetGoal::Request> queue_;
    boost::shared_ptr<boost::thread> thread_;
    bool thread_exit_;
    bool planning_busy_;
    ros::Publisher pub_planning_state_;

    AccelerationLimitFilter accel_filter_;

    std::vector<ros::ServiceServer> servers_;

    void set_heading(bool drive_in_reverse) {
        // 지원하는 경우 좌표변환
        tf2::Quaternion quat;

#if 0
        // SeRo 는 drive_in_reverse 지원 안함.
        quat.setRPY(0, 0, 0);
#else
        if(drive_in_reverse) {
            quat.setRPY(0, 0, M_PI);
        }
        else {
            quat.setRPY(0, 0, 0);
        }
#endif

        geometry_msgs::TransformStamped transform_msg;
        transform_msg.transform.rotation.x = quat.x();
        transform_msg.transform.rotation.y = quat.y();
        transform_msg.transform.rotation.z = quat.z();
        transform_msg.transform.rotation.w = quat.w();
        transform_msg.header.stamp = ros::Time::now();
        transform_msg.header.frame_id = robot_frame_id_;
        transform_msg.child_frame_id = robot_heading_frame_id_;

        broadcaster_.sendTransform(transform_msg);
    }

    bool srv_compute_velocity(workerbee_platform::MoveToComputeVelocity::Request& req, workerbee_platform::MoveToComputeVelocity::Response& rsp) {
        std::unique_lock<std::mutex> lock(controller_mutex_);
        geometry_msgs::Twist cmd_vel;

        costmap_2d::Costmap2D* cm = controller_costmap_ros_->getCostmap();
        boost::unique_lock<costmap_2d::Costmap2D::mutex_t> costmap_lock(*cm->getMutex());

        bool success = controller_->computeVelocityCommands(cmd_vel);
        if(!success) {
            std::string details;
            workerbee_status::get_info(std::string("LocalPlanner/state"), details);
            rsp.state.code = workerbee_platform_msgs::MoveToDriverState::ERROR_CONTROLLER;
            rsp.state.details = workerbee_utils::string_format("%s. %s", code2str(rsp.state.code).c_str(), details.c_str());
            cmd_vel = geometry_msgs::Twist();
        }

        {
            geometry_msgs::Vector3 vi, vo;

            try {
                geometry_msgs::TransformStamped trans = tf_.lookupTransform(robot_frame_id_, controller_costmap_ros_->getBaseFrameID(), ros::Time(0));

                vi = cmd_vel.linear;
                tf2::doTransform(vi, vo, trans);
                cmd_vel.linear = vo;

                vi = cmd_vel.angular;
                tf2::doTransform(vi, vo, trans);
                cmd_vel.angular = vo;
            }
            catch (tf::TransformException e)
            {
                ROS_WARN("Failed to compute robot platform pose (%s)", e.what());
            }
        }

        rsp.cmd_vel = accel_filter_.filtering(cmd_vel);
        return true;
    }

    bool srv_set_goal(workerbee_platform::MoveToSetGoal::Request& req, workerbee_platform::MoveToSetGoal::Response& rsp) {
        if(req.wait_for_completion) {
            rsp.state = planning(req);
        }
        else {
            queue_.push(req);
        }

        return true;
    }

    bool srv_is_goal_reached(workerbee_platform::IsGoalReached::Request& req, workerbee_platform::IsGoalReached::Response& rsp) {
        if(planning_busy_ == false) {
            rsp.state = controller_->isGoalReached();
        }
        else {
            rsp.state = false;
        }
        return true;
    }

    bool srv_reset(std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& rsp) {
        accel_filter_.reset();
        rsp.success = true;
        return true;
    }

    workerbee_platform_msgs::MoveToDriverState planning(workerbee_platform::MoveToSetGoal::Request& request) {
        costmap_2d::Costmap2D* cm = planner_costmap_ros_->getCostmap();
        boost::unique_lock<costmap_2d::Costmap2D::mutex_t> costmap_lock(*cm->getMutex());

        planning_busy_ = true;

        geometry_msgs::PoseStamped goal = request.goal;

        workerbee_platform_msgs::MoveToDriverState state;
        std::string details;

        std::vector<geometry_msgs::PoseStamped> plan;
        geometry_msgs::PoseStamped robot_pose_msg;
        geometry_msgs::PoseStamped end;

        plan.clear();


        if(request.drive_in_reverse) {
            /*
             *  지원하지 않으면 에러 리턴.
             */
        }

        do {
            if(!planner_costmap_ros_->getRobotPose(robot_pose_msg)) {
                state.code = workerbee_platform_msgs::MoveToDriverState::ERROR_ETC;
                state.details = workerbee_utils::string_format("move_to_driver_node.planning() could not get robot pose");
                break;
            }

            if(!workerbee_utils::Geometry::transform_pose(tf_, planner_costmap_ros_->getGlobalFrameID(), goal, end)) {
                state.code = workerbee_platform_msgs::MoveToDriverState::ERROR_ETC;
                state.details = workerbee_utils::string_format("move_to_driver_node.planning() transform error");
                break;
            }

            // z 가 0이 아니면 2D navigation에서 간혹 에러가 나는 수 있다.
            robot_pose_msg.pose.position.z = 0;
            end.pose.position.z = 0;

            if(request.disable_global_path_planning) {
                robot_pose_msg.header.stamp = ros::Time::now();
                end.header.stamp = ros::Time::now();

                // make simple path
                std::vector<geometry_msgs::PoseStamped> plan_tmp;
                plan_tmp.push_back(robot_pose_msg);
                plan_tmp.push_back(end);

                workerbee_utils::ListInterpolationWithSpace<geometry_msgs::PoseStamped> interp(plan_tmp, "moveto_driver::planning");
                if(!interp.interpolate_list(plan, 0.1)) {
                    state.code = workerbee_platform_msgs::MoveToDriverState::ERROR_PLANNER;
                    state.details = workerbee_utils::string_format("%s. %s", code2str(state.code).c_str(), "simple planning error");
                    break;
                }
            }
            else {
                if(!planner_->makePlan(robot_pose_msg, end, plan)) {
                    state.code = workerbee_platform_msgs::MoveToDriverState::ERROR_PLANNER;
                    state.details = workerbee_utils::string_format("%s. %s", code2str(state.code).c_str(), "global planning error");
                    break;
                }
            }

            {
                std::unique_lock<std::mutex> lock(controller_mutex_);

                set_heading(request.drive_in_reverse);

                if(!controller_->setPlan(plan)) {
                    workerbee_status::get_info(std::string("LocalPlanner/state"), details);
                    state.code = workerbee_platform_msgs::MoveToDriverState::ERROR_CONTROLLER;
                    state.details = workerbee_utils::string_format("%s. %s", code2str(state.code).c_str(), details.c_str());
                    break;
                }
            }
        } while(false);

        planning_busy_ = false;

        return state;
    }

    void planning_proc() {
        std::vector<workerbee_platform::MoveToSetGoal::Request> requests;

        while(true) {
            workerbee_platform::MoveToSetGoal::Request req;
            requests.clear();

            do {
                queue_.pop(req);
                requests.push_back(req);
            } while(queue_.size() > 0);

            if(thread_exit_){
                return;
            }

            pub_planning_state_.publish(planning(requests.back()));
        }
    }

#if 0
    bool srv_set_plan(workerbee_platform::MoveToSetPlan::Request& req, workerbee_platform::MoveToSetPlan::Response& rsp) {
        bool success = controller_->setPlan(req.global_plan);
        if(!success) {
            std::string details;
            workerbee_status::get_info(std::string("LocalPlanner/state"), details);
            rsp.state.code = workerbee_platform_msgs::MoveToDriverState::ERROR_CONTROLLER;
            rsp.state.details = workerbee_utils::string_format("%s. %s", code2str(rsp.state.code).c_str(), details.c_str());
        }

        return true;
    }

    bool make_plan_service(workerbee_platform::MoveToMakeNavPlan::Request& req, workerbee_platform::MoveToMakeNavPlan::Response& rsp) {
        std::vector<geometry_msgs::PoseStamped> path;

        // FIXME. z 가 0이 아니면 2D navigation에서 간혹 에러가 나는 수 있다.
        geometry_msgs::PoseStamped goal_2d = req.goal;
        goal_2d.pose.position.z = 0;

        bool success = planner_->makePlan(req.start, goal_2d, path);

        if(success) {
            rsp.path = path;
        }
        else {
            std::string details;
            workerbee_status::get_info(std::string("GlobalPlanner/state"), details);
            rsp.state.code = workerbee_platform_msgs::MoveToDriverState::ERROR_PLANNER;
            rsp.state.details = workerbee_utils::string_format("%s. %s", code2str(rsp.state.code).c_str(), details.c_str());
        }

        return true;
    }
#endif

public:
    MoveToDriverNode()
    : tf_(ros::Duration(10))
    , tfl_(tf_)
#if !DIRECT_INTERFACE
    , blp_loader_("nav_core", "nav_core::BaseLocalPlanner")
#endif
    , bgp_loader_("nav_core", "nav_core::BaseGlobalPlanner")
    , queue_(10)
    , thread_exit_(false)
    , planning_busy_(false)
    {
        ros::NodeHandle private_nh("~");
        private_nh.param("robot_frame_id", robot_frame_id_, std::string("base_link"));
        private_nh.param("robot_heading_frame_id", robot_heading_frame_id_, std::string("robot_head_link"));
        private_nh.param("base_local_planner", local_planner_, std::string("base_local_planner/TrajectoryPlannerROS"));
        private_nh.param("base_global_planner", global_planner_, std::string("navfn/NavfnROS"));

        double max_translational_acceleration;
        double max_rotational_acceleration;
        double control_freqeuncy;

        private_nh.param("max_translational_acceleration", max_translational_acceleration, 1.0);
        private_nh.param("max_rotational_acceleration",    max_rotational_acceleration,    3.0);
        private_nh.param("control_freqeuncy",              control_freqeuncy, 10.0);

        accel_filter_.set_accelerations(max_translational_acceleration, max_rotational_acceleration);
        accel_filter_.set_freq(control_freqeuncy);
        accel_filter_.enable(true);

        set_heading(false);

        controller_costmap_ros_ = new costmap_2d::Costmap2DROS("local_costmap", tf_);
        controller_costmap_ros_->pause();

#if DIRECT_INTERFACE
        controller_ = boost::shared_ptr<water_strider::LocalPlannerROS>(new water_strider::LocalPlannerROS);
        controller_->initialize("WsLocalPlanner", &tf_, controller_costmap_ros_);
#else
        try {
            controller_ = blp_loader_.createInstance(local_planner_);
            ROS_INFO("Created local_planner %s", local_planner_.c_str());
            controller_->initialize(blp_loader_.getName(local_planner_), &tf_, controller_costmap_ros_);
        } catch (const pluginlib::PluginlibException& ex) {
            ROS_FATAL("Failed to create the %s planner, are you sure it is properly registered and that the containing library is built? Exception: %s", local_planner_.c_str(), ex.what());
            exit(1);
        }
#endif

        planner_costmap_ros_ = new costmap_2d::Costmap2DROS("global_costmap", tf_);
        planner_costmap_ros_->pause();

        try {
            planner_ = bgp_loader_.createInstance(global_planner_);
            planner_->initialize(bgp_loader_.getName(global_planner_), planner_costmap_ros_);
        } catch (const pluginlib::PluginlibException& ex) {
            ROS_FATAL("Failed to create the %s planner, are you sure it is properly registered and that the containing library is built? Exception: %s", global_planner_.c_str(), ex.what());
            exit(1);
        }

        /*
         *  ASSERTION !!!
         *  controller_costmap_ros_->getBaseFrameID() == robot_heading_frame_id_
         */

        controller_costmap_ros_->start();
        planner_costmap_ros_->start();

        pub_planning_state_ = private_nh.advertise<workerbee_platform_msgs::MoveToDriverState>("planning_state", 10);

        thread_ = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&MoveToDriverNode::planning_proc, this)));

        servers_.push_back(private_nh.advertiseService("compute_velocity", &MoveToDriverNode::srv_compute_velocity, this));
        servers_.push_back(private_nh.advertiseService("set_goal", &MoveToDriverNode::srv_set_goal, this));
        servers_.push_back(private_nh.advertiseService("is_goal_reached", &MoveToDriverNode::srv_is_goal_reached, this));
        servers_.push_back(private_nh.advertiseService("reset", &MoveToDriverNode::srv_reset, this));
    }

    virtual ~MoveToDriverNode() {
        if(controller_costmap_ros_) {
            delete controller_costmap_ros_;
        }

        thread_exit_ = true;
        queue_.push(workerbee_platform::MoveToSetGoal::Request());
    }
};


int main(int argc, char** argv) {
    ros::init(argc, argv, "move_to_driver_node");
    MoveToDriverNode node;
    ros::spin();
    return 0;
}



