/*
 * manual_driver_node.cpp
 *
 *  Created on: 2019.07.22
 *      Author: ('c')void
 */

#include "common.h"
#include <std_srvs/Trigger.h>
#include <workerbee_platform_msgs/ManualDriverState.h>
#include <workerbee_platform/ManualDrivingComputeVelocity.h>

#include "accel_limit.h"


class ManualDriverNode
{
    ros::NodeHandle private_nh_;
    ManualDriver driver_;
    std::vector<ros::ServiceServer> servers_;

    AccelerationLimitFilter accel_filter_;

    std::string code2str(int code) {
        switch(code) {
        case workerbee_platform_msgs::ManualDriverState::NO_ERROR:
            return "NO_ERROR";
        case workerbee_platform_msgs::ManualDriverState::ERROR_CONTROLLER:
            return "ERROR_CONTROLLER";
        case workerbee_platform_msgs::ManualDriverState::ERROR_TIMEOUT:
            return "ERROR_TIMEOUT";
        case workerbee_platform_msgs::ManualDriverState::ERROR_ETC:
            return "ERROR_ETC";
        default:
            return workerbee_utils::string_format("UNKNOWN_CODE(%d)", code);
        }
    }

    bool srv_compute_velocity(workerbee_platform::ManualDrivingComputeVelocity::Request& req, workerbee_platform::ManualDrivingComputeVelocity::Response& rsp) {

        geometry_msgs::Twist cmd_vel;

        if(driver_.compute_velocity(req.collision_detection, cmd_vel) == false) {
            cmd_vel = geometry_msgs::Twist();
            std::string details;
            CommonDriverStatePublisher::State state = driver_.get_state(details);

            switch(state) {
                case CommonDriverStatePublisher::ERROR_COSTMAP:
                case CommonDriverStatePublisher::ERROR_COLLISION:
                case CommonDriverStatePublisher::ERROR_OBSTACLE:
                    rsp.state.code = workerbee_platform_msgs::ManualDriverState::ERROR_CONTROLLER;
                    break;
                case CommonDriverStatePublisher::ERROR_TIMEOUT:
                    rsp.state.code = workerbee_platform_msgs::ManualDriverState::ERROR_TIMEOUT;
                    break;
                default:
                    rsp.state.code = workerbee_platform_msgs::ManualDriverState::ERROR_ETC;
                    break;
            }

            rsp.state.details = workerbee_utils::string_format("%s. %s", code2str(rsp.state.code).c_str(), details.c_str());
        }
        else {
            rsp.state.code = workerbee_platform_msgs::ManualDriverState::NO_ERROR;
        }

        driver_.clear();

        rsp.cmd_vel = accel_filter_.filtering(cmd_vel);

        return true;
    }

    bool srv_reset(std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& rsp) {
        accel_filter_.reset();
        rsp.success = true;
        return true;
    }

public:
    ManualDriverNode() : private_nh_("~"), driver_(private_nh_){

        double max_translational_acceleration;
        double max_rotational_acceleration;
        double control_freqeuncy;

        private_nh_.param("max_translational_acceleration", max_translational_acceleration, 1.0);
        private_nh_.param("max_rotational_acceleration",    max_rotational_acceleration,    3.0);
        private_nh_.param("control_freqeuncy",              control_freqeuncy, 10.0);

        accel_filter_.set_accelerations(max_translational_acceleration, max_rotational_acceleration);
        accel_filter_.set_freq(control_freqeuncy);
        accel_filter_.enable(true);

        servers_.push_back(private_nh_.advertiseService("compute_velocity", &ManualDriverNode::srv_compute_velocity, this));
        servers_.push_back(private_nh_.advertiseService("reset", &ManualDriverNode::srv_reset, this));
    }
    ~ManualDriverNode() {}
};


int main(int argc, char** argv) {
    ros::init(argc, argv, "manual_driver_node");
    ManualDriverNode node;
    ros::spin();
    return 0;
}



