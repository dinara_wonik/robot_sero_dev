#!/usr/bin/python
# -*- coding: utf8 -*-#

'''
2019.10.30 ('c')void 
'''

import os 
import sys
import time 
import traceback
import thread, threading 
import numpy as np 
import rospy
import tf 
import tf.transformations as trans
import wr_map_server.srv
import workerbee_platform.srv
from workerbee_utils.utils import *
from workerbee_platform_msgs.msg import MapUpdateDriverState
from workerbee_utils.utils import handler_guard, lookup_pose
from geometry_msgs.msg import PoseWithCovarianceStamped


class MapUpdater(object):
    def __init__(self, *args, **kwargs):
        self._lock = threading.Lock()
        self._request = None  
        self._inprogress = False
        
        self._tfl = tf.TransformListener()  
        
        self._state = MapUpdateDriverState()
                
        self._services =[] 
        s = rospy.Service('~map_update', workerbee_platform.srv.MapUpdate, self._srv_map_update)
        self._services.append(s)
        s = rospy.Service('~map_update_progress', workerbee_platform.srv.MapUpdateProgress, self._srv_map_update_progress)
        self._services.append(s)    
    
    def _srv_map_update(self, req):
        rospy.loginfo('## MapUpdater.map_update. req=%s\n'%(str(req)))
        rsp = workerbee_platform.srv.MapUpdateResponse()
        state = MapUpdateDriverState()
        
        while(True):
            if(self._inprogress): 
                state.code = MapUpdateDriverState.ERROR_ETC
                state.details = 'busy(%s)'%(self._request.filename)           
                break  
                    
            if(not os.path.exists(req.filename)):
                state.code = MapUpdateDriverState.ERROR_FILE_NOT_EXIST
                state.details = '%s is not exist'%(req.filename)
                break 
            
            pos = position2array(req.init_pose.pose.pose.position)
            if(valid_position(pos) == False):
                state.code = MapUpdateDriverState.ERROR_INVALID_INITPOSE
                state.details = 'invalid position %s'%(str(pos))        
                break     
    
            orientation = orientation2array(req.init_pose.pose.pose.orientation)            
            if(valid_orientation(orientation) == False):
                state.code = MapUpdateDriverState.ERROR_INVALID_INITPOSE
                state.details = 'invalid orientation %s'%(str(orientation))  
                break 
            
            self._state.code = MapUpdateDriverState.BUSY
            self._state.details = 'busy(%s)'%(req.filename)  
            self._inprogress = True
            
            self._request = req  
            self._thread = thread.start_new_thread(self.update_process, ())
            break 
        
        rsp.state = state 
        return rsp 
    
    def _srv_map_update_progress(self, req):
        rsp = workerbee_platform.srv.MapUpdateProgressResponse() 
        with self._lock:
            rsp.state = self._state 
        return rsp       
    
    def update_process(self):  
        try:
            self._map_update_process() 
        except:
            exception_str = traceback.format_exc()
            rospy.logerr("%s.%s"%(rospy.get_name(), exception_str))
            with self._lock:
                self._state.code = MapUpdateDriverState.ERROR_ETC
                self._state.details = exception_str
                
        self._inprogress = False
            
    def _map_update_process(self):        
        # 
        # NOTE) 초기위치를 ros parameter로 전달하기 위해선 AMCL의 save_pose_rate = -1 이어야 함. 
        #
        initial_pose_x, initial_pose_y, _ = position2array(self._request.init_pose.pose.pose.position)
        _, _, initial_pose_a = trans.euler_from_quaternion(orientation2array(self._request.init_pose.pose.pose.orientation)) 
        cov = np.array(self._request.init_pose.pose.covariance).reshape((6,6))
                
        rospy.set_param('/amcl/initial_pose_x', float(initial_pose_x))
        rospy.set_param('/amcl/initial_pose_y', float(initial_pose_y))
        rospy.set_param('/amcl/initial_pose_a', float(initial_pose_a))
        
        # 딱히 당장은 cov까지 필요 없겠다. 
        # rospy.set_param('/amcl/initial_cov_xx', float(cov[0,0]))
        # rospy.set_param('/amcl/initial_cov_yy', float(cov[1,1]))
        # rospy.set_param('/amcl/initial_cov_aa', float(cov[5,5]))
                
        # rospy.wait_for_service(service, timeout=None) 
        rospy.wait_for_service('map_update')
        map_update = rospy.ServiceProxy('map_update', wr_map_server.srv.MapUpdate)
        rsp = map_update(self._request.filename)
        
        if(rsp.state == False):
            with self._lock:
                self._state.code = MapUpdateDriverState.ERROR_MAPSERVER
                self._state.details = rsp.error
            return 

        # waitting 
        while(True):
            try:
                robot_pose = lookup_pose(self._tfl, target_frame=self._request.init_pose.header.frame_id, source_frame=self._request.robot_frameid)
                distance = np.array((initial_pose_x, initial_pose_y)) - position2array(robot_pose.pose.position)[:2]
                if(np.linalg.norm(distance) < 1.0):
                    break  
            except:
                traceback.print_exc() 
            
            time.sleep(0.5)
                    
        with self._lock:
            self._state.code = MapUpdateDriverState.UPDATE_COMPLETE
            self._state.details = ''         
        
        rospy.loginfo('MAP UPDATE COMPLETE. %s'%(self._request.filename))


if __name__ == '__main__':
    rospy.init_node('map_update_driver_node')
    m = MapUpdater()
    rospy.spin()