#!/usr/bin/python
# -*- coding: utf8 -*-#
import sys
import numpy as np 
import rospy
from sero_lpt_controller.msg import LPTJoint
from sero_lpt_controller.srv import LptControl

service_name = '/sero_lpt_controller/command'


def lpt_set_goal(lift, pan, tilt):
    rospy.wait_for_service(service_name)
    try:
        proxy = rospy.ServiceProxy(service_name, LptControl)
        rsp = proxy(lift, pan, tilt)
        return rsp
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


if __name__ == "__main__": 

    command = [0.0, 0.0, 0.0]

    if len(sys.argv) == 4 :
        command = [float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3])]    

    lift = LPTJoint()
    lift.apply = True
    lift.value = command[0]

    pan = LPTJoint()
    pan.apply = True
    pan.value = command[1]

    tilt = LPTJoint()
    tilt.apply = True
    tilt.value = command[2]

    rsp = lpt_set_goal(lift, pan, tilt)
    print rsp
    
