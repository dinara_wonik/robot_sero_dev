#!/usr/bin/env python
# -*- coding: utf8 -*-#

#
# 2020.07.23 jglee
#

import sys
import os

import traceback
import numpy as np 
import rospy  

from sensor_msgs.msg import Joy
from control_msgs.msg import JointTrajectoryControllerState
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sero_lpt_controller.msg import LPTJoint

from time import sleep

import xml.dom.minidom
from sero_mobile.srv import *
from sero_lpt_controller.srv import *

service_vel_name = '/sero_mobile/lpt_set_velocity'
service_pos_name = '/sero_mobile/lpt_set_position'

def lpt_set_velocity(lift, pan, tilt):
    rospy.wait_for_service(service_vel_name)
    try:
        proxy = rospy.ServiceProxy(service_vel_name, LptSetVelocity)
        rsp = proxy(lift, pan, tilt)
        return rsp
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

def lpt_set_position(lift, pan, tilt):
    rospy.wait_for_service(service_pos_name)
    try:
        proxy = rospy.ServiceProxy(service_pos_name, LptSetPosition)
        rsp = proxy(lift, pan, tilt)
        return rsp
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

def handler_guard(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            # rospy.logerr("%s.%s"%(rospy.get_name(), str(e)))
            rospy.logerr("%s.%s" % (rospy.get_name(), traceback.format_exc()))
            return None

    return wrapper

def get_joint_limits(key='robot_description', use_smallest_joint_limits=True):
    use_small = use_smallest_joint_limits
    use_mimic = True

    description = rospy.get_param(key)
    robot = xml.dom.minidom.parseString(description)\
        .getElementsByTagName('robot')[0]
    free_joints = {}
    dependent_joints = {}

    # Find all non-fixed joints
    for child in robot.childNodes:
        if child.nodeType is child.TEXT_NODE:
            continue
        if child.localName == 'joint':
            jtype = child.getAttribute('type')
            if jtype == 'fixed':
                continue
            name = child.getAttribute('name')
            try:
                limit = child.getElementsByTagName('limit')[0]
            except:
                continue
            if jtype == 'continuous':
                minval = -pi
                maxval = pi
            else:
                try:
                    minval = float(limit.getAttribute('lower'))
                    maxval = float(limit.getAttribute('upper'))
                except:
                    continue
            try:
                maxvel = float(limit.getAttribute('velocity'))
            except:
                continue
            safety_tags = child.getElementsByTagName('safety_controller')
            if use_small and len(safety_tags) == 1:
                tag = safety_tags[0]
                if tag.hasAttribute('soft_lower_limit'):
                    minval = max(minval,
                                 float(tag.getAttribute('soft_lower_limit')))
                if tag.hasAttribute('soft_upper_limit'):
                    maxval = min(maxval,
                                 float(tag.getAttribute('soft_upper_limit')))

            mimic_tags = child.getElementsByTagName('mimic')
            if use_mimic and len(mimic_tags) == 1:
                tag = mimic_tags[0]
                entry = {'parent': tag.getAttribute('joint')}
                if tag.hasAttribute('multiplier'):
                    entry['factor'] = float(tag.getAttribute('multiplier'))
                if tag.hasAttribute('offset'):
                    entry['offset'] = float(tag.getAttribute('offset'))

                dependent_joints[name] = entry
                continue

            if name in dependent_joints:
                continue

            joint = {'min_position': minval, 'max_position': maxval}
            joint["has_position_limits"] = jtype != 'continuous'
            joint['max_velocity'] = maxvel
            free_joints[name] = joint
    return free_joints


class LptTrajectoryController(object):
    def __init__(self): 
        state_topic = '/head_controller/state'
        cmd_topic = '/head_controller/command'        

        self._state_sub = rospy.Subscriber(state_topic, JointTrajectoryControllerState, self._state_cb, queue_size=1)
        self._cmd_pub   = rospy.Publisher(cmd_topic, JointTrajectory, queue_size=1)

        self._joint_pos = {'joint3': {'position': 0.0,'command': 0.0},  'joint2': {'position': 0.0,'command': 0.0}, 'joint1': {'position': 0.0, 'command': 0.0}}   # name->pos map for joints of selected controller

        self._joint_names = []  # Ordered list of selected controller joints
        self._robot_joint_limits = get_joint_limits()  # Lazy evaluation

        self._init = True
        self._speed_scale = 0.8 # 50% / range 0.01(1%) ~ 1.0(100%)
        self._joint_names = ['joint1', 'joint2', 'joint3']

        self._cmd_pub_freq = 5.0  # Hz

        self._min_traj_dur = 5.0 / self._cmd_pub_freq  # Minimum trajectory duration
        self._input_target = [0.0, 0.0, 0.0]
        self._service = rospy.Service("~command", LptControl, self._service_server)


    @handler_guard    
    def _state_cb(self, msg):        
        actual_pos = {}
        # print "lift( p:%.2f, v:%.2f), pan( p:%.2f, v:%.2f), tilt( p:%.2f, v:%.2f)"%(msg.actual.positions[0],msg.actual.velocities[0],msg.actual.positions[1],msg.actual.velocities[1],msg.actual.positions[2],msg.actual.velocities[2])
        if (self._init):
            self._init = False
            self._joint_names = msg.joint_names
            print self._joint_names

        for i in range(len(msg.joint_names)):
            joint_name = msg.joint_names[i]
            joint_pos = msg.actual.positions[i]
            self._joint_pos[joint_name]['position'] = joint_pos
        
        lpt_set_velocity(msg.actual.velocities[0]*1000, msg.actual.velocities[1], msg.actual.velocities[2])
        lpt_set_position(msg.actual.positions[0]*1000,  msg.actual.positions[1],  msg.actual.positions[2])

    @handler_guard
    def _update_cmd(self,rcv_command):
        # print "publish command data!!!"
        self._input_target = rcv_command;
        # print "command : joint1(%.2f), joint2(%.2f), joint3(%.2f)"%(rcv_command[0], rcv_command[1],rcv_command[2])
        self._joint_pos['joint1']['command'] = rcv_command[0]
        self._joint_pos['joint2']['command'] = rcv_command[1]
        self._joint_pos['joint3']['command'] = rcv_command[2]
        dur = []
        traj = JointTrajectory()
        traj.joint_names = self._joint_names
        point = JointTrajectoryPoint()
        for name in traj.joint_names:
            pos = self._joint_pos[name]['position']
            try:
                cmd = self._joint_pos[name]['command']
            except (KeyError):
                pass
            max_vel = self._robot_joint_limits[name]['max_velocity']
            dur.append(max(abs(cmd - pos) / max_vel, self._min_traj_dur))
            point.positions.append(cmd)
        point.time_from_start = rospy.Duration(max(dur) / self._speed_scale)
        traj.points.append(point)

        print "_cmd_pub_freq : ",self._cmd_pub_freq
        self._cmd_pub.publish(traj)   
        
    def _service_server(self,req):
        print "lpt_controller_service_call : "       
        print "lift : (",req.lift.apply,",",req.lift.value,"), pan : (",req.pan.apply,",",req.pan.value,"), tilt : (",req.tilt.apply,",",req.tilt.value,")"

        if req.lift.apply :
            lift = req.lift.value
        else :
            lift = self._joint_pos['joint1']['position'] 

        if req.pan.apply :
            pan = req.pan.value
        else :
            pan = self._joint_pos['joint2']['position']

        if req.tilt.apply : 
            tilt = req.tilt.value
        else :
            tilt = self._joint_pos['joint3']['position']
               
        self._update_cmd([lift,pan,tilt])
        return True

NODE_NAME = 'test_lpt_trajectory_control_node'

if __name__ == '__main__':
    rospy.init_node(NODE_NAME)
    rospy.loginfo("start %s"%(rospy.get_name()))
    
    traj = LptTrajectoryController()

    command = [0.0, 0.0, 0.0]

    if len(sys.argv) == 4 :
            command = [float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3])]
    traj._update_cmd(command)

    rospy.spin()
