#!/usr/bin/env python
# -*- coding: utf8 -*-#

#
# 2020.07.17 ('c')void 
#  - trajectory controller 를 사용하지 않는 임시버전 
#

import sys
import os

import traceback
import numpy as np 
import rospy  

from sensor_msgs.msg import Joy
from control_msgs.msg import JointTrajectoryControllerState
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sero_lpt_controller.msg import LPTJoint

from time import sleep

import xml.dom.minidom
from sero_mobile.srv import *
from sero_lpt_controller.srv import *


service_vel_name = '/sero_mobile/lpt_set_velocity'
service_pos_name = '/sero_mobile/lpt_set_position'

def lpt_set_velocity(lift, pan, tilt):
    rospy.wait_for_service(service_vel_name)
    try:
        proxy = rospy.ServiceProxy(service_vel_name, LptSetVelocity)
        rsp = proxy(lift, pan, tilt)
        return rsp
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

def lpt_set_position(lift, pan, tilt):
    rospy.wait_for_service(service_pos_name)
    try:
        proxy = rospy.ServiceProxy(service_pos_name, LptSetPosition)
        rsp = proxy(lift, pan, tilt)
        return rsp
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

def handler_guard(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            # rospy.logerr("%s.%s"%(rospy.get_name(), str(e)))
            rospy.logerr("%s.%s" % (rospy.get_name(), traceback.format_exc()))
            return None

    return wrapper 

class LptTrajectoryController(object):
    def __init__(self):  

        # positions 
        self._lift = 0
        self._pan = 0
        self._tilt = 0

        self._service = rospy.Service("~command", LptControl, self._service_server) 
    
    def _set_position(self, lift, pan, tilt):
        lpt_set_position(lift*1000, pan, tilt)     
        
        self._lift = lift 
        self._pan = pan 
        self._tilt = tilt 

    
    def _service_server(self, req):
        print "lpt_controller_service_call : "
        print "lift : (",req.lift.apply,",",req.lift.value,"), pan : (",req.pan.apply,",",req.pan.value,"), tilt : (",req.tilt.apply,",",req.tilt.value,")"

        if req.lift.apply:
            lift = req.lift.value
        else :
            lift = self._lift

        if req.pan.apply:
            pan = req.pan.value
        else :
            pan = self._pan

        if req.tilt.apply:
            tilt = req.tilt.value
        else :
            tilt = self._tilt

        self._set_position(lift, pan, tilt)

        return True


NODE_NAME = 'test_lpt_trajectory_control_node'

if __name__ == '__main__':
    rospy.init_node(NODE_NAME)
    rospy.loginfo("start %s"%(rospy.get_name()))
    
    t = LptTrajectoryController()

    sleep(1)   
        
    # # command = [0.5, 0.5, 0.5]
    command = [0.0, 0.0, 0.0]

    if len(sys.argv) == 4 :
        command = [float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3])]

    t._set_position(*command)

    rospy.spin()
