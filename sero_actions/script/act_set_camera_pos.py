#!/usr/bin/python
# -*- coding: utf8 -*-#

'''
2019.11.29 ('c')void 

set_camera_pose action  
'''

import time
import traceback
import numpy as np 
import json 
import rospy
import workerbee_actionlib
import tf
import tf.transformations as trans 

import workerbee_status
from sero_mobile_msgs.msg import LptDriverState, LptSetPositionState
from workerbee_msgs.msg import ActionState
from sero_actions.msg import SetCameraPoseAction, SetCameraPoseFeedback, SetCameraPoseResult
from sero_mobile.srv import LptSetPosition 
from sero_mobile_msgs.msg import LptPosVel

from geometry_msgs.msg import PoseStamped, Point, Quaternion
from workerbee_utils.utils import handler_guard, lookup_pose, transform_pose, orientation2array

# from workerbee_platform_config import WorkerbeeNavigationConfig 
# navigation_config = WorkerbeeNavigationConfig()


class Params(object):
    action_name=None 
    lpt_base_frame=None 


def msg2dict(ros_msg):
    return message_converter.convert_ros_message_to_dictionary(ros_msg) 


def create_action_state(code, details='', faults=[], error_conditions=[], driver_state=None): 
    import workerbee_msgs.act_common 
    return workerbee_msgs.act_common.create_action_state(ActionStateMsgClass=LptSetPositionState, 
                                                         code=code, 
                                                         details=details,
                                                         faults=faults,
                                                         error_conditions=error_conditions,
                                                         driver_state=driver_state)


def check_error_conditions():
    '''
    해당하는 조건의 InfoItem을 리턴한다.         
    workerbee_status.get_info('A\\B\\C')
    return {'Some condition': 'Some error'}
    ''' 
    return [] 



class LptPosVelReceiver(object):
    def __init__(self, topic):
        self._pos_vel = LptPosVel() 
        self._sub = rospy.Subscriber(topic, LptPosVel, self._callback)
    
    @property
    def pos_vel(self):
        return self._pos_vel 
    
    def _callback(self, pos_vel):
        self._pos_vel = pos_vel


class LptControllerProxy(object): 
    def __init__(self, tfl): 
        
        self._tfl = tfl
        
        self._lift = 0
        self._pan = 0
        self._tilt = 0

        # radian 
        self._angle_tolerance = 1 * np.pi/180 
        
        # mm  
        self._position_tolerance = 1.0 

        srv_lpt_set_position = '/sero_mobile_node/lpt_set_position' 
                        
        # global planner 
        rospy.wait_for_service(srv_lpt_set_position)  
        
        # proxy 
        self._lpt_set_position = rospy.ServiceProxy(srv_lpt_set_position, LptSetPosition) 
        
        self._pos = LptPosVelReceiver(topic='/sero_mobile_node/lpt') 
        
    def _transform(self, head_pose):   
        # lpt_tilt frame      
        pose_t = transform_pose(self._tfl, Params.lpt_base_frame, head_pose)

        # pan, tilt 
        mat_t2b = trans.quaternion_matrix(orientation2array(pose_t.pose.orientation))
        _, tilt, pan = trans.euler_from_matrix(mat_t2b)
         
        # lift(mm) 
        lift = pose_t.pose.position.z*1000
                                
        rospy.loginfo('SET_LPT(lift=%d mm, pan=%.3f deg, tilt=%.3f deg'%(lift, pan*180/np.pi, tilt*180/np.pi))    
        #rospy.loginfo('pose_t \n%s'%(pose_t))  
                                
        return (lift, pan, tilt) 

    @property
    def pos_vel(self):
        return self._pos.pos_vel

    def set_camera_pose(self, head_pose):
        lift_pan_tilt = self._transform(head_pose)
        return self.lpt_set_position(*lift_pan_tilt)
    
    def lpt_set_position(self, lift, pan, tilt): 
        '''
        '''
        self._lift = lift
        self._pan = pan
        self._tilt = tilt
                
        result = self._lpt_set_position(lift, pan, tilt)
        
        return result
    
    def is_goal_reached(self):
        error_lift = np.abs(self._lift - self.pos_vel.lift_pos)
        error_pan = np.abs(self._pan - self.pos_vel.pan_pos)
        error_tilt = np.abs(self._tilt - self.pos_vel.tilt_pos)
                                 
        goal_reached = (error_lift < self._position_tolerance and error_pan < self._angle_tolerance and error_tilt < self._angle_tolerance)
        
        if(goal_reached):
            return True 
        else:
            return False 


class ActionServer(workerbee_actionlib.MutuallyExclusiveActionServer):
    def __init__(self, action_server_group):
        super(ActionServer, self).__init__(action_server_group, Params.action_name, SetCameraPoseAction)
        self._action_server_group = action_server_group
        self._last_execution_time = None 
           
    def _result_for_preemption(self, new_action_name):
        result = SetCameraPoseResult()
        result.pose = lookup_pose(self.ag.transform_listener, target_frame=Params.global_frame, source_frame=Params.robot_frame) 
        result.state = create_action_state(code=ActionState.ERROR_PREEMPTED, details='PREEMPTED BY NEW ACTION(%s)'%(new_action_name))
        return result 
    
    
    def _excute_callback(self, goal):      
        rospy.loginfo('start SetCameraPoseAction. goal=\n%s'%(str(goal)))
        
        result = SetCameraPoseResult()
                
        controller = LptControllerProxy(self.ag.transform_listener)    

        res = controller.set_camera_pose(goal.head_pose)
        if(res.state.code != LptDriverState.NO_ERROR): 
            result.state = create_action_state(code=ActionState.ERROR_DRIVER, driver_state=res.state)
            self._as.set_aborted(result)    
            return
                        
        # start executing the action
        rate = rospy.Rate(10)
        self._last_execution_time = rospy.Time.now()
        while(True): 
            rate.sleep()

            result = SetCameraPoseResult()
            feedback = SetCameraPoseFeedback() 
            
            if(self._as.is_active() is False):
                # aborted
                break   

            # check complete condition 
            if(controller.is_goal_reached() is True):
                rospy.loginfo('%s: Succeeded' % Params.action_name) 
                result.state = create_action_state(code=ActionState.NO_ERROR)
                self._as.set_succeeded(result) 
                break

            # check faults 
            faults = workerbee_status.get_fault()
            if(len(faults) > 0):
                rospy.loginfo('%s: Fault' % Params.action_name)
                result.state = create_action_state(code=ActionState.ERROR_FAULT, faults=faults)
                self._as.set_aborted(result)
                break
            
            # check preemption
            if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % Params.action_name) 
                result.state = create_action_state(code=ActionState.ERROR_PREEMPTED, details='PREEMPTED BY NEW GOAL')
                self._as.set_preempted(result)
                break
            
            # check error conditions 
            #  e.g.) docking 
            error_conditions = check_error_conditions()
            if(len(error_conditions) > 0):
                rospy.loginfo('%s: Error Conditions' % Params.action_name) 
                result.state = create_action_state(code=ActionState.ERROR_CONDITION, error_conditions=error_conditions)   
                self._as.set_aborted(result)
                break            

            # check timeout      
#             duration_sec = (rospy.Time.now() - self._last_execution_time).to_sec()
#             if(duration_sec > goal.patience_timeout):
#                 rospy.loginfo('%s: TimeOut' % Params.action_name) 
#                 result.state = create_action_state(code=ActionState.ERROR_TIMEOUT, details='%.2f sec'%(duration_sec))
#                 self._as.set_aborted(result)
#                 self._send_zero_velocity() 
#                 break

            self._as.publish_feedback(feedback) 

