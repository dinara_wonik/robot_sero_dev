#!/usr/bin/python
# -*- coding: utf8 -*-#

'''
2019.11.29 ('c')void  
'''

import thread, threading 
import traceback
import rospy 
import tf 

import act_set_camera_pos 
import act_set_object_finder
from workerbee_utils.utils import handler_guard
from workerbee_actionlib import RosLaunch, MutuallyExclusiveActionServerGroup, SimpleActionClient


class SeRoActionGroup(MutuallyExclusiveActionServerGroup):
    def __init__(self):
        super(SeRoActionGroup, self).__init__()  
        self._lpt_base_frame = rospy.get_param('~lpt_base_frame', 'lpt_base')

        self._image_view = rospy.get_param('~image_view', True)
        self._rgb_camera_info_topic = rospy.get_param('~rgb_camera_info_topic', '/rgbd/rgb/camera_info')
        self._marker_image_topic    = rospy.get_param('~marker_image_topic', '/marker_detect/fiducial_images')
        self._marker_vertices_topic = rospy.get_param('~marker_vertices_topic', '/marker_detect/fiducial_vertices')

        self._p_step_array = rospy.get_param('~p_step_array', '[-45.0, 0, 45.0]')
        self._t_step_array = rospy.get_param('~t_step_array', '[-30.0, 0, 15.0]')

        self._action_servers = [] 
        
        self._tfl = tf.TransformListener()  
        
        # lpt_set_pos 
        act_set_camera_pos.Params.action_name = rospy.get_name() + '/set_camera_pose'
        act_set_camera_pos.Params.lpt_base_frame  = self._lpt_base_frame
        self._action_servers.append(act_set_camera_pos.ActionServer(self))

                # # lpt_set_pos 
        act_set_object_finder.Params.action_name = rospy.get_name() + '/set_object_finder'
        act_set_object_finder.Params.image_view = self._image_view
        act_set_object_finder.Params.rgb_camera_info_topic = self._rgb_camera_info_topic
        act_set_object_finder.Params.marker_image_topic    = self._marker_image_topic
        act_set_object_finder.Params.marker_vertices_topic = self._marker_vertices_topic

        act_set_object_finder.Params.p_step_array = self._p_step_array
        act_set_object_finder.Params.t_step_array = self._t_step_array

        self._action_servers.append(act_set_object_finder.ActionServer(self))    

    @property
    def transform_listener(self):
        return self._tfl
            

if __name__ == '__main__':
    rospy.init_node('sero_action_node')
    wn = SeRoActionGroup()
    rospy.spin()


