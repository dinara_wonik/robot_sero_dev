#!/usr/bin/python
# -*- coding: utf8 -*-#

'''
2019.11.29 ('c')void 

set_object_finder action  
'''

import rospy
import numpy as np 
import json 
import time
import traceback
import threading
import tf
import tf.transformations as trans 
import cv2
from cv_bridge import CvBridge, CvBridgeError
import sensor_msgs.msg
from geometry_msgs.msg import Point, Pose, Quaternion, PoseStamped, PoseArray, Point32, TransformStamped
from message_filters import ApproximateTimeSynchronizer, Subscriber
from sero_mobile.srv import LptSetPosition, LptSetVelocity
from sero_mobile_msgs.msg import LptDriverState, LptSetPositionState, LptPosVelStatus
import fiducial_msgs.msg 
from collections import namedtuple
import workerbee_actionlib
import workerbee_status
from workerbee_msgs.msg import ActionState
from workerbee_utils.utils import handler_guard, lookup_pose, transform_pose, orientation2array

from dynamic_reconfigure.server import Server as DynRecfgServer 
from dynamic_reconfigure.client import Client as DynRecfgClient
from sero_actions.cfg import ObjectFinderConfig
from sero_actions.msg import (
    SetObjectFinderAction, 
    SetObjectFinderGoal,
    SetObjectFinderFeedback, 
    SetObjectFinderResult,
    ObjectFinderState,
)

ProcInput = namedtuple("ProcInput", "rgb_camera_info,marker_image_msg, marker_vertices_msg")

red_color = (0, 0, 255)
blue_color = (255, 0, 0)

class Params(object):
    action_name=None 
    image_view=None
    rgb_camera_info_topic = None
    marker_image_topic = None
    marker_vertices_topic = None
    p_step_array = None
    t_step_array = None

def msg2dict(ros_msg):
    return message_converter.convert_ros_message_to_dictionary(ros_msg) 

def check_error_conditions():
    '''
    해당하는 조건의 InfoItem을 리턴한다.         
    workerbee_status.get_info('A\\B\\C')
    return {'Some condition': 'Some error'}
    ''' 
    return [] 

class LptPosVelReceiver(object):
    def __init__(self, topic):
        self._pos_vel = LptPosVelStatus() 
        self._sub = rospy.Subscriber(topic, LptPosVelStatus, self._callback)
        self._status_error = False
    
    @property
    def pos_vel(self):
        return self._pos_vel
    
    @property
    def lpt_status(self):
        return self._status_error
    
    def _callback(self, pos_vel):
        self._pos_vel = pos_vel
        if((pos_vel.lift_status == 1 or pos_vel.lift_status == 65) and pos_vel.pan_status == 1 and pos_vel.tilt_status == 1):
            self._status_error = False
        else : 
            self._status_error = True

class MarkerDetectionProxy(object): 
    def __init__(self, tfl, rgb_camera_info_topic, marker_image_topic, marker_vertices_topic):        
        self._tfl = tfl

        # radian 
        self._angle_tolerance = 2.0 * np.pi/180 
        
        # mm  
        self._position_tolerance = 3.0 

        srv_lpt_set_position = '/sero_mobile/lpt_set_position' 
        srv_lpt_set_velocity = '/sero_mobile/lpt_set_velocity'
                        
        # # global planner 
        rospy.wait_for_service(srv_lpt_set_position)  
        rospy.wait_for_service(srv_lpt_set_velocity)
        
        # # proxy 
        self._lpt_set_position = rospy.ServiceProxy(srv_lpt_set_position, LptSetPosition) 
        self._lpt_set_velocity = rospy.ServiceProxy(srv_lpt_set_velocity, LptSetVelocity) 
        
        self._pos = LptPosVelReceiver(topic='/sero_mobile/lpt') 

        self._rgb_camera_info_topic  = rgb_camera_info_topic
        self._marker_image_topic     = marker_image_topic
        self._marker_vertices_topic = marker_vertices_topic

        self.ready_flag = False
        self.marker_detection = False
        self.mission_fail = False
        self.no_acting = True
        self.set_target_flag = False
        self.is_done = False
        self.image_tolerance = 20
        self.image_view = False
        self.is_done_count = 0 
        self.targeting_on_view = False
        self.lpt_status_error = False

        self.bridge = CvBridge()
        self.get_marker_image_msg = sensor_msgs.msg.Image()
        self.marker_image_update = False

        self.marker_pose = [0, 0]
        self.marker_prediction_pose = [0, 0]

        self.kalman = cv2.KalmanFilter(4,2)
        self.kalman.measurementMatrix = np.array([[1,0,0,0],[0,1,0,0]],np.float32)
        self.kalman.transitionMatrix = np.array([[1,0,1,0],[0,1,0,1],[0,0,1,0],[0,0,0,1]],np.float32)
        self.kalman.processNoiseCov = np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]],np.float32) * 0.03        

        self.lpt_set_velocity()
        self.dynamic_srv = DynRecfgServer(ObjectFinderConfig, self.reconfigure_callback)

        self.lift = 0
        self.p_step_array = [-45., 0, 45.]
        self.t_step_array = [-30., 0, 15.]

        self.ats_subs = []

    def __del__(self):   
        if self.image_view :
            cv2.destroyAllWindows()
            
    def marker_sub(self):        
        self.ats_subs.append(Subscriber(self._rgb_camera_info_topic, sensor_msgs.msg.CameraInfo))    
        self.ats_subs.append(Subscriber(self._marker_image_topic, sensor_msgs.msg.Image)) 
        self.ats_subs.append(Subscriber(self._marker_vertices_topic, fiducial_msgs.msg.FiducialArray)) 

        self._ats = ApproximateTimeSynchronizer(self.ats_subs, queue_size=5, slop=0.1)
        self._ats.registerCallback(self.callback_ats)
        
    def marker_unsub(self):
        [sub.unregister() for sub in self.ats_subs]
        # self._ats.unregister()
    
    def marker_regist(self):
        [sub.register() for sub in self.ats_subs]

    def reconfigure_callback(self, config, level):
        print "======== call dynamic_reconfigure_callback ========"
        self.image_tolerance = config.image_target_tolerance
        self._position_tolerance = config.lpt_position_tolerance
        self._angle_tolerance = config.lpt_angle_tolerance / 180.0 * np.pi 
        print "image_tolerance    : ", config.image_target_tolerance
        print "position_tolerance : ", config.lpt_position_tolerance
        print "angle_tolerance    : ", config.lpt_angle_tolerance
        return config 

    def set_image_view(self, flag):
        self.image_view = flag
        if self.image_view : 
            self.v = threading.Thread(target=self.image_view_run)
            self.v.start()
        return True

    def image_view_run(self):
        while not rospy.is_shutdown():
            if self.marker_image_update is False : 
                rospy.sleep(3)
            else : 
                try:
                    frame = self.bridge.imgmsg_to_cv2(self.get_marker_image_msg, "bgr8")
                    frame = np.array(frame, dtype=np.uint8)
                    if self.marker_detection:
                        cu = int(self.marker_pose[0])
                        cv = int(self.marker_pose[1])
                        p_cu = int(self.marker_prediction_pose[0])
                        p_cv = int(self.marker_prediction_pose[1])
                        frame = cv2.circle(frame,(cu, cv),50,red_color,15)
                        frame = cv2.rectangle(frame,(p_cu-25, p_cv-25),(p_cu+25, p_cv+25),blue_color,10)
                    cv2.imshow("Image window", frame)
                    cv2.waitKey(50)
                except CvBridgeError, e:
                    print e
        # end while loop
        cv2.destroyAllWindows()

    def set_movement_step(self, p_step, t_step):
        self.p_step_array = p_step
        self.t_step_array = t_step
        print "=== set_movement_step ==="
        print "p_step_array    : ", self.p_step_array
        print "t_step_array    : ", self.t_step_array

    def set_fail(self):
        self.mission_fail = True
 
    def searching_move(self):
        # while not rospy.is_shutdown():
        if self.mission_fail:
            return

        for t_step in self.t_step_array:
            t_ang = t_step / 180.0 * np.pi

            for p_step in self.p_step_array:
                p_ang = p_step / 180.0 * np.pi
                                
                if self.marker_detection :
                    loop_cnt = 0
                    while self.targeting_on_view is False:

                        print "target l ",self.marker_target_l
                        print "target p ",self.marker_target_p
                        print "target t ",self.marker_target_t

                        print "now l",self.pos_vel.lift_pos
                        print "now p",self.pos_vel.pan_pos
                        print "now t",self.pos_vel.tilt_pos

                        self.lpt_set_position(self.marker_target_l, self.marker_target_p, self.marker_target_t)
                        self.wait_for_lpt_pose(self.marker_target_l, self.marker_target_p, self.marker_target_t)
                        rospy.sleep(0.2)

                        if self.no_acting :
                            return 

                        if loop_cnt > 50:
                            break
                        loop_cnt += 1 
                        # self.is_done = True

                    self.is_done = True
                    self.no_acting = True
                    return

                self.lpt_set_position(self.lift, p_ang, t_ang)
                self.wait_for_lpt_pose(self.lift, p_ang, t_ang)

        self.lpt_set_position(0.0, 0.0, 0.0)
        self.mission_fail = True
        return

    def wait_for_lpt_pose(self,_lift,_pan,_tilt):
        while not rospy.is_shutdown():
            rospy.sleep(0.1)

            error_lift = np.abs(_lift - self.pos_vel.lift_pos)
            error_pan = np.abs(_pan - self.pos_vel.pan_pos)
            error_tilt = np.abs(_tilt - self.pos_vel.tilt_pos)
                                     
            goal_reached = (error_lift < self._position_tolerance and error_pan < self._angle_tolerance and error_tilt < self._angle_tolerance)

            if self.no_acting:                
                return            

            if(goal_reached):
                print 'lpt goal_reached !!!'
                return
            else:
                print 'lpt moving'

    def callback_ats(self, rgb_camera_info, marker_image_msg, marker_vertices_msg):
        print 'ats...'
        input = ProcInput(rgb_camera_info=rgb_camera_info,marker_image_msg=marker_image_msg, marker_vertices_msg=marker_vertices_msg) 

        self.ready_flag = True
        if self.lpt_status :
            self.lpt_status_error = True

        self.get_marker_image_msg = input.marker_image_msg
        self.marker_image_update = True

        if self.no_acting :
            return 

        if self.marker_detection:
            self.current_prediction = self.kalman.predict()

        if(len(marker_vertices_msg.fiducials) == 0):
            # print 'searching ...'
            return 


        # print("len input.marker_vertices_msg.fiducials: ", len(input.marker_vertices_msg.fiducials))
        self.marker_detection = True
        marker_idx = 0

        image_height = input.rgb_camera_info.height
        image_width = input.rgb_camera_info.width

        fx = input.rgb_camera_info.P[0]
        cx = input.rgb_camera_info.P[2]
        fy = input.rgb_camera_info.P[5]
        cy = input.rgb_camera_info.P[6]

        print ('camera info : cx %.2f, cy %.2f, fx %.2f, fy %.2f')%(cx, cy, fx, fy)

        center_image_x = cx
        center_image_y = cy

        x0 = input.marker_vertices_msg.fiducials[marker_idx].x0
        x1 = input.marker_vertices_msg.fiducials[marker_idx].x1
        x2 = input.marker_vertices_msg.fiducials[marker_idx].x2
        x3 = input.marker_vertices_msg.fiducials[marker_idx].x3

        y0 = input.marker_vertices_msg.fiducials[marker_idx].y0
        y1 = input.marker_vertices_msg.fiducials[marker_idx].y1
        y2 = input.marker_vertices_msg.fiducials[marker_idx].y2
        y3 = input.marker_vertices_msg.fiducials[marker_idx].y3

        center_marker_x = (x0 + x1 + x2 + x3)/4.
        center_marker_y = (y0 + y1 + y2 + y3)/4.

        self.marker_pose = [center_marker_x, center_marker_y]
        self.current_measurement = np.array([[np.float32(center_marker_x)],[np.float32(center_marker_y)]])
        self.kalman.correct(self.current_measurement)
        self.current_prediction = self.kalman.predict()
        self.marker_prediction_pose = [self.current_prediction[0], self.current_prediction[1]]
        print "current_measurement x : ",self.current_measurement[0],", y : ",self.current_measurement[1]
        print "current_prediction x : ",self.current_prediction[0],", y : ",self.current_prediction[1]

        marker_prediction__x = self.current_prediction[0]
        marker_prediction__y = self.current_prediction[1]
        if self.set_target_flag is True :
            x = (marker_prediction__x - self.target_u) / fx
            y = (marker_prediction__y - self.target_v) / fy

            error_u = np.abs((marker_prediction__x - self.target_u))
            error_v = np.abs((marker_prediction__y - self.target_v))
        else:
            x = (marker_prediction__x - cx) / fx
            y = (marker_prediction__y - cy) / fy    

            error_u = np.abs((marker_prediction__x - cx))
            error_v = np.abs((marker_prediction__y - cy))

        print "error_u : ",error_u
        print "error_v : ",error_v

        print "error_u < self.image_tolerance and error_v < self.image_tolerance", (error_u < self.image_tolerance and error_v < self.image_tolerance)
        if (error_u < self.image_tolerance and error_v < self.image_tolerance):
            print 'Done'
            print "Terminated : Done complete"
            self.targeting_on_view = True

        print ('x %.2f, y %.2f')%(x, y)

        d_pan  = np.arctan2(x, 1.)
        d_tilt = np.arctan2(y, 1.)

        print 'd_pan  : ',d_pan
        print 'd_tilt : ',d_tilt

        if (np.abs(d_pan) < 0.07 and np.abs(d_tilt) < 0.07):
            self.is_done_count += 1

            if (self.is_done_count > 30 ):
                print "Terminated : is_done_count"
                self.is_done = True
                self.no_acting = True
                self.marker_detection = False                
        else:            
            self.is_done_count = 0

        self.marker_target_l = self.pos_vel.lift_pos
        self.marker_target_p = self.pos_vel.pan_pos - d_pan

        print 'now pan pose : ', self.pos_vel.pan_pos, ', d_pan_pose : ', d_pan
        self.marker_target_t = self.pos_vel.tilt_pos + d_tilt

        print "set target l ",self.marker_target_l
        print "set target p ",self.marker_target_p
        print "set target t ",self.marker_target_t

    @property
    def lpt_status(self):
        return self._pos.lpt_status

    @property
    def pos_vel(self):
        return self._pos.pos_vel

    def run(self):
        rospy.loginfo('run()')
        if(self.ready_flag is True):
            rospy.sleep(1)
            self.searching_move()

    def reset(self):
        self.marker_detection = False
        self.mission_fail = False
        self.no_acting = False
        self.is_done = False
        self.set_target_flag = False
        self.image_view = False
        self.is_done_count = 0 
        self.targeting_on_view = False
        self.lpt_status_error = False

    def set_target_view(self,target_u,target_v):
        self.target_u = target_u
        self.target_v = target_v
        if(self.target_u == 0 and self.target_v == 0):
            self.set_target_flag = False
        else:
            self.set_target_flag = True

    def set_marker_detection(self):
        self.t = threading.Thread(target=self.run)
        self.t.start()
        return True
    
    def lpt_set_position(self, lift, pan, tilt): 
        self._lift = lift
        self._pan = pan
        self._tilt = tilt
               
        result = self._lpt_set_position(lift, pan, tilt)
        
        return result

    def lpt_set_velocity(self):
        lift_vel = 75/4.
        pan_vel = 1.0471975511965976/4.
        tilt_vel = 0.5235987755982988/4.
        self._lpt_set_velocity(lift_vel, pan_vel, tilt_vel)  
    
    def is_done_check(self):        
        if(self.is_done):
            self.marker_detection = False
            return True 
        else:
            return False 

class ActionServer(workerbee_actionlib.MutuallyExclusiveActionServer):
    def __init__(self, action_server_group):
        super(ActionServer, self).__init__(
            action_server_group, 
            Params.action_name, 
            SetObjectFinderAction
        )        
        self._action_server_group = action_server_group
        self._last_execution_time = None 
        print 'Action Name : ',Params.action_name
        print 'image_view : ',Params.image_view
        print 'rgb_camera_info_topic : ',Params.rgb_camera_info_topic
        print 'marker_image_topic    : ',Params.marker_image_topic
        print 'marker_vertices_topic : ',Params.marker_vertices_topic

        print 'p_step_array          : ',Params.p_step_array
        print 't_step_array          : ',Params.t_step_array

        self.controller = MarkerDetectionProxy(self.ag.transform_listener, Params.rgb_camera_info_topic , Params.marker_image_topic , Params.marker_vertices_topic)
        self.controller.set_image_view(Params.image_view)
        self.controller.set_movement_step(Params.p_step_array, Params.t_step_array)
        self.controller.marker_sub()
        # self.controller.marker_unsub()
        
           
    def _result_for_preemption(self, new_action_name):
        result = SetObjectFinderResult()
        result.state.code =ObjectFinderState.ERROR_PREEMPTED
        result.state.details= ('PREEMPTED BY NEW ACTION(%s)'%(new_action_name))
        return result 
    
    
    def _excute_callback(self, goal):      
        rospy.loginfo('start SetObjectFinderAction. goal=\n%s'%(str(goal)))
        # self.controller.marker_sub()
        rospy.sleep(3)
        
        self.controller.reset()
        self.controller.lpt_set_velocity()
        self.controller.no_acting = False

        result = SetObjectFinderResult()
        self.controller.set_target_view(goal.target.u, goal.target.v)
        res = self.controller.set_marker_detection()

        if(res != True): 
            result.state.code = ObjectFinderState.ERROR_ETC
            result.state.details = 'ERROR_ETC'
            self._as.set_aborted(result)    
            return
                        
        # start executing the action
        rate = rospy.Rate(10)
        self._last_execution_time = rospy.Time.now()
        while(True): 
            rate.sleep()

            result = SetObjectFinderResult()
            feedback = SetObjectFinderFeedback() 

            # check faults 
            # faults = workerbee_status.get_fault()
            # if(len(faults) > 0):
            #     rospy.loginfo('%s: Fault' % Params.action_name)
            #     self.controller.set_fail()
            #     result.state.code = ObjectFinderState.ERROR_ETC
            #     result.state.details = 'ERROR_ETC : workerbee_status faults'
            #     self._as.set_aborted(result)
            #     self.controller.marker_unsub()
            #     break

            # check complete condition 
            if(self.controller.is_done_check() is True):
                rospy.loginfo('%s: Succeeded' % Params.action_name) 
                result.state.code = ObjectFinderState.NO_ERROR
                result.state.details = 'NO_ERROR : Action Succeeded'
                self._as.set_succeeded(result)
                self.controller.marker_unsub() 
                break

            if(self.controller.lpt_status_error is True):
                rospy.loginfo('%s: Fault' % Params.action_name)
                result.state.code = ObjectFinderState.ERROR_CONTROLLER
                result.state.details = 'ERROR_CONTROLLER : LPT Status Error'
                self._as.set_aborted(result)
                self.controller.marker_unsub()
                break

            if(self.controller.mission_fail is True):
                rospy.loginfo('%s: Fault' % Params.action_name)
                result.state.code = ObjectFinderState.ERROR_TIMEOUT
                result.state.details = 'ERROR_TIMEOUT : NO MARKER'
                self._as.set_aborted(result)
                self.controller.marker_unsub()
                break

            if(self.controller.ready_flag is False):
                rospy.loginfo('%s: Fault' % Params.action_name)
                result.state.code = ObjectFinderState.ERROR_ETC
                result.state.details = 'ERROR_ETC : No RGBD Image Topic'
                rospy.logerr('ERROR_ETC : No RGBD Image Topic')
                self._as.set_aborted(result)
                self.controller.marker_unsub()
                break

            if(self._as.is_active() is False):
                self.controller.marker_unsub()
                # aborted
                break   

            # check preemption
            if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % Params.action_name)
                result.state.code = ObjectFinderState.ERROR_ETC
                result.state.details = 'ERROR_ETC : Preempted'
                self._as.set_preempted(result)
                self.controller.marker_unsub()
                break
            feedback.state.code = 0
            feedback.state.details = 'NO_ERROR'

            self._as.publish_feedback(feedback) 


