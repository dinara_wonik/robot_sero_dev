#!/usr/bin/python
# -*- coding: utf8 -*-#

'''
2019.12.04 ('c')void 
'''

import numpy as np 
import rospy
import tf.transformations as trans 
import workerbee_actionlib
from geometry_msgs.msg import PoseStamped, Point, Quaternion
from workerbee_navigation.msg import DockingAction, DockingGoal, DockingFeedback, DockingResult
from workerbee_navigation.msg import MoveToAction, MoveToGoal, MoveToFeedback, MoveToResult

from sero_mobile_msgs.msg import LptDriverState, LptSetPositionState
from sero_actions.msg import SetCameraPoseAction, SetCameraPoseGoal, SetCameraPoseFeedback, SetCameraPoseResult



# frame_id, x, y, z, pitch(deg), yaw(deg) 
HEAD_ZERO_POSE    = ('lpt_base', 0, 0, 0, 0, 0)
HEAD_TARGET_POSE  = ('map', 20.7, 3, 1.2, -30, 90)
MOVETO_GOAL_POSE  = ('map', 20.7, 3, 0, 0, 45)


def make_pose(simple_pose):
    frame_id, sx, sy, sz, pitch, yaw = simple_pose
    pose = PoseStamped()
    pose.header.frame_id = frame_id
    pose.pose.position = Point(sx, sy, sz)  
    pose.pose.orientation = Quaternion(*trans.quaternion_from_euler(0, pitch*np.pi/180, yaw*np.pi/180)) 
    return pose
    

def set_camera_client(): 
    ac_moveto = workerbee_actionlib.SimpleActionClient('/workerbee_navigation/moveto', MoveToAction) 
    ac_set_cam_pose = workerbee_actionlib.SimpleActionClient('/sero_action_node/set_camera_pose', SetCameraPoseAction) 
    
    ac_set_cam_pose.wait_for_server() 
    ac_moveto.wait_for_server() 

    def on_feedback(fb):
        rospy.loginfo_throttle(1.0, 'Rcv feedback\n%s'%(fb))
        
    def on_result(goal_status, result):
        rospy.loginfo('Rcv result. goal_status=%s, result=\n%s'%(goal_status, result)) 


    # reset_camera_pose 
    goal = SetCameraPoseGoal(head_pose=make_pose(HEAD_ZERO_POSE))
    ac_set_cam_pose.send_goal(goal, feedback_cb=on_feedback, done_cb=on_result)
    ac_set_cam_pose.wait_for_result()

    # move_to 
    goal = MoveToGoal(goal=make_pose(MOVETO_GOAL_POSE), speed=1.0, patience_timeout=5)
    ac_moveto.send_goal(goal, feedback_cb=on_feedback, done_cb=on_result)
    ac_moveto.wait_for_result()

    # set_camera_pose 
    goal = SetCameraPoseGoal(head_pose=make_pose(HEAD_TARGET_POSE))
    ac_set_cam_pose.send_goal(goal, feedback_cb=on_feedback, done_cb=on_result)
    ac_set_cam_pose.wait_for_result()
    
    return ac_set_cam_pose.get_result() 


if __name__ == '__main__':
    try: 
        rospy.init_node('test_camera_pose') 
        result = set_camera_client()
        print 'Result\n', result
    except rospy.ROSInterruptException:
        print 'program interrupted before completion'




