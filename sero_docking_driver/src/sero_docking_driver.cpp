/*
 * sero_docking_driver.cpp
 *
 *  Created on: Oct 17, 2018
 *      Author: ('c')void
 */


#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <sero_docking_driver/docking_driver.h>

#include "docking_driver_base.h"


namespace sero_docking_driver {


DockingDriver::DockingDriver()
: impl_(0), setup_(false)
{
    impl_ = new DockingDriverImpl;
}

DockingDriver::~DockingDriver()
{
    if(impl_) {
        delete static_cast<DockingDriverImpl*>(impl_);
    }
}

void DockingDriver::initialize(std::string name, tf2_ros::Buffer* tf, costmap_2d::Costmap2DROS* costmap_ros)
{
    if(!impl_) {
        return;
    }

    static_cast<DockingDriverImpl*>(impl_)->initialize(name, tf, costmap_ros);

    ros::NodeHandle private_nh("~/" + name);
    dsrv_ = boost::shared_ptr<dynamic_reconfigure::Server<DockingDriverConfig> > ( new dynamic_reconfigure::Server<DockingDriverConfig>(private_nh) );
    dsrv_->setCallback(boost::bind(&DockingDriver::reconfigure_callback, this, _1, _2));

    sub_docking_state_ = private_nh.subscribe(docking_topic_, 10, &DockingDriver::cb_docking_state, this);
}

void DockingDriver::cb_docking_state(const sero_mobile_msgs::DockingState::ConstPtr& msg)
{
    if(!impl_) {
        return;
    }

    if(msg->state) {
        static_cast<DockingDriverImpl*>(impl_)->connect_event("TOPIC");
    }
}

void DockingDriver::reconfigure_callback(DockingDriverConfig &config, uint32_t level) {

    if (setup_ && config.restore_defaults) {
      config = default_config_;
      config.restore_defaults = false;
    }
    if ( ! setup_) {
      default_config_ = config;
      setup_ = true;
    }

    docking_topic_ = config.docking_topic;

    if(!impl_) {
        return;
    }

    static_cast<DockingDriverImpl*>(impl_)->reconfigure(config);
}

bool DockingDriver::computeVelocityCommands(geometry_msgs::Twist& cmd_vel)
{
    if(!impl_) {
        return false;
    }

    bool res = static_cast<DockingDriverImpl*>(impl_)->compute_cmd_vel(cmd_vel);
    driver_state_pub.publish();
    return res;
}

bool DockingDriver::start(unsigned int mode, unsigned int retry) {
    if(!impl_) {
        return false;
    }

    return static_cast<DockingDriverImpl*>(impl_)->start(mode, retry);
}

bool DockingDriver::isGoalReached()
{
    if(!impl_) {
        return false;
    }

    return static_cast<DockingDriverImpl*>(impl_)->isGoalReached();
}

bool DockingDriver::isInitialized()
{
    if(!impl_) {
        return false;
    }

    return static_cast<DockingDriverImpl*>(impl_)->isInitialized();
}


}; // namespace

