/*
 * docking_driver_base.h
 *
 *  Created on: May 9, 2020
 *      Author: void
 *
 *  2020.10.20 ('c')void
 *
 */

#ifndef __DOCKING_DRIVER_BASE_H__
#define __DOCKING_DRIVER_BASE_H__

#include <cmath>
#include <algorithm>
#include <limits>
#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>

#include <costmap_2d/footprint.h>
#include <base_local_planner/goal_functions.h>
#include <base_local_planner/odometry_helper_ros.h>
#include <base_local_planner/local_planner_util.h>
#include <base_local_planner/local_planner_limits.h>
#include <base_local_planner/costmap_model.h>
#include <dynamic_reconfigure/server.h>

#include <std_msgs/ColorRGBA.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PolygonStamped.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <workerbee_platform/StartDocking.h>
#include <workerbee_status/workerbee_status.h>
#include <workerbee_utils/accessories.h>
#include <workerbee_utils/geometry_utils.h>
#include <workerbee_utils/interpolation.h>
#include <workerbee_utils/cubic_spline.h>
#include <workerbee_utils/moving.h>



double rad2deg(double radian);
void get_robot_geometry(const std::vector<geometry_msgs::Point>& footprint,
                        double& radius,
                        double& center_offset,
                        double& footprint_sz_x, double& footprint_sz_y);


class DockingDriverStatePublisher
{
public:
    enum State  {
        NO_ERROR=0,
        GOAL_REACHED,
        ERROR_COSTMAP,
        ERROR_COLLISION,
        ERROR_OBSTACLE,
        ERROR_MISALIGNMENT,
        ERROR_CONNECTION_TIMEOUT,
        ERROR_ETC,
        ERROR_UNKNOWN,
    };

private:
    State state_;
    std::string message_;

    const char* state_code_str(State code) {
        switch(code) {
        case NO_ERROR:
            return "NO_ERROR";
        case GOAL_REACHED:
            return "GOAL_REACHED";
        case ERROR_COSTMAP:
            return "ERROR_COSTMAP";
        case ERROR_COLLISION:
            return "ERROR_COLLISION";
        case ERROR_OBSTACLE:
            return "ERROR_OBSTACLE";
        case ERROR_MISALIGNMENT:
            return "ERROR_MISALIGNMENT";
        case ERROR_CONNECTION_TIMEOUT:
            return "ERROR_CONNECTION_TIMEOUT";
        case ERROR_ETC:
            return "ERROR_ETC";
        default:
            return "NONE";
        }
    }

public:
    DockingDriverStatePublisher()
    : state_(NO_ERROR)
    {}
    virtual ~DockingDriverStatePublisher() {}

    void set_state(State code, std::string message) {
        state_ = code;
        message_ = message;

        if(!(code == NO_ERROR || code == GOAL_REACHED)) {
            ROS_ERROR("DockingDriverState. error=%s, message=%s", state_code_str(code), message.c_str());
        }
    }

    void publish() {
        workerbee_status::set_info("DockingDriverState/state", state_code_str(state_));
        workerbee_status::set_info("DockingDriverState/message", message_);
    }

    void clear() {
        state_ = NO_ERROR;
        message_ = "";
    }
};

extern DockingDriverStatePublisher driver_state_pub;

class Controller
{
public:
    enum Direction {
        /*
         *  robot 기준으로 전/후진 구분
         */
        FORWARD=1,
        BACKWARD,
    };

private:
    Direction direction_;
    tf::Pose robot_s_;
    bool enable_obstacle_detection_;
    double obstacle_detection_range_from_surface;

    workerbee_utils::CostmapUtil& costmap_util_;
    workerbee_utils::MotionController motion_controller_;

    std::string dir_str(Direction direction) {
        std::string dir;
        switch(direction) {
        case FORWARD:
            dir = "FORWARD"; break;
        case BACKWARD:
            dir = "BACKWARD"; break;
        }
        return dir;
    }

    bool collision_detection(const std::vector<geometry_msgs::Point>& footprint) {
        tf::Stamped<tf::Pose> robot_pose_g;
        if (!costmap_util_.getRobotPose(robot_pose_g)) {
            std::string e = "could not get robot pose";
            driver_state_pub.set_state(DockingDriverStatePublisher::ERROR_COSTMAP, "could not get robot pose in the global frame of costmap");
            return false;
        }

        std::vector<tf::Pose> lethal_obstacle_list;
        if(costmap_util_.get_obstacle_list(lethal_obstacle_list, robot_pose_g) == false) {
            driver_state_pub.set_state(DockingDriverStatePublisher::ERROR_COSTMAP, "could not get obstacle list");
            return false;
        }

        if(footprint.size() == 0) {
            driver_state_pub.set_state(DockingDriverStatePublisher::ERROR_ETC, "invalid footprint. footprint.size() == 0");
            return false;
        }

        double footprint_x_min =  std::numeric_limits<double>::infinity();
        double footprint_x_max = -std::numeric_limits<double>::infinity();
        double footprint_y_min =  std::numeric_limits<double>::infinity();
        double footprint_y_max = -std::numeric_limits<double>::infinity();

        for(size_t i=0; i<footprint.size(); i++) {
            if(footprint[i].x < footprint_x_min) {
                footprint_x_min = footprint[i].x;
            }

            if(footprint[i].x > footprint_x_max) {
                footprint_x_max = footprint[i].x;
            }

            if(footprint[i].y < footprint_y_min) {
                footprint_y_min = footprint[i].y;
            }

            if(footprint[i].y > footprint_y_max) {
                footprint_y_max = footprint[i].y;
            }
        }

        double footprint_x_min_p = footprint_x_min - obstacle_detection_range_from_surface;
        double footprint_y_min_p = footprint_y_min - obstacle_detection_range_from_surface;
        double footprint_x_max_p = footprint_x_max + obstacle_detection_range_from_surface;
        double footprint_y_max_p = footprint_y_max + obstacle_detection_range_from_surface;

        std::vector<tf::Pose> obstacle_list;

        if(direction_ == FORWARD) {
            // check front side
            for(std::vector<tf::Pose>::iterator p = lethal_obstacle_list.begin(); p != lethal_obstacle_list.end(); p++) {
                if(footprint_y_min_p <= p->getOrigin().y() && p->getOrigin().y() <= footprint_y_max_p &&
                   footprint_x_min   <= p->getOrigin().x() && p->getOrigin().x() <= footprint_x_max_p) {
                    obstacle_list.push_back(*p);
                }
            }
        }
        else {
            // check rear side
            for(std::vector<tf::Pose>::iterator p = lethal_obstacle_list.begin(); p != lethal_obstacle_list.end(); p++) {
                if(footprint_y_min_p <= p->getOrigin().y() && p->getOrigin().y() <= footprint_y_max_p &&
                   footprint_x_min_p <= p->getOrigin().x() && p->getOrigin().x() <= footprint_x_max) {
                    obstacle_list.push_back(*p);
                }
            }
        }

        if(obstacle_list.size() > 0) {
            std::string obstacle_message = workerbee_utils::string_format("DIRECTION=%s, footprint_x_max_p=%.3f, footprint_x_min=%.3f, OBSTACLE_NUM=%lu, OBSTACLES(x,y)=",
                    dir_str(direction_).c_str(),
                    footprint_x_max_p,
                    footprint_x_min,
                    obstacle_list.size());
            for(size_t i=0; i<obstacle_list.size(); i++) {
                obstacle_message += workerbee_utils::string_format(" (%.3f,%.3f)", obstacle_list[i].getOrigin().x(), obstacle_list[i].getOrigin().y());
            }

            driver_state_pub.set_state(DockingDriverStatePublisher::ERROR_OBSTACLE, obstacle_message);
            return false;
        }
        else {
            return true;
        }
    }

public:
    Controller(workerbee_utils::CostmapUtil& costmap_util)
    : costmap_util_(costmap_util), direction_(FORWARD), enable_obstacle_detection_(true)
    {}

    void set_motion_controller(const workerbee_utils::MotionControllerConfig& config) {
        // FIXME. omni platform이 생기면 그 때 생각해 보세
        motion_controller_.set_system_constraint(workerbee_utils::NON_HOLONOMIC);
        motion_controller_.set_config(config);
    }

    void set_direction(Direction direction) {
        direction_ = direction;
    }

    void set_obstacle(bool enable, double range) {
        enable_obstacle_detection_ = enable;
        obstacle_detection_range_from_surface = range;
    }

    void set_robot_pose(tf::Stamped<tf::Pose>& robot_pose_s) {
        /*
         *  station 좌표계로 표현한 robot pose
         */
        robot_s_ = robot_pose_s;
    }

    tf::Pose get_robot_pose() {
        tf::Pose robot_pose = robot_s_;

        if(direction_ == BACKWARD) {
            // 후진을 위해 로봇 좌표계를 180도 회전
            tf::Transform R(tf::createQuaternionFromYaw(M_PI), tf::Vector3(0,0,0));
            robot_pose.setOrigin(tf::Vector3());
            robot_pose = R*robot_pose;
            robot_pose.setOrigin(robot_s_.getOrigin());
        }

        return robot_pose;
    }

    bool compute_cmd_vel(geometry_msgs::Twist& cmd_vel,
            const std::vector<geometry_msgs::Point>& footprint,
            const tf::Pose target_frame,
            const tf::Pose drive_frame,
            double translation_speed,
            double rotation_speed=0) {

        cmd_vel = geometry_msgs::Twist();

        if(enable_obstacle_detection_) {
            if(!collision_detection(footprint)) {
                return false;
            }
        }

        /*
         *  drive_c 에서 target_c 까지 주행
         *  target_c, drive_c : station frame
         */
        double pos_tolerance = 0.02;
        motion_controller_.get_cmd_vel(target_frame, drive_frame, get_robot_pose(), translation_speed, rotation_speed, pos_tolerance, cmd_vel);

        tf::Vector3 v(cmd_vel.linear.x, cmd_vel.linear.y, cmd_vel.linear.z);
        tf::Vector3 w(cmd_vel.angular.x, cmd_vel.angular.y, cmd_vel.angular.z);

        ROS_DEBUG("compute_cmd_vel(dir=%s, offset_err=%.3f, yaw_err=%.3f, vx=%.3f, wz=%.3f)",
                   dir_str(direction_).c_str(),
                   motion_controller_.offset_error,
                   motion_controller_.yaw_error * 180/M_PI,
                   cmd_vel.linear.x, cmd_vel.angular.z);

        if(direction_ == BACKWARD) {
            tf::Transform R(tf::createQuaternionFromYaw(M_PI), tf::Vector3(0,0,0));
            // ROTATION : 구동부 좌표계로 변환
            v = R*v;
            w = R*w;
        }

        if(v.length() > motion_controller_.get_config().translation_speed_max) {
            v = v/v.length() * motion_controller_.get_config().translation_speed_max;
        }

        if(w.length() > motion_controller_.get_config().rotation_speed_max) {
            w = w/w.length() * motion_controller_.get_config().rotation_speed_max;
        }

        cmd_vel.linear.x = v.getX();
        cmd_vel.linear.y = v.getY();
        cmd_vel.linear.z = v.getZ();

        cmd_vel.angular.x = w.getX();
        cmd_vel.angular.y = w.getY();
        cmd_vel.angular.z = w.getZ();

        return true;
    }
};


class DockingControllerConfig
{
public:
    enum DockingFace {
        DOCKFACE_FRONT = 0,
        DOCKFACE_BACK,
    };

    struct Config
    {
        std::vector<geometry_msgs::Point> footprint;
        std::string station_frame_id;
        std::string robot_frame_id;
        int docking_face; // DOCKFACE_FRONT(0), DOCKFACE_BACK(1)

        double approaching_length;
        double docking_goal;
        double approaching_goal;
        double undocking_goal;
        double docking_start_goal;

        double obstacle_range_from_surface;
        double translation_speed;
        double rotation_inplace_speed;

        double heading_error_threshold;
        double offset_error_threshold;

        double connection_timeout;
        bool enable_fake_event;

        /*
         * Motion Controller
         */
        workerbee_utils::MotionControllerConfig mc;

        /*
         *
         */
        double center_offset;
        tf::Vector3 docking_goal_center;
        tf::Vector3 approching_goal_center;
        tf::Vector3 undocking_goal_center;
        tf::Vector3 docking_start_goal_center;

        std::vector<geometry_msgs::Point> approching_path;
    };

private:
    costmap_2d::Costmap2DROS* costmap_ros_;
    bool initialized_;

    Config config_;
    Controller* controller_;

    void generate_approching_path() {
        std::vector<tf::Vector3> approching_path;

        for(double x=0; x < config_.approaching_length; x += 0.1) {
            approching_path.push_back(config_.docking_goal_center + tf::Vector3(x, 0, 0));
        }
        approching_path.push_back(config_.docking_goal_center + tf::Vector3(config_.approaching_length, 0, 0));

        // marker
        config_.approching_path.clear();
        for(size_t i=0; i<approching_path.size(); i++) {
            geometry_msgs::Point p;
            p.x = approching_path[i].getX();
            p.y = approching_path[i].getY();
            p.z = approching_path[i].getZ();
            config_.approching_path.push_back(p);
        }
    }

public:
    DockingControllerConfig() : initialized_(false), costmap_ros_(0), controller_(0){}
    ~DockingControllerConfig() {}

    void initialize(ros::NodeHandle& private_nh, costmap_2d::Costmap2DROS* costmap_ros, Controller* controller) {
        if(initialized_) {
            return;
        }

        costmap_ros_ = costmap_ros;
        controller_ = controller;
        initialized_ = true;
    }

    const Config& get() {
        return config_;
    }

    template <typename DockingDriverConfigType>
    void reconfigure(DockingDriverConfigType &config) {

        costmap_2d::makeFootprintFromString(config.footprint, config_.footprint);
        config_.station_frame_id                   =  config.station_frame_id;
        config_.robot_frame_id                     =  config.robot_frame_id;
        config_.docking_face                       =  config.docking_face;

        config_.approaching_length                 =  config.approaching_length;
        config_.docking_goal                       =  config.docking_goal;
        config_.approaching_goal                   =  config.approaching_goal;
        config_.undocking_goal                     =  config.undocking_goal;
        config_.docking_start_goal                 =  config.docking_start_goal;

        config_.obstacle_range_from_surface        =  config.obstacle_range;
        config_.translation_speed                  =  config.translation_speed;
        config_.rotation_inplace_speed             =  config.rotation_inplace_speed;

        config_.heading_error_threshold            =  config.heading_error_threshold;
        config_.offset_error_threshold             =  config.offset_error_threshold;

        config_.connection_timeout                 =  config.connection_timeout;
        config_.enable_fake_event                  =  config.enable_fake_event;

        if(config.motion_constraint) {
            config_.mc.motion_constraint =  workerbee_utils::NON_HOLONOMIC;
        }
        else {
            config_.mc.motion_constraint =  workerbee_utils::HOLONOMIC;
        }

        config_.mc.translation_speed_max    =  config.max_trans_vel;
        config_.mc.translation_speed_min    =  config.min_trans_vel;

        config_.mc.rotation_speed_max       =  config.max_rot_vel;
        config_.mc.rotation_speed_min       =  config.min_rot_vel;

        config_.mc.rotation_inplace_angle  =  config.rotation_inplace_angle;

        config_.mc.rotational_motion_p_gain =  config.rotational_motion_p_gain;
        config_.mc.rotational_motion_d_gain =  config.rotational_motion_d_gain;

        config_.mc.lateral_motion_p_gain    =  config.lateral_motion_p_gain;
        config_.mc.lateral_motion_d_gain    =  config.lateral_motion_d_gain;

        // degree -> radian
        config_.mc.rotation_speed_max *= M_PI/180;
        config_.mc.rotation_speed_min *= M_PI/180;
        config_.mc.rotation_inplace_angle *= M_PI/180;
        config_.rotation_inplace_speed *= M_PI/180;

        /*
         *
         */
        double footprint_sz_x, footprint_sz_y;
        get_robot_geometry(config_.footprint,
                           config_.mc.robot_radius,
                           config_.center_offset,
                           footprint_sz_x, footprint_sz_y);
        /*
         *  docking goal
         */
        config_.docking_goal_center       = tf::Vector3(config_.docking_goal + config_.center_offset, 0, 0);
        config_.approching_goal_center    = tf::Vector3(config_.approaching_goal + config_.center_offset, 0, 0);
        config_.undocking_goal_center     = tf::Vector3(config_.undocking_goal + config_.center_offset, 0, 0);
        config_.docking_start_goal_center = tf::Vector3(config_.docking_start_goal + config_.center_offset, 0, 0);

        /*
         *
         */
        generate_approching_path();

        /*
         *
         */
        controller_->set_motion_controller(config_.mc);

        /*
         * show
         */
        ROS_INFO("DOCKING_PARAM docking_goal           %.3f m", config_.docking_goal);
        ROS_INFO("DOCKING_PARAM undocking_goal         %.3f m", config_.undocking_goal);
        ROS_INFO("DOCKING_PARAM docking_start_goal     %.3f m", config_.undocking_goal);
        ROS_INFO("DOCKING_PARAM approaching_length     %.3f m", config_.approaching_length);
        ROS_INFO("DOCKING_PARAM robot_center_offset    %.3f m", config_.center_offset);
        ROS_INFO("DOCKING_PARAM approching_goal_center %.3f m", config_.approching_goal_center.x());
        ROS_INFO("DOCKING_PARAM docking_goal_center    %.3f m", config_.docking_goal_center.x());
    }
};


class VisualizationMarker
{
    boost::recursive_mutex mutex_;

    ros::Publisher pub_marker_array_;
    ros::Duration lifetime_;
    float alpha_;
    std::string ns_;
    std::vector<visualization_msgs::Marker> marker_array_;

    boost::shared_ptr<visualization_msgs::Marker> create_marker(std::string frame_id) {
        boost::shared_ptr<visualization_msgs::Marker> m = boost::shared_ptr<visualization_msgs::Marker>(new visualization_msgs::Marker());
        m->header.frame_id = frame_id;
        m->header.stamp = ros::Time::now();
        m->ns = ns_;
        m->lifetime = lifetime_;
        return m;
    }

    void set_marker(int marker_id, const boost::shared_ptr<visualization_msgs::Marker>& marker) {
        boost::recursive_mutex::scoped_lock lock(mutex_);

        visualization_msgs::Marker m = *marker;
        m.id = marker_id;
        marker_array_.push_back(m);
    }

public:
    VisualizationMarker(ros::NodeHandle& nh, std::string topic, std::string ns, float lifetime)
    : ns_(ns)
    , lifetime_(ros::Duration(lifetime))
    , alpha_(0.5)
    {
        pub_marker_array_ = nh.advertise<visualization_msgs::MarkerArray>(topic, 50);
    }

    void publish() {
        boost::recursive_mutex::scoped_lock lock(mutex_);

        visualization_msgs::MarkerArray ma;
        ma.markers = marker_array_;

        pub_marker_array_.publish(ma);
        marker_array_.clear();
    }

    void draw_docking_path(geometry_msgs::PoseStamped& pose,
            const std::vector<geometry_msgs::Point>& path,
            const tf::Vector3& docking_start_goal_center) {
        int id = 0;

        const float scale = 0.05;
        const float width = 0.4;

        boost::shared_ptr<visualization_msgs::Marker> marker;

        // path
        {
            marker = create_marker(pose.header.frame_id);

            marker->type = visualization_msgs::Marker::LINE_STRIP;
            marker->action = visualization_msgs::Marker::ADD;
            marker->pose = pose.pose;

            marker->scale.x = scale;
            marker->scale.y = scale;
            marker->scale.z = scale;

            marker->color.r = 1.0;
            marker->color.g = 0.0;
            marker->color.b = 0.0;
            marker->color.a = alpha_;

            marker->points = path;

            set_marker(id++, marker);
        }

        // docking start point
        {
            geometry_msgs::Point pp;
            double cross_point_x = docking_start_goal_center.x();
            double cross_point_y = docking_start_goal_center.y();

            pp.x = cross_point_x;
            pp.y = 0;
            pp.z = 0;

            marker = create_marker(pose.header.frame_id);

            marker->type = visualization_msgs::Marker::LINE_STRIP;
            marker->action = visualization_msgs::Marker::ADD;
            marker->pose = pose.pose;

            marker->scale.x = scale;
            marker->scale.y = scale;
            marker->scale.z = scale;

            marker->color.r = 1.0;
            marker->color.g = 0.0;
            marker->color.b = 0.0;
            marker->color.a = alpha_;

            pp.y = cross_point_y - width/2;
            marker->points.push_back(pp);

            pp.y = cross_point_y + width/2;
            marker->points.push_back(pp);

            set_marker(id++, marker);
        }

        // front point
        {
            geometry_msgs::Point pp;
            double cross_point_x = path.front().x;
            double cross_point_y = path.front().y;

            pp.x = cross_point_x;
            pp.y = 0;
            pp.z = 0;

            marker = create_marker(pose.header.frame_id);

            marker->type = visualization_msgs::Marker::LINE_STRIP;
            marker->action = visualization_msgs::Marker::ADD;
            marker->pose = pose.pose;

            marker->scale.x = scale;
            marker->scale.y = scale;
            marker->scale.z = scale;

            marker->color.r = 1.0;
            marker->color.g = 0.0;
            marker->color.b = 0.0;
            marker->color.a = alpha_;

            pp.y = cross_point_y - width/2;
            marker->points.push_back(pp);

            pp.y = cross_point_y + width/2;
            marker->points.push_back(pp);

            set_marker(id++, marker);
        }

        // end point
        {
            geometry_msgs::Point pp;
            double cross_point_x = path.back().x;
            double cross_point_y = path.back().y;

            pp.x = cross_point_x;
            pp.y = 0;
            pp.z = 0;

            marker = create_marker(pose.header.frame_id);

            marker->type = visualization_msgs::Marker::LINE_STRIP;
            marker->action = visualization_msgs::Marker::ADD;
            marker->pose = pose.pose;

            marker->scale.x = scale;
            marker->scale.y = scale;
            marker->scale.z = scale;

            marker->color.r = 1.0;
            marker->color.g = 0.0;
            marker->color.b = 0.0;
            marker->color.a = alpha_;

            pp.y = cross_point_y - width/2;
            marker->points.push_back(pp);

            pp.y = cross_point_y + width/2;
            marker->points.push_back(pp);

            set_marker(id++, marker);
        }
    }
};


class DockingDriverImpl
{
    DockingControllerConfig config_;
    bool initialized_;

    boost::mutex mutex_;

    tf2_ros::Buffer* tf_;
    costmap_2d::Costmap2DROS* costmap_ros_;
    workerbee_utils::CostmapUtil costmap_util_;

    std::string global_frame_;
    std::string robot_base_frame_;

    Controller controller_;

    bool cfg_simulation_;
    unsigned int cfg_retry_max_;
    unsigned int retry_count_;
    bool complete_;

    int footprint_cost_;

    boost::shared_ptr<VisualizationMarker> marker_;
    ros::Timer timer_;

    bool get_robot_pose_in_station_frame(tf::Stamped<tf::Pose>& robot_pose_s) {
        tf::Stamped<tf::Pose> robot_pose_l;
        robot_pose_l.setIdentity();
        robot_pose_l.frame_id_ = config_.get().robot_frame_id;
        robot_pose_l.stamp_ = ros::Time();
        return workerbee_utils::Geometry::transform_pose(*tf_, config_.get().station_frame_id, robot_pose_l, robot_pose_s);
    }


    /*********************************************************************************
     *
     *  state machine
     *
     *********************************************************************************/

    class Clock
    {
        ros::Time entry_time_;

    public:
        Clock() {}

        inline double reset() {
            entry_time_ = ros::Time::now();
        }

        inline double get_elapsed_time() {
            return (ros::Time::now() - entry_time_).toSec();
        }
    } state_clock_;


    class State
    {
    protected:
        std::string name_;
        DockingDriverImpl* self_;

    public:
        State(const char* name, DockingDriverImpl* self) : name_(name), self_(self)
        {}

        inline std::string get_name() {
            return name_;
        }

        inline double get_execute_time() {
            return self_->state_clock_.get_elapsed_time();
        }

        inline void entry_() {
            ROS_INFO("DOCKING::%s.entry()",name_.c_str());
            self_->state_clock_.reset();
            this->entry();
        }

        inline void exit_() {
            ROS_INFO("DOCKING::%s.exit()",name_.c_str());
            this->exit();
        }

        /*
         *  events
         */
        virtual void entry() {}
        virtual void exit() {}

        virtual void start_docking() {}
        virtual void start_undocking() {}
        virtual void event_connect(const char* event_type="") {}
        virtual void event_disconnect() {}

        virtual bool compute_cmd_vel(geometry_msgs::Twist& cmd_vel) {
            cmd_vel = geometry_msgs::Twist();
            return true;
        }
    };


    State* state_current_;
    void state_transition(State* next_state) {
        if(state_current_) {
            state_current_->exit_();
        }
        state_current_ = next_state;
        state_current_->entry_();
    }


    /*********************************************************************************
     *
     *
     *
     *********************************************************************************/
    class StateIdle : public State
    {
    public:
        StateIdle(DockingDriverImpl* self) : State("IDLE", self) {}

        void entry() {
        }

        void exit() {
        }

        void start_docking() {
            self_->state_transition(&self_->state_init_docking_);
        }

        void start_undocking() {
            self_->state_transition(&self_->state_init_undocking_);
        }

    } state_idle_;

    friend StateIdle;

    /*********************************************************************************
     *
     *
     *
     *********************************************************************************/
    class StateInitDocking : public State
    {
        double init_delay_;
    public:
        StateInitDocking(DockingDriverImpl* self) : State("INIT_DOCKING", self) {
            init_delay_ = 3.0; // sec
        }

        void entry() {
        }

        void exit() {
        }

        bool compute_cmd_vel(geometry_msgs::Twist& cmd_vel) {
            if(get_execute_time() > init_delay_) {
                self_->state_transition(&self_->state_positioning_);
            }

            cmd_vel = geometry_msgs::Twist();
            return true;
        }

    } state_init_docking_;

    friend StateInitDocking;

    /*********************************************************************************
     *
     *
     *
     *********************************************************************************/
    class StateInitUndocking : public State
    {
        ros::Timer timer;

        void timer_callback(const ros::TimerEvent& event) {
            self_->disconnect_event();
        }

    public:
        StateInitUndocking(DockingDriverImpl* self) : State("INIT_UNDOCKING", self) {

        }

        void entry() {
            ros::NodeHandle nh;
            timer = nh.createTimer(ros::Duration(0.1), &StateInitUndocking::timer_callback, this, true);
        }

        void exit() {
            timer.stop();
        }

        void event_disconnect() {
            self_->state_transition(&self_->state_undocking_);
        }
    } state_init_undocking_;

    friend StateInitUndocking;


    /*********************************************************************************
     *
     *
     *
     *********************************************************************************/
    class StatePositioning : public State
    {
        tf::Pose drive_s;
        tf::Pose target_s;

    public:
        StatePositioning(DockingDriverImpl* self) : State("POSITIONING", self) {}

        void entry() {
            double starting_pose_yaw;
            switch(self_->config_.get().docking_face) {
                case DockingControllerConfig::DOCKFACE_FRONT:
                    starting_pose_yaw = M_PI;
                    break;
                case DockingControllerConfig::DOCKFACE_BACK:
                    starting_pose_yaw = 0;
                    break;
            }

            tf::Pose robot_pose_s = self_->controller_.get_robot_pose();
            drive_s = robot_pose_s;
            target_s = tf::Pose(tf::createQuaternionFromYaw(starting_pose_yaw), robot_pose_s.getOrigin());
            self_->controller_.set_direction(Controller::FORWARD); // 회전만 할 것이므로 FORWARD 든 BACKWARD 든 상관 없음
        }

        bool compute_cmd_vel(geometry_msgs::Twist& cmd_vel) {
            cmd_vel = geometry_msgs::Twist();

            tf::Pose robot_pose_s = self_->controller_.get_robot_pose();
            // Target frame 으로 표현한 robot pose
            tf::Pose robot_pose_t = target_s.inverse()*robot_pose_s;

            double pos_x, offsets_error, heading_error;
            workerbee_utils::Geometry::get_pose_2d(robot_pose_t, pos_x, offsets_error, heading_error);

            if(fabs(heading_error) < 5 * M_PI/180) {
                self_->state_transition(&self_->state_post_positioning_);
                return true;
            }

            double translation_speed = 0;
            self_->controller_.set_obstacle(true, self_->config_.get().obstacle_range_from_surface);
            return self_->controller_.compute_cmd_vel(cmd_vel,
                    self_->config_.get().footprint,
                    target_s,
                    drive_s,
                    translation_speed, self_->config_.get().rotation_inplace_speed);
        }

    } state_positioning_;




    friend StatePositioning;

    /*********************************************************************************
     *
     *
     *
     *********************************************************************************/
    class StatePostPositioning : public State
    {
        double delay_;
    public:
        StatePostPositioning(DockingDriverImpl* self) : State("POST_POSITIONING", self) {
            delay_ = 1.0; // sec
        }

        bool compute_cmd_vel(geometry_msgs::Twist& cmd_vel) {
            if(get_execute_time() > delay_) {
                self_->state_transition(&self_->state_approaching_);
            }

            cmd_vel = geometry_msgs::Twist();
            return true;
        }

    } state_post_positioning_;


    /*********************************************************************************
     *
     *
     *
     *********************************************************************************/
    class StateApproaching : public State
    {
        // station coordinate
        tf::Pose target_s_goal, target_s;
        tf::Pose drive_s;

    public:
        StateApproaching(DockingDriverImpl* self) : State("APPROACHING", self) {}

        void entry() {
            tf::Vector3 drive_pos  = self_->config_.get().docking_goal_center + tf::Vector3(self_->config_.get().approaching_length, 0, 0);

            target_s_goal = tf::Pose(tf::createQuaternionFromYaw(M_PI), self_->config_.get().approching_goal_center);
            target_s = tf::Pose(tf::createQuaternionFromYaw(M_PI), self_->config_.get().docking_goal_center);
            drive_s = tf::Pose(tf::createQuaternionFromYaw(M_PI),  drive_pos);

            switch(self_->config_.get().docking_face) {
                case DockingControllerConfig::DOCKFACE_FRONT:
                    self_->controller_.set_direction(Controller::FORWARD);
                    break;
                case DockingControllerConfig::DOCKFACE_BACK:
                    self_->controller_.set_direction(Controller::BACKWARD);
                    break;
            }

            self_->retry_count_++;
        }

        bool compute_cmd_vel(geometry_msgs::Twist& cmd_vel) {
            cmd_vel = geometry_msgs::Twist();

            if(self_->retry_count_ >= self_->cfg_retry_max_) {
                self_->state_failure_.set_error(DockingDriverStatePublisher::ERROR_MISALIGNMENT);
                self_->state_transition(&self_->state_failure_);
                return true;
            }

            tf::Pose robot_pose_s = self_->controller_.get_robot_pose();
            // Target frame 으로 표현한 robot pose
            tf::Pose robot_pose_t = target_s_goal.inverse()*robot_pose_s;

            double pos_x, offsets_error, heading_error;
            workerbee_utils::Geometry::get_pose_2d(robot_pose_t, pos_x, offsets_error, heading_error);

            self_->controller_.set_obstacle(true, self_->config_.get().obstacle_range_from_surface);
            bool controller_res = self_->controller_.compute_cmd_vel(cmd_vel, self_->config_.get().footprint, target_s, drive_s, self_->config_.get().translation_speed);

            if(pos_x > -0.2) {
                ROS_INFO("DOCKING_DRIVER. APPROACHING(retry=%d/%d) pos_x %.2f cm, offset_error %.3f cm, heading error %.1f deg, vx %.3f m/s",
                        self_->retry_count_, self_->cfg_retry_max_,
                        std::fabs(pos_x)*100, offsets_error*100, rad2deg(heading_error), cmd_vel.linear.x);
            }

            if(pos_x >= 0) {
                if(std::fabs(offsets_error) <= self_->config_.get().offset_error_threshold &&
                   std::fabs(rad2deg(heading_error)) <= self_->config_.get().heading_error_threshold ) {
                    self_->state_transition(&self_->state_docking_);
                }
                else {
                    self_->state_transition(&self_->state_leaving_);
                    cmd_vel = geometry_msgs::Twist();
                    return true;
                }
            }

            return controller_res;
        }

    } state_approaching_;

    friend StateApproaching;


    /*********************************************************************************
     *
     *
     *
     *********************************************************************************/
    class StateDocking : public State
    {
        // station coordinate
        tf::Pose target_s;
        tf::Pose drive_s;

    public:
        StateDocking(DockingDriverImpl* self) : State("DOCKING", self) {}

        void entry() {
            tf::Vector3 target_pos = self_->config_.get().docking_goal_center;
            tf::Vector3 drive_pos  = self_->config_.get().docking_goal_center + tf::Vector3(self_->config_.get().approaching_length, 0, 0);

            target_s = tf::Pose(tf::createQuaternionFromYaw(M_PI), target_pos);
            drive_s = tf::Pose(tf::createQuaternionFromYaw(M_PI),  drive_pos);

            switch(self_->config_.get().docking_face) {
                case DockingControllerConfig::DOCKFACE_FRONT:
                    self_->controller_.set_direction(Controller::FORWARD);
                    break;
                case DockingControllerConfig::DOCKFACE_BACK:
                    self_->controller_.set_direction(Controller::BACKWARD);
                    break;
            }
        }

        bool compute_cmd_vel(geometry_msgs::Twist& cmd_vel) {
            cmd_vel = geometry_msgs::Twist();

            tf::Pose robot_pose_s = self_->controller_.get_robot_pose();
            // Target frame 으로 표현한 robot pose
            tf::Pose robot_pose_t = target_s.inverse()*robot_pose_s;

            double pos_x, offsets_error, heading_error;
            workerbee_utils::Geometry::get_pose_2d(robot_pose_t, pos_x, offsets_error, heading_error);

            bool enable_obstacle_detection = (std::fabs(pos_x) > self_->config_.get().obstacle_range_from_surface + 0.2);

            self_->controller_.set_obstacle(enable_obstacle_detection, self_->config_.get().obstacle_range_from_surface);
            bool controller_res = self_->controller_.compute_cmd_vel(cmd_vel, self_->config_.get().footprint, target_s, drive_s, self_->config_.get().translation_speed);

            if(pos_x > -0.2) {
                ROS_INFO("DOCKING_DRIVER. DOCKING. pos_x %.2f cm, offset_error %.3f cm, heading error %.1f deg, vx %.3f m/s, obstacle_detection=%s",
                        std::fabs(pos_x)*100, offsets_error*100, rad2deg(heading_error), cmd_vel.linear.x, enable_obstacle_detection?"True":"False");
            }

            if(pos_x >= 0) {
                if(std::fabs(offsets_error) <= self_->config_.get().offset_error_threshold &&
                        std::fabs(rad2deg(heading_error)) <= self_->config_.get().heading_error_threshold ) {
                    self_->state_transition(&self_->state_wait_connection_);
                }
                else {
                    self_->state_failure_.set_error(DockingDriverStatePublisher::ERROR_MISALIGNMENT);
                    self_->state_transition(&self_->state_failure_);
                    cmd_vel = geometry_msgs::Twist();
                }

                return true;
            }

            return controller_res;
        }


    } state_docking_;

    friend StateDocking;



    /*********************************************************************************
     *
     *
     *
     *********************************************************************************/
    class StateLeaving : public State
    {
        // station coordinate
        tf::Pose target_s;
        tf::Pose drive_s;

    public:
        StateLeaving(DockingDriverImpl* self) : State("LEAVING", self) {}

        void entry() {
            tf::Vector3 target_pos = self_->config_.get().docking_goal_center + tf::Vector3(self_->config_.get().approaching_length, 0, 0);
            tf::Vector3 drive_pos  = self_->config_.get().docking_goal_center;

            target_s = tf::Pose(tf::createQuaternionFromYaw(0), target_pos);
            drive_s = tf::Pose(tf::createQuaternionFromYaw(0),  drive_pos);

            switch(self_->config_.get().docking_face) {
                case DockingControllerConfig::DOCKFACE_FRONT:
                    self_->controller_.set_direction(Controller::BACKWARD);
                    break;
                case DockingControllerConfig::DOCKFACE_BACK:
                    self_->controller_.set_direction(Controller::FORWARD);
                    break;
            }
        }

        void exit() {
        }

        bool compute_cmd_vel(geometry_msgs::Twist& cmd_vel) {
            cmd_vel = geometry_msgs::Twist();

            tf::Pose robot_pose_s = self_->controller_.get_robot_pose();
            bool enable_obstacle_detection = true;
            self_->controller_.set_obstacle(enable_obstacle_detection, self_->config_.get().obstacle_range_from_surface);

            // Target frame 으로 표현한 robot pose
            tf::Pose robot_pose_t = target_s.inverse()*robot_pose_s;

            if(robot_pose_t.getOrigin().x() >= 0) {
                self_->state_transition(&self_->state_approaching_);
                return true;
            }

            return self_->controller_.compute_cmd_vel(cmd_vel, self_->config_.get().footprint, target_s, drive_s, self_->config_.get().translation_speed);
        }

    } state_leaving_;

    friend StateLeaving;

    /*********************************************************************************
     *
     *
     *
     *********************************************************************************/
    class StateUndocking : public State
    {
        tf::Pose target_c;
        tf::Pose drive_c;

    public:
        StateUndocking(DockingDriverImpl* self) : State("UNDOCKING", self) {}

        void entry() {
            tf::Vector3 target_pos = self_->config_.get().docking_goal_center + tf::Vector3(self_->config_.get().undocking_goal, 0, 0);
            tf::Vector3 drive_pos  = self_->config_.get().docking_goal_center;

            target_c = tf::Pose(tf::createQuaternionFromYaw(0), target_pos);
            drive_c = tf::Pose(tf::createQuaternionFromYaw(0),  drive_pos);

            switch(self_->config_.get().docking_face) {
                case DockingControllerConfig::DOCKFACE_FRONT:
                    self_->controller_.set_direction(Controller::BACKWARD);
                    break;
                case DockingControllerConfig::DOCKFACE_BACK:
                    self_->controller_.set_direction(Controller::FORWARD);
                    break;
            }
        }

        bool compute_cmd_vel(geometry_msgs::Twist& cmd_vel) {
            cmd_vel = geometry_msgs::Twist();

            tf::Pose robot_pose_s = self_->controller_.get_robot_pose();
            bool enable_obstacle_detection = true;
            self_->controller_.set_obstacle(enable_obstacle_detection, self_->config_.get().obstacle_range_from_surface);

            // Target frame 으로 표현한 robot pose
            tf::Pose robot_pose_t = target_c.inverse()*robot_pose_s;

            if(robot_pose_t.getOrigin().x() >= 0) {
                self_->state_transition(&self_->state_success_);
                return true;
            }

            return self_->controller_.compute_cmd_vel(cmd_vel, self_->config_.get().footprint, target_c, drive_c, self_->config_.get().translation_speed);
        }

    } state_undocking_;

    friend StateUndocking;


    /*********************************************************************************
     *
     *
     *
     *********************************************************************************/
    class StateWaitForConnection : public State
    {
        ros::Time start;
        ros::Timer timer_fake_event;
        ros::Timer timer;
        bool docking_failure;

        void cb_timer_fake_event(const ros::TimerEvent& event) {
            self_->connect_event("FAKE");
        }

        // connection_timeout_event
        void cb_timer(const ros::TimerEvent& event) {
            double duration = (ros::Time::now() - start).toSec();
            if(duration > self_->config_.get().connection_timeout) {
                docking_failure = true;
                timer.stop();
                ROS_ERROR("DOCKING_DRIVER. connection timeout. %.2f sec", self_->config_.get().connection_timeout);
            }
        }

    public:
        StateWaitForConnection(DockingDriverImpl* self) : State("WAIT_FOR_CONNECT", self) {}

        void entry() {
            ros::NodeHandle nh;
            if(self_->cfg_simulation_ || self_->config_.get().enable_fake_event) {
                double fake_event_delay = 2;
                timer_fake_event = nh.createTimer(ros::Duration(fake_event_delay), &StateWaitForConnection::cb_timer_fake_event, this, true);
                ROS_WARN("DOCKING_DRIVER. ## ENABLE FAKE CONNECT_EVENT");
            }

            timer = nh.createTimer(ros::Duration(0.2), &StateWaitForConnection::cb_timer, this);
            docking_failure = false;
            start = ros::Time::now();
        }

        void exit() {
            if(self_->cfg_simulation_) {
                timer_fake_event.stop();
            }
            timer.stop();
        }

        bool compute_cmd_vel(geometry_msgs::Twist& cmd_vel) {
            cmd_vel = geometry_msgs::Twist();

            if(docking_failure) {
                self_->state_failure_.set_error(DockingDriverStatePublisher::ERROR_CONNECTION_TIMEOUT);
                self_->state_transition(&self_->state_failure_);
            }

            return true;
        }

        void event_connect(const char* event_type="") {
            if(docking_failure == false) {
                ROS_INFO("DOCKING_DRIVER. CONNECT_EVENT(%s)", event_type);
                self_->state_transition(&self_->state_success_);
            }
        }

    } state_wait_connection_;

    friend StateWaitForConnection;

    /*********************************************************************************
     *
     *
     *
     *********************************************************************************/
    class StateSuccess : public State
    {
    public:
        StateSuccess(DockingDriverImpl* self) : State("SUCCUESS", self) {}

        void entry() {
            self_->complete_ = true;
        }

    } state_success_;

    friend StateSuccess;

//    /*********************************************************************************
//     *
//     *
//     *
//     *********************************************************************************/
//    class StateFailure : public State
//    {
//    public:
//        StateFailure(DockingDriverImpl* self) : State("FAILURE", self) {}
//
//        void entry() {
//            self_->complete_ = true;
//        }
//
//    } state_failure_;
//
//    friend StateFailure;

    /*********************************************************************************
     *
     *
     *
     *********************************************************************************/
    class StateFailure : public State
    {
        // station coordinate
        tf::Pose target_s;
        tf::Pose drive_s;
        DockingDriverStatePublisher::State error_;

    public:
        StateFailure(DockingDriverImpl* self) : State("FAILURE", self), error_(DockingDriverStatePublisher::ERROR_UNKNOWN) {}

        void entry() {
            tf::Vector3 target_pos = self_->config_.get().docking_goal_center + tf::Vector3(self_->config_.get().approaching_length, 0, 0);
            tf::Vector3 drive_pos  = self_->config_.get().docking_goal_center;

            target_s = tf::Pose(tf::createQuaternionFromYaw(0), target_pos);
            drive_s = tf::Pose(tf::createQuaternionFromYaw(0),  drive_pos);

            switch(self_->config_.get().docking_face) {
                case DockingControllerConfig::DOCKFACE_FRONT:
                    self_->controller_.set_direction(Controller::BACKWARD);
                    break;
                case DockingControllerConfig::DOCKFACE_BACK:
                    self_->controller_.set_direction(Controller::FORWARD);
                    break;
            }
        }

        void exit() {
        }

        bool compute_cmd_vel(geometry_msgs::Twist& cmd_vel) {
            cmd_vel = geometry_msgs::Twist();

            tf::Pose robot_pose_s = self_->controller_.get_robot_pose();
            bool enable_obstacle_detection = true;
            self_->controller_.set_obstacle(enable_obstacle_detection, self_->config_.get().obstacle_range_from_surface);

            // Target frame 으로 표현한 robot pose
            tf::Pose robot_pose_t = target_s.inverse()*robot_pose_s;

            if(robot_pose_t.getOrigin().x() >= 0) {
                driver_state_pub.set_state(error_, "");
                return false;
            }

            return self_->controller_.compute_cmd_vel(cmd_vel, self_->config_.get().footprint, target_s, drive_s, self_->config_.get().translation_speed);
        }

        void event_connect(const char* event_type="") {
            ROS_ERROR("DOCKING_DRIVER. CONNECT_EVENT(%s) TOO LATE!!!", event_type);
        }

        void set_error(DockingDriverStatePublisher::State code) {
            error_ = code;
        }

    } state_failure_;

    friend StateFailure;




    void disconnect_event() {
        boost::mutex::scoped_lock l(mutex_);
        state_current_->event_disconnect();
    }

public:
    DockingDriverImpl()
    : tf_(0)
    , costmap_ros_(0)
    , controller_(costmap_util_)
    , initialized_(false)
    , cfg_retry_max_(3)
    , cfg_simulation_(false)
    , retry_count_(0)
    , complete_(false)
    , footprint_cost_(0)
    , state_current_(0)
    , state_idle_(this)
    , state_init_docking_(this)
    , state_init_undocking_(this)
    , state_positioning_(this)
    , state_post_positioning_(this)
    , state_approaching_(this)
    , state_docking_(this)
    , state_leaving_(this)
    , state_undocking_(this)
    , state_wait_connection_(this)
    , state_success_(this)
    , state_failure_(this)
    {

    }

    virtual ~DockingDriverImpl() {}

    bool isInitialized() {
        return initialized_;
    }

    void initialize(
            std::string name,
            tf2_ros::Buffer* tf,
            costmap_2d::Costmap2DROS* costmap_ros) {

        if(isInitialized()) {
            ROS_WARN("This planner has already been initialized, doing nothing.");
            return;
        }

        state_transition(&state_idle_);

        tf_ = tf;
        costmap_ros_ = costmap_ros;
        global_frame_ = costmap_ros_->getGlobalFrameID();
        robot_base_frame_ = costmap_ros_->getBaseFrameID();

        ros::NodeHandle private_nh("~/" + name);

        private_nh.param<bool>("/simulation", cfg_simulation_, false);

        config_.initialize(private_nh, costmap_ros, &controller_);

        costmap_util_.initialize(costmap_ros);
        marker_ = boost::shared_ptr<VisualizationMarker>(new VisualizationMarker(private_nh, "markers", "docking", 1.0));
        timer_ = private_nh.createTimer(ros::Duration(0.1), &DockingDriverImpl::timer_callback, this);

        initialized_ = true;
    }

    void timer_callback(const ros::TimerEvent& event) {
        tf::Stamped<tf::Pose> station_pose;
        station_pose.setIdentity();
        station_pose.frame_id_ = config_.get().station_frame_id;

        geometry_msgs::PoseStamped station_pose_msg;
        tf::poseStampedTFToMsg(station_pose, station_pose_msg);

        marker_->draw_docking_path(station_pose_msg, config_.get().approching_path, config_.get().docking_start_goal_center);
        marker_->publish();
    }

    template <typename DockingDriverConfigType>
    void reconfigure(DockingDriverConfigType &config) {
        boost::mutex::scoped_lock l(mutex_);
        config_.reconfigure(config);
    }

    bool isGoalReached() {
        return complete_;
    }

    bool start(unsigned int mode, unsigned int retry) {
        boost::mutex::scoped_lock l(mutex_);

        cfg_retry_max_ = retry;
        retry_count_ = 0;
        complete_ = false;

        state_transition(&state_idle_);

        switch(mode) {
        case workerbee_platform::StartDocking::Request::DOCKING:
            state_current_->start_docking();
            break;
        case workerbee_platform::StartDocking::Request::UNDOCKING:
            state_current_->start_undocking();
            break;
        default:
            break;
        }

        return true;
    }

    bool compute_cmd_vel(geometry_msgs::Twist& cmd_vel) {
        boost::mutex::scoped_lock l(mutex_);

#if 0
        geometry_msgs::PoseStamped robot_pose_msg;
        if (!costmap_ros_->getRobotPose(robot_pose_msg)) {
            std::string e = "could not get robot pose";
            driver_state_pub.set_state(DockingDriverStatePublisher::ERROR_COSTMAP, "could not get robot pose in the global frame of costmap");
            return false;
        }

        tf::Stamped<tf::Pose> robot_pose_g;
        tf::poseStampedMsgToTF(robot_pose_msg, robot_pose_g);

        footprint_cost_ = costmap_util_.get_footprint_cost(config_.get().footprint, robot_pose_g);
        if(footprint_cost_ < 0) {
            driver_state_pub.set_state(DockingDriverStatePublisher::ERROR_COLLISION, "collision detected");
            return false;
        }
#endif

        tf::Stamped<tf::Pose> robot_pose_s;
        if(!get_robot_pose_in_station_frame(robot_pose_s)) {
            driver_state_pub.set_state(DockingDriverStatePublisher::ERROR_ETC, "could not get robot pose in the station frame");
            return false;
        }

        controller_.set_robot_pose(robot_pose_s);
        return state_current_->compute_cmd_vel(cmd_vel);
    }

    void connect_event(const char* event_type="") {
        boost::mutex::scoped_lock l(mutex_);
        state_current_->event_connect(event_type);
    }
};


#endif /* __DOCKING_DRIVER_BASE_H__ */
