/*
 * docking_driver.cpp
 *
 *  Created on: Oct 17, 2018
 *      Author: ('c')void
 */

#include "docking_driver_base.h"

double rad2deg(double radian)
{
    return radian*180/M_PI;
}


void get_robot_geometry(const std::vector<geometry_msgs::Point>& footprint,
                        double& radius,
                        double& center_offset,
                        double& footprint_sz_x, double& footprint_sz_y) {
    std::vector<double> list;

    /*
     * robot radius
     */
    for(int i=0; i<footprint.size(); i++) {
        double rx = footprint[i].x;
        double ry = footprint[i].y;
        double r = sqrt(rx*rx + ry*ry);
        list.push_back(r);
    }

    radius = *std::max_element(list.begin(), list.end());

    /*
     *  bounding box size
     */
    list.clear();
    for(int i=0; i<footprint.size(); i++) {
        list.push_back(footprint[i].x);
    }

    double x_min = *std::min_element(list.begin(), list.end());
    double x_max = *std::max_element(list.begin(), list.end());

    list.clear();
    for(int i=0; i<footprint.size(); i++) {
        list.push_back(footprint[i].y);
    }

    double y_min = *std::min_element(list.begin(), list.end());
    double y_max = *std::max_element(list.begin(), list.end());

    footprint_sz_x = x_max - x_min;
    footprint_sz_y = y_max - y_min;

    /*
     *  center offset
     *   - 충전면과 robot center와의 거리
     */
    center_offset = fabs(x_min);
}

DockingDriverStatePublisher driver_state_pub;



