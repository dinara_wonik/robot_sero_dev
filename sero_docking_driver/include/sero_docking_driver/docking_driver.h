/*
 * local_planner.h
 *
 *  Created on: Sep 7, 2018
 *      Author: ('c')void
 */

#ifndef __DOCKING_DRIVER_H__
#define __DOCKING_DRIVER_H__

#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <costmap_2d/costmap_2d.h>
#include <costmap_2d/costmap_2d_ros.h>
#include <geometry_msgs/Twist.h>
#include <sero_mobile_msgs/DockingState.h>
#include <sero_docking_driver/DockingDriverConfig.h>

namespace sero_docking_driver {

class DockingDriver {
    void* impl_;

    DockingDriverConfig default_config_;
    bool setup_;

    std::string docking_topic_;
    ros::Subscriber sub_docking_state_;

    boost::shared_ptr<dynamic_reconfigure::Server<DockingDriverConfig> > dsrv_;

    void reconfigure_callback(DockingDriverConfig &config, uint32_t level);
    void cb_docking_state(const sero_mobile_msgs::DockingState::ConstPtr& msg);

public:
    DockingDriver();
    virtual ~DockingDriver();
    void initialize(std::string name, tf2_ros::Buffer* tf, costmap_2d::Costmap2DROS* costmap_ros);
    bool computeVelocityCommands(geometry_msgs::Twist& cmd_vel);
    bool isGoalReached();
    bool isInitialized();

    bool start(unsigned int mode, unsigned int retry);
};

}; // namespace sero_docking_driver


#endif /* __DOCKING_DRIVER_H__ */
