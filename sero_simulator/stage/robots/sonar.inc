
# https://github.com/rtv/Stage/blob/master/worlds/pioneer.inc

define p2dxsonar sensor
(
  update_interval 50 # == interval_sim
  # define the size of each transducer [xsize ysize zsize] in meters
  size [0.01 0.05 0.01 ]
  # define the range bounds [min max]
  range [0 5.0]
  # define the angular field of view in degrees
  fov 15
  # define the number of samples spread over the fov
  samples 1
)



# x, y, z, th(deg)
define sonar_1 ranger ( # 0 번 sonar에 대응
 p2dxsonar( pose [  0.1445  0.0000  0.2132   0 ] ) 
)

define sonar_2 ranger (
 p2dxsonar( pose [  0.0000 -0.1445  0.2132   -90 ] )
)

define sonar_3 ranger (
 p2dxsonar( pose [ -0.1445  0.0000  0.2132   -180 ] )
)

define sonar_4 ranger (
 p2dxsonar( pose [  0.0000  0.1445  0.2132   90 ] )
)


define sonar_5 ranger (
 p2dxsonar( pose [  0.1445  0.1000  0.2132   0 ] ) 
)

define sonar_6 ranger (
 p2dxsonar( pose [  0.1445 -0.1000  0.2132   0 ] ) 
)


define sonar_7 ranger (
 p2dxsonar( pose [  0.1000 -0.1445  0.2132   -90 ] ) 
)

define sonar_8 ranger (
 p2dxsonar( pose [ -0.1000 -0.1445  0.2132   -90 ] ) 
)


define sonar_9 ranger (
 p2dxsonar( pose [ -0.1445 -0.1000  0.2132   -180 ] )
)

define sonar_10 ranger (
 p2dxsonar( pose [ -0.1445  0.1000  0.2132   -180 ] )
)


define sonar_11 ranger (
 p2dxsonar( pose [ -0.1000  0.1445  0.2132   90 ] )
)

define sonar_12 ranger (
 p2dxsonar( pose [  0.1000  0.1445  0.2132   90 ] )
)
