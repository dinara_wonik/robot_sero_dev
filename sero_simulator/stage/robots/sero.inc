#
# 2018.02.12 ('c')void
#  - addy ver.2
#    - mecanum drive
#

include "robots/sick.inc"
include "robots/sonar.inc"


### ROBOT ###

define diff_drive_robot position
(
  update_interval 50 # == interval_sim
  gui_nose 0
  laser_return 1

  pose [ 0.0 0.0 0.0 0.0 ]

  # kinematics
  drive "diff"
  #drive "omni"

  # https://github.com/rtv/Stage/blob/master/libstage/model_position.cc
  # odometry error model parameters,
  # only used if localization is set to "odom"
  #

  localization "odom" # integration error [x, y, z, theta]
  odom_error [0.05 0.05 0.0 0.2]

  # localization "gps"
  # localization_origin [ 0 0 0 0 ]

  size [ 0.576 0.476 0.40 ]
  # center of rotation offset
  origin [ 0.0 0.0 0.0 0.0 ]
  color "red"

  # http://wiki.ros.org/stage/Tutorials/IntroductiontoStageControllers
  sicklaser(
    update_interval 50 # == interval_sim
    ctrl "lasernoise"
    pose [ 0.0 0.0 -0.11 0.0 ]
    alwayson 1
  )

#  sonar_1 ( alwayson 1 )
#  sonar_2 ( alwayson 1 )
#  sonar_3 ( alwayson 1 )
#  sonar_4 ( alwayson 1 )
#  sonar_5 ( alwayson 1 )
#  sonar_6 ( alwayson 1 )
#  sonar_7 ( alwayson 1 )
#  sonar_8 ( alwayson 1 )
#  sonar_9 ( alwayson 1 )
#  sonar_10 ( alwayson 1 )
#  sonar_11 ( alwayson 1 )
#  sonar_12 ( alwayson 1 )



  # [ xmin xmax ymin ymax zmin zmax amin amax ]
  velocity_bounds [-2 2 -2 2 -2 2 -180 180 ]
  acceleration_bounds [-10 10 -10 10 -10 10 -900 900]
)

### Wanderer ###

define wanderer position
(
  update_interval 50 # == interval_sim
  laser_return 1

  odom_error [0 0 0 0 0 0]

  size [ 0.3 0.3 0.3 ]
  origin [ 0.0 0.0 0.0 0.0 ]
  color "blue"

  # ranger:0
  sicklaser_low_cost(
  #pose [ 0.0 0.0 0.0 0.0 ]
    alwayson 0
  )

  # ranger:1
  sicklaser_low_cost(
  #pose [ -0.1 0.0 -0.11 0.0 ]
    alwayson 1
  )

  # kinematics
  drive "diff"

  # report error-free position in world coordinates
  localization "gps"
  localization_origin [ 0 0 0 0 ]

  ctrl "wander"
)


define charging_station model
(
  update_interval 50 # == interval_sim
  size [0.400 0.600 1.500]
  gui_nose 0
)


# define charging_station_vmark model
# (
#     block( points 7
#            point[0] [0.          0.0]
#            point[1] [0.16666667  0.0]
#            point[2] [0.41666667  0.5]
#            point[3] [0.66666667  0.0]
#            point[4] [1.          0.0]
#            point[5] [1.          1.0]
#            point[6] [0.          1.0]
#            z [0 0.5]
#   )
#   size [0.6 0.2 1.500]
#   update_interval 50 # == interval_sim
#   gui_nose 0
# )


define charging_station_vmark model
(
  block( points 6
         point[0] [0.000  0.000]
         point[1] [1.000  0.000]
         point[2] [0.500  0.18034884]
         point[3] [0.500  0.81965116]
         point[4] [1.000  1.000]
         point[5] [0.000  1.000]
         z [0 0.5]
  )
  size [0.3102 0.860 1.500]
  update_interval 50 # == interval_sim
  gui_nose 0
)



