/* Copyright 2017 Wonik Robotics. All rights reserved.
 *
 * This file is a proprietary of Wonik Robotics
 * and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF Wonik Robotics.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * world_frame.cpp
 *
 *  Created on: May 9, 2017
 *      Author: ('c')void
 *
 * stage 에서 여러개의 model을 만드는 경우 모델간 tf 관계는 정의되지 않는다.
 * 시뮬레이션을 위한 측정값(UWB measurements)을 편리하게 만들기위해 tf를 정의한다.
 */

#include <iostream>
#include <string>

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>


static const std::string cfg_node_name = "world_frame_node";


class WorldFrame
{
    const std::string _cfg_user_odom_topic;
    const std::string _cfg_robot_odom_topic;

    const std::string _cfg_frame_id_robot_odom;
    const std::string _cfg_frame_id_user_odom;
    const std::string _cfg_frame_id_robot;
    const std::string _cfg_frame_id_world; // groud truth

    ros::Subscriber _sub_robot_ground_truth;
    tf::TransformBroadcaster _tf_br;
    tf::TransformListener _tf_listener;

    void robot_odom_callback(const nav_msgs::Odometry& robot_odom) {

        ros::Time now = robot_odom.header.stamp;

        tf::StampedTransform tf_robot2rodom_stamped;
        try{
            _tf_listener.lookupTransform(_cfg_frame_id_robot, _cfg_frame_id_robot_odom, ros::Time(0), tf_robot2rodom_stamped);
//            _tf_listener.lookupTransform(_cfg_frame_id_robot, _cfg_frame_id_robot_odom, now, tf_robot2rodom_stamped);
        }
        catch (tf::TransformException ex){
            ROS_ERROR("%s",ex.what());
            return;
        }

        tf::Transform tf_robot2rodom;
        tf_robot2rodom.setOrigin(tf_robot2rodom_stamped.getOrigin());
        tf_robot2rodom.setRotation(tf_robot2rodom_stamped.getRotation());

        // odom_msg_ground_truth.header.frame_id가 "/robot/odom" 이지만 실은 "/world"다.
        tf::Transform tf_world2robot;
        tf_world2robot.setOrigin(
                tf::Vector3(
                    robot_odom.pose.pose.position.x,
                    robot_odom.pose.pose.position.y,
                    robot_odom.pose.pose.position.z));
        tf_world2robot.setRotation(
                tf::Quaternion(
                    robot_odom.pose.pose.orientation.x,
                    robot_odom.pose.pose.orientation.y,
                    robot_odom.pose.pose.orientation.z,
                    robot_odom.pose.pose.orientation.w));

        tf::Transform tf_world2rodom = tf_world2robot*tf_robot2rodom;
        tf::Transform tf_rodom2world = tf_world2rodom.inverse();

        tf::Transform tf_world2uodom;
        tf_world2uodom.setOrigin(tf::Vector3(0, 0, 0));
        tf_world2uodom.setRotation(tf::Quaternion(0, 0, 0, 1));

        _tf_br.sendTransform(tf::StampedTransform(tf_rodom2world, now, _cfg_frame_id_robot_odom, _cfg_frame_id_world));
        _tf_br.sendTransform(tf::StampedTransform(tf_world2uodom, now, _cfg_frame_id_world, _cfg_frame_id_user_odom));
    }

public:
    WorldFrame()
    : _cfg_user_odom_topic("/user/base_pose_ground_truth")
    , _cfg_robot_odom_topic("/robot/base_pose_ground_truth")
    , _cfg_frame_id_robot_odom("robot/odom")
    , _cfg_frame_id_user_odom("user/odom")
    , _cfg_frame_id_robot("robot/base_link")
    , _cfg_frame_id_world("world")
    {
        ros::NodeHandle nh;
        _sub_robot_ground_truth = nh.subscribe(_cfg_robot_odom_topic, 10, &WorldFrame::robot_odom_callback, this);
    }

    ~WorldFrame() {}
};


int main(int argc, char **argv) {
    ros::init(argc, argv, cfg_node_name);
    WorldFrame wf;
    ROS_INFO("%s has been initialized", cfg_node_name.c_str());
    ros::spin();
}



