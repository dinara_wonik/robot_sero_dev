/* Copyright 2017 Wonik Robotics. All rights reserved.
 *
 * This file is a proprietary of Wonik Robotics
 * and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF Wonik Robotics.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file fake_sonar.h
 *
 * @brief void.
 */

/*
 *  Created on: Nov 2, 2017
 *      Author: ('c')void
 */

#ifndef __FAKE_SONAR_H__
#define __FAKE_SONAR_H__

#include <iostream>
#include <string>

#define _USE_MATH_DEFINES
#include <math.h>

#include <ros/ros.h>
#include <tf/tf.h>
#include <tf2/LinearMath/Quaternion.h>
#include "tf2_ros/static_transform_broadcaster.h"

#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Range.h>
#include <geometry_msgs/PoseStamped.h>

#include "sonar_publisher.h"


class FakeSonarArray
{
    ros::NodeHandle& private_nh_;
    double cfg_publish_rate_hz_;
    ros::Timer timer_;

    std::vector<ros::Subscriber> sub_array_;

    SonarPublisher pub_sonar_;

    bool enable_;

    // stage는 sonar message로 sensor_msgs::LaserScan 발행함.
    void callback_sonar(int id, const sensor_msgs::LaserScanPtr& scan) {
        pub_sonar_.set_range(id, scan->ranges[0]);
    }

    void callback_sonar_1 (const sensor_msgs::LaserScanPtr& scan) { callback_sonar( 1, scan); }
    void callback_sonar_2 (const sensor_msgs::LaserScanPtr& scan) { callback_sonar( 2, scan); }
    void callback_sonar_3 (const sensor_msgs::LaserScanPtr& scan) { callback_sonar( 3, scan); }
    void callback_sonar_4 (const sensor_msgs::LaserScanPtr& scan) { callback_sonar( 4, scan); }
    void callback_sonar_5 (const sensor_msgs::LaserScanPtr& scan) { callback_sonar( 5, scan); }
    void callback_sonar_6 (const sensor_msgs::LaserScanPtr& scan) { callback_sonar( 6, scan); }
    void callback_sonar_7 (const sensor_msgs::LaserScanPtr& scan) { callback_sonar( 7, scan); }
    void callback_sonar_8 (const sensor_msgs::LaserScanPtr& scan) { callback_sonar( 8, scan); }
    void callback_sonar_9 (const sensor_msgs::LaserScanPtr& scan) { callback_sonar( 9, scan); }
    void callback_sonar_10(const sensor_msgs::LaserScanPtr& scan) { callback_sonar(10, scan); }
    void callback_sonar_11(const sensor_msgs::LaserScanPtr& scan) { callback_sonar(11, scan); }
    void callback_sonar_12(const sensor_msgs::LaserScanPtr& scan) { callback_sonar(12, scan); }

    void timer_callback(const ros::TimerEvent& event) {
        if(enable_) {
            pub_sonar_.publish();
        }
    }

public:
    FakeSonarArray(ros::NodeHandle& private_nh)
    : private_nh_(private_nh)
    , cfg_publish_rate_hz_(5)
    , pub_sonar_(private_nh)
    , enable_(true)
    {
        const int qsz = 10;
        sub_array_.push_back(private_nh_.subscribe("/robot/base_scan_1",  qsz, &FakeSonarArray::callback_sonar_1,  this));
        sub_array_.push_back(private_nh_.subscribe("/robot/base_scan_2",  qsz, &FakeSonarArray::callback_sonar_2,  this));
        sub_array_.push_back(private_nh_.subscribe("/robot/base_scan_3",  qsz, &FakeSonarArray::callback_sonar_3,  this));
        sub_array_.push_back(private_nh_.subscribe("/robot/base_scan_4",  qsz, &FakeSonarArray::callback_sonar_4,  this));
        sub_array_.push_back(private_nh_.subscribe("/robot/base_scan_5",  qsz, &FakeSonarArray::callback_sonar_5,  this));
        sub_array_.push_back(private_nh_.subscribe("/robot/base_scan_6",  qsz, &FakeSonarArray::callback_sonar_6,  this));
        sub_array_.push_back(private_nh_.subscribe("/robot/base_scan_7",  qsz, &FakeSonarArray::callback_sonar_7,  this));
        sub_array_.push_back(private_nh_.subscribe("/robot/base_scan_8",  qsz, &FakeSonarArray::callback_sonar_8,  this));
        sub_array_.push_back(private_nh_.subscribe("/robot/base_scan_9",  qsz, &FakeSonarArray::callback_sonar_9,  this));
        sub_array_.push_back(private_nh_.subscribe("/robot/base_scan_10", qsz, &FakeSonarArray::callback_sonar_10, this));
        sub_array_.push_back(private_nh_.subscribe("/robot/base_scan_11", qsz, &FakeSonarArray::callback_sonar_11, this));
        sub_array_.push_back(private_nh_.subscribe("/robot/base_scan_12", qsz, &FakeSonarArray::callback_sonar_12, this));

        timer_ = private_nh_.createTimer(ros::Duration(1.0/cfg_publish_rate_hz_), &FakeSonarArray::timer_callback, this);
    }

    ~FakeSonarArray() {}

    void enable(bool en) {
        enable_ = en;
    }
};



#endif /* __FAKE_SONAR_H__ */
