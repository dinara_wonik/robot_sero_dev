/* Copyright 2017 Wonik Robotics. All rights reserved.
 *
 * This file is a proprietary of Wonik Robotics
 * and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF Wonik Robotics.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file fake_status.h
 *
 * @brief void.
 */

/*
 *  Created on: Nov 4, 2017
 *      Author: ('c')void
 *
 *  2019.10.08 ('c')void
 *   - revision. workerbee_status
 */

#ifndef __FAKE_STATUS_H__
#define __FAKE_STATUS_H__

#include <iostream>
#include <string>

#define _USE_MATH_DEFINES
#include <math.h>

#include <ros/ros.h>
#include <tf/tf.h>
#include <tf2/LinearMath/Quaternion.h>
#include "tf2_ros/static_transform_broadcaster.h"

#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Range.h>
#include <geometry_msgs/PoseStamped.h>
#include <workerbee_utils/accessories.h>
#include <workerbee_status/workerbee_status.h>

#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/update_functions.h>
#include <diagnostic_updater/DiagnosticStatusWrapper.h>


class EventPublisher
{
    ros::NodeHandle& nh_;
    ros::Timer timer_;

    void timer_callback(const ros::TimerEvent& event) {
        workerbee_status::fault("CLIFF_DETECTED", enable_cliff_detected_, "cliff detected");
        workerbee_status::fault("CLIFF_BACK_OFF", enable_cliff_back_off_, "");
        workerbee_status::fault("EMERGENCY_STOP", enable_emergency_stop_, "");
        workerbee_status::fault("LOW_BATTERY", enable_low_battery_, "");
        workerbee_status::fault("PROXIMITY_STOP", enable_proximity_stop_, "");
        workerbee_status::fault("COMMUNICATION_ERROR", enable_communication_error_, "");
    }

public:
    bool enable_cliff_detected_;
    bool enable_cliff_back_off_;
    bool enable_emergency_stop_;
    bool enable_low_battery_;
    bool enable_proximity_stop_;
    bool enable_communication_error_;

    EventPublisher(ros::NodeHandle& nh)
    : nh_(nh)
    , enable_cliff_detected_(false)
    , enable_cliff_back_off_(false)
    , enable_emergency_stop_(false)
    , enable_low_battery_(false)
    , enable_proximity_stop_(false)
    , enable_communication_error_(false)
    {
        timer_ = nh.createTimer(ros::Duration(1.0/20), &EventPublisher::timer_callback, this);
    }

    ~EventPublisher() {}

    void enable_cliff_detected(bool enable) {
        enable_cliff_detected_ = enable;
    }

    void enable_cliff_back_off(bool enable) {
        enable_cliff_back_off_ = enable;
    }

    void enable_emergency_stop(bool enable) {
        enable_emergency_stop_ = enable;
    }

    void enable_low_battery(bool enable) {
        enable_low_battery_ = enable;
    }

    void enable_proximity_stop(bool enable) {
        enable_proximity_stop_ = enable;
    }

    void enable_communication_error(bool enable) {
        enable_communication_error_ = enable;
    }
};


class FakeStatus
{
    double cfg_publish_rate_hz_;
    ros::Timer timer_;
    EventPublisher& event_publisher_;

    diagnostic_updater::Updater updater_status;
    diagnostic_updater::Updater updater_bat;

    void diagnostics_stat(diagnostic_updater::DiagnosticStatusWrapper &stat) {
        std::string str_dunit_state;
        std::string str_dunit_condition;

        str_dunit_state = "PWR_ON_SRV_ON";

        if(event_publisher_.enable_cliff_back_off_) {
            str_dunit_condition = "CLIFF_BACK_OFF";
        }
        else if(event_publisher_.enable_emergency_stop_) {
            str_dunit_condition = "EMERGENCY_STOP";
        }
        else if(event_publisher_.enable_low_battery_) {
            str_dunit_condition = "LOW_BATTERY";
        }
        else if(event_publisher_.enable_proximity_stop_) {
            str_dunit_condition = "PROXIMITY_STOP";
        }
        else {
            str_dunit_condition = "NORMAL";
        }

        stat.add("DriverStatus", str_dunit_state);
        stat.add("DriverCondition", str_dunit_condition);

        /*
         * cliff
         */
        bool msg_cliff_detected = false;
        bool msg_cliff_top_left = false;
        bool msg_cliff_top_right = false;
        bool msg_cliff_bottom_right = false;
        bool msg_cliff_bottom_left = false;
        if(event_publisher_.enable_cliff_detected_) {
            msg_cliff_detected = true;
            msg_cliff_bottom_left = true;
        }


//        std::vector<std::pair<std::string, std::string> > info_list;
//
//        //
//        info_list.push_back(std::make_pair(std::string("platform"), std::string("TRUE")));
//
//        // bat
//        info_list.push_back(std::make_pair(std::string("platform\\bat\\0\\voltage"), workerbee_utils::string_format("%.2f", 99)));
//
//        // weight
//        info_list.push_back(std::make_pair(std::string("platform\\weight"), workerbee_utils::string_format("%.2f", 120)));
//
//        // driving uint
//        info_list.push_back(std::make_pair(std::string("platform\\driving_unit\\state"), workerbee_utils::string_format("%s", str_dunit_state.c_str())));
//        info_list.push_back(std::make_pair(std::string("platform\\driving_unit\\condition"), workerbee_utils::string_format("%s", str_dunit_condition.c_str())));
//
//        // cliff
//        info_list.push_back(std::make_pair(std::string("platform\\cliff\\"), workerbee_utils::string_format("%s", msg_cliff_detected?"True":"False")));
//        info_list.push_back(std::make_pair(std::string("platform\\cliff\\top_left"), workerbee_utils::string_format("%s", msg_cliff_top_left?"Detected":"NotDetected")));
//        info_list.push_back(std::make_pair(std::string("platform\\cliff\\top_right"), workerbee_utils::string_format("%s", msg_cliff_top_right?"Detected":"NotDetected")));
//        info_list.push_back(std::make_pair(std::string("platform\\cliff\\bottom_right"), workerbee_utils::string_format("%s", msg_cliff_bottom_right?"Detected":"NotDetected")));
//        info_list.push_back(std::make_pair(std::string("platform\\cliff\\bottom_left"), workerbee_utils::string_format("%s", msg_cliff_bottom_left?"Detected":"NotDetected")));
//
//        // send info list
//        workerbee_status::set_info(info_list);

        // TEST
        // workerbee_status::error(std::string("FakeErrorItem_1"));
        // workerbee_status::error(std::string("FakeErrorItem_2"));


        stat.addf("Weight", "%f kg", 120.0);


        // default 가 level2
        stat.summary(diagnostic_msgs::DiagnosticStatus::OK, "No Message");
    }

    void diagnostics_bat(diagnostic_updater::DiagnosticStatusWrapper &stat) {
        stat.add("charging", "FALSE");
        stat.addf("voltage_level", "%f %%", 99.0);
        stat.addf("time_to_empty", "%f", 1234.0);
        stat.summary(diagnostic_msgs::DiagnosticStatus::OK, "No Message");
    }

    void timer_callback(const ros::TimerEvent& event) {
        //diagnostics();
        updater_status.update();
        updater_bat.update();
    }

public:
    FakeStatus(ros::NodeHandle& nh, EventPublisher& event_publisher)
    : cfg_publish_rate_hz_(5)
    , event_publisher_(event_publisher)
    {

        updater_status.setHardwareID("SeRoMobile");
        // updater_.setHardwareIDf("Device-%i-%i", 27, 46);
        updater_status.add("Status", this, &FakeStatus::diagnostics_stat);

        updater_bat.setHardwareID("SeRoMobile");
        updater_bat.add("Battery", this, &FakeStatus::diagnostics_bat);

        timer_ = nh.createTimer(ros::Duration(1.0/cfg_publish_rate_hz_), &FakeStatus::timer_callback, this);
    }

    ~FakeStatus() {}
};


#endif /* __FAKE_STATUS_H__ */
