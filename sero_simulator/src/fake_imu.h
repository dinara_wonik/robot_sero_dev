/* Copyright 2017 Wonik Robotics. All rights reserved.
 *
 * This file is a proprietary of Wonik Robotics
 * and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF Wonik Robotics.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file fake_imu.h
 *
 * @brief void.
 */

/*
 *  Created on: May 9, 2017
 *      Author: ('c')void
 */

#ifndef __FAKE_IMU_H__
#define __FAKE_IMU_H__

#include <iostream>
#include <string>

#include <ros/ros.h>


#include <tf/tf.h>
#include <tf/transform_datatypes.h>

#include <random_numbers/random_numbers.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>


#define ENABLE_IMU_DRIFT    0


class Odometry
{
    double pos_x_, pos_y_, pos_th_;
    ros::Time last_time_;
    bool first_;

public:
    Odometry()
    : pos_x_(0), pos_y_(0), pos_th_(0), first_(true) {}

    void update(double vx, double vy, double vth) {
        if(first_) {
            first_ = false;
            last_time_ = ros::Time::now();
            return;
        }

        ros::Time current_time = ros::Time::now();

        /*
         *  add bias
         */
    #if 0
        if(vx > 0) {
            vx += 0.1;
        }

        if(vth > 0 && (0.05 < vx && vx <= 0.3 )) {
            vth += M_PI/180 * 0.00001;
        }
    #endif

        /*
         *
         */

        //compute odometry in a typical way given the velocities of the robot
        double dt = (current_time - last_time_).toSec();
        last_time_ = current_time;

        double delta_x = (vx * cos(pos_th_) - vy * sin(pos_th_)) * dt;
        double delta_y = (vx * sin(pos_th_) + vy * cos(pos_th_)) * dt;
        double delta_th = vth * dt;

        pos_x_ += delta_x;
        pos_y_ += delta_y;
        pos_th_ += delta_th;
    }

    double pos_x() {
        return pos_x_;
    }

    double pos_y() {
        return pos_y_;
    }

    double pos_th() {
        return pos_th_;
    }
};



class FakeImu
{
    const std::string cfg_robot_odom_truth_topic_;
    const std::string cfg_robot_odom_topic_;
    const std::string cfg_imu_topic_;
    const std::string cfg_odom_topic_;

    const std::string cfg_frame_id_imu_;
    const std::string cfg_frame_id_world_; // "/world" groud truth

    double cfg_std_accel_; // m/sec^2
    double cfg_std_omega_; // rad/sec
    double cfg_std_orientation_; // rad

    double drift_yaw_;

    bool enable_odom_;
    bool enable_imu_;

    ros::Publisher pub_imu_;
    ros::Publisher pub_odom_;
//    ros::Subscriber sub_robot_odom_ground_truth_;
    ros::Subscriber sub_robot_odom_;
    tf::TransformListener tf_listener_;
    tf::TransformBroadcaster tf_broadcaster_;

    ros::Time prv_time_;
    tf::Vector3 prv_vel_;
    tf::Quaternion orientation_;

    Odometry odometry_;

    random_numbers::RandomNumberGenerator rand_;

#if 1
    void robot_odom_ground_truth_callback(const nav_msgs::Odometry& robot_odom_ground_truth) {
        /*
         * output
         */
        sensor_msgs::Imu msg_imu;
        msg_imu.header.frame_id = cfg_frame_id_imu_;
        msg_imu.header.stamp = ros::Time::now();  //robot_odom_ground_truth.header.stamp;


#if ENABLE_IMU_DRIFT
        //// 2017.11.27. ('c')void 저속에서 DRIFT 재현 ////
        if(robot_odom_ground_truth.twist.twist.linear.x > 0.05 || robot_odom_ground_truth.twist.twist.angular.z > 0.1) {
            // drift_yaw_ += rand_.gaussian(0, M_PI/180*0.1); // random walk
            drift_yaw_ += M_PI/180*0.1;
        }
#endif // ENABLE_IMU_DRIFT

        double yaw = drift_yaw_ + tf::getYaw(robot_odom_ground_truth.pose.pose.orientation);
        tf::Quaternion quat = tf::createQuaternionFromYaw(yaw);

        msg_imu.orientation.x = quat.x();
        msg_imu.orientation.y = quat.y();
        msg_imu.orientation.z = quat.z();
        msg_imu.orientation.w = quat.w();


        msg_imu.angular_velocity.z = robot_odom_ground_truth.twist.twist.angular.z;

        {
            double var = cfg_std_omega_*cfg_std_omega_;
            boost::array<double, 9> t = { var, 0, 0,
                                          0, var, 0,
                                          0, 0, var};

            msg_imu.angular_velocity_covariance = t;
        }

        {
            double var = cfg_std_accel_*cfg_std_accel_;
            boost::array<double, 9> t = { var, 0, 0,
                                          0, var, 0,
                                          0, 0, var};
            msg_imu.linear_acceleration_covariance = t;
        }

        {
            double var = cfg_std_orientation_*cfg_std_orientation_;
            boost::array<double, 9> t = { var, 0, 0,
                                          0, var, 0,
                                          0, 0, var};
            msg_imu.orientation_covariance = t;
        }

        if(enable_imu_) {
            pub_imu_.publish(msg_imu);
        }

        // odom 에 bias를 주기위해 ground truth를 이용
        robot_odom_callback(robot_odom_ground_truth);
    }

#else
    void robot_odom_ground_truth_callback(const nav_msgs::Odometry& robot_odom_ground_truth) {
        if(prv_time_ == ros::TIME_MIN) {
            prv_time_ = robot_odom_ground_truth.header.stamp;
            tf::vector3MsgToTF(robot_odom_ground_truth.twist.twist.linear, prv_vel_);
            return;
        }

        /*
         *  dt
         */
        ros::Duration e = robot_odom_ground_truth.header.stamp - prv_time_;
        prv_time_ = robot_odom_ground_truth.header.stamp;
        double dt = e.toSec();

        /*
         *  xxx_g : global frame
         *  xxx_l : body frame
         */
        tf::Vector3   r_v_true_g;  // linear velocity
        tf::Vector3   r_w_true_g, r_w_true_l; // angular velocity
        tf::Vector3   r_a_true_g, r_a_true_l; // linear acceleration
        tf::Matrix3x3 R_true_l2g, R_true_g2l;
        tf::Quaternion q_true_l2g;

        {
            geometry_msgs::Point      r_p = robot_odom_ground_truth.pose.pose.position;
            geometry_msgs::Quaternion r_q = robot_odom_ground_truth.pose.pose.orientation;
            geometry_msgs::Vector3    r_v = robot_odom_ground_truth.twist.twist.linear;
            geometry_msgs::Vector3    r_w = robot_odom_ground_truth.twist.twist.angular;

            tf::vector3MsgToTF(r_v, r_v_true_g);
            tf::vector3MsgToTF(r_w, r_w_true_g);

            tf::quaternionMsgToTF(r_q, q_true_l2g);
            R_true_l2g = tf::Matrix3x3(q_true_l2g);
            R_true_g2l = R_true_l2g.transpose(); // inverse

            if(false) {
                ROS_INFO("Robot pose %.2f, %.2f, %.2f, v %.2f, %.2f, %.2f w %.2f, %.2f, %.2f",
                        r_p.x, r_p.y, r_p.z,
                        r_v.x, r_v.y, r_v.z,
                        r_w.x, r_w.y, r_w.z
                );
            }

            if(false) {
                /*
                 *  Quaternion frame 확인
                 */
                tf::Vector3 v_l(1, 0, 0);
                tf::Vector3 v_g = R_true_l2g*v_l;
                ROS_INFO("vp %.2f, %.2f, %.2f", v_g.getX(), v_g.getY(), v_g.getZ());
            }
        }

        /*
         * velocities
         */
        r_w_true_l = R_true_g2l*r_w_true_g;

        /*
         * acceleration
         */
        r_a_true_g = (r_v_true_g - prv_vel_)/dt;
        prv_vel_ = r_v_true_g;
        r_a_true_l = R_true_g2l*r_a_true_g;


        /*
         *  add noise
         */
        tf::Vector3 w_noise(0, 0, rand_.gaussian(0, cfg_std_omega_)); // yaw 만.
        tf::Vector3 a_noise(rand_.gaussian(0, cfg_std_accel_), rand_.gaussian(0, cfg_std_accel_), rand_.gaussian(0, cfg_std_accel_));
        tf::Vector3 r_w_noise_l = r_w_true_l + w_noise;
        tf::Vector3 r_a_noise_l = r_a_true_l + a_noise;


//        ROS_INFO("r_w_noise_l %.2f, %.2f, %.2f", r_w_noise_l.getX(), r_w_noise_l.getY(), r_w_noise_l.getZ());
//        ROS_INFO("r_a_noise_l %.2f, %.2f, %.2f", r_a_noise_l.getX(), r_a_noise_l.getY(), r_a_noise_l.getZ());

        /*
         *  Zeroth order integration
         *   - drift 포함.
         */
        tf::Vector3 rot_vec = r_w_noise_l*dt;
        double angle = rot_vec.length();
        if(angle > 0) {
            tf::Quaternion dq(rot_vec.normalize(), angle);
            orientation_ *= dq;
            orientation_ = orientation_.normalize();
        }

        if(false) {
            /*
             * ok : q_true_l2g == _orientation
             *  - 초기값 같고 noise 없는 경우
             */
            ROS_INFO("q_true_l2g   : %.4e, %.4e, %.4e, %.4e", q_true_l2g.getX(), q_true_l2g.getY(), q_true_l2g.getZ(), q_true_l2g.getW());
            ROS_INFO("_orientation : %.4e, %.4e, %.4e, %.4e", orientation_.getX(), orientation_.getY(), orientation_.getZ(), orientation_.getW());
        }

        /*
         * output
         */
        sensor_msgs::Imu msg;
        msg.header.frame_id = cfg_frame_id_imu_;
        msg.header.stamp = robot_odom_ground_truth.header.stamp;

        tf::quaternionTFToMsg(orientation_, msg.orientation);
        tf::vector3TFToMsg(r_w_noise_l, msg.angular_velocity);
        tf::vector3TFToMsg(r_a_noise_l, msg.linear_acceleration);

        {
            double var = cfg_std_omega_*cfg_std_omega_;
            boost::array<double, 9> t = { var, 0, 0,
                                          0, var, 0,
                                          0, 0, var};

            msg.angular_velocity_covariance = t;
        }

        {
            double var = cfg_std_accel_*cfg_std_accel_;
            boost::array<double, 9> t = { var, 0, 0,
                                          0, var, 0,
                                          0, 0, var};
            msg.linear_acceleration_covariance = t;
        }

        {
            double var = cfg_std_orientation_*cfg_std_orientation_;
            boost::array<double, 9> t = { var, 0, 0,
                                          0, var, 0,
                                          0, 0, var};
            msg.orientation_covariance = t;
        }

        if(enable_imu_) {
            pub_imu_.publish(msg);
        }
    }
#endif

    void robot_odom_callback(const nav_msgs::Odometry& robot_odom) {
        /*
         *  remapping odom
         *   - added covariance
         */

#if 1
        nav_msgs::Odometry odom = robot_odom;
#else
        odometry_.update(robot_odom.twist.twist.linear.x, robot_odom.twist.twist.linear.y, robot_odom.twist.twist.angular.z);

        geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(odometry_.pos_th());

        // publish odometry message as topic:
        nav_msgs::Odometry odom;
        odom.header.stamp = ros::Time::now();
        //odom.header.frame_id = "odom";
        odom.child_frame_id = "base_footprint";
        odom.pose.pose.position.x = odometry_.pos_x();
        odom.pose.pose.position.y = odometry_.pos_y();
        odom.pose.pose.position.z = 0;
        odom.pose.pose.orientation = odom_quat;

        odom.twist.twist.linear.x = robot_odom.twist.twist.linear.x;
        odom.twist.twist.linear.y = robot_odom.twist.twist.linear.y;
        odom.twist.twist.linear.z = 0;
        odom.twist.twist.angular.x = 0;
        odom.twist.twist.angular.y = 0;
        odom.twist.twist.angular.z = robot_odom.twist.twist.angular.z;
#endif

        odom.header.frame_id = "odom";
        odom.header.stamp = ros::Time::now();
        odom.child_frame_id = "base_link";

        static const double std_pose = 5.0;
        static const double std_yaw  = M_PI*2;

        static const double var_pos = std_pose*std_pose;
        static const double var_rp = 0.01*0.01;
        static const double var_y = std_yaw*std_yaw;

        boost::array<double, 36> t = { var_pos, 0, 0, 0, 0, 0,
                                       0, var_pos, 0, 0, 0, 0,
                                       0, 0, var_pos, 0, 0, 0,
                                       0, 0, 0,  var_rp, 0, 0,
                                       0, 0, 0, 0,  var_rp, 0,
                                       0, 0, 0, 0, 0,   var_y,};

        odom.pose.covariance = t;

        if(enable_odom_) {
            pub_odom_.publish(odom);
        }

        /*
         *  robot_pose_ekf 를 쓰기위한 처리
         */
        tf::Stamped<tf::Pose> odom_to_baselink;
        tf::Transform baselink_to_odom;

        geometry_msgs::PoseStamped p;
        p.header = odom.header;
        p.pose = odom.pose.pose;
        tf::poseStampedMsgToTF(p, odom_to_baselink);
        baselink_to_odom = odom_to_baselink.inverse();

        tf_broadcaster_.sendTransform(tf::StampedTransform(baselink_to_odom, odom.header.stamp, "base_footprint", "odom"));
    }

public:
    FakeImu(ros::NodeHandle& nh)
    : cfg_robot_odom_truth_topic_("/robot/base_pose_ground_truth")
    , cfg_robot_odom_topic_("/robot/odom")
    , cfg_imu_topic_("/imu_data")
    , cfg_frame_id_imu_("/imu")
    , cfg_odom_topic_("/odom")
    , cfg_frame_id_world_("/world")
    , prv_time_(ros::TIME_MIN)
    , orientation_(0, 0, 0, 1)
    , cfg_std_accel_(0.1)
    , cfg_std_omega_(0.01)
    , cfg_std_orientation_(0.1 * M_PI / 180)
    , drift_yaw_(0)
    , enable_odom_(true)
    , enable_imu_(true)
    {
        pub_imu_ = nh.advertise<sensor_msgs::Imu>(cfg_imu_topic_, 10);
        pub_odom_ = nh.advertise<nav_msgs::Odometry>(cfg_odom_topic_, 10);

        //sub_robot_odom_ = nh.subscribe(cfg_robot_odom_truth_topic_, 10, &FakeImu::robot_odom_ground_truth_callback, this);
        sub_robot_odom_ = nh.subscribe(cfg_robot_odom_topic_, 10, &FakeImu::robot_odom_ground_truth_callback, this);
    }

    void enable_odom(bool enable) {
        enable_odom_ = enable;
    }

    void enable_imu(bool enable) {
        enable_imu_ = enable;
    }

    ~FakeImu() {}
};

#endif /* __FAKE_IMU_H__ */
