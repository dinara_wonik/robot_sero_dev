/* Copyright 2017 Wonik Robotics. All rights reserved.
 *
 * This file is a proprietary of Wonik Robotics
 * and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF Wonik Robotics.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file sonar_publisher.h
 *
 * @brief void.
 */

/*
 *  Created on: Nov 3, 2017
 *      Author: ('c')void
 *
 *  ver. 0.1
 *  ver. 0.2
 */

#ifndef __SONAR_PUBLISHER_H__
#define __SONAR_PUBLISHER_H__

#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <ros/ros.h>
#include <tf/tf.h>
#include <tf2/LinearMath/Quaternion.h>
#include "tf2_ros/static_transform_broadcaster.h"

#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Range.h>
#include <geometry_msgs/PoseStamped.h>

class SonarPublisher
{
    /*
     *  configuration file example
     *
     *  sensors:
     *
     *    _1_ :
     *      id : 1
     *      radiation_type : ULTRASOUND  # ULTRASOUND, INFRARED
     *      pose : [  0.1445,  0.0000,  0.2132,   0 ]  x, y, z, yaw(rad), robot frame 기준
     *      fov : 0.5235987755982988 # 30 deg
     *      range_max : 2.0 # meter
     *      range_min : 0.1 # meter
     *
     *    _2_ :
     *      id : 2
     *      radiation_type : ULTRASOUND
     *      pose : [  0.0000, -0.1445,  0.2132,   -1.5707963267948966 ]
     *      fov : 0.5235987755982988
     *      range_max : 2.0
     *      range_min : 0.1
     *
     */

    struct SensorConfig {
        int id;
        std::string radiation_type;
        double pose[4];
        double fov;
        double range_max, range_min;

        void print() {
            ROS_INFO("      sonar id : %d", id);
            ROS_INFO("radiation_type : %s", radiation_type.c_str());
            ROS_INFO("          pose : (%.4e, %.4e, %.4e, %.2f)", pose[0], pose[1], pose[2], pose[3]);
            ROS_INFO("           fov : %.2f", fov);
            ROS_INFO(" range_min/max : [%.2f, %.2f]", range_min, range_max);
        }

        geometry_msgs::TransformStamped get_pose() {
            // base_link 기준
            geometry_msgs::TransformStamped tf;
            return tf;
        }

        static double read_number(XmlRpc::XmlRpcValue& xml_rpc_val) {
            ROS_ASSERT(xml_rpc_val.getType() == XmlRpc::XmlRpcValue::TypeDouble || xml_rpc_val.getType() == XmlRpc::XmlRpcValue::TypeInt);
            switch(xml_rpc_val.getType()) {
            case XmlRpc::XmlRpcValue::TypeDouble:
                return static_cast<double>(xml_rpc_val);
            case XmlRpc::XmlRpcValue::TypeInt:
                return static_cast<int>(xml_rpc_val);
            default:
                return 0;
            }
        }

        static bool load_param(ros::NodeHandle& nh, std::vector<SensorConfig>& sensor_config_list) {
            XmlRpc::XmlRpcValue xmlrpc_sensors;

            std::string radiation_type;
            if(!nh.getParam("sensors", xmlrpc_sensors)) {
                return false;
            }

            std::vector<XmlRpc::XmlRpcValue> xml_rpc_sensor_config_list;
            ROS_ASSERT(xmlrpc_sensors.getType() == XmlRpc::XmlRpcValue::TypeStruct); // sensors
            for(XmlRpc::XmlRpcValue::iterator it=xmlrpc_sensors.begin(); it!=xmlrpc_sensors.end(); ++it) { // _1, _2, ...
                //ROS_INFO("load sensors/%s", it->first.c_str());
                ROS_ASSERT(it->second.getType() == XmlRpc::XmlRpcValue::TypeStruct);
                sensor_config_list.push_back(load_config(it->second));
            }

            return true;
        }

        static SensorConfig load_config(XmlRpc::XmlRpcValue& xmlrpc_sensor) {
            SensorConfig config;

            ROS_ASSERT(xmlrpc_sensor.getType() == XmlRpc::XmlRpcValue::TypeStruct); // sensors
            for(XmlRpc::XmlRpcValue::iterator it=xmlrpc_sensor.begin(); it!=xmlrpc_sensor.end(); ++it) { // id, radiation_type, ...
                if(it->first == "id") {
                    ROS_ASSERT(it->second.getType() == XmlRpc::XmlRpcValue::TypeInt);
                    config.id = static_cast<int>(it->second);
                }
                if(it->first == "radiation_type") {
                    ROS_ASSERT(it->second.getType() == XmlRpc::XmlRpcValue::TypeString);
                    config.radiation_type = static_cast<std::string>(it->second);
                }
                if(it->first == "pose") {
                    ROS_ASSERT(it->second.getType() == XmlRpc::XmlRpcValue::TypeArray);
                    for(int i=0; i<4; i++) {
                        config.pose[i] = read_number(it->second[i]);
                    }
                }
                if(it->first == "fov") {
                    config.fov = read_number(it->second);
                }
                if(it->first == "range_max") {
                    config.range_max = read_number(it->second);
                }
                if(it->first == "range_min") {
                    config.range_min = read_number(it->second);
                }
            }

            return config;
        }
    }; // SensorConfig

    struct RangeContex {
        sensor_msgs::Range range_msg;
        ros::Publisher pub;

        RangeContex() {
            range_msg.range = -1;
        }

        void publish() {
            if(range_msg.range >= 0) {
                pub.publish(range_msg);
            }
        }
    };

    ros::NodeHandle& private_nh_;

    std::string robot_frame_;

    boost::recursive_mutex mutex_;

    std::vector<RangeContex> range_context_list_;
    tf2_ros::StaticTransformBroadcaster broadcaster_;

public:
    SonarPublisher(ros::NodeHandle& private_nh, std::string robot_frame="base_link")
    : private_nh_(private_nh)
    , robot_frame_(robot_frame)
    {
        std::vector<SensorConfig> sensor_config_list;
        SensorConfig::load_param(private_nh, sensor_config_list);
        size_t sensor_num = sensor_config_list.size();

        //for(int i=0; i<sensor_num; i++) {
        //    sensor_config_list[i].print();
        //}

        /*
         * sensor id는 1부터 시작함.
         * range_context_list_[0] 은 무의미한 데이터
         */
        range_context_list_.resize(sensor_num+1);

        for(int i=0; i<sensor_num; i++) {
            geometry_msgs::TransformStamped msg;
            SensorConfig& cfg = sensor_config_list[i];
            int id = cfg.id;

            char text_buffer[64];
            sprintf(text_buffer, "/sonar/%d", id);
            std::string common_id(text_buffer); // frame_id 와 topic 으로 활용.

            // tf
            geometry_msgs::Quaternion quat = tf::createQuaternionMsgFromYaw(cfg.pose[3]);
            msg.transform.translation.x = cfg.pose[0];
            msg.transform.translation.y = cfg.pose[1];
            msg.transform.translation.z = cfg.pose[2];
            msg.transform.rotation.x = quat.x;
            msg.transform.rotation.y = quat.y;
            msg.transform.rotation.z = quat.z;
            msg.transform.rotation.w = quat.w;
            msg.header.stamp = ros::Time::now();
            msg.header.frame_id = robot_frame_;
            msg.child_frame_id = common_id;
            broadcaster_.sendTransform(msg);

            // topic
            range_context_list_[id].range_msg.header.frame_id = common_id;
            range_context_list_[id].range_msg.field_of_view = cfg.fov;
            range_context_list_[id].range_msg.max_range = cfg.range_max;
            range_context_list_[id].range_msg.min_range = cfg.range_min;

            if(cfg.radiation_type == "ULTRASOUND") {
                range_context_list_[id].range_msg.radiation_type = sensor_msgs::Range::ULTRASOUND;
            }
            else if(cfg.radiation_type == "INFRARED") {
                range_context_list_[id].range_msg.radiation_type = sensor_msgs::Range::INFRARED;
            }
            else {
                range_context_list_[id].range_msg.radiation_type = 0xFF;
            }

            range_context_list_[id].pub = private_nh_.advertise<sensor_msgs::Range>(common_id, 10);
        }
    }

    ~SonarPublisher() {}

    void set_range(int id, float range) {
        boost::recursive_mutex::scoped_lock lock(mutex_);

        range_context_list_[id].range_msg.range = range;
    }

    void publish() {
        boost::recursive_mutex::scoped_lock lock(mutex_);

        for(int i=1; i<range_context_list_.size(); i++) {
            range_context_list_[i].publish();
        }
    }
}; // SonarPublisher

#endif /* __SONAR_PUBLISHER_H__ */
