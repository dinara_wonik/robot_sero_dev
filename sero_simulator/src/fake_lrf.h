/* Copyright 2017 Wonik Robotics. All rights reserved.
 *
 * This file is a proprietary of Wonik Robotics
 * and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF Wonik Robotics.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file fake_lrf.h
 *
 * @brief void.
 */

/*
 *  Created on: Nov 26, 2017
 *      Author: ('c')void
 */

#ifndef __FAKE_LRF_H__
#define __FAKE_LRF_H__

#include <iostream>
#include <string>

#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>

class FakeLrf
{
    std::string cfg_lrf_scan_topic_;
    std::string cfg_lrf_scan_org_topic_;
    ros::Publisher pub_scan_;
    ros::Subscriber sub_scan_org_;

    void robot_base_scan_callback(const sensor_msgs::LaserScan& scan) {
        sensor_msgs::LaserScan msg = scan;

        msg.header.stamp = ros::Time::now();
        msg.header.frame_id = "base_link";

        pub_scan_.publish(msg);
    }

public:
    FakeLrf(ros::NodeHandle& nh)
    : cfg_lrf_scan_topic_("/lrf/scan")
    , cfg_lrf_scan_org_topic_("/robot/base_scan")
    {
        pub_scan_ = nh.advertise<sensor_msgs::LaserScan>(cfg_lrf_scan_topic_, 20);
        sub_scan_org_ = nh.subscribe(cfg_lrf_scan_org_topic_, 20, &FakeLrf::robot_base_scan_callback, this);
    }

    ~FakeLrf() {}

};

#endif /* __FAKE_LRF_H__ */
