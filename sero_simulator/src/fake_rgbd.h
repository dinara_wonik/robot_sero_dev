/* Copyright 2017 Wonik Robotics. All rights reserved.
 *
 * This file is a proprietary of Wonik Robotics
 * and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF Wonik Robotics.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file fake_rgbd.h
 *
 * @brief void.
 */

/*
 *  Created on: Oct 24, 2017
 *      Author: ('c')void
 */

#ifndef __FAKE_RGBD_H__
#define __FAKE_RGBD_H__

#include <iostream>
#include <string>

#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>

class FakeRgbd
{
    std::string cfg_lrf_scan_topic_;
    ros::Publisher pub_scan_;
    ros::Subscriber sub_robot_control_;

    double cfg_publish_rate_hz_;
    ros::Timer timer_;

    bool enable_;

    void timer_callback(const ros::TimerEvent& event) {
        sensor_msgs::LaserScan msg;

        msg.header.stamp = ros::Time::now();
        msg.header.frame_id = "base_link";

        if(enable_) {
            pub_scan_.publish(msg);
        }
    }

public:
    FakeRgbd(ros::NodeHandle& nh)
    : cfg_lrf_scan_topic_("/rgbd/scan")
    , cfg_publish_rate_hz_(10)
    , enable_(false)
    {
        pub_scan_ = nh.advertise<sensor_msgs::LaserScan>(cfg_lrf_scan_topic_, 10);
        timer_ = nh.createTimer(ros::Duration(1.0/cfg_publish_rate_hz_), &FakeRgbd::timer_callback, this);
    }

    ~FakeRgbd() {}

    void enable(bool en) {
        enable_ = en;
    }
};

#endif /* __FAKE_RGBD_H__ */
