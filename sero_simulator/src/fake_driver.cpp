/* Copyright 2017 Wonik Robotics. All rights reserved.
 *
 * This file is a proprietary of Wonik Robotics
 * and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF Wonik Robotics.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * fake_driver.cpp
 *
 *  Created on: Oct 18, 2017
 *      Author: ('c')void
 */

#include <iostream>
#include <string>

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TwistStamped.h>


#include "fake_imu.h"
#include "fake_lrf.h"
#include "fake_rgbd.h"
#include "fake_sonar.h"
#include "fake_status.h"

#include <dynamic_reconfigure/server.h>
#include <sero_simulator/NavSimConfig.h>

#include <sero_mobile/LptSetPosition.h>
#include <sero_mobile/LptSetVelocity.h>
#include <sero_mobile/LptResetEncoder.h>

#include <sero_mobile_msgs/LptPosVel.h>
#include <sero_mobile_msgs/LptPosVelStatus.h>
#include <sero_mobile_msgs/LptDriverState.h>

#include <workerbee_utils/accessories.h>


static const std::string cfg_node_name = "fake_driver_node";

class AccelerationLimit
{
    bool init_;
    bool enable_;
    geometry_msgs::Twist twist_prv_;
    double control_freq_;

    double translation_acceleration_limit_;  // m/sec^2
    double angular_acceleration_limit_;      // rad/sec^2

    double delta_ta_max_, delta_aa_max_;

    double sign(double a) {
        if(a >= 0) {
            return 1;
        }
        else {
            return -1;
        }
    }

public:
    AccelerationLimit(double control_freq=20)
    : init_(false), enable_(true), control_freq_(control_freq)
    {
        set_accelerations(5.0, 5.0);
    }

    ~AccelerationLimit() {}

    void set_accelerations(double max_translation_accel, double max_angular_accel) {
        translation_acceleration_limit_ = max_translation_accel;
        angular_acceleration_limit_ = max_angular_accel;

        delta_ta_max_ = translation_acceleration_limit_/control_freq_;
        delta_aa_max_ = angular_acceleration_limit_/control_freq_;

        ROS_INFO("AccelerationLimit was updated. max_translation_accel=%.2f, max_angular_accel=%.2f", max_translation_accel, max_angular_accel);
    }

    geometry_msgs::Twist limit(const geometry_msgs::Twist::ConstPtr& twist_in) {
        geometry_msgs::Twist t = *twist_in;
        return limit(t);
    }

    geometry_msgs::Twist limit(geometry_msgs::Twist& twist_in) {
        if(!init_) {
            init_ = true;
            twist_prv_ = twist_in;
            return twist_in;
        }

        if(!enable_) {
            return twist_in;
        }

        geometry_msgs::Twist twist_out;

        double delta_ta_x = twist_in.linear.x  - twist_prv_.linear.x;
        double sign_ta_x = sign(delta_ta_x);

        double delta_ta_y = twist_in.linear.y  - twist_prv_.linear.y;
        double sign_ta_y = sign(delta_ta_y);

        double delta_aa = twist_in.angular.z - twist_prv_.angular.z;
        double sign_aa = sign(delta_aa);

        if(fabs(delta_ta_x) > delta_ta_max_) {
            delta_ta_x = sign_ta_x*delta_ta_max_;
        }

        if(fabs(delta_ta_y) > delta_ta_max_) {
            delta_ta_y = delta_ta_y*delta_ta_max_;
        }

        if(fabs(delta_aa) > delta_aa_max_) {
            delta_aa = sign_aa*delta_aa_max_;
        }

        twist_out = twist_in;
        twist_out.linear.x  = twist_prv_.linear.x + delta_ta_x;
        twist_out.linear.y  = twist_prv_.linear.y + delta_ta_y;
        twist_out.angular.z = twist_prv_.angular.z + delta_aa;

        twist_prv_ = twist_out;

        return twist_out;
    }

    void set_freq(float freq) {
        control_freq_ = freq;
    }

    void enable(bool e) {
        enable_ = e;
    }
};


class DigitalFilter
{
    bool valid_;
    std::string name_;
    std::vector<double> a_, b_, x_, y_;

    double dot(std::vector<double>& p, std::vector<double>& q) {
        ROS_ASSERT(p.size() == q.size());
        size_t N = p.size();

        double sum = 0;

        for(size_t i=0; i<N; i++) {
            sum += p[i]*q[i];
        }

        return sum;
    }

    void print_vec(std::string name, std::vector<double>& p) {
        printf("\t\t vector %s : ", name.c_str());
        for(int i=0; i<p.size(); i++) {
            printf("%f,", p[i]);
        }
        printf("\n");
    }

public:
    DigitalFilter() : name_("_NO_NAME_"), valid_(false) {}
    DigitalFilter(std::string name, std::vector<double>& a, std::vector<double>& b)
    : valid_(false)
    {
        set_param(name, a, b);
    }

    bool set_param(std::string name, std::vector<double>& a, std::vector<double>& b) {
        name_ = name;
        a_ = a;
        b_ = b;

        valid_ = (a_.size() > 0);
        if(!valid_) {
            return valid_;
        }


        valid_ = (a_.size() + 1 == b_.size());
        if(!valid_) {
            return valid_;
        }

        x_.assign(b_.size(), 0);
        y_.assign(a_.size(), 0);

        return valid_;
    }

    bool is_valid() {
        return valid_;
    }

    double filtering(double x) {
        if(!valid_) {
            ROS_ERROR("DigitalFilter(%s) is not valid(%d)", name_.c_str(), valid_);
            return 0;
        }

        x_.insert(x_.begin(), x);
        x_.pop_back();

        double y = dot(x_, b_) - dot(y_, a_);

        y_.insert(y_.begin(), y);
        y_.pop_back();

        return y;
    }

    static void unit_test_1st() {

        //         0.3935
        //  y =  ----------
        //       z - 0.6065

        //    t 0.000000, y 0.000000
        //    t 0.050000, y 0.393500
        //    t 0.100000, y 0.632158
        //    t 0.150000, y 0.776904
        //    t 0.200000, y 0.864692
        //    t 0.250000, y 0.917936
        //    t 0.300000, y 0.950228
        //    t 0.350000, y 0.969813
        //    t 0.400000, y 0.981692
        //    t 0.450000, y 0.988896
        //    t 0.500000, y 0.993265
        //    t 0.550000, y 0.995916

        std::vector<double> va;
        va.push_back(-0.6065);

        std::vector<double> vb;
        vb.push_back(0);
        vb.push_back(0.3935);

        DigitalFilter test_filter("test_filter", va, vb);

        ROS_INFO("### DigitalFilter::unit_test()");

        double dx = 0.05;
        for(int i=0; i<12; i++) {
            double t = dx*i; // step
            double y = test_filter.filtering(1); // step
            printf("t %f, y %f\n", t, y);
        }
    }
};


class DifferentialDrive
{
    double wheel_baseline_;
    AccelerationLimit accel_limit_;
    DigitalFilter wheel_left_, wheel_right_;

    void get_wheel_speed(double vr_x, double vr_th, double& vl, double& vr) {
        /*
         *  vr_x  : linear speed. robot frame. m/sec
         *  vr_th : angular speed. robot frame. rad/sec
         */
        vl = vr_x - wheel_baseline_/2*vr_th;
        vr = vr_x + wheel_baseline_/2*vr_th;
    }

    geometry_msgs::Twist make_twist(double vl, double vr) {
        geometry_msgs::Twist twist;

        twist.linear.x = (vr + vl)/2;
        twist.linear.y = 0;
        twist.angular.z = (vr - vl)/wheel_baseline_;

        return twist;
    }

public:
    DifferentialDrive() {
        // P1K
        const float   PLATFORM_CONFIG_WHEEL_BASELINE = 0.42;

        wheel_baseline_ = PLATFORM_CONFIG_WHEEL_BASELINE;

#if 0
        //
        // 1st order
        //         0.3935
        //  y =  ----------
        //       z - 0.6065

        std::vector<double> va;
        va.push_back(-0.6065);

        std::vector<double> vb;
        vb.push_back(0);
        vb.push_back(0.3935);
#else
        //
        // 2nd order
        //          0.3048 z + 0.1886
        //  y =  -----------------------
        //       z^2 - 0.7497 z + 0.2431

        std::vector<double> va;
        va.push_back(-0.7497);
        va.push_back(0.2431);

        std::vector<double> vb;
        vb.push_back(0);
        vb.push_back(0.3048);
        vb.push_back(0.1886);
#endif

        wheel_left_.set_param("left wheel", va, vb);
        wheel_right_.set_param("right wheel", va, vb);
    }

    ~DifferentialDrive() {}

    geometry_msgs::Twist transfer_function(geometry_msgs::Twist& in) {
        geometry_msgs::Twist set_point = accel_limit_.limit(in);

        double set_point_vl, set_point_vr;
        get_wheel_speed(set_point.linear.x, set_point.angular.z, set_point_vl, set_point_vr);

        double output_vl = wheel_left_.filtering(set_point_vl);
        double output_vr = wheel_right_.filtering(set_point_vr);

        return make_twist(output_vl, output_vr);
    }
};


class FakeLptDriver
{
    struct Lpt {
        float lift; // mm
        float pan;  // rad
        float tilt; // rad
        Lpt(float l=0, float p=0, float t=0):lift(l),pan(p),tilt(t) {}
    };

    Lpt lpt_current_, ltp_target_;
    Lpt lpt_vel_, lpt_limit_vel_; // d(lpt)/dt
    Lpt lpt_sign_;
    Lpt lpt_range_min_, lpt_range_max_;

    bool activate_;

    ros::Publisher pub_;
    std::string topic_;

    ros::Time time_prv_;
    tf::TransformBroadcaster tf_broadcaster_;

public:
    FakeLptDriver(ros::NodeHandle& private_nh, std::string topic="lpt")
    : topic_(topic)
    , lpt_vel_(150,  120*M_PI/180,   60*M_PI/180)
    , lpt_limit_vel_(150,  120*M_PI/180,   60*M_PI/180)
    , lpt_range_min_(  0, -180*M_PI/180,  -90*M_PI/180)
    , lpt_range_max_(350,  180*M_PI/180,   60*M_PI/180)
    , activate_(false)
    {
        time_prv_ = ros::Time::now();
        pub_ = private_nh.advertise<sero_mobile_msgs::LptPosVelStatus>(topic_, 10, true);
    }

    ~FakeLptDriver() {}


    bool set_position(float lift, float pan, float tilt, std::string& error) {
        /*
         *  lift : mm
         *  pan, tilt : radian
         */

        if(!(lpt_range_min_.lift <= lift && lift <= lpt_range_max_.lift)) {
            error = workerbee_utils::string_format("lift_limit(%d)", (int)lift);
            return false;
        }

        if(!(lpt_range_min_.pan <= pan && pan <= lpt_range_max_.pan)) {
            error = workerbee_utils::string_format("pan_limit(%.3f)", pan);
            return false;
        }

        if(!(lpt_range_min_.tilt <= tilt && tilt <= lpt_range_max_.tilt)) {
            error = workerbee_utils::string_format("tilt_limit(%.3f)", tilt);
            return false;
        }


        ltp_target_.lift = lift;
        ltp_target_.pan = pan;
        ltp_target_.tilt = tilt;

        lpt_sign_.lift = (ltp_target_.lift >= lpt_current_.lift)?1:-1;
        lpt_sign_.pan  = (ltp_target_.pan >= lpt_current_.pan)?1:-1;
        lpt_sign_.tilt = (ltp_target_.tilt >= lpt_current_.tilt)?1:-1;

        // ROS_INFO("ltp_target_ lift %.3f, pan %.3f tilt %.3f", lift, pan, tilt);
        // ROS_INFO("lpt_sign_ lift %.3f, pan %.3f tilt %.3f", lpt_sign_.lift, lpt_sign_.pan, lpt_sign_.tilt);
        // ROS_INFO("LPT activated");

        activate_ = true;

        return true;
    }

    bool set_velocity(float lift, float pan, float tilt, std::string& error) {
        /*
         *  lift : mm
         *  pan, tilt : radian
         */

        double dt = 0.05;
        if(lpt_limit_vel_.lift <= abs(lift)) {
            error = workerbee_utils::string_format("lift_limit_val((%.3f)", lift);
            return false;
        }
        if(lpt_limit_vel_.pan <= abs(pan)) {
            error = workerbee_utils::string_format("pan_limit_val((%.3f)", pan);
            return false;
        }
        if(lpt_limit_vel_.tilt <= abs(tilt)) {
            error = workerbee_utils::string_format("tilt_limit_val((%.3f)", tilt);
            return false;
        }

        lpt_vel_.lift = lift;
        lpt_vel_.pan  = pan;
        lpt_vel_.tilt = tilt;

        lpt_sign_.lift = 1;
        lpt_sign_.pan = 1;
        lpt_sign_.tilt = 1;

        // ROS_INFO("ltp_target_ lift %.3f, pan %.3f tilt %.3f", lift, pan, tilt);
        // ROS_INFO("lpt_sign_ lift %.3f, pan %.3f tilt %.3f", lpt_sign_.lift, lpt_sign_.pan, lpt_sign_.tilt);
        // ROS_INFO("LPT activated");

        activate_ = false;

        return true;
    }

    void send_transform(sero_mobile_msgs::LptPosVelStatus& msg) {
        tf::Transform transform_lift;
        tf::Transform transform_pan;
        tf::Transform transform_tilt;

        transform_lift.setOrigin(tf::Vector3(0, 0, msg.lift_pos/1000));
        transform_lift.setRotation(tf::Quaternion::getIdentity());

        tf::Quaternion q_pan;
        q_pan.setRPY(0, 0, msg.pan_pos);
        transform_pan.setRotation(q_pan);

        tf::Quaternion q_tilt;
        q_tilt.setRPY(0, msg.tilt_pos, 0);
        transform_tilt.setRotation(q_tilt);

        ros::Time tf_expiration = ros::Time::now(); //  + ros::Duration(0.01);

        tf_broadcaster_.sendTransform(tf::StampedTransform(transform_lift, tf_expiration, "/lpt_base", "/lpt_lift"));
        tf_broadcaster_.sendTransform(tf::StampedTransform(transform_pan,  tf_expiration, "/lpt_lift", "/lpt_pan"));
        tf_broadcaster_.sendTransform(tf::StampedTransform(transform_tilt, tf_expiration, "/lpt_pan", "/lpt_tilt"));
    }

    void timer_tick() {
        float dt = (ros::Time::now() - time_prv_).toSec();
        time_prv_ = ros::Time::now();

        sero_mobile_msgs::LptPosVelStatus msg;

        if(activate_) {
            float d_lift = lpt_sign_.lift*lpt_vel_.lift*dt;
            float d_pan  = lpt_sign_.pan*lpt_vel_.pan*dt;
            float d_tilt = lpt_sign_.tilt*lpt_vel_.tilt*dt;

            float e_lift = fabs(ltp_target_.lift - lpt_current_.lift);
            float e_pan  = fabs(ltp_target_.pan - lpt_current_.pan);
            float e_tilt = fabs(ltp_target_.tilt - lpt_current_.tilt);

            bool a_lift = e_lift > fabs(d_lift);
            if(a_lift) {
                lpt_current_.lift += d_lift;
                msg.lift_vel = lpt_sign_.lift*lpt_vel_.lift/1000; // m/sec
            }
            else {
                lpt_current_.lift = ltp_target_.lift;
            }

            bool a_pan = e_pan > fabs(d_pan);
            if(a_pan) {
                lpt_current_.pan += d_pan;
                msg.pan_vel = lpt_sign_.pan*lpt_vel_.pan;
            }
            else {
                lpt_current_.pan = ltp_target_.pan;
            }

            bool a_tilt = e_tilt > fabs(d_tilt);
            if(a_tilt) {
                lpt_current_.tilt += d_tilt;
                msg.tilt_vel = lpt_sign_.tilt*lpt_vel_.tilt;
            }
            else {
                lpt_current_.tilt = ltp_target_.tilt;
            }

            if(a_lift == false && a_pan == false && a_tilt == false) {
                activate_ = false;
                // ROS_INFO("LPT deactivated");
            }
        }

        msg.lift_pos = lpt_current_.lift;
        msg.pan_pos  = lpt_current_.pan;
        msg.tilt_pos = lpt_current_.tilt;

        msg.lift_status = 1;
        msg.pan_status = 1;
        msg.tilt_status = 1;

        send_transform(msg);

        pub_.publish(msg);
    }

};



class FakeDriver
{
    std::string cfg_cmd_vel_topic_;
    std::string cfg_robot_control_topic_;
    ros::Publisher pub_vel_;
    ros::Publisher pub_vel_mon_;
    ros::Subscriber sub_robot_control_;

    std::vector<ros::ServiceServer> srv_servers_;

    double cfg_robot_control_rate_hz_;
    ros::Timer timer_;
    ros::Time last_control_input_;

    geometry_msgs::Twist cmd_vel_;
    DifferentialDrive diff_drive_;

    /*
     *  sensors
     */
    FakeRgbd sensor_rgbd_;
    FakeImu sensor_imu_;
    FakeLrf sensor_lrf_;
//    FakeSonarArray sensor_sonar_;
    EventPublisher event_publisher_;
    FakeStatus driving_unit_status_;
    FakeLptDriver lpt_driver_;

    dynamic_reconfigure::Server<sero_simulator::NavSimConfig> dynamic_reconfigure_server_;

    void robot_control_callback(const geometry_msgs::TwistConstPtr& robot_control) {
        last_control_input_ = ros::Time::now();
        cmd_vel_ = *robot_control;
    }

    void timer_callback(const ros::TimerEvent& event) {
        if((ros::Time::now() - last_control_input_).toSec() > 0.3) {
            // zero velocity
            cmd_vel_ = geometry_msgs::Twist();
        }

        geometry_msgs::TwistStamped cmd_vel_mon;
        cmd_vel_mon.header.frame_id = "base_link";
        cmd_vel_mon.header.stamp = ros::Time::now();
        cmd_vel_mon.twist = cmd_vel_;

        pub_vel_mon_.publish(cmd_vel_mon);

#if 0
        /*
         *  stage의 robot은 동특성을 표현하지 못함.
         *   - transfer function = 1
         *   - 여기서 simulate
         */
        pub_vel_.publish(diff_drive_.transfer_function(cmd_vel_));
#else
        /*
         *  주행 알고리즘 확인시 동특성이 방해일 경우가 있다.
         */
        pub_vel_.publish(cmd_vel_);
#endif

        lpt_driver_.timer_tick();
    }

    void dynamic_reconf_callback(sero_simulator::NavSimConfig &config, uint32_t level) {
        sensor_imu_.enable_odom(config.enable_odom);
        sensor_imu_.enable_imu(config.enable_imu);
        sensor_rgbd_.enable(config.enable_rgbd);
//        sensor_sonar_.enable(config.enable_sonar);

        event_publisher_.enable_cliff_back_off(config.enable_event_cliff_back_off);
        event_publisher_.enable_emergency_stop(config.enable_event_emergency_stop);
        event_publisher_.enable_low_battery(config.enable_event_low_battery);
        event_publisher_.enable_proximity_stop(config.enable_event_event_proximity_stop);
    }

    bool srv_lpt_set_position(sero_mobile::LptSetPosition::Request& req, sero_mobile::LptSetPosition::Response& rsp) {
        if(!lpt_driver_.set_position(req.lift, req.pan, req.tilt, rsp.state.details)) {
            rsp.state.code = sero_mobile_msgs::LptDriverState::ERROR_CONTROLLER;
        }
        return true;
    }

    bool srv_lpt_set_velocity(sero_mobile::LptSetVelocity::Request& req, sero_mobile::LptSetVelocity::Response& rsp) {
        if(!lpt_driver_.set_velocity(req.lift, req.pan, req.tilt, rsp.state.details)) {
            rsp.state.code = sero_mobile_msgs::LptDriverState::ERROR_CONTROLLER;
        }        
        return true;
    }

    bool srv_lpt_reset_encoder(sero_mobile::LptResetEncoder::Request& req, sero_mobile::LptResetEncoder::Response& rsp) {
        return true;
    }

public:
    FakeDriver(ros::NodeHandle& nh, ros::NodeHandle& private_nh)
    : cfg_cmd_vel_topic_("/cmd_vel")
    , cfg_robot_control_topic_("/robot_control/cmd_vel")
    , cfg_robot_control_rate_hz_(20)
    , sensor_rgbd_(nh)
    , sensor_imu_(nh)
    , sensor_lrf_(nh)
//    , sensor_sonar_(private_nh)
//    , sensor_cliff_(nh)
    , event_publisher_(nh)
    , lpt_driver_(nh, "/sero_mobile/lpt")
    , driving_unit_status_(nh, event_publisher_)
    {
        std::string cmd_vel_monitor_topic;
        private_nh.param<std::string>("cmd_vel_monitor_topic", cmd_vel_monitor_topic, "cmd_vel_mon");

        pub_vel_ = nh.advertise<geometry_msgs::Twist>(cfg_cmd_vel_topic_, 10);
        pub_vel_mon_ = nh.advertise<geometry_msgs::TwistStamped>(cmd_vel_monitor_topic, 10);

        sub_robot_control_ = nh.subscribe(cfg_robot_control_topic_, 10, &FakeDriver::robot_control_callback, this);
        timer_ = nh.createTimer(ros::Duration(1.0/cfg_robot_control_rate_hz_), &FakeDriver::timer_callback, this);

        srv_servers_.push_back(nh.advertiseService("/sero_mobile/lpt_set_position", &FakeDriver::srv_lpt_set_position, this));
        srv_servers_.push_back(nh.advertiseService("/sero_mobile/lpt_set_velocity", &FakeDriver::srv_lpt_set_velocity, this));
        srv_servers_.push_back(nh.advertiseService("/sero_mobile/lpt_reset_encoder", &FakeDriver::srv_lpt_reset_encoder, this));

        dynamic_reconfigure::Server<sero_simulator::NavSimConfig>::CallbackType cb = boost::bind(&FakeDriver::dynamic_reconf_callback, this, _1, _2);
        dynamic_reconfigure_server_.setCallback(cb);
    }

    ~FakeDriver() {}
};

int main(int argc, char **argv) {
    ros::init(argc, argv, cfg_node_name);

    ros::NodeHandle nh;
    ros::NodeHandle private_nh("~");

    FakeDriver driver(nh, private_nh);

    ROS_INFO("%s has been initialized", cfg_node_name.c_str());
    ros::spin();
}

