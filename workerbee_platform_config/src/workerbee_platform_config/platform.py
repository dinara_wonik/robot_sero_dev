#!/usr/bin/python
# -*- coding: utf8 -*-#

'''
2019.10.15 ('c')void 

Platform 의존적인 기능을 정의한다. 

가령 MoveTo Action을 수행하면 안되는 조건을 확인한다 하자. 
SeRo의 경우 목이 움직이고 있을 때엔 움직이지 않는게 옳다. 
하지만 다른 로봇은 목이 없는데? 

'''

import abc
import rospy 
import workerbee_status
from workerbee_platform import WorkerbeeNavigationConfigInterface
from workerbee_actionlib import RosLaunch


class WorkerbeeNavigationConfig(WorkerbeeNavigationConfigInterface):
    
    def __init__(self):
        self._simulation = rospy.get_param("simulation", False)
        self._service_connection_timeout = 10 # second
    
    @property
    def service_connection_timeout(self):
        return self._service_connection_timeout
    
    def check_moveto_error_conditions(self):
        '''
        해당하는 조건의 InfoItem을 리턴한다.         
        workerbee_status.get_info('A\\B\\C')
        return {'Some condition': 'Some error'}
        ''' 
        return [] 
    
    def check_docking_error_conditions(self):
        '''
        해당하는 조건의 InfoItem을 리턴한다.         
        workerbee_status.get_info('A\\B\\C')
        return {'Some condition': 'Some error'}
        ''' 
        return [] 

    def check_manual_control_error_conditions(self):
        '''
        해당하는 조건의 InfoItem을 리턴한다.         
        workerbee_status.get_info('A\\B\\C')
        return {'Some condition': 'Some error'}
        ''' 
        return [] 

    def check_update_map_error_conditions(self):
        '''
        해당하는 조건의 InfoItem을 리턴한다.         
        workerbee_status.get_info('A\\B\\C')
        return {'Some condition': 'Some error'}
        ''' 
        return []  

    def check_mapping_error_conditions(self):
        '''
        해당하는 조건의 InfoItem을 리턴한다.         
        workerbee_status.get_info('A\\B\\C')
        return {'Some condition': 'Some error'}
        ''' 
        return []      

    def check_pose_recovery_error_conditions(self):
        '''
        해당하는 조건의 InfoItem을 리턴한다.         
        workerbee_status.get_info('A\\B\\C')
        return {'Some condition': 'Some error'}
        ''' 
        return []     

    def check_following_error_conditions(self):
        '''
        해당하는 조건의 InfoItem을 리턴한다.         
        workerbee_status.get_info('A\\B\\C')
        return {'Some condition': 'Some error'}
        ''' 
        return [] 

    def roslaunch_localization(self): 
        '''
        returns RosLaunch object
        '''
        return RosLaunch(pkg='sero_navigation_drivers', file='wbn_localization.launch') 

    def roslaunch_navigation(self):
        '''
        returns RosLaunch object
        '''
        return RosLaunch(pkg='sero_navigation_drivers', file='wbn_navigation.launch')         

    def roslaunch_moveto(self): 
        '''
        returns RosLaunch object
        '''
        return None # RosLaunch(pkg='sero_navigation_drivers', file='wbn_moveto.launch') 
    
    def roslaunch_docking(self): 
        '''
        returns RosLaunch object
        '''
        return RosLaunch(pkg='sero_navigation_drivers', file='wbn_docking.launch') 
    
    def roslaunch_manual_driving(self): 
        '''
        returns RosLaunch object
        '''
        return None # RosLaunch(pkg='sero_navigation_drivers', file='wbn_manual_driving.launch') 

    def roslaunch_update_map(self): 
        '''
        returns RosLaunch object
        '''
        return RosLaunch(pkg='sero_navigation_drivers', file='wbn_update_map.launch')     

    def roslaunch_mapping(self): 
        '''
        returns RosLaunch object
        '''
        return RosLaunch(pkg='sero_navigation_drivers', file='wbn_mapping.launch')     

    def roslaunch_remapping(self): 
        '''
        returns RosLaunch object
        '''
        return RosLaunch(pkg='sero_navigation_drivers', file='wbn_remapping.launch')     

    def roslaunch_pose_recovery(self): 
        '''
        returns RosLaunch object
        '''
        return RosLaunch(pkg='sero_navigation_drivers', file='wbn_pose_recovery.launch')     

    def roslaunch_following(self): 
        '''
        returns RosLaunch object
        '''
        return None