#!/usr/bin/python
# -*- coding: utf8 -*-#


import numpy as np 
import transformations as tf 


DEG2RAD = np.pi/180.0


def get_quat(yaw):
    return tf.quaternion_from_euler(0, 0, yaw, 'rxyz')


def print_tf(name, translation, rotation):
    x, y, z = translation
    qx, qy, qz, qw = rotation
    name = name.strip()
    
    print '#### %s'%(name)
    print 'translation      ', (x, y, z)
    print 'rotation(x,y,z,w)', (qx, qy, qz, qw)     
    
    print '<node pkg="tf2_ros" type="static_transform_publisher" name="%s_link_tf" args="%f %f %f  %f %f %f %f base_link %s_link"/>'%(name, x, y, z, qx, qy, qz, qw, name)

def get_left_cam_tf():
    # base_link -> cam_left_link
    T1 = tf.translation_matrix((0, 0, (870 + 96)/1000.))
    R1 = tf.quaternion_matrix(tf.quaternion_from_euler(0, 0, 45*DEG2RAD))
    T2 = tf.translation_matrix((180/1000., 0, 0))
    R2 = tf.quaternion_matrix(tf.quaternion_from_euler(0, -20*DEG2RAD, 0))
    
    T = np.identity(4)
    T = np.dot(T, T1)
    T = np.dot(T, R1)
    T = np.dot(T, T2)
    T = np.dot(T, R2)
    
    translation = tf.translation_from_matrix(T)
    rotation = tf.quaternion_from_matrix(T)   
    
    print_tf('cam_left', translation, rotation)
    


def get_right_cam_tf():
    # base_link -> cam_left_link
    T1 = tf.translation_matrix((0, 0, (870 + 96)/1000.))
    R1 = tf.quaternion_matrix(tf.quaternion_from_euler(0, 0, -45*DEG2RAD))
    T2 = tf.translation_matrix((180/1000., 0, 0))
    R2 = tf.quaternion_matrix(tf.quaternion_from_euler(0, -20*DEG2RAD, 0))
    
    T = np.identity(4)
    T = np.dot(T, T1)
    T = np.dot(T, R1)
    T = np.dot(T, T2)
    T = np.dot(T, R2)
    
    translation = tf.translation_from_matrix(T)
    rotation = tf.quaternion_from_matrix(T) 

    print_tf('cam_right', translation, rotation)
    


if __name__ == '__main__':
    get_left_cam_tf()
    get_right_cam_tf()

