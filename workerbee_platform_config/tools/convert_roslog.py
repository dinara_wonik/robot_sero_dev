#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
2017.12.27 ('c')void 
 - rosout parser 
'''

import sys, datetime, re 

def ts2strftime(timestamp):
    return datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S.%f')


def convert_test(line): 
    # timestamp 
    ts = re.findall(r'\d{10}[\.]\d{9}', line)
    print ts 

    # [*]
    m = re.findall(r'\[(.+?)\]', line)
    print m


pattern_ts = re.compile(r'\d{10}[\.]\d{9}')

def convert(line):
    try:
        m = pattern_ts.match(line)
        ts_txt = ts2strftime(float(m.group()))
    except:
        print '## NO_TIMESTAMP ', line.strip()
        return 
    
    braket_idx = line.find(']')
    msg = line[braket_idx+2:]

    print ts_txt, msg.strip()


def test():
    line = '1514286485.611365212 INFO [/home/p/addy_ws/src/addy_navigation_pkgs/navigation_service/src/navigation_service.h:1018(NavigationActionServer<ActionSpec, HandlerType>::as_goal_callback) [topics: /rosout, /navigation_service/status, /navigation_service/robot/pose, /navigation_service/robot/twist, /navigation_service/action/move_to/goal, /tf_static, /navigation_service/parameter_descriptions, /navigation_service/parameter_updates, /navigation_service/free_space_costmap/static_free_space_layer/parameter_descriptions, /navigation_service/free_space_costmap/static_free_space_layer/parameter_updates, /navigation_service/free_space_costmap/static_prohibited_area_layer/parameter_descriptions, /navigation_service/free_space_costmap/static_prohibited_area_layer/parameter_updates, /navigation_service/free_space_costmap/inflation_layer/parameter_descriptions, /navigation_service/free_space_costmap/inflation_layer/parameter_updates, /navigation_service/free_space_costmap/footprint, /navigation_service/free_space_costmap/costmap, /navigation_service/free_space_costmap/costmap_updates, /navigation_service/free_space_costmap/parameter_descriptions, /navigation_service/free_space_costmap/parameter_updates, /navigation_service/free_space_planner_/plan, /navigation_service/driving_area_costmap/static_free_space_layer/parameter_descriptions, /navigation_service/driving_area_costmap/static_free_space_layer/parameter_updates, /navigation_service/driving_area_costmap/static_prohibited_area_layer/parameter_descriptions, /navigation_service/driving_area_costmap/static_prohibited_area_layer/parameter_updates, /navigation_service/driving_area_costmap/static_driving_area_layer/parameter_descriptions, /navigation_service/driving_area_costmap/static_driving_area_layer/parameter_updates, /navigation_service/driving_area_costmap/inflation_layer/parameter_descriptions, /navigation_service/driving_area_costmap/inflation_layer/parameter_updates, /navigation_service/driving_area_costmap/footprint, /navigation_service/driving_area_costmap/costmap, /navigation_service/driving_area_costmap/costmap_updates, /navigation_service/driving_area_costmap/parameter_descriptions, /navigation_service/driving_area_costmap/parameter_updates, /navigation_service/driving_area_planner_/plan, /navigation_service/service_plan, /navigation_service/global_costmap/static_prohibited_area_layer/parameter_descriptions, /navigation_service/global_costmap/static_prohibited_area_layer/parameter_updates, /navigation_service/global_costmap/static_free_space_layer/parameter_descriptions, /navigation_service/global_costmap/static_free_space_layer/parameter_updates, /navigation_service/global_costmap/rgbd_obstacle_layer/parameter_descriptions, /navigation_service/global_costmap/rgbd_obstacle_layer/parameter_updates, /navigation_service/global_costmap/laser_obstacle_layer/parameter_descriptions, /navigation_service/global_costmap/laser_obstacle_layer/parameter_updates, /navigation_service/global_costmap/inflation_layer/parameter_descriptions, /navigation_service/global_costmap/inflation_layer/parameter_updates, /navigation_service/global_costmap/footprint, /navigation_service/global_costmap/costmap, /navigation_service/global_costmap/costmap_updates, /navigation_service/global_costmap/parameter_descriptions, /navigation_service/global_costmap/parameter_updates, /navigation_service/NavfnROS/plan, /navigation_service/local_costmap/static_prohibited_area_layer/parameter_descriptions, /navigation_service/local_costmap/static_prohibited_area_layer/parameter_updates, /navigation_service/local_costmap/static_free_space_layer/parameter_descriptions, /navigation_service/local_costmap/static_free_space_layer/parameter_updates, /navigation_service/local_costmap/rgbd_obstacle_layer/parameter_descriptions, /navigation_service/local_costmap/rgbd_obstacle_layer/parameter_updates, /navigation_service/local_costmap/laser_obstacle_layer/parameter_descriptions, /navigation_service/local_costmap/laser_obstacle_layer/parameter_updates, /navigation_service/local_costmap/inflation_layer/parameter_descriptions, /navigation_service/local_costmap/inflation_layer/parameter_updates, /navigation_service/local_costmap/footprint, /navigation_service/local_costmap/costmap, /navigation_service/local_costmap/costmap_updates, /navigation_service/local_costmap/parameter_descriptions, /navigation_service/local_costmap/parameter_updates, /navigation_service/EBandPlannerROS/global_plan, /navigation_service/EBandPlannerROS/local_plan, /navigation_service/EBandPlannerROS/eband_visualization, /navigation_service/EBandPlannerROS/eband_visualization_array, /navigation_service/EBandPlannerROS/parameter_descriptions, /navigation_service/EBandPlannerROS/parameter_updates, /navigation_service/obstacle_costmap/rgbd_obstacle_layer/parameter_descriptions, /navigation_service/obstacle_costmap/rgbd_obstacle_layer/parameter_updates, /navigation_service/obstacle_costmap/laser_obstacle_layer/parameter_descriptions, /navigation_service/obstacle_costmap/laser_obstacle_layer/parameter_updates, /navigation_service/obstacle_costmap/footprint, /navigation_service/obstacle_costmap/costmap, /navigation_service/obstacle_costmap/costmap_updates, /navigation_service/obstacle_costmap/parameter_descriptions, /navigation_service/obstacle_costmap/parameter_updates, /robot_control/cmd_vel, /navigation_service/visualization_marker, /navigation_service/action/test/result, /navigation_service/action/test/feedback, /navigation_service/action/test/status, /navigation_service/action/move_to/result, /navigation_service/action/move_to/feedback, /navigation_service/action/move_to/status, /navigation_service/action/follow_via_points/result, /navigation_service/action/follow_via_points/feedback, /navigation_service/action/follow_via_points/status, /navigation_service/action/direct_control/result, /navigation_service/action/direct_control/feedback, /navigation_service/action/direct_control/status] # NavSrv # DirectControlActionHander received new goal from manual_controller'
    convert(line)


def run():
    if(len(sys.argv) == 1):
        print 'error. no arg'
        return 

    filename = sys.argv[1]

    with open(filename, 'r') as f:
        for line in f:
            convert(line) 

if __name__ == '__main__':
    run()

