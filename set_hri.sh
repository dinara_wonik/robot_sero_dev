alias cw='cd ~/sero_ws'
alias cs='cd ~/sero_ws/src'
alias cm='cd ~/sero_ws && catkin_make -DCMAKE_BUILD_TYPE=Release'
source ~/sero_ws/devel/setup.bash
source ~/sero_ws/install/setup.bash
export ROS_MASTER_URI=http://192.168.0.1:11311
export ROS_HOSTNAME=192.168.0.2
export DISPLAY=:0
export OPENBLAS_NUM_THREADS=1
export LC_ALL=en_US.UTF-8



